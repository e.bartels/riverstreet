import re
import os
import codecs
from setuptools import setup, find_packages

install_requires = [
    'future',
    'pytz',
    'requests>=2.9.1,<3.0',
    'Pillow>=5.1,<6.0',
    'beautifulsoup4==4.6.3',
    'Django>=1.11.4,<1.12',
    'sorl_thumbnail @ https://github.com/bartels/sorl-thumbnail/archive/bugfix/reduce-overhead-deseriaized-storage-instances.tar.gz#egg=sorl_thumbnail',
    'django-taggit==0.23.0',
    'django-positions==0.6.0',
    'django_polymorphic==2.0.3',
    'django-environ>=0.4.4,<0.5',
    'django-autoslug==1.9.3',
    'django-mptt==0.9.1',
    'jsonfield==2.0.2',
    'django-ckeditor==5.6.1',
    'django-webpack-loader==0.6.0',
    'django-cleanup>=3.0.1,<4.0',
]

tests_require = [
    'mock==2.0.0',
    'factory-boy==2.11.1',
    'dj-inmemorystorage==1.4.1',
    'django-test-without-migrations>=0.6',
]

here = os.path.abspath(os.path.dirname(__file__))


def read(*parts):
    # intentionally *not* adding an encoding option to open, See:
    #   https://github.com/pypa/virtualenv/issues/201#issuecomment-3145690
    return codecs.open(os.path.join(here, *parts), 'r').read()


def find_version(*file_paths):
    version_file = read(*file_paths)
    version_match = re.search(r"^__version__ = ['\"]([^'\"]*)['\"]",
                              version_file, re.M)
    if version_match:
        return version_match.group(1)
    raise RuntimeError("Unable to find version string.")


setup(
    name='ado',
    version=find_version('ado', '__init__.py'),
    description='A set of tools useful for building a custom cms on top of Django.',
    author='Eric Bartels',
    author_email='ebartels@gmail.com',
    packages=find_packages(),
    zip_safe=False,
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Framework :: Django',
    ],
    include_package_data=True,
    python_requires='>=2.7,!=3.0.*,!=3.1.*,!=3.2.*',
    install_requires=install_requires,
    tests_require=tests_require,
    extras_require={
        'tests': tests_require,
        'develop': tests_require + [
            'flake8',
            'flake8-per-file-ignores',
            'invoke',
            'termcolor',
        ]
    },
)
