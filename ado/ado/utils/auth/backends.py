from __future__ import unicode_literals

from django.contrib.auth.backends import ModelBackend
from django.contrib.auth.models import User
from django.core.validators import validate_email
from django.core.exceptions import ValidationError


class EmailAuthBackend(ModelBackend):
    """
    Allow authentication against user email.
    """

    def authenticate(self, request, username=None, password=None, **kwargs):
        # see if an email was given
        try:
            validate_email(username)
            user = User.objects.filter(email__iexact=username)
            if user.count() == 1:
                user = user[0]
                if user.check_password(password) and self.user_can_authenticate(user):
                    return user
        except ValidationError:
            pass

        return None
