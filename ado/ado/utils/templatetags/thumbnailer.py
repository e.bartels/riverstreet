from __future__ import unicode_literals
from builtins import str

import re
from urllib.parse import urlparse
from django.utils.encoding import smart_str
from django.template import Library, Node, TemplateSyntaxError
from sorl.thumbnail.conf import settings

from sorl.thumbnail.templatetags.thumbnail import kw_pat
from sorl.thumbnail import default


register = Library()

RE_IMAG_PATH = re.compile(r'^' + urlparse(settings.MEDIA_URL).path)


class ThumbnailerTag(Node):
    error_msg = ('Syntax error. Expected: ``thumbnailer source geometry '
                 '[key1=val1 key2=val2...]``')

    def __init__(self, parser, token):
        bits = token.split_contents()
        if len(bits) < 2:
            raise TemplateSyntaxError(self.error_msg)
        self.geometry = parser.compile_filter(bits[1])
        self.options = []
        for bit in bits[2:]:
            m = kw_pat.match(bit)
            if not m:
                raise TemplateSyntaxError(self.error_msg)
            key = smart_str(m.group('key'))
            expr = parser.compile_filter(m.group('value'))
            self.options.append((key, expr))

        self.nodelist = parser.parse(('endthumbnailer',))
        parser.delete_first_token()

    def render(self, context):
        try:
            return self._render(context)
        except Exception:
            if settings.THUMBNAIL_DEBUG:
                raise
            return self.nodelist.render(context)

    def _render(self, context):
        from BeautifulSoup import BeautifulSoup
        geometry = self.geometry.resolve(context)
        options = {}
        for key, expr in self.options:
            noresolve = {u'True': True, u'False': False, u'None': None}
            value = noresolve.get(str(expr), expr.resolve(context))
            if key == 'options':
                options.update(value)
            else:
                options[key] = value

        output = self.nodelist.render(context)

        soup = BeautifulSoup(output)
        imgs = soup.findAll('img')
        for img in imgs:
            src = img.get('src').strip()
            if not src:
                continue
            src_url = urlparse(src)
            media_url = urlparse(settings.MEDIA_URL)
            if src.startswith('/') or (src_url.hostname == media_url.hostname):
                file_ = RE_IMAG_PATH.sub('', src_url.path)
                thumbnail = default.backend.get_thumbnail(
                        file_, geometry, **options)
                img['src'] = thumbnail.url
                img['width'] = thumbnail.width
                img['height'] = thumbnail.height

        output = str(soup)
        return output


@register.tag
def thumbnailer(parser, token):
    """
    Generates thumbnails for all "local" images found within the tag.
    "Local" images are any images which are stored under settings.MEDIA_ROOT.

    This tag uses `sorl.thumbnail` to generate the thumbnails. The syntax is
    identical to sorl's own `thumbnail` tag, except that the first  argument is
    ommitted.  Instead all image tags found between "thumbnailer" and
    "endthumnbailer" tags will be converted.  This is useful for things like
    blog posts where you would like to rescale uploaded images to fit within a
    certain bounds.

    Usage:
    {% thumbnailer "500x700" %}
     [html potentially containing images...]
    {% endthumbnailer %}
    """
    return ThumbnailerTag(parser, token)
