from __future__ import unicode_literals

from threading import local

_threadlocals = local()


def get_thread_variable(key, default=None):
    return getattr(_threadlocals, key, default)


def set_thread_variable(key, var):
    setattr(_threadlocals, key, var)


def delete_thread_variable(key):
    try:
        delattr(_threadlocals, key)
    except AttributeError:
        pass


def get_current_request():
    return get_thread_variable('request', None)


def get_current_user():
    request = get_current_request()
    return getattr(request, 'user', None)
