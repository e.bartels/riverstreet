# Use with django-taggit, add to settings.py:
#
# TAGGIT_TAGS_FROM_STRING = 'ado.utils.tags.comma_splitter'
# TAGGIT_STRING_FROM_TAGS = 'ado.utils.tags.comma_joiner'
#
# Joins and splits tags by comma (rather than default spaces or commas)
from __future__ import unicode_literals


def comma_splitter(tag_string):
    return [t.strip() for t in tag_string.split(',') if t.strip()]


def comma_joiner(tags):
    return ', '.join(t.name for t in tags)
