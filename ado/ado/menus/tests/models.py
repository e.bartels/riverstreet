from __future__ import unicode_literals

from django.db import models
from ado import menus


class Page(models.Model):
    title = models.CharField(max_length=200)
    slug = models.SlugField(max_length=200, unique=True)
    published = models.BooleanField(default=True)


menus.register_model(Page)
