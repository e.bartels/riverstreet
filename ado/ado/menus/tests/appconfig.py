from __future__ import unicode_literals

from django.apps import AppConfig


class MenusTestConfig(AppConfig):
    icon = '<i class="icon material-icons">menu</i>'
    name = 'ado.menus.tests'
    label = 'menutests'
