# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='MenuItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('item_title', models.CharField(max_length=100, verbose_name='Title')),
                ('slug', models.CharField(editable=False, max_length=100, unique=True)),
                ('url', models.CharField(max_length=255, blank=True)),
                ('text', models.TextField(blank=True)),
                ('type', models.CharField(db_index=True, max_length=20, editable=False, choices=[('object', 'object'), ('app', 'app'), ('container', 'container')])),
                ('object_id', models.PositiveIntegerField(null=True, blank=True)),
                ('lft', models.PositiveIntegerField(editable=False, db_index=True)),
                ('rght', models.PositiveIntegerField(editable=False, db_index=True)),
                ('tree_id', models.PositiveIntegerField(editable=False, db_index=True)),
                ('level', models.PositiveIntegerField(editable=False, db_index=True)),
                ('content_type', models.ForeignKey(blank=True, to='contenttypes.ContentType', null=True)),
                ('parent', models.ForeignKey(related_name='children', blank=True, to='menus.MenuItem', null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
