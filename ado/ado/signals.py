from django import dispatch
from django.db.models import QuerySet

post_bulk_update = dispatch.Signal()
post_bulk_delete = dispatch.Signal()


def _monkey_patch_queryset_with_bulk_signals():
    """
    Moneky patches django's Queryset 'update' & 'delete' methods so they send
    'post_bulk_update' and 'post_bulk_delete' signals
    """
    _orig_update = QuerySet.update
    _orig_delete = QuerySet.delete

    def queryset_update_with_signal(self, **kwargs):
        _orig_update(self, **kwargs)
        post_bulk_update.send(sender=self.model)

    def queryset_delete_with_signal(self, **kwargs):
        _orig_delete(self, **kwargs)
        post_bulk_delete.send(sender=self.model)

    QuerySet.update = queryset_update_with_signal
    QuerySet.delete = queryset_delete_with_signal
