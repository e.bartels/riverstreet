from __future__ import unicode_literals

from hashlib import md5
from django.http import HttpResponse, HttpResponseRedirect
from django.http import HttpResponseNotModified, Http404


def thumbnail(request, path):
    """
    Generates a thumbnail using the sorl.thumbnail module and redirects to the
    new image. This view is useful for generating thumbnail links from
    javascript code.
    """
    from sorl.thumbnail import get_thumbnail
    default_size = '90x90'
    options = dict(list(request.GET.items()))

    if 'size' in options:
        size = options.pop('size')
    else:
        size = default_size

    thumb = get_thumbnail(path, size, **options)

    return HttpResponseRedirect(thumb.url)


def color_bg(request, color, opacity=100):
    """
    Generates a 10x10 square image in the color requested.
    Useful for generating background colors based on user-provided
    color settings.
    """
    from PIL import Image, ImageColor

    alpha = int((int(opacity) / 100.0) * 255)

    if len(color) != 3 and len(color) != 6:
        raise Http404
    color = '#%s' % color
    color = ImageColor.getrgb(color) + (alpha,)

    size = (10, 10)

    etag = md5("%s%s" % (color, size)).hexdigest()
    if request.META.get("HTTP_IF_NONE_MATCH") == etag:
        return HttpResponseNotModified()

    img = Image.new("RGBA", size=size, color=color)

    response = HttpResponse(content_type="image/png")
    img.save(response, "PNG")
    response["Etag"] = etag
    return response
