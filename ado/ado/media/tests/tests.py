from __future__ import unicode_literals
from builtins import range, object

import os
import shutil
import random
import string
import tempfile
from PIL import Image as PILImage, ImageDraw
from io import BytesIO
import factory
from mock import Mock, patch
import requests

from django.test import TestCase, TransactionTestCase, override_settings
from django.core.files.base import ContentFile
from django.contrib.contenttypes.models import ContentType
from sorl import thumbnail

from ado.utils.mockutils import patchif
from ado.signals import post_bulk_delete, post_bulk_update
from ..models import (
        MediaItem,
        Image,
        File,
        Video,
        MediaRelation,
        ImageRelation,
        FileRelation)
from ..admin.forms import ImageAdminForm, MediaUploadImageForm
from .. import oembed
from ..tests import models as test_models


# Set to False to disable mocking of network responses
ENABLE_MOCKS = True


IMAGE_FORMATS = [
    ('jpeg', 'jpeg'),
    ('png', 'png'),
    ('gif', 'gif'),
    ('tiff', 'tiff'),
    ('jpeg2000', 'jp2'),
    ('webp', 'webp'),
]

TEMP_MEDIA_ROOT = tempfile.mkdtemp(prefix='django_test')


def tearDownModule():
    for subdir in ('files', 'images'):
        try:
            shutil.rmtree(os.path.join(TEMP_MEDIA_ROOT, subdir))
        except FileNotFoundError:
            pass
    try:
        os.rmdir(TEMP_MEDIA_ROOT)
    except FileNotFoundError:
        pass


def make_image(size=(100, 100), name=None, format=None):
    im = PILImage.new('L', size)
    draw = ImageDraw.Draw(im)
    random_text = ''.join(random.sample(string.ascii_letters, 10))
    draw.text((0, 0), random_text, fill=128)
    if format:
        ext = format
    else:
        format, ext = random.choice(IMAGE_FORMATS)
    name = name or 'test.{}'.format(ext)
    buf = BytesIO()
    im.save(buf, format=format)
    file_ = ContentFile(buf.getvalue(), name=name)
    buf.close()
    return file_


class ImageFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Image

    title = 'test image'

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        instance = model_class(*args, **kwargs)
        image = make_image()
        instance.filename.save(image.name, image)
        return instance


class FileFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = File

    title = 'test file'

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        instance = model_class(*args, **kwargs)
        image = make_image()
        instance.filename.save(image.name, image)
        return instance


class MediaUploadFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'media.MediaUpload'

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        instance = model_class(*args, **kwargs)
        image = make_image(name='test.jpg', format='jpeg')
        instance.original_filename = image.name
        instance.media_type = 'image'
        instance.metadata = {
            'original_filename': image.name,
            'filesize': image.size,
            'filetype': 'image/jpeg',
        }
        instance.filename.save(image.name, image)
        return instance


@override_settings(
    DEFAULT_FILE_STORAGE='ado.utils.storage.overwrite.OverwriteFileSystemStorage',
    MEDIA_ROOT=TEMP_MEDIA_ROOT,
)
class OverwriteFileSystemStorageTest(TestCase):
    def test_file_field_overwrites_existing_file(self):
        f = FileFactory.build()
        im = make_image(format='jpeg')
        f.filename.save('test.jpeg', im)
        filename = f.filename.name

        im = make_image(format='jpeg')
        f.filename.save('test.jpeg', im)
        self.assertEqual(filename, f.filename.name)


class PostBulkDeleteTest(TestCase):
    def setUp(self):
        pass

    def test_post_bulk_delete_fired(self):
        ImageFactory()
        ImageFactory()

        receiver_called = False

        def test_receiver(sender, *args, **kwargs):
            nonlocal receiver_called
            receiver_called = True

        post_bulk_delete.connect(test_receiver)
        Image.objects.all().delete()
        self.assertTrue(receiver_called, 'post_bulk_delete signal not fired')
        post_bulk_delete.disconnect(test_receiver)


class PostBulkUpdateTest(TestCase):
    def setUp(self):
        pass

    def test_post_bulk_update_fired(self):
        ImageFactory()
        ImageFactory()

        receiver_called = False

        def test_receiver(sender, *args, **kwargs):
            nonlocal receiver_called
            receiver_called = True

        post_bulk_update.connect(test_receiver)
        Image.objects.all().update(caption='test')
        self.assertTrue(receiver_called, 'post_bulk_update signal not fired')
        post_bulk_update.disconnect(test_receiver)


class MediaItemTest(TestCase):
    def test_uid_available_before_save(self):
        for factory_cls in (FileFactory, ImageFactory):
            instance = factory_cls.build()
            self.assertTrue(instance.uid)
            uid = instance.uid
            instance.filename.save('test.jpeg', make_image())
            self.assertEqual(uid, instance.uid)


class ImageTest(TransactionTestCase):
    def setUp(self):
        self.image = ImageFactory.create()

    def test_image_creation(self):
        image = self.image
        self.assertTrue(
            image.filename.storage.exists(image.filename.name),
            "Image file was not saved properly."
        )
        self.assertEqual(image.title, 'test image')
        self.assertTrue(image.uid.hex in image.filename.name)

        image.delete()

    def test_delete(self):
        image = self.image
        filename = image.filename.name
        image.delete()

        self.assertFalse(
            image.filename.storage.exists(filename),
            "File was not deleted after instance deletion.",
        )

    def test_mimetype(self):
        image = ImageFactory.build()
        im = make_image(format='jpeg')
        image.filename.save('test_file.jpg', im)
        self.assertEqual(image.mimetype, 'image/jpeg')

    def test_filesize_saved(self):
        image = ImageFactory()
        self.assertTrue(image.filesize)

    def test_subtype(self):
        self.assertEqual(self.image.subtype, self.image)
        item = MediaItem.objects.all()[0]
        self.assertEqual(item.subtype, self.image)
        item = MediaItem.objects.non_polymorphic()[0]
        self.assertEqual(item.subtype, self.image)
        self.assertEqual(item.subtype, self.image)

    def test_source_image(self):
        self.assertEqual(self.image.source_image, self.image.filename)

    def test_media_url(self):
        self.assertEqual(self.image.media_url, self.image.filename.url)

    def test_source_url(self):
        self.assertEqual(self.image.source_url, self.image.filename.url)

    def test_automatic_title(self):
        self.image.title = ''
        self.image.save()
        self.assertTrue(self.image.title)

    def test_image_dimensions_saved(self):
        image = ImageFactory.build()
        width = 213
        height = 107
        im = make_image(size=(width, height))
        image.filename.save(im.name, im)
        self.assertEqual(width, image.width)
        self.assertEqual(height, image.height)


class FileTest(TransactionTestCase):
    def setUp(self):
        self.file = FileFactory.create()

    def test_file_creation(self):
        file = self.file

        self.assertTrue(
            file.filename.storage.exists(file.filename.name),
            "File file was not saved properly.",
        )

        self.assertEqual(file.title, 'test file')
        self.assertTrue(file.uid.hex in file.filename.name)

        file.delete()

    def test_delete(self):
        file = self.file
        filename = file.filename.name
        file.delete()

        self.assertFalse(
            file.filename.storage.exists(filename),
            "File was not deleted after instance deletion.",
        )

    def test_mimetype(self):
        file = FileFactory.build()
        im = make_image(format='jpeg')
        file.filename.save('test_file.jpg', im)
        self.assertEqual(file.mimetype, 'image/jpeg')

        file = FileFactory.build()
        file.filename.save('test file', ContentFile(''))
        self.assertEqual(file.mimetype, 'unknown')

    def test_filesize_saved(self):
        file = FileFactory()
        self.assertTrue(file.filesize)

    def test_subtype(self):
        self.assertEqual(self.file.subtype, self.file)
        item = MediaItem.objects.all()[0]
        self.assertEqual(item.subtype, self.file)
        item = MediaItem.objects.non_polymorphic()[0]
        self.assertEqual(item.subtype, self.file)
        self.assertEqual(item.subtype, self.file)

    def test_source_image(self):
        self.assertEqual(self.file.source_image, None)

    def test_media_url(self):
        self.assertEqual(self.file.media_url, self.file.filename.url)

    def test_source_url(self):
        self.assertEqual(self.file.source_url, self.file.filename.url)

    def test_automatic_title(self):
        self.file.title = ''
        self.file.save()
        self.assertTrue(self.file.title)


# Video locations
VIMEO_VIDEO_URL = 'http://vimeo.com/7100569'
YOUTUBE_VIDEO_URL = 'http://www.youtube.com/watch?v=bDOYN-6gdRE'


# Mocking
def _mock_vimeo_request(*args, **kwargs):
    """
    Mock a Vimeo oembed response
    """
    return {
        'is_plus': '0',
        'provider_url': 'https://vimeo.com/',
        'description': 'Test description',
        'uri': '/videos/7100569',
        'title': 'Brad!',
        'video_id': 7100569,
        'html': '<iframe src="https://player.vimeo.com/video/7100569"></iframe>',
        'author_name': 'Casey Donahue',
        'height': 720,
        'thumbnail_width': 1280,
        'width': 1280,
        'upload_date': '2009-10-16 11:37:32',
        'version': '1.0',
        'author_url': 'http://vimeo.com/caseydonahue',
        'duration': 118,
        'provider_name': 'Vimeo',
        'thumbnail_url': 'http://i.vimeocdn.com/video/29412830_1280.jpg',
        'type': 'video',
        'thumbnail_height': 720
    }


mock_vimeo_request = Mock(side_effect=_mock_vimeo_request)


def _mock_youtube_request(*args, **kwargs):
    """
    Mock a Youtube oembed response
    """
    return {
        'provider_url': 'https://www.youtube.com/',
        'author_name': 'schmoyoho',
        'title': 'Test title',
        'type': 'video',
        'html': '<iframe src="https://www.youtube.com/embed/bDOYN-6gdRE?feature=oembed"></iframe>',
        'thumbnail_width': 480,
        'height': 344,
        'width': 459,
        'version': '1.0',
        'author_url': 'https://www.youtube.com/user/schmoyoho',
        'provider_name': 'YouTube',
        'thumbnail_url': 'https://i.ytimg.com/vi/bDOYN-6gdRE/hqdefault.jpg',
        'thumbnail_height': 360
    }


mock_youtube_request = Mock(side_effect=_mock_youtube_request)


def _mock_oembed_error_request(*args, **kwargs):
    """
    Raise OembedError on request
    """
    raise oembed.OembedError


mock_oembed_error_request = Mock(side_effect=_mock_oembed_error_request)


def _mock_404_request(*args, **kwargs):
    """
    Mock a 404 response
    """
    raise requests.RequestException()


mock_404_request = Mock(side_effect=_mock_404_request)


class BaseOembedTest(object):
    """Base class for Oembed Tests"""
    pass


class OembedErrorTest(BaseOembedTest, TestCase):
    """
    Test oembed error conditions
    """

    @patchif(ENABLE_MOCKS, 'requests.get',
                Mock(side_effect=requests.ConnectionError()))
    def test_bad_url(self):
        with self.assertRaises(oembed.OembedError):
            oembed.fetch('notaurl')

    @patchif.object(ENABLE_MOCKS, requests, 'get',
                Mock(side_effect=mock_404_request))
    def test_bad_oembed_source(self):
        with self.assertRaises(oembed.OembedError):
            oembed.fetch('http://vimeo.com/blah')


@patchif(ENABLE_MOCKS, 'requests.get',
        Mock(side_effect=RuntimeError('no network allowed')))
@patchif(ENABLE_MOCKS, 'ado.media.oembed.send_request',
        mock_vimeo_request)
class VimeoOembedVideoTest(BaseOembedTest, TestCase):
    """
    Test Vimeo Oembed response
    """
    def test_vimeo_response(self):
        data = oembed.fetch(VIMEO_VIDEO_URL)
        self.assertTrue(data)


@patchif(ENABLE_MOCKS, 'requests.get',
        Mock(side_effect=RuntimeError('no network allowed')))
@patchif(ENABLE_MOCKS, 'ado.media.oembed.send_request',
        mock_youtube_request)
class YoutubeOembedVideoTest(BaseOembedTest, TestCase):
    """
    Test Youtube Oembed response
    """
    def test_youtube_response(self):
        data = oembed.fetch(YOUTUBE_VIDEO_URL)
        self.assertTrue(data)


def _mock_fetch_image():
    """Mock Video.fetch_image"""
    return make_image()


mock_fetch_image = Mock(side_effect=_mock_fetch_image)


@patchif(ENABLE_MOCKS, 'requests.get',
        Mock(side_effect=RuntimeError('no network allowed')))
@patchif.object(ENABLE_MOCKS, Video, 'fetch_image',
        Mock(side_effect=mock_fetch_image))
class BaseVideoTestCase(object):
    """Base test cases for videos  (vimeo, youtube, etc)"""

    def test_metadata(self):
        video = Video.objects.create(url=self.video_url)
        self.assertTrue(video.data)
        self.assertEqual(video.thumbnail_url,
                         video.data['thumbnail_url'])
        self.assertEqual(video.data, video.info)
        self.assertEqual(video.title, video.data['title'])

    def test_thumbnails(self):
        video = Video.objects.create(url=self.video_url)
        self.assertTrue(video.thumbnail)
        self.assertTrue(video.screenshot)
        self.assertTrue(video.large_screen)

    def test_mimetype(self):
        video = Video(url=self.video_url)
        self.assertEqual(video.mimetype, 'unknown')

    def test_subtype(self):
        video = Video.objects.create(url=self.video_url)
        self.assertEqual(video.subtype, video)
        item = MediaItem.objects.all()[0]
        self.assertEqual(item.subtype, video)
        item = MediaItem.objects.non_polymorphic()[0]
        self.assertEqual(item.subtype, video)
        self.assertEqual(item.subtype, video)

    def test_source_image(self):
        video = Video.objects.create(url=self.video_url)
        self.assertTrue(video.source_image)
        self.assertEqual(video.source_image, video.video_image)
        self.assertTrue(video.screenshot)

    def test_media_url(self):
        video = Video(url=self.video_url)
        self.assertEqual(video.media_url, None)

    def test_source_url(self):
        video = Video(url=self.video_url)
        self.assertEqual(video.source_url, None)

    def test_automatic_title(self):
        video = Video(url=self.video_url)
        video.title = ''
        video.save()
        self.assertTrue(video.title)

    def test_image_fetch(self):
        video = Video.objects.create(url=self.video_url)
        self.assertTrue(video.video_image)


@patchif(ENABLE_MOCKS, 'ado.media.oembed.send_request',
        mock_vimeo_request)
class VimeoVideoTest(BaseVideoTestCase, TestCase):
    video_url = VIMEO_VIDEO_URL


@patchif(ENABLE_MOCKS, 'ado.media.oembed.send_request',
        mock_youtube_request)
class YoutubeVideoTest(BaseVideoTestCase, TestCase):
    video_url = YOUTUBE_VIDEO_URL


@patchif(ENABLE_MOCKS, 'ado.media.oembed.send_request',
        mock_oembed_error_request)
class VimeoOembedErrorTest(TestCase):
    def test_oembed_error_on_create(self):
        with self.assertRaises(oembed.OembedError):
            Video.objects.create(url='http://vimeo.com/nopenotavideo')

    def test_oembed_error_on_save_new(self):
        with self.assertRaises(oembed.OembedError):
            video = Video(url='http://vimeo.com/nopenotavideo')
            video.save()

    def test_oembed_error_on_save_existing(self):
        video = Video.objects.create(
            url=VIMEO_VIDEO_URL,
            data=_mock_vimeo_request(),
            video_image=make_image(),
        )
        video.save()


class ImageRelationTest(TestCase):
    def setUp(self):
        self.images = []
        for x in range(0, 5):
            self.images.append(ImageFactory.create())

    def _do_relation_test(self, set_class):
        set = set_class.objects.create(title='TESTING')

        # Assign single image
        image = Image.objects.all()[0]
        set.images.add(image)
        self.assertEqual(image.mediarelation_set.count(), 1,
                                "Add one image failed.")
        self.assertEqual(set.related_images.count(), 1)

        # Add one image
        image2 = Image.objects.all()[1]
        set.images.add(image2)
        self.assertEqual(set.images.count(), 2, "Add a second image failed.")
        self.assertEqual(set.related_images.count(), 2)

        # Assign random images
        set.images = Image.objects.all().order_by('?')
        self.assertEqual(set.images.count(), len(self.images),
                    "Adding random images failed.")

        # Test sort order change
        first_image = set.images.all()[0]
        set.images.set_position(first_image.pk, set.images.count())
        self.assertEqual(first_image.pk, set.images.all().reverse()[0].pk,
                            "Reordering failed.")

        # Test removing an image
        set.images.remove(image)
        self.assertFalse(image in set.images.all(),
                            "Image was not removed properly.")

        # Tests that related object deletion also deletes the ImageRelation
        set.delete()
        self.assertEqual(image.mediarelation_set.count(), 0)
        self.assertEqual(ImageRelation.objects.count(), 0)

        # Images should still be there after though
        self.assertEqual(Image.objects.count(), len(self.images),
                            "Image count is incorrect.")

    def test_image_relations(self):
        self._do_relation_test(test_models.RegularSet)

    def test_abstract_subclasses(self):
        self._do_relation_test(test_models.AbsChildSet)

    def test_subclass(self):
        self._do_relation_test(test_models.ChildSet)

    def test_media_registry(self):
        for klass in (test_models.RegularSet,
                      test_models.ChildSet,
                      test_models.AbsChildSet):
            self.assertTrue(ImageRelation.relates_to(klass),
                "%s.%s was not found in the list of related models." % (
                            klass._meta.app_label, klass._meta.object_name))
        self.assertFalse(ImageRelation.relates_to(test_models.AbstractSet))

    def test_alt_field_name(self):
        a = test_models.AltNameModel.objects.create()
        a.image_attachments = Image.objects.all()
        self.assertEqual(a.image_attachments.count(), len(self.images),
                                "Failed with alternate field name")

    def test_object_deletion(self):
        set1 = test_models.RegularSet.objects.create(title='test 1')
        set1.images = Image.objects.all()
        image_count = Image.objects.count()
        self.assertEqual(set1.images.count(), image_count)
        set1.delete()
        self.assertEqual(Image.objects.count(), image_count)
        self.assertEqual(ImageRelation.objects.count(), 0)

        set1 = test_models.RegularSet.objects.create(title='test 1')
        set1.images = Image.objects.all()
        set2 = test_models.RegularSet2.objects.create(title='test 2')
        set2.images = Image.objects.all()
        self.assertEqual(set1.images.count(), set2.images.count())
        self.assertEqual(ImageRelation.objects.count(),
                         Image.objects.count() * 2)

        image_count = Image.objects.count()
        test_models.RegularSet.objects.all().delete()
        self.assertEqual(set2.images.count(), image_count)
        self.assertEqual(Image.objects.count(), image_count)

        set2.delete()
        self.assertEqual(Image.objects.count(), image_count)
        self.assertEqual(ImageRelation.objects.count(), 0)

    def test_pk_required(self):
        set1 = test_models.RegularSet(title='test 1')
        with self.assertRaises(ValueError):
            set1.images.all()
        with self.assertRaises(ValueError):
            set1.images = Image.objects.all()

    def test_duplicate_items_ignored_on_save(self):
        set1 = test_models.RegularSet.objects.create(title='test 1')
        all_images = Image.objects.all()
        set1.images = all_images
        self.assertEqual(set1.images.count(), len(all_images))

        # Adding an existing image relation should be silently ignored, count
        # should remain the same.
        ImageRelation.objects.create(
                content_type=ContentType.objects.get_for_model(set1),
                object=set1,
                item=all_images[0])
        self.assertEqual(set1.images.count(), len(all_images))


class FileRelationTest(TestCase):
    def setUp(self):
        self.files = []
        for i in range(0, 5):
            self.files.append(FileFactory.create())

    def _do_relation_test(self, set_class):
        set = set_class.objects.create(title='TESTING')

        # Assign single file
        file = File.objects.all()[0]
        set.files.add(file)
        self.assertEqual(file.mediarelation_set.count(), 1,
                            "Add one file failed.")
        self.assertEqual(set.related_files.count(), 1)

        # Add one file
        file2 = File.objects.all()[1]
        set.files.add(file2)
        self.assertEqual(set.files.count(), 2, "Add a second file failed.")
        self.assertEqual(set.related_files.count(), 2)

        # Assign random files
        set.files = File.objects.all().order_by('?')
        self.assertEqual(set.files.count(), len(self.files),
                            "Add random files failed")

        # Test sort order change
        first_file = set.files.all()[0]
        set.files.set_position(first_file.pk, set.files.count())
        self.assertEqual(first_file.pk, set.files.all().reverse()[0].pk,
                            "Reordering failed.")

        # Test removing a file
        set.files.remove(file)
        self.assertFalse(file in set.files.all(),
                            "File was not removed properly.")

        # Tests that related object deletion also deletes the FileRelation
        set.delete()
        self.assertEqual(file.mediarelation_set.count(), 0)
        self.assertEqual(FileRelation.objects.count(), 0)

        # Files should still be there after though
        self.assertEqual(File.objects.count(), len(self.files),
                            "File count is incorrect.")

    def test_file_relations(self):
        self._do_relation_test(test_models.RegularSet)

    def test_abstract_subclasses(self):
        self._do_relation_test(test_models.AbsChildSet)

    def test_subclass(self):
        self._do_relation_test(test_models.ChildSet)

    def test_media_registry(self):
        for klass in (test_models.RegularSet,
                      test_models.ChildSet,
                      test_models.AbsChildSet):
            self.assertTrue(FileRelation.relates_to(klass),
                "%s.%s was not found in the list of related models." % (
                        klass._meta.app_label, klass._meta.object_name))
        self.assertFalse(FileRelation.relates_to(test_models.AbstractSet))


class MultipleMediaTestCase(TestCase):
    set_class = test_models.RegularSet

    def setUp(self):
        self.files = []
        for i in range(0, 5):
            self.files.append(FileFactory.create())

        self.images = []
        for i in range(0, 5):
            self.images.append(ImageFactory.create())

    def test_multi_media_types(self):
        set = self.set_class.objects.create(title='TESTING')

        set.files = File.objects.all()
        self.assertEqual(set.files.count(), File.objects.count())
        self.assertEqual(set.files.count(), len(self.files))

        set.images = Image.objects.all()
        self.assertEqual(set.images.count(), Image.objects.count())
        self.assertEqual(set.images.count(), len(self.images))

        self.assertEqual(set.media.count(), len(self.images) + len(self.files))

        self.assertEqual(set.related_images.count(), len(self.images))
        self.assertEqual(set.related_files.count(), len(self.files))

        set.images.clear()
        self.assertEqual(set.images.count(), 0)
        self.assertEqual(len(set.images.all()), 0)
        self.assertEqual(set.files.count(), len(self.files))
        self.assertEqual(set.media.count(), len(self.files))

        set.images = Image.objects.all()
        self.assertEqual(set.media.count(), len(self.files) + len(self.images))

        set.media.clear()
        self.assertEqual(set.media.count(), 0)
        self.assertEqual(set.files.count(), 0)
        self.assertEqual(set.images.count(), 0)
        self.assertEqual(MediaRelation.objects.count(), 0)

        set.media.add(*tuple(Image.objects.all()))
        self.assertEqual(set.media.count(), len(self.images))
        self.assertEqual(set.images.count(), len(self.images))

        set.media.add(*tuple(File.objects.all()))
        self.assertEqual(set.media.count(), len(self.images) + len(self.files))
        self.assertEqual(set.files.count(), len(self.files))

        set.media.set(*tuple(Image.objects.all()))
        self.assertEqual(set.media.count(), len(self.images))
        self.assertEqual(set.images.count(), len(self.images))
        self.assertEqual(set.files.count(), 0)

    def test_bulk_related(self):
        set = self.set_class.objects.create(title='TESTING')
        set.images = Image.objects.all()
        self.assertEqual(set.related_images.count(), Image.objects.count())

        image_relation = set._meta.get_field('related_images')
        related_objects = image_relation.bulk_related_objects([set])
        self.assertEqual(len(related_objects), set.related_images.count())

        file_relation = set._meta.get_field('related_files')
        related_objects = file_relation.bulk_related_objects([set])
        self.assertEqual(len(related_objects), 0)

    def test_add_wrong_types(self):
        set = self.set_class.objects.create(title='TESTING')

        # Can't add the wront types
        self.assertRaises(ValueError, set.images.set, File.objects.all()[0])
        self.assertRaises(ValueError, set.files.add, Image.objects.all()[0])


class MultipleMediaAbstractTestCase(MultipleMediaTestCase):
    set_class = test_models.AbsChildSet


class CustomRelationTestCase(TestCase):
    set_class = test_models.TestImageCollection

    def setUp(self):
        self.files = []
        for i in range(0, 5):
            self.files.append(ImageFactory.create())

    def test_image_relations(self):
        collection = self.set_class.objects.create(title='TESTING')

        # Add single image
        image = Image.objects.all()[0]
        collection.images.add(image)
        self.assertEqual(collection.images.count(), 1)
        self.assertEqual(test_models.TestCollectionImage.objects.count(), 1)

        # Add one image
        image2 = Image.objects.all()[1]
        collection.images.add(image2)
        self.assertEqual(collection.images.count(), 2)
        self.assertEqual(collection.related_images.count(), 2)

        # Assign random images
        collection.images = Image.objects.all().order_by('?')
        self.assertEqual(collection.images.count(), len(self.files))

        # Change sort order
        first_image = collection.images.all()[0]
        collection.images.set_position(first_image.pk,
                                       collection.images.count())
        self.assertEqual(first_image.pk,
                         collection.images.all().reverse()[0].pk)

        collection.images.remove(image)
        self.assertFalse(image in collection.images.all())

        # Delete collection
        collection.delete()
        self.assertEqual(test_models.TestCollectionImage.objects.count(), 0)
        self.assertEqual(Image.objects.count(), len(self.files))


class MediaTagTest(TestCase):
    def test_untagged_queryset(self):
        file = FileFactory.create()
        image = ImageFactory.create()
        untagged = MediaItem.objects.all().exclude(tags__isnull=False)
        self.assertEqual(len(untagged), 2)

        image.tags.add('testtag')
        untagged = MediaItem.objects.all().exclude(tags__isnull=False)
        self.assertEqual(len(untagged), 1)

        file.tags.add('testtag2')
        untagged = MediaItem.objects.all().exclude(tags__isnull=False)
        self.assertEqual(len(untagged), 0)


# Testing thumbnail deletion
class ThumbnailDeletionTest(TransactionTestCase):
    def test_delete_image_deletes_thumbs(self):
        image = ImageFactory.create()
        thumb = thumbnail.get_thumbnail(image.filename, '100x100',
                                        crop='center')

        self.assertTrue(image.filename.storage.exists(image.filename.name),
                "Saving file to storage failed.")
        self.assertTrue(thumb.exists(),
                "Thumbnail creation failed.")
        self.assertTrue(thumb.storage.exists(thumb.name),
                "Thumbnail creation failed.")

        image.delete()
        self.assertFalse(thumb.exists(),
                "Thumbnail deletion failed after image deletion.")
        self.assertFalse(thumb.storage.exists(thumb.name),
                "Thumbnail deletion failed after image deletion.")

    def test_thumbnail_not_deleted_if_image_file_not_changed(self):
        image = ImageFactory.create()
        with patch.object(thumbnail, 'delete', return_value=None) as mock_delete:
            image = Image.objects.get(pk=image.pk)
            image.save()
            self.assertFalse(mock_delete.called,
                             'File did not change, but thumbnail was still deleted.')


@override_settings(
    DEFAULT_FILE_STORAGE='ado.utils.storage.overwrite.OverwriteFileSystemStorage',
    MEDIA_ROOT=TEMP_MEDIA_ROOT,
)
class ImageAdminFormTest(TestCase):
    def test_replace_image_deletes_thumbs(self):
        # Create image & thumb
        image = ImageFactory.build()
        image.filename.save('test.jpeg', make_image(format='jpeg'))
        thumb = thumbnail.get_thumbnail(image.filename, '50x50')
        self.assertTrue(thumb.exists())

        # Save new file with form
        form = ImageAdminForm(
            instance=image,
            files={
                'filename': make_image(format='jpeg'),
            },
            data={
                'title': image.title,
                'caption': '',
                'alt_text': '',
                'tags': '',
            },
        )
        self.assertTrue(form.is_valid(), form.errors)

        # Thumb should have been deleted
        self.assertFalse(thumb.exists(),
                "Thumbnail deletion failed after image changed.")
        self.assertFalse(thumb.storage.exists(thumb.name),
                "Thumbnail deletion failed after image changed.")

    def test_replace_image_thumbnails_are_different(self):
        # Create image & thumb
        image = ImageFactory.build()
        image.filename.save('test.jpeg', make_image(format='jpeg'))
        original_filename = image.filename.name
        thumb1 = thumbnail.get_thumbnail(image.filename, '50x50')
        thumb1_buf = BytesIO()
        thumb1_buf.write(thumb1.read())

        # Save new file with form
        form = ImageAdminForm(
            instance=image,
            files={
                'filename': make_image(format='jpeg'),
            },
            data={
                'title': image.title,
                'caption': '',
                'alt_text': '',
                'tags': '',
            },
        )
        self.assertTrue(form.is_valid(), form.errors)
        image = form.save(commit=True)

        # Make thumbnail from new image
        thumb2 = thumbnail.get_thumbnail(image.filename, '50x50')
        thumb2_buf = BytesIO()
        thumb2_buf.write(thumb2.read())

        # Thumbnail contents should not be the same
        self.assertEqual(original_filename, image.filename.name)
        self.assertNotEqual(thumb1_buf.getvalue(),
                            thumb2_buf.getvalue(),
                            'Thumbnails should not be same')


class MediaUploadTest(TransactionTestCase):
    def test_create(self):
        upload = MediaUploadFactory.create()
        self.assertTrue(upload.filename)
        self.assertTrue(upload.media_type)
        self.assertTrue(upload.metadata)
        self.assertTrue(upload.metadata['original_filename'])
        self.assertTrue(upload.metadata['filesize'])
        self.assertTrue(upload.metadata['filetype'])

    def test_delete_deletes_file(self):
        upload = MediaUploadFactory.create()
        self.assertTrue(upload.filename.storage.exists(upload.filename.name))

        upload.delete()
        self.assertTrue(upload.filename.name)
        self.assertFalse(upload.filename.storage.exists(upload.filename.name))

    def test_delete_with_no_cleanup(self):
        upload = MediaUploadFactory.create()
        self.assertTrue(upload.filename.storage.exists(upload.filename.name))

        filename = upload.filename.name
        upload.delete(no_cleanup=True)
        self.assertTrue(filename)
        self.assertTrue(upload.filename.storage.exists(filename))

    def test_upload_admin_form_image(self):
        upload = MediaUploadFactory.create()
        form = MediaUploadImageForm(upload=upload, data={
            'upload_id': upload.id,
            'title': 'Test',
            'caption': '',
            'alt_text': '',
            'tags': '',
        })
        self.assertTrue(form.is_valid(), form.errors)

        self.assertTrue(upload.filename.storage.exists(upload.filename.name))
        image = form.save()
        self.assertTrue(image.id)
        self.assertTrue(image.filename.storage.exists(image.filename.name),
                        'Image does not exist after upload form save.')
