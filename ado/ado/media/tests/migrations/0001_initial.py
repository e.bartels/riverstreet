# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-21 20:21
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import ado.media.fields.related


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('media', '0005_remove_uuid_alter_uid'),
    ]

    operations = [
        migrations.CreateModel(
            name='AbsChildSet',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=20)),
                ('description', models.TextField(blank=True)),
                ('files', ado.media.fields.related.RelatedFilesField(through='media.FileRelation', to='media.File', verbose_name='Files')),
                ('images', ado.media.fields.related.RelatedImagesField(through='media.ImageRelation', to='media.Image', verbose_name='Images')),
                ('media', ado.media.fields.related.RelatedMediaField(through='media.MediaRelation', to='media.MediaItem', verbose_name='Media Items')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='AltNameModel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image_attachments', ado.media.fields.related.RelatedImagesField(through='media.ImageRelation', to='media.Image', verbose_name='Images')),
            ],
        ),
        migrations.CreateModel(
            name='RegularSet',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='RegularSet2',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=20)),
                ('files', ado.media.fields.related.RelatedFilesField(through='media.FileRelation', to='media.File', verbose_name='Files')),
                ('images', ado.media.fields.related.RelatedImagesField(through='media.ImageRelation', to='media.Image', verbose_name='Images')),
                ('media', ado.media.fields.related.RelatedMediaField(through='media.MediaRelation', to='media.MediaItem', verbose_name='Media Items')),
            ],
        ),
        migrations.CreateModel(
            name='TestCollectionImage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sort', models.PositiveIntegerField(blank=True, default=0, null=True, verbose_name='sort order')),
                ('item', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='testcollectionimage_set', to='media.MediaItem')),
            ],
            options={
                'ordering': ('sort',),
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='TestImageCollection',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=20)),
                ('images', ado.media.fields.related.RelatedImagesField(through='mediatests.TestCollectionImage', to='media.Image', verbose_name='Images')),
            ],
        ),
        migrations.CreateModel(
            name='ChildSet',
            fields=[
                ('regularset_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='mediatests.RegularSet')),
                ('notes', models.TextField()),
            ],
            bases=('mediatests.regularset',),
        ),
        migrations.AddField(
            model_name='testcollectionimage',
            name='object',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='related_images', to='mediatests.TestImageCollection'),
        ),
        migrations.AddField(
            model_name='regularset',
            name='files',
            field=ado.media.fields.related.RelatedFilesField(through='media.FileRelation', to='media.File', verbose_name='Files'),
        ),
        migrations.AddField(
            model_name='regularset',
            name='images',
            field=ado.media.fields.related.RelatedImagesField(through='media.ImageRelation', to='media.Image', verbose_name='Images'),
        ),
        migrations.AddField(
            model_name='regularset',
            name='media',
            field=ado.media.fields.related.RelatedMediaField(through='media.MediaRelation', to='media.MediaItem', verbose_name='Media Items'),
        ),
    ]
