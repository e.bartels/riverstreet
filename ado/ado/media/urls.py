from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^thumbnails/(?P<path>.+)$', views.thumbnail, name="media-thumbnail"),
    url(r'^bg/(?P<color>[a-zA-Z0-9]{3,6})\.png$', views.color_bg),
    url(r'^bg/(?P<color>[a-zA-Z0-9]{3,6})_(?P<opacity>[0-9]{1,3})\.png$', views.color_bg),
]
