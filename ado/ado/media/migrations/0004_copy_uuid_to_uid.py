# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-04-01 21:39
from __future__ import unicode_literals

import base64
import uuid
from django.db import migrations


def copy_uuid_to_uid(apps, schema_editor):
    MediaItem = apps.get_model('media', 'MediaItem')
    items = MediaItem.objects.all()
    for item in items:
        base64_uuid = str.encode('{0}=='.format(item.uuid))
        uuid_bytes = base64.urlsafe_b64decode(base64_uuid)
        item.uid = uuid.UUID(bytes=uuid_bytes)
        item.save()
        assert base64.urlsafe_b64encode(item.uid.bytes)[:-2] == item.uuid.encode()


def copy_uid_to_uuid(apps, schema_editor):
    MediaItem = apps.get_model('media', 'MediaItem')
    items = MediaItem.objects.all()
    for item in items:
        item.uuid = base64.urlsafe_b64encode(item.uid.bytes)[:-2]
        item.save()


class Migration(migrations.Migration):

    dependencies = [
        ('media', '0003_mediaitem_uid'),
    ]

    operations = [
        migrations.RunPython(copy_uuid_to_uid, copy_uid_to_uuid),
    ]
