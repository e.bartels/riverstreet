from __future__ import unicode_literals

import re
import magic


def legacy_mime_from_file(f):
    # Brett Funderburg's older python magic bindings
    m = magic.open(magic.MAGIC_MIME)
    m.load()
    if isinstance(f, str):
        mtype = m.file(f)
    else:
        f.seek(0)
        mtype = m.buffer(f.read(1024))
        f.seek(0)
    return mtype


def mime_from_file(f):
    if hasattr(magic, 'open'):
        return legacy_mime_from_file(f)

    # Adam Hupp's ctypes-based magic library
    # https://github.com/ahupp/python-magic
    m = magic.Magic(mime=True)
    if isinstance(f, str):
        mtype = m.from_file(f)
    else:
        f.seek(0)
        mtype = m.from_buffer(f.read(1024))
        f.seek(0)
    return mtype


RE_MIMETYPE = re.compile(r'^([a-z]+/[a-z0-9-]+).*$')


def get_mime_type(f):
    """
    Given a file, uses magic to return the mimetype.
    """
    mtype = mime_from_file(f)
    # Make sure mime type is in the right format for our use.
    mtype = RE_MIMETYPE.sub(r'\1', mtype.strip().lower())
    return mtype
