from __future__ import unicode_literals
from builtins import str

from django import http
from django.apps import apps
from django.shortcuts import render, get_object_or_404
from django.contrib.admin.views.decorators import staff_member_required
from django.core.urlresolvers import reverse
from django.utils.datastructures import MultiValueDictKeyError
from sorl import thumbnail


@staff_member_required
def fetch_mediaitem_info(request):
    """
    Returns the url to a
    """
    try:
        model_name = request.GET.get('model', 'media.mediaitem')
        pk = int(request.GET['pk'])
    except (MultiValueDictKeyError, ValueError, TypeError):
        return http.HttpResponseBadRequest('The "pk" param is required and was not provided')

    try:
        (app_label, model_name) = model_name.split('.')
    except ValueError:
        raise http.Http404

    model = apps.get_model(app_label, model_name)
    mediaitem = get_object_or_404(model, pk=pk)
    image = mediaitem.source_image
    thumb = None
    if image:
        thumb = thumbnail.get_thumbnail(image, '360x300')

    opts = model._meta

    data = {
        'pk': pk,
        'title': mediaitem.title,
        'str': str(mediaitem),
        'type': opts.model_name,
        'mimetype': mediaitem.mimetype,
        'url': mediaitem.media_url,
        'image': image and image.url or None,
        'thumbnail': thumb and thumb.url or None,
        'admin_url': reverse('admin:media_{0}_change'.format(opts.model_name), args=[mediaitem.pk]),
    }
    return http.JsonResponse(data)


# Files browsers used by popup dialogs
@staff_member_required
def link_browser(request):
    return render(request, 'admin/media/link_browser.html', {})


@staff_member_required
def image_browser(request):
    return render(request, 'admin/media/image_browser.html', {})
