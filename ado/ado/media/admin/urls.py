from django.conf.urls import url
from ..admin import views

urlpatterns = [
    url(r'fetch_mediaitem_info/$', views.fetch_mediaitem_info, name="admin-media-fetch_mediaitem_info"),

    url(r'link_browser/$', views.link_browser, name="admin-media-link_browse"),
    url(r'image/file_browser/$', views.image_browser, name="admin-media-image_browser"),
]
