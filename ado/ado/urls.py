from django.conf.urls import include, url
from django.conf.urls.static import static
from django.views.generic import TemplateView
from django.conf import settings
from django.contrib import admin

urlpatterns = [
    url(r'^$', TemplateView.as_view(template_name='index.html'), name='index'),
    url(r'^media/', include('ado.media.urls')),

    # App examples
    url(r'^pages/', include('ado.examples.pages.urls')),
    url(r'^portfolio/', include('ado.examples.portfolio.urls')),
    url(r'^artists/', include('ado.examples.artists.urls')),
    url(r'^exhibitions/', include('ado.examples.exhibitions.urls')),
    url(r'^blog/', include('ado.examples.blog.urls')),

    # Admin
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^customadmin/', include('ado.customadmin.urls')),
    url(r'^admin/media/', include('ado.media.admin.urls')),
    url(r'^admin/', include(admin.site.urls)),
]

# MEDIA For Development Server
urlpatterns += static(settings.MEDIA_URL,
                      document_root=settings.MEDIA_ROOT,
                      show_indexes=True)
