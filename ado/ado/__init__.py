# Python 3 compatible standard library for Python 2
from future import standard_library
standard_library.install_aliases()

# Package version
__version__ = "0.15.31"
