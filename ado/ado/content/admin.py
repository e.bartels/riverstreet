from __future__ import unicode_literals
from builtins import super

from django.contrib import admin
from django import forms

from webpack_loader import utils
from ado.customadmin.admin import BaseModelAdminMixin


class ContentItemForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Disable ckeditor field for non-text type ContentItem instances
        # This prevents havaing a bunch of hidden ckeditors initialized in the
        # page, which take up a lot of resources.
        if self.instance and self.instance.type != 'text':
            self.fields['text'].widget = forms.Textarea()


class ContentItemsInline(BaseModelAdminMixin, admin.StackedInline):
    """Inline for ContentItem model (images/video/text)"""
    template = "admin/edit_inline/content_item_inline.html"
    classes = ['sortable', 'sortable-field__position']
    extra = 0
    form = ContentItemForm
    verbose_image_fk_fields = ['image']
    verbose_video_fk_fields = ['video']
    html_editor_fields = ['text']

    @property
    def media(self):
        # add webpack assets
        webpack_js = utils.get_files('content_inlines', 'js', config='CUSTOMADMIN')
        webpack_css = utils.get_files('content_inlines', 'css', config='CUSTOMADMIN')

        media = super().media
        media.add_js([f['url'] for f in webpack_js])
        media.add_css({'all': [f['url'] for f in webpack_css]})
        return media
