import os
from .dev import *

config_path = os.path.join(BASE_PATH, 'config', 'local.py')
try:
    exec(open(os.path.abspath(config_path)).read())
except IOError:
    pass

SECRET_KEY = '__secret__'
DEBUG = False
TESTING = True

# Database Settings
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': ':memory:',
    }
}

# Customize test apps (to pull in test-specific models)
INSTALLED_APPS = list(INSTALLED_APPS) + [
    'test_without_migrations',
    'ado.media.tests',
    'ado.menus.tests',
]

# Use the following for spatialite
# SPATIALITE_SQL = os.path.join(BASE_PATH, 'config', 'init_spatialite-3.0.sql')

# Use the following for postgis
# TEST_RUNNER='django.contrib.gis.tests.run_tests'

# Override media path
DEFAULT_FILE_STORAGE = 'inmemorystorage.InMemoryStorage'
INMEMORYSTORAGE_PERSIST = True
STATIC_ROOT = os.path.join(BASE_PATH, 'static')

# Cache Setup (use locmem for tests)
CACHE_TIMEOUT = 60 * 5
CACHE_PREFIX = 'ADO'
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': 'ado-tests',
        'KEY_PREFIX': CACHE_PREFIX,
        'TIMEOUT': CACHE_TIMEOUT
    }
}

# Email settings
EMAIL_BACKEND = 'django.core.mail.backends.filebased.EmailBackend'
EMAIL_FILE_PATH = os.path.join(TEMP_DIR, 'emails')

# Speed up tests by setting faster hash algo
PASSWORD_HASHERS = (
    'django.contrib.auth.hashers.SHA1PasswordHasher',
    'django.contrib.auth.hashers.MD5PasswordHasher',
)
