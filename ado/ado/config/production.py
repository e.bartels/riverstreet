"""
Settings for use in production.
"""
from .defaults import *

DEBUG = False

MANAGERS = list(MANAGERS) + [
    # Add any extra email addresses
]

ALLOWED_HOSTS = (
    # Add any domains that will be served in production
    # '.foo.com',
)

# Cache Setup
CACHE_TIMEOUT = 60 * 10
CACHE_PREFIX = 'ADO'
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.PyLibMCCache',
        'LOCATION': '127.0.0.1:11211',
        'KEY_PREFIX': CACHE_PREFIX,
        'TIMEOUT': CACHE_TIMEOUT
    }
}
CACHE_MIDDLEWARE_SECONDS = CACHE_TIMEOUT
CACHE_MIDDLEWARE_KEY_PREFIX = CACHE_PREFIX
SESSION_ENGINE = "django.contrib.sessions.backends.cached_db"

# Cached template loader
# TEMPLATES[0]['OPTIONS']['loaders'] = [
#     ('django.template.loaders.cached.Loader', [
#         'django.template.loaders.filesystem.Loader',
#         'django.template.loaders.app_directories.Loader',
#     ]),
# ]

# Temp folders
TEMP_DIR = '/tmp/'
FILE_UPLOAD_TEMP_DIR = TEMP_DIR

STATIC_ROOT = root('../../htdocs/static')
STATIC_URL = '/s/'
