from django.conf.urls import url
from ado.examples.artists import views
from ado import menus

urlpatterns = [
    url(r'^$', views.index, name='artists-artist-index'),
    url(r'^(?P<slug>[\w-]+)/$', views.view, name='artists-artist-view'),
]

menus.register_url('Artists', name='artists-artist-index')
