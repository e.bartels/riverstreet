from __future__ import unicode_literals

from django.apps import AppConfig


class ArtistsConfig(AppConfig):
    name = 'ado.examples.artists'
    icon = '<i class="icon material-icons">person_outline</i>'
