from __future__ import unicode_literals

from django.contrib import admin
from django import forms

from ado.examples.exhibitions.models import Exhibition
from ado.media.admin.inlines import RelatedImagesInline
from ado.customadmin.admin import BaseModelAdminMixin
from ado.customadmin.forms import SmartMultiSelect


class ExhibitionAdminForm(forms.ModelForm):
    class Meta:
        widgets = {
            'artists': SmartMultiSelect,
        }


class ExhibitionAdmin(BaseModelAdminMixin, admin.ModelAdmin):
    icon = '<i class="icon material-icons">date_range</i>'
    model = Exhibition
    list_display = ('image_column', 'title',
                    'start_date', 'end_date', 'published')
    list_filter = ('published', )
    date_hierarchy = 'start_date'
    form = ExhibitionAdminForm
    inlines = [RelatedImagesInline]
    html_editor_fields = ('text',)

    def get_image(self, obj):
        try:
            return obj.images.all()[:1][0].filename
        except IndexError:
            return None


admin.site.register(Exhibition, ExhibitionAdmin)
