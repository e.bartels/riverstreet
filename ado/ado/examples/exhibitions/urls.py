from django.conf.urls import url
from ado.examples.exhibitions import views
from ado import menus

urlpatterns = [
    url(r'^$', views.index, name='exhibitions-exhibition-index'),
    url(r'^current/$', views.current, name='exhibitions-exhibition-current'),
    url(r'^past/$', views.past, name='exhibitions-exhibition-past'),
    url(r'^upcoming/$', views.future, name='exhibitions-exhibition-future'),
    url(r'^view/(?P<slug>[\w-]+)/$', views.view, name='exhibitions-exhibition-view'),
]

menus.register_url('Exhibitions', name='exhibitions-exhibition-index')
