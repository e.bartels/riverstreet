from __future__ import unicode_literals

from django.apps import AppConfig


class ExhibitionsConfig(AppConfig):
    name = 'ado.examples.exhibitions'
    icon = '<i class="icon material-icons">date_range</i>'
