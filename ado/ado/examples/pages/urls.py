from django.conf.urls import url
from ado.examples.pages import views

urlpatterns = [
    url(r'^(?P<slug>[\w-]+)/$', views.view, name='pages-page-view'),
]
