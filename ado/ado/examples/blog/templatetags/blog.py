from __future__ import unicode_literals

from django import template
from django.contrib.auth.models import User

from ado.examples.blog.models import Entry, Link, Category


register = template.Library()


@register.inclusion_tag('blog/includes/archive_list.html')
def blog_archive_list():
    """
    An inclusion tag to create a archive menu of previous blog entries.
    """
    dates = Entry.objects.published().datetimes('pub_date', 'month', order='DESC')

    return {
        'dates': dates,
    }


@register.inclusion_tag('blog/includes/link_list.html')
def blog_link_list():
    """
    An inclusion tag to show list of blog links (blogroll).
    """
    links = Link.objects.all()

    return {
        'links': links,
    }


@register.inclusion_tag('blog/includes/category_list.html', takes_context=True)
def blog_category_list(context):
    """
    An inclusion tag to show list of blog entry categories.
    """
    categories = Category.objects.all()

    return {
        'categories': categories,
        'active_category': context['request'].GET.get('cat'),
    }


@register.inclusion_tag('blog/includes/latest.html')
def latest_blog_entries():
    """
    An inclusion tag to show a list of latest blog entries.
    """
    entries = Entry.objects.filter(published=True)[:5]
    return {
        'entries': entries,
    }


@register.inclusion_tag('blog/includes/author_list.html', takes_context=True)
def blog_author_list(context):
    """
    An inclusion tag to show a list of blog authors.
    """
    try:
        active_author = int(context['request'].GET.get('author', ''))
    except ValueError:
        active_author = None
    authors = User.objects.filter(entry__isnull=False).distinct()
    return {
        'authors': authors,
        'active_author': active_author,
    }
