from __future__ import unicode_literals

from django import forms
from django.contrib import admin
from django.contrib.auth.models import User

from ado.content.admin import ContentItemsInline
from ado.customadmin.admin import BaseModelAdminMixin, SortableAdminListMixin
from ado.customadmin.forms import SmartMultiSelect, SmartTagSelect
from ado.examples.blog.models import Entry, EntryContent, Category, Link


class EntryContentInline(ContentItemsInline):
    """Inline for blog entry contents (images/video/text)"""
    model = EntryContent
    verbose_name_plural = 'Entry Content'


class EntryAdminForm(forms.ModelForm):
    """Base Entry admin form"""
    class Meta:
        widgets = {
            'categories': SmartMultiSelect,
            'tags': SmartTagSelect,
        }


@admin.register(Entry)
class EntryAdmin(BaseModelAdminMixin, admin.ModelAdmin):
    icon = '<i class="icon material-icons">format_align_left</i>'
    list_display = ('title', 'pub_date', 'author',
                    'created', 'modified', 'published',)
    list_filter = ('categories', 'published', 'author', 'pub_date')
    list_select_related = True
    search_fields = ['title', 'tags__name', 'categories__name', 'content__text']
    date_hierarchy = 'pub_date'
    ordering = ('-pub_date',)
    form = EntryAdminForm
    fieldsets = (
        (None, {
            'fields': ('title', 'author', ('pub_date', 'published'))
        }),
        (None, {
            'fields': ('categories', 'tags',)
        }),
    )
    raw_id_fields = ['related_entries']
    inlines = [EntryContentInline]

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        # Override author field. If not a superuser, then only allow that user
        # to select themselves as author.
        if db_field.name == 'author':
            if request:
                if not request.user.is_superuser:
                    kwargs['queryset'] = User.objects.filter(
                                                        pk=request.user.pk)
                kwargs['empty_label'] = None
                kwargs['initial'] = request.user.pk
        return super(EntryAdmin, self).formfield_for_foreignkey(
                                        db_field, request, **kwargs)


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    icon = '<i class="icon material-icons">collections_bookmark</i>'
    ordering = ('name',)


@admin.register(Link)
class LinkAdmin(SortableAdminListMixin, admin.ModelAdmin):
    icon = '<i class="icon material-icons">link</i>'
    list_display = ('title', 'url', 'position',)
