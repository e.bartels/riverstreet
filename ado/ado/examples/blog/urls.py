from django.conf.urls import url

from ado.examples.blog import feeds
from ado.examples.blog import views
from ado import menus


urlpatterns = [
    url(r'^$', views.blog_index, name='blog-index'),

    # Main URLS
    url(r'^(?P<year>\d{4})/(?P<month>\w{3})/$', views.month_archive, name='blog-month-archive'),
    url(r'^(?P<year>\d{4})/(?P<month>\w{3})/(?P<day>\d{1,2})/$', views.day_archive, name='blog-day-archive'),
    url(r'^(?P<year>\d{4})/(?P<month>\w{3})/(?P<day>\d{1,2})/(?P<slug>[\w-]+)/$', views.entry_detail, name='blog-entry-view'),
    url(r'^(?P<year>\d{4})/(?P<month>\w{3})/(?P<slug>[\w-]+)/$', views.entry_month_detail),

    # Alternative URLS with number format for month
    url(r'^(?P<year>\d{4})/(?P<month>\d{2})/$', views.month_archive_alt),
    url(r'^(?P<year>\d{4})/(?P<month>\d{2})/(?P<day>\d{1,2})/$', views.day_archive_alt),
    url(r'^(?P<year>\d{4})/(?P<month>\d{2})/(?P<day>\d{1,2})/(?P<slug>[\w-]+)/$', views.entry_detail_alt),
    url(r'^(?P<year>\d{4})/(?P<month>\d{2})/(?P<slug>[\w-]+)/$', views.entry_month_detail_alt),

    # Feeds
    url(r'^feed/$', feeds.LatestEntriesFeed(), name='blog-feed'),
    url(r'^feed/rss/$', feeds.LatestEntriesFeed(), name='blog-feed-rss'),
    url(r'^feed/atom/$', feeds.AtomLatestEntriesFeed(), name='blog-feed-atom'),
]

menus.register_url('Blog', name='blog-index')
