from __future__ import unicode_literals

from django.apps import AppConfig


class BlogConfig(AppConfig):
    name = 'ado.examples.blog'
    icon = '<i class="icon material-icons">format_align_left</i>'
