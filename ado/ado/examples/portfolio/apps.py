from __future__ import unicode_literals

from django.apps import AppConfig


class PortfolioConfig(AppConfig):
    name = 'ado.examples.portfolio'
    icon = '<i class="icon material-icons">collections</i>'
