from __future__ import unicode_literals

from django.contrib import admin
from django import forms
from django.core.urlresolvers import reverse

from ado.examples.portfolio.models import Collection, CollectionCategory
from ado.media.admin.inlines import RelatedImagesInline
from ado.customadmin.admin import BaseModelAdminMixin, SortableAdminListMixin
from ado.customadmin.forms import SmartMultiSelect


class CollectionAdminForm(forms.ModelForm):
    class Meta:
        widgets = {
            'categories': SmartMultiSelect,
        }


class CollectionAdmin(BaseModelAdminMixin, admin.ModelAdmin):
    icon = '<i class="icon material-icons">collections</i>'
    model = Collection
    exclude = ('sort',)
    list_filter = ['published', 'categories']
    list_display = [
        'image_column',
        'title',
        'published',
        'date',
        'category_column',
    ]
    form = CollectionAdminForm
    html_editor_fields = ('text', )
    verbose_image_fk_fields = ['image']
    inlines = [RelatedImagesInline]

    def category_column(self, obj):
        categories = obj.categories.all()
        return u", ".join([u'<a href="{url}">{name}</a>'.format(**{
            'name': c.name,
            'url': reverse('admin:portfolio_collection_change', args=[c.id])
            }) for c in categories])
    category_column.short_description = 'Categories'
    category_column.allow_tags = True

    def get_image(self, obj):
        try:
            return obj.images.all()[:1][0].filename
        except IndexError:
            return None


class CollectionCategoryAdmin(SortableAdminListMixin, admin.ModelAdmin):
    icon = '<i class="icon material-icons">collections_bookmark</i>'
    model = CollectionCategory
    sortable_position_field = 'sort'


admin.site.register(Collection, CollectionAdmin)
admin.site.register(CollectionCategory, CollectionCategoryAdmin)
