from django.conf.urls import url
from ado.examples.portfolio import views
from ado import menus

urlpatterns = [
    url(r'^$', views.index, name='portfolio-collection-index'),
    url(r'^(?P<slug>[\w-]+)/$', views.view, name='portfolio-collection-view'),
]

menus.register_url('Portfolio', name='portfolio-collection-index')
