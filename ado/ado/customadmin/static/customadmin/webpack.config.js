/**
 * Base webpack config
 */
const path = require('path')
const autoprefixer = require('autoprefixer')
const BundleTracker = require('webpack-bundle-tracker')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin


const config = (env = {}) => ({
  mode: env.production ? 'production' : 'development',
  context: path.resolve(__dirname),

  entry: {
    main: [
      'whatwg-fetch',
      './index.js',
    ],
    smartselect: './components/widgets/smartselect/index.js',
    sortablelist: './components/sortablelist/index.js',
    menus: './components/menus/index.js',
    media_changelist: './components/media/changelist/index.js',
    media_widgets: './components/media/widgets/index.js',
    media_inlines: './components/media/inlines/index.js',
    media_upload: './components/media/upload/index.js',
    content_inlines: './components/content/inlines/index.js',
  },

  output: {
    path: path.resolve('./bundle'),
    filename: `customadmin.[name]${env.production ? '.[contenthash:8]' : ''}.js`,
  },

  resolve: {
    extensions: ['.js', '.jsx', '.json'],
    // Allow app imports from '~/some/module'
    alias: {
      '~': path.resolve('./'),
    },
  },

  module: {
    // Loaders rules for different file types
    rules: [
      {
        test: /react-virtualized/,
        use: {
          loader: 'null-loader',
        },
      },
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            cacheDirectory: true,
          },
        },
      },
      {
        test: /\.s?css$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              sourceMap: !env.production,
              minimize: env.production,
            },
          },
          {
            loader: 'postcss-loader',
            options: {
              sourceMap: !env.production,
              plugins: [
                autoprefixer({
                  remove: false,
                }),
              ],
            },
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: !env.production,
              includePaths: [path.resolve('./styles/')],
            },
          },
        ],
      },
      {
        test: /\.(png|gif|jpg|jpeg|svg)?$/,
        use: 'url-loader',
      },
      {
        test: /\.(ttf|eot|woff|woff2)(\?\S*)?$/,
        use: !env.production ? 'url-loader' : {
          loader: 'file-loader',
          options: {
            name: 'fonts/[hash].[ext]',
          },
        },
      },
    ],
  },

  plugins: [
    // Outputs stats on generated files (used by django-webpack-loader)
    new BundleTracker({ filename: './bundle/webpack-stats.json' }),

    new MiniCssExtractPlugin({
      filename: `customadmin.[name]${env.production ? '.[contenthash:8]' : ''}.css`,
    }),

    // production only
    ...(env.production ? [
      // new BundleAnalyzerPlugin({ analyzerMode: 'server' }),
    ] : []),

    // dev & production, but not for dev-server
    ...(!env.devServer ? [
      new CleanWebpackPlugin(['./bundle'], {
        exclude: ['webpack-stats.json', 'editor_content.css'],
        beforeEmit: true,
      }),
    ] : []),
  ],

  optimization: {
    hashedModuleIds: env.production,
    runtimeChunk: 'single',
    splitChunks: {
      cacheGroups: {
        // commons chunk for shared code across entrypoints
        commons: {
          name: 'commons',
          chunks: 'initial',
          minChunks: 2,
          test: module => (
            // module.context.indexOf('/node_modules/') !== -1 &&
            !/\.s?css$/.test(module.nameForCondition && module.nameForCondition())
          ),
        },
      },
    },
  },

  ...(!env.production ? {
    devtool: 'inline-cheap-module-source-map',
  } : {}),
})

module.exports = config
