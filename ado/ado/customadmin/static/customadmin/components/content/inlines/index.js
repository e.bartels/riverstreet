/**
 * Functionality for ContentItem admin inlines.
 */
import ContentItemInline from './ContentItemInline'

ContentItemInline.init()
