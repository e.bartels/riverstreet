/* eslint no-param-reassign: off */
import { jQuery as $ } from '~/lib/jQuery'

import './MediaItemChangeList.scss'


/**
 * Handle setup & functionality for media popup
 */
const MediaItemChangeList = () => {
  // Must have opener (be a popup window)
  if (!window.opener || !window.opener.dismissPopup) {
    return
  }

  // Must be in media app changelist for images or files
  const classList = document.body.classList
  if (!classList.contains('change-list') ||
      !classList.contains('app-media') ||
     (!classList.contains('model-image') &&
      !classList.contains('model-file') &&
      !classList.contains('model-video') &&
      !classList.contains('model-mediaitem'))) {
    return
  }


  // Get changelist table elements
  const table = document.getElementById('result_list')
  const thead = table.querySelector('thead')
  const tbody = table.querySelector('tbody')

  // Set up selectAll checkbox
  const selectAll = $(`
  <th scope="col" class="action-checkbox-column">
    <div class="text"><span><input type="checkbox" id="action-toggle"></span></div>
    <div class="clear"></div>
  </th>
  `).prependTo(thead.querySelector('tr')).find('input')[0]

  // Set up row checkboxes
  const rowCheckboxTemplate = `
  <td class="action-checkbox">
    <input class="action-select" name="_selected_action" type="checkbox">
  </td>`
  const checkboxes = []
  Array.from(tbody.querySelectorAll('tr')).forEach(tr => {
    const checkbox = $(rowCheckboxTemplate).prependTo(tr).find('input')[0]
    checkboxes.push(checkbox)
  })

  // Set up submit row
  const submitRow = $(`
  <div class="submit-row select-row">
    <input type="submit" value="Select" class="default" />
  </div>
  `).insertAfter(table).css('display', 'none')[0]

  // Select a checkbox row
  const selectRow = checkbox => {
    checkbox.checked = true
    $(checkbox).closest('tr').addClass('selected')
    if (checkboxes.some(c => c.checked)) {
      submitRow.style.display = ''
    }
  }

  // Deselect a checkbox row
  const deselectRow = checkbox => {
    checkbox.checked = false
    $(checkbox).closest('tr').removeClass('selected')
    if (!checkboxes.some(c => c.checked)) {
      submitRow.style.display = 'none'
    }
  }

  // Checkbox event handlers
  checkboxes.forEach(checkbox => {
    $(checkbox).on('click', e => {
      const checked = $(checkbox).is(':checked')

      // Handle shift key to select a range
      if (e.shiftKey) {
        const [index1, index2] = [
          checkboxes.findIndex(c => c !== checkbox && c.checked),
          checkboxes.indexOf(checkbox),
        ].sort()
        checkboxes.slice(index1, index2).forEach(c => {
          if (checked) {
            selectRow(c)
          }
          else {
            deselectRow(c)
          }
        })
      }

      // No shift key (single checkbox)
      if (checked) {
        selectRow(checkbox)
      }
      else {
        deselectRow(checkbox)
      }
    })
  })

  // selectAll checkbox event handler
  $(selectAll).on('click', () => {
    const checked = $(selectAll).is(':checked')
    checkboxes.forEach(checkbox => {
      if (checked) {
        selectRow(checkbox)
      }
      else {
        deselectRow(checkbox)
      }
    })
  })

  // Changelist link clicks
  $(tbody).find('td a, th a').parent().on('click', e => {
    e.preventDefault()
    e.stopPropagation()
    e.stopImmediatePropagation()
    const link = $(e.currentTarget).find('a')[0]
    const id = link.dataset.popupOpener
    window.opener.dismissPopup([id])
  })

  // Submit event handler
  $(submitRow).on('click', e => {
    e.preventDefault()
    const selected = checkboxes.filter(c => c.checked)
    const ids = selected.map(checkbox => {
      const link = $(checkbox).closest('tr').find('a[href]')[0]
      return link.dataset.popupOpener
    })

    window.opener.dismissPopup(ids)
  })
}

export default {
  init: MediaItemChangeList,
}
