import React from 'react'
import PropTypes from 'prop-types'

export const Icon = ({ children = null, className = 'icon material-icons' }) => (
  <i className={`${className}`}>{children}</i>
)

Icon.propTypes = {
  children: PropTypes.string,
  className: PropTypes.string,
}

export default Icon
