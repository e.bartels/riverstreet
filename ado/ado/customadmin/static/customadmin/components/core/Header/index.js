
const init = () => {
  // Get #header
  const el = document.getElementById('header')

  if (el) {
    // Set 'View site' link to open a new tab/window
    const viewSiteLink = el.querySelector('a[href="/"]')
    if (viewSiteLink) {
      viewSiteLink.setAttribute('target', '_blank')
    }
  }
}

export default {
  init,
}
