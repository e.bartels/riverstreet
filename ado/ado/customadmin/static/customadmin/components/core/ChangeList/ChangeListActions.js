/* eslint no-param-reassign: off */

/**
 * Customize appearance of changelist actions select
 */
export const ChangeListActions = () => {
  const actionsEl = document.querySelector('#changelist-form .actions')
  if (!actionsEl) {
    return
  }

  // Remove label text
  const label = actionsEl.querySelector('label')
  Array.from(label.childNodes)
    .filter(node => node.nodeType === 3)
    .forEach(node => {
      node.nodeValue = ''
    })

  // Better select first option value
  const select = actionsEl.querySelector('select')
  select.children[0].textContent = 'Actions'
  select.children[0].setAttribute('disabled', true)
}

export default ChangeListActions
