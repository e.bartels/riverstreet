/**
 * Customize appearance of changelist search
 */
const replaceSearchButton = (el) => {
  const input = el.querySelector('input#searchbar')
  input.removeAttribute('autofocus')

  // Add placeholder
  input.placeholder = 'Search'

  // Add submit <button />
  const submitButton = document.createElement('button')
  submitButton.classList.add('submit-button')
  submitButton.innerHTML = '<i class="material-icons icon">search</i>'
  input.parentNode.insertBefore(submitButton, input.nextSibling)

  // remove old submit <input />
  const submitInput = el.querySelector('input[type="submit"]')
  submitInput.parentNode.removeChild(submitInput)
}

const changeListSearch = (selector) => {
  const el = document.querySelector(selector)
  if (el) {
    replaceSearchButton(el)
  }
}

export const ChangeListSearch = (opt = {}) => {
  // options
  const {
    selector = '#changelist #toolbar',
  } = opt

  changeListSearch(selector)
}

export default ChangeListSearch
