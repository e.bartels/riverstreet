/**
 * This file pulls in styles and javascript components for customized admin
 * theme and UI. It should be built using webpack.
 */

// Import custom admin styles
import '~/styles/base.scss'
import domReady from '~/lib/domReady'

import SidebarNav from './components/core/SidebarNav'
import ChangeList from './components/core/ChangeList'
import ChangeForm from './components/core/ChangeForm'
import Header from './components/core/Header'
import Breadcrumbs from './components/core/Breadcrumbs'
import Popup from './components/core/Popup'

// Components
SidebarNav.init()
Header.init()
Breadcrumbs.init()
ChangeList.init()
ChangeForm.init()
Popup.init()

domReady(() => {
  document.body.classList.add('ready')
})
