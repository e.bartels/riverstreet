from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^fetch_url', views.fetch_url),
]
