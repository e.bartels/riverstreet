Introduction
============

The `ado` package provides Django admin customizations to provide CMS-like functionality. It includes a media library, a menu system, and a content module api. It layers on top of the built-in Django admin and adds its own styling and javascript code to extend functionality.

**Dependencies:**

* Python 3.x
* Django 1.11.x

Media Library
=============

The `ado.media` package provides a media library for of images, files and videos (supporting Youtube and Vimeo). It stores media in a central media
library, which other apps can tap into. An app requiring images, for example, can link into the media library using a foreign key instead of adding its own file upload field.

Models:

**`media.models.MediaItem`**

A generic base class for for other media types. The media app uses Django's
model inheritance. THe `MediaItem` model can be used to query for all media.

    MediaItem.objects.all()


**`media.models.Image`**

An image type. This model will most likely be used by your Django apps the most. It allows uploading and linking to images.


**`media.models.Video`**

Allows adding videos from either Youtube or Vimeo. Unlike the other models, no file is stored with the server. Instead the `Video` model provides the information to embed external videos. Currently Youtube & Vimeo are
supported. A Video is created in the admin by entering a youtube or vimeo url. The admin will then retrieve information about the video using `oembed` and store that information in the database.

**`media.models.File`**

A generic file model. Any generic file (other than images) can be uploaded into the media library.


The media library should be used any time a one of these media types is
required in another Django app. 

An example gallery site might have an Artist model. Each artist
has a representative image to display on the website. We might have an `Artist` model like so:


    class Artist(models.Model):
        name = models.CharField(max_length=255)
        image = models.ForeignKey('media.Image')

Here we use a Django foreign key to link to the media.Image model.

**`media.models.RelatedImagesField`**

This model field can be used to provide a set of images items for a related model.

**`media.models.RelatedVideosField`**  
**`media.models.RelatedFilesField`**  

These work the same as `RelatedImagesField` but for the `Video` or `File` models.

We might extend our artist model to add multiple images like this:

    from ado.media.fields.related import RelatedImagesField, RelatedVideosField

    class Artist(models.Model):
        name = models.CharField(max_length=255)
        image = models.ForeignKey('media.Image')
        images = RelatedImagesField()
        videos = RelatedVideosField()

We can now attach multiple images to our Artist model.


**`media.fields.related.RelatedMediaField`**

This field will allow attaching any generic media item from the media library. Usually you only want to attach images, or videos. But in some circumstances, you might want to attach both images and videos to your model. e.g.:

    class Artist(models.Model):
        ...
        media = RelatedMediaField()


Under the hood, these fields create a generic many-to-many relation and can be queried using Django's ORM similarly as for a ManyToManyField. For example

    artist = Artist.objects.all()[0]
    print artist.images.all()
    [<Image: Image: Spelterini Al Ashraf (Spelterini_Al_Ashraf.jpg)>,
     <Image: Image: Hieroglyphs (Hieroglyphs.jpg)>, ...]

    artist.images.count()
    10

    artist.images.add(image)

    artist.images.clear()
    artist.images.count()
    0


**`ado.media.admin.options.RelatedFilesInline`**  
**`ado.media.admin.options.RelatedImagesInline`**  
**`ado.media.admin.options.RelatedVideosInline`**  

The media app provides admin Inline classes to work with the above `Related*` fields.

Example of adding multiple images in the admin using inlines:
    

    from ado.media.admin.inlines import RelatedImagesInline

    class ArtistImagesInline(RelatedImagesInline):
        verbose_name_plural = 'Artist Images'
        verbose_name = 'Artist Image'


    @admin.register(Artist)
    class ArtistAdmin(admin.ModelAdmin):
        ...
        inlines = [CollectionImagesInline]


Content app
===========

The `ado.content` app provides an abstract Django model useful for attaching content to another model. It allows attaching text, image, or video content, and provides a custom admin interface for adding and sorting these content items.

**`ado.content.models.ContentItem`**  
Provides an abstract model to create content instances


**`ado.content.admin.ContentsInline`**  
Provides an admin inline interface for adding/removing/sorting content items.

See: [ado/examples/blog/models.py](ado/examples/blog/models.py) and [ado/examples/blog/admin.py](ado/examples/blog/admin.py) for example usage.


Admin Customizations
====================

All admin customizations are provided in the `ado.customadmin` Django app. This includes templates, stylesheets and javascript which enhance the built-in Django admin functionality.

**`ado.customadmin.admin.BaseModelAdminMixin`**

This class can be used to extend the normal Django `ModelAdmin` class. It can provide features like a CKEditor (https://ckeditor.com/) field for extending TextField with WYSIWYG functionality. It will also let you specify which image or file fields should be customized.

Example:

    from django.contrib import admin
    from ado.customadmin.admin import BaseModelAdminMixin
    ...

    @admin.register(Entry)
    class EntryAdmin(BaseModelAdminMixin, admin.ModelAdmin):
        icon = '<i class="icon material-icons">format_align_left</i>'
        html_editor_fields = ['excerpt', 'text']  # give CKEditor to 'excerpt' and 'text' model fields
        verbose_image_fk_fields = ['image']  # give verbose image formfield to 'image' model field
        ...


**`ado.customadmin.admin.SortableAdminListMixin`**

This admin mixin can provide an interface to sort model instances that use a `PositionField`.

    from django.contrib import admin
    from ado.customadmin.admin import SortableAdminListMixin

    @admin.register(Link)
    class LinkAdmin(SortableAdminListMixin, admin.ModelAdmin):
        icon = '<i class="icon material-icons">link</i>'
        ...

 The `admin.py` files in the example apps (see below) can be looked at to understand usage for these classes.

Example Apps
============

A number of example apps are provided in the `ado.examples` package. You can view these apps to see the full range of what you can do with the `ado` package.
Contains a few example apps. In particular, you should look in each app's `models.py` file to see how the `ado.media` package is used, as well as each app's `admin.py` file to see how to extend the Django admin with specific features that are provided by `ado.customadmin`.

**ado.examples.blog**

**ado.examples.portfolio**

**ado.examples.artists**

**ado.examples.exhibitions**


Testing
=======

A full set of tests are provided using the Django's test runner. A task is provided using `invoke` (http://www.pyinvoke.org/). To run tests:

    invoke test

You can also run in watch mode:

    invoke test -w

See `tasks.py` in the top-level of this repository for more info.
