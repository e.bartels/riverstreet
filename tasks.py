"""
Tasks for build, deploy, and development
"""
import os
import importlib
from invoke import task, Collection
from termcolor import colored

import riverstreet
project_path = os.path.dirname(riverstreet.__file__)

# docker registry
# NOTE: This is an example. Set this to your own docker registry URL where you
# want to "docker push" to.
DOCKER_REGISTRY = "registry.gitlab.com/USERNAME/riverstreet"


# Colors
green = lambda s: colored(s, 'green', attrs=['bold']) # noqa
red = lambda s: colored(s, 'red', attrs=['bold']) # noqa
yellow = lambda s: colored(s, 'yellow', attrs=['bold']) # noqa


# Utils
def get_git_hash(c):
    """
    Get the current hash for the git HEAD
    """
    result = c.run('git rev-parse --short HEAD', hide=True)
    return result.stdout.strip()


def make_release_tag(c):
    hash_ref = get_git_hash(c)
    return f'release-{hash_ref}'


def _generate_secret_key():
    from django.utils.crypto import get_random_string
    chars = 'abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)'
    secret_key = get_random_string(50, chars)
    return secret_key


@task
def generate_secret_key(c):
    """
    Print a random string for use in django SECRET_KEY setting
    """
    print(_generate_secret_key())


@task
def add_superuser(c, username, pw):
    import django
    from django.apps import apps
    django.setup()
    auth_app = apps.get_app_config('auth')
    User = auth_app.get_model('User')

    try:
        User.objects.get(username=username)
        print(yellow(f"User '{username}' already exists"))
    except User.DoesNotExist:
        user = User(
            username=username,
            is_staff=True,
            is_superuser=True,
            is_active=True,
            email='')
        user.set_password(pw)
        user.save()


def load_initial_data(c):
    """
    Loads test data into the database for development
    """
    add_superuser(c, 'admin', 'admin')

    path = os.path.join(project_path, 'local', 'initial_data.json')
    if os.path.exists(path):
        c.run('manage.py loaddata', '-i', path)


@task
def bootstrap(c):
    """
    Initializes the django project for development
    """
    if not os.path.exists(os.path.join(project_path, '..', '.env')):
        print(green('\nCreate .env file'))
        c.run(f'cp {project_path}/../config/env.template {project_path}/../.env')

    print(green('\nSyncing database & running migrations:'))
    c.run('./manage.py migrate')

    print(green('\nLoading initial data:'))
    load_initial_data(c)

    with c.cd(project_path):
        node_modules_path = os.path.join(project_path, '..', 'node_modules')
        if not os.path.exists(node_modules_path):
            print(green('\nInstalling npm modules (npm install):'))
            c.run('npm ci')

    print(green('\nDone!\n'))
    print(green('Next steps:'))
    print('1) To run the django dev server: ./manage.py runserver')
    print('2) To run webpack dev server: npm start')
    print('3) visit http://localhost:8000/')


# Build & Deploy
@task(help={
    'tag': 'tag for docker image',
})
def docker_build(c, tag=None):
    """
    Run docker build
    """
    print(green('\nRunning docker build\n'))

    tag = tag or make_release_tag(c)
    c.run('docker build -t {url}:latest -t {url}:{tag} .'.format(
        url=DOCKER_REGISTRY,
        tag=tag,
    ), pty=True)

    return tag


@task
def docker_push(c, tag):
    """
    Run docker push
    """
    print(green(f'\nRunning docker push: {DOCKER_REGISTRY}:{tag}\n'))
    c.run(f'docker push {DOCKER_REGISTRY}:{tag}', pty=True)
    c.run(f'docker push {DOCKER_REGISTRY}:latest', pty=True)


# Testing
@task(help={
    'module': 'python module to find tests',
    'settings': 'django settings module',
    'watch': 'run tests when code changes',
    'opts': 'options to pass to django tests',
})
def test(c,
         module='riverstreet',
         settings='riverstreet.config.tests',
         watch=False,
         opts='--traceback --failfast --nomigration'):
    """
    Run django tests
    """
    path = os.path.realpath(os.path.join(project_path, '..'))
    test_command = f'{path}/manage.py test --settings={settings} {module} {opts}'

    c.run(test_command, warn=True, echo=True, pty=True)
    if watch:
        module_to_test = importlib.import_module(module.split('.')[0])
        inot_paths = set([
            os.path.dirname(module_to_test.__file__),
            project_path,
        ])
        inot_path_arg = ' '.join(list(inot_paths))
        inot_command = (
            'while true; do '  # noqa
            'inotifywait -qr --exclude \'.*[^\.py]$\' '  # noqa
            '-e modify,attrib,close_write,move,create,delete '
            f'{inot_path_arg} && {test_command}; done;')

        c.run(inot_command, warn=True, pty=True)


# docker namespace
docker = Collection('docker')
docker.add_task(docker_build, name='build')
docker.add_task(docker_push, name='push')

# dev namespace
dev = Collection('dev')
dev.add_task(bootstrap)
dev.add_task(generate_secret_key, name='secret-key')

# root namespace
ns = Collection(
    test,

    # namespaces
    dev,
    docker,
)
