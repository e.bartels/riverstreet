Static Assets
============

This folder contains static assets that are served by the python server.

During development, you an find these with prefix `http://localhost:8000/s/`

In production, they are served from `https://www.riverstreet.net/s/`

The [bundle/](bundle/) directory contains the output from the webpack build, which contains all .js and .css files for the front-end application.
