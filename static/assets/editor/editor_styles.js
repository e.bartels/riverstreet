CKEDITOR.addStylesSet('admin_styles',
[
  // Block Styles

  // Inline Styless
  //{name: 'small', element: 'span', attributes: {'class': 'small'}},
  // {name: 'muted', element: 'span', attributes: {'class': 'muted'}},
  // {name: 'quiet', element: 'span', attributes: {'class': 'quiet'}},
  {name: 'Heading', element: 'h2', attributes: {'class': 'heading'}},
  {name: 'Heading 2', element: 'h2', attributes: {'class': 'heading-medium'}},
  {name: 'Heading 3', element: 'h2', attributes: {'class': 'heading-large'}},
  {name: 'Heading 4', element: 'h2', attributes: {'class': 'heading-larger'}},

  {name: 'standout', element: 'span', attributes: {'class': 'standout'}},
  {name: 'large', element: 'span', attributes: {'class': 'large'}},
  {name: 'small', element: 'span', attributes: {'class': 'small'}},
]);
