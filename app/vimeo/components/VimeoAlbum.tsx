/* @flow */
import * as React from 'react'
import ColorTheme from '~/ui/components/ColorTheme'
import Markup from '~/ui/components/Markup'

import './VimeoAlbum.scss'

type Props = {
  album: any,
}

export const VimeoAlbum = ({ album }: Props) => (
  <ColorTheme theme="black-blue">
    <div className="VimeoAlbum">
      <h1 className="VimeoAlbum__title">{album.name}</h1>

      {album.videos.map(video => (
        <div className="VimeoAlbum__video" key={video.id}>
          <Markup>{video.embed}</Markup>
          <div className="VimeoAlbum__video__name">{video.name}</div>
        </div>
      ))}
    </div>
  </ColorTheme>
)

export default VimeoAlbum
