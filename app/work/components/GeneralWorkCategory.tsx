import * as React from 'react'
import { Link } from 'react-router-dom'
import CategoryView from '~/projects/components/CategoryView'
import { Category } from '~/types'

import './GeneralWorkCategory.scss'

export type Props = {
  category: Category,
}

export const GeneralWorkCategory = ({ category }: Props) => (
  <div className="GeneralWorkCategory">
    <CategoryView category={category} />

    <p className="GeneralWorkCategory__view-more">
      <Link to="/work/category/" className="button">View more work</Link>
    </p>
  </div>
)

export default GeneralWorkCategory
