import 'normalize.css'
import '~/styles/index.scss'

import { hot } from 'react-hot-loader/root'
import * as React from 'react'
import { Provider } from 'react-redux'
import { Store } from 'redux'
import { ApolloProvider } from '@apollo/react-hooks'
import { ApolloClient } from 'apollo-boost'
import { BrowserRouter as Router } from 'react-router-dom'
import { HelmetProvider, Helmet } from 'react-helmet-async'

import ErrorBoundary from '~/core/components/ErrorBoundary'
import GAListener from '~/analytics/ga/GAListener'
import RouteHistoryWatcher from '~/routes/components/RouteHistoryWatcher'
import ViewportListener from '~/ui/components/ViewportListener'
import DetectTouch from '~/ui/components/DetectTouch'
import ScrollBehavior from '~/ui/components/ScrollBehavior'
import MainLayout from '~/core/components/MainLayout'
import routes from '~/routes'
import { DOCUMENT_TITLE } from '~/core/constants'

type Props = {
  store: Store<any>,
  apolloClient: ApolloClient<any>,
}

export const App = ({ store, apolloClient }: Props) => (
  <Provider store={store}>
    <ApolloProvider client={apolloClient}>
      <HelmetProvider>
        <Helmet defaultTitle={DOCUMENT_TITLE} titleTemplate={`%s | ${DOCUMENT_TITLE}`} />
        <Router>
          <ErrorBoundary>
            <GAListener />
            <RouteHistoryWatcher />
            <ViewportListener />
            <DetectTouch />
            <ScrollBehavior>
              <MainLayout>
                {routes}
              </MainLayout>
            </ScrollBehavior>
          </ErrorBoundary>
        </Router>
      </HelmetProvider>
    </ApolloProvider>
  </Provider>
)

export default hot(App)
