declare module '*.svg'
declare module '*.graphql'
declare module '*.json'

declare interface Window {
  visualViewport?: {
    readonly width: number,
    readonly height: number,
    readonly scale: number,
    readonly offsetleft: number,
    readonly offsettop: number,
    readonly pageleft: number,
    readonly pagetop: number,
  },
}

declare module 'scroll-to-element' {
  const content: (selector: string | HTMLElement, opts: {
    offset?: number,
    align?: 'top' | 'middle' | 'bottom',
    ease?: string,
    duration?: number,
  }) => void
  export default content
}
