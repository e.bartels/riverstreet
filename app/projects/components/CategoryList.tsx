import * as React from 'react'
import { useState, useEffect, useRef } from 'react'
import { withRouter, RouteComponentProps } from 'react-router'
import { NavLink } from 'react-router-dom'
import scrollTo from 'scroll-to-element'
import ScrollNav from '~/ui/components/ScrollNav'
import CategoryView from '~/projects/components/CategoryView'
import { Category, ListProject } from '~/types'

import './CategoryList.scss'

type Props = {
  categories: CategoryWithProjects[],
} & RouteComponentProps<{ slug: string, category_slug: string }, any, any>

interface CategoryWithProjects extends Category {
  projects: ListProject[],
}

export const CategoryList = ({ categories, history, match, location }: Props) => {
  const elRef = useRef<HTMLDivElement>(null)

  // Scroll spy - state
  const [visibleItems, setVisibleItems] = useState<IntersectionObserverEntry[]>([])

  // Scroll spy - select active item
  useEffect(() => {
    const activeItem = visibleItems
      .sort((a, b) => (
        a.intersectionRatio < b.intersectionRatio ? 1 : -1
      ))[0]
    if (activeItem) {
      const url = `/promotion/${match.params.slug}/category/${activeItem.target.id}`
      history.replace(url, { noAutoScroll: true, scrolled: true })
    }
  }, [visibleItems])

  // Scroll to category when location path changes
  useEffect(() => {
    // Ignore if 'scrolled' state exists (means we just updated via scroll observer)
    const { state } = location
    if (state && (state.scrolled || state.modal)) {
      return
    }

    // Find element and scroll to it
    const element = document.getElementById(match.params.category_slug)
    if (element) {
      scrollTo(element, {
        duration: 650,
        ease: 'in-out-quad',
      })
    }
  }, [location.pathname, (location.state || {}).scrolled])

  // Set up on mount
  useEffect(() => {
    // Scroll to items matched by slug param
    scrollTo(`#${match.params.category_slug}`, {
      duration: 0.001,
    })

    // This requires IntersectionObserver api
    const el = elRef.current
    if (!el || !IntersectionObserver) return

    // Use observer to detect visible categories
    const observer = new IntersectionObserver((entries) => {
      const itemsToAdd = entries.filter(e => e.isIntersecting)
      const itemsToRemove = entries.filter(e => !e.isIntersecting)

      setVisibleItems(items => {
        const map = new Map(items.map(i => ([i.target.id, i])))
        itemsToRemove.forEach(i => { map.delete(i.target.id) })
        itemsToAdd.forEach(i => { map.set(i.target.id, i) })
        return [...map.values()]
      })
    }, {
      threshold: [0, 0.25, 0.5, 0.7, 1],
    })

    const categoryEls = [...el.querySelectorAll('.CategoryList__category')]
    categoryEls.forEach(cEl => {
      observer.observe(cEl)
    })

    // umount
    return () => { observer.disconnect() }  // eslint-disable-line
  }, [])

  return (
    <div className="CategoryList" ref={elRef}>
      <ScrollNav>
        {categories.map((category, i) => (
          <NavLink
            key={category.slug}
            to={{
              pathname: `/promotion/${match.params.slug}/category/${category.slug}`,
              state: { noAutoScroll: true },
            }}
            title={category.title}
          />
        ))}
      </ScrollNav>

      {categories.map(category => (
        <div
          className="CategoryList__category"
          id={category.slug}
          key={category.slug}
        >
          <CategoryView
            category={category}
          />
        </div>
      ))}
    </div>
  )
}

export default withRouter(CategoryList)
