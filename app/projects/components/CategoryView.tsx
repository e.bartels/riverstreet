import * as React from 'react'
import { default as cx } from 'classnames'
import IntroSection from '~/ui/components/IntroSection'
import Image from '~/ui/components/Image'
import Markup from '~/ui/components/Markup'
import ProjectsList from './ProjectsList'

import { Category } from '~/types'

import './CategoryView.scss'

type Props = {
  category: Category,
}

export const CategoryView = ({ category }: Props) => (
  <div className={cx('CategoryView', {
    'CategoryView--no-intro': !category.introImage && !category.introText,
  })}>
    {category.introImage || category.introText ? (
      <div className="CategoryView__intro">
        <IntroSection className="CategoryView__IntroSection" showDownArrow={false}>
          {category.introImage ? (
            <div className="CategoryView__intro-image">
              <Image image={category.introImage} />
            </div>
          ) : null}
          {category.introText ? (
            <div className="CategoryView__intro-caption">
              <Markup>
                {category.introText}
              </Markup>
            </div>
          ) : null}

        </IntroSection>
      </div>
    ) : null}

    <ProjectsList projects={category.projects} />
  </div>
)

export default CategoryView
