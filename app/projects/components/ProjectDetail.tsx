/* eslint-disable react/no-array-index-key */
import * as React from 'react'
import { default as cx } from 'classnames'
import { useSelector } from 'react-redux'
import { withRouter, RouteComponentProps } from 'react-router'
import { Link, Route } from 'react-router-dom'
import TextContentItem from '~/core/components/TextContentItem'
import Image from '~/ui/components/Image'
import Icon from '~/ui/components/Icon'
import Modal from '~/ui/components/Modal'
import MediaView from '~/ui/components/MediaView'
import ColorTheme from '~/ui/components/ColorTheme'
import CloseButton from '~/ui/components/CloseButton'
import { getRouteHistory } from '~/routes/selectors'
import { getGroupedContent } from '~/core/selectors'
import { getProjectMedia } from '~/projects/selectors'

import { Project } from '~/types'

import './ProjectDetail.scss'


export type Props = {
  project: Pick<Project, 'title' | 'slug' | 'content'>,
} & RouteComponentProps


export const ProjectDetail = ({ project, location, history, match }: Props) => {
  // Get route history (for conditionally showing back button)
  const routeHistory = useSelector(getRouteHistory)
  const showBackButton = routeHistory.length > 1

  // Get project content, grouped by media
  const content = getGroupedContent(project.content)
  const mediaItems = getProjectMedia(project)

  const getMediaPath = (mediaitem) => (
    mediaitem.type === 'image'
      ? `${match.url}/media/${mediaitem.image.id}`
      : `${match.url}/media/${mediaitem.video.id}`
  )

  return (
    <>
      <div className="ProjectDetail">
        {showBackButton ? (
          <CloseButton
            className="ProjectDetail__close"
            onClick={() => { history.goBack() }}
          />
        ) : null}

        <ColorTheme theme="black-blue">
          <div className="ProjectDetail__main">
            {content.map((item, i) => (
              <section
                key={`content-${item.id}`}
                className={cx(
                  'ProjectDetail__content-item',
                  `ProjectDetail__content-item--${item.type}`,
                )}
              >
                {item.type === 'text' ? <TextContentItem item={item} /> : null }
                {item.type === 'mediagroup' ? (
                  item.items.map(mediaitem => (
                    <Link
                      to={{
                        pathname: getMediaPath(mediaitem),
                        search: mediaitem.type === 'video' ? 'play' : '',
                        state: { modal: true },
                      }}
                      key={mediaitem.id}
                      className={`media-link media-link--${mediaitem.type}`}
                    >
                      {mediaitem.type === 'image' ? <Image image={mediaitem.image} alt="image" /> : null}
                      {mediaitem.type === 'video' ? (
                        <>
                          <Image image={mediaitem.video} alt="video" />
                          <Icon name="play" />
                        </>
                      ) : null}
                    </Link>
                  ))
                ) : null}
              </section>
            ))}
          </div>
        </ColorTheme>
      </div>

      <Route
        path={`${match.path}/media/:media_id`}
        exact
        render={(routeProps) => {
          const onClose = () => {
            const path = routeProps.match.url.replace(/\/media.+$/, '')
            const previousRoute = routeHistory.slice(1)[0]
            if (previousRoute && previousRoute.pathname === path) {
              history.goBack()
            }
            else {
              history.push(path)
            }
          }

          return (
            <Modal onClose={onClose} className="MediaViewModal">
              <MediaView
                items={mediaItems}
                activeId={routeProps.match.params.media_id}
                onShowItem={(itemId) => {
                  const url = routeProps.match.url.replace(/\d+$/, itemId)
                  history.replace(url, { modal: true })
                }}
                onClose={onClose}
              />
            </Modal>
          )
        }}
      />
    </>
  )
}

export default withRouter(ProjectDetail)
