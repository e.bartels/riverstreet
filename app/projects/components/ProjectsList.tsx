import * as React from 'react'
import { join } from 'path'
import { withRouter, RouteComponentProps } from 'react-router'
import { Link } from 'react-router-dom'
import ColorTheme from '~/ui/components/ColorTheme'
import Image from '~/ui/components/Image'
import { ListProject } from '~/types'

import './ProjectsList.scss'

type Props = {
  projects: ListProject[],
} & RouteComponentProps


export const ProjectsList = ({ projects, history, match }: Props) => {
  // Returns path to the project detail page
  const getProjectDetailPath = project => {
    const basePath = match.url.startsWith('/work') ? '/work' : '/promotion'
    return `${basePath}/project/${project.slug}`
  }

  // Returns path to project video page
  const getProjectVideoPath = project => (
    join(match.url, `projectvideo/${project.slug}`)
  )

  // Get 'to' param for project Link, which is different depending on whether a
  // project has a video or not.
  const getProjectLinkTo = (project) => {
    if (!project.video) return getProjectDetailPath(project)

    return {
      pathname: getProjectVideoPath(project),
      search: 'play',
      state: { scrolled: true, modal: true },
    }
  }

  return (
    <ColorTheme theme="black-blue">
      <div className="ProjectsList">
        {projects.map(project => (
          <React.Fragment key={project.slug}>
            <Link
              className="ProjectsList__project"
              to={getProjectLinkTo(project)}
            >
              <div className="ProjectsList__project__image">
                {project.image ? (
                  <Image
                    image={{ images: project.image }}
                    src={project.image.small.url}
                    sizes={[
                      '(min-width: 1100px) calc(33.33vw - 120px)',
                      '(min-width: 740px) calc(50vw - 300px)',
                      'calc(100vw - 60px)',
                    ].join(', ')}
                  />
                ) : <div className="ProjectsList__project__image__dummy" />}
              </div>

              <div className="ProjectsList__project__info">
                <h3 className="project-caption">{project.title}</h3>
              </div>
            </Link>
          </React.Fragment>
        ))}
      </div>
    </ColorTheme>
  )
}

export default withRouter(ProjectsList)
