import * as React from 'react'
import { RouteComponentProps } from 'react-router'
import LoadMoreButton from '~/ui/components/LoadMoreButton'
import InfiniteScroll from '~/ui/components/InfiniteScroll'
import ProjectsList from '~/projects/components/ProjectsList'
import useCurrentUser from '~/core/hooks/useCurrentUser'
import { ListProject } from '~/types'

import './UserProjectsList.scss'

type Props = {
  projects: ListProject[],
  location: RouteComponentProps['location'],
  onLoadMore?: () => void,
  cursor?: string,
  isLoading?: boolean,
}

export const UserProjectsList = ({ projects, ...props }: Props) => {
  const currentUser = useCurrentUser()

  return (
    <div className="UserProjectsList">
      <InfiniteScroll
        loadMore={props.onLoadMore}
        hasMore={!!props.cursor}
        cursor={props.cursor}
        offset={50}>

        <ProjectsList projects={projects} />

        {!projects.length && (
          <div className="UserProjectsList__notFound">
            <p>
              Sorry, no content was
              found{currentUser && currentUser.authenticated ? ` for user ${currentUser.username}` : null}.
            </p>
          </div>
        )}

        {props.onLoadMore ? (
          <p className="UserProjectsList__load-more">
            <LoadMoreButton
              to={props.location.pathname}
              cursor={props.cursor}
              onLoadMore={props.onLoadMore}
            >
              {props.isLoading ? 'Loading...' : 'Load More'}
            </LoadMoreButton>
          </p>
        ) : null}
      </InfiniteScroll>
    </div>
  )
}

export default UserProjectsList
