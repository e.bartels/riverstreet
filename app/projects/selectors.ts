import { defaultMemoize as memoize } from 'reselect'
import { Project, VisualMedia } from '~/types'

type ProjectArg = Pick<Project, 'content'>

/**
 * Given a project, returns all media items
 */
export const getProjectMedia = memoize((project) => (
  project.content
    .map(item => {
      if (item.type === 'image') return item.image
      if (item.type === 'video') return item.video
      return null
    })
    .filter(item => !!item)
)) as (p: ProjectArg) => VisualMedia[]
