import { combineReducers } from 'redux'
import { routeHistory } from '~/routes/reducer'
import { default as ui } from '~/ui/reducer'

const rootReducer = combineReducers({
  routeHistory,
  ui,
})

export default rootReducer
