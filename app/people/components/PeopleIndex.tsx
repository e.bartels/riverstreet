import * as React from 'react'
import { useState, useRef } from 'react'
import { default as cx } from 'classnames'
import { Route } from 'react-router-dom'
import anime from 'animejs'
import ModalLink from '~/ui/components/ModalLink'
import Image from '~/ui/components/Image'
import ColorTheme from '~/ui/components/ColorTheme'
import PersonDetail from '~/people/components/PersonDetail'
import { Person } from '~/types'

import './PeopleIndex.scss'

type Props = {
  people: Person[],
}

export const PeopleIndex = ({ people }: Props) => {
  // state tracks whether animation is running
  const [isAnimating, setAnimating] = useState(false)

  // Element ref
  const containerRef = useRef<HTMLDivElement>(null)

  const runAnimation = (target: HTMLElement) => {
    // Set up DOM elements
    if (!containerRef || !containerRef.current) return
    const container = containerRef.current as HTMLElement

    const targetImage = target.querySelector('.person-image') as HTMLImageElement
    const targetBbox = targetImage.getBoundingClientRect()

    // find .PersonDetail .person-image in modal
    const modal = container.querySelector('.PersonDetail') as HTMLElement
    const modalImage = modal.querySelector('.person-image') as HTMLElement
    const modalBbox = modalImage.getBoundingClientRect()

    // set up clone of .person-image
    const clone = targetImage.cloneNode(true) as HTMLImageElement
    clone.classList.add('cloned')
    clone.style.width = `${targetBbox.width}px`
    clone.style.height = `${targetBbox.height}px`
    clone.style.top = `${targetBbox.top}px`
    clone.style.left = `${targetBbox.left}px`
    targetImage.after(clone)

    // Set up initial .person-image styles
    targetImage.style.opacity = '0'
    modalImage.style.opacity = '0'

    // Animation duration
    const duration = 350

    // Run animataion on cloned .person-image
    anime({
      targets: [clone],
      duration,
      easing: 'linear',

      width: modalBbox.width,
      height: modalBbox.height,
      top: modalBbox.top,
      left: modalBbox.left,

      complete: () => {
        modalImage.style.opacity = ''
        targetImage.style.opacity = ''
        clone.remove()
        setAnimating(false)
      },
    })

    // Fade modal in
    anime({
      targets: [modal],
      duration: duration * 0.8,
      delay: duration - (duration * 0.8),
      easing: 'linear',

      opacity: 1,
    })
  }

  const onLinkClick = (e: React.MouseEvent) => {
    const target = e.currentTarget as HTMLElement
    setAnimating(true)
    setTimeout(() => { runAnimation(target) }, 100)
  }

  return (
    <>
      <ColorTheme theme="black-blue">
        <div
          id="people"
          ref={containerRef}
          className={cx('PeopleIndex', { 'PeopleIndex--isAnimating': isAnimating })}
        >
          <div className="PeopleIndex__intro">
            <h1 className="heading-large standout">A Team like no&nbsp;other</h1>

            <p>
              With our unique staff and relationships, we are able to offer concept
              development, scripting, design, animation, live action production,
              editorial and completion services under one unified direction.
            </p>
          </div>

          <div className="PeopleIndex__person-list">
            {people.map(person => (
              <ModalLink
                key={person.slug}
                className="person-link"
                to={`/people/person/${person.slug}`}
                onClick={onLinkClick}
              >
                <div className="person-image">
                  {person.image ? (
                    <Image src={person.image.images.thumb.url} image={person.image} sizes="220px" />
                  ) : null}
                </div>
                <div className="person-name">{person.name}</div>
                <div className="person-title quiet">{person.title}</div>
              </ModalLink>
            ))}
          </div>
          <Route
            path="/people/person/:slug"
            render={({ history, match }) => {
              const person = people.find(p => p.slug === match.params.slug)
              if (!person) return null
              return (
                <PersonDetail
                  person={person}
                  history={history}
                />
              )
            }}
          />
        </div>
      </ColorTheme>
    </>
  )
}

export default PeopleIndex
