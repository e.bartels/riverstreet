import * as React from 'react'
import { connect } from 'react-redux'
// import { Link } from 'react-router-dom'
// import Icon from '~/ui/components/Icon'
import ModalPage from '~/ui/components/ModalPage'
import Markup from '~/ui/components/Markup'
import Image from '~/ui/components/Image'
import { getPreviousRoute } from '~/routes/selectors'
import { Person } from '~/types'

import './PersonDetail.scss'

type Props = {
  person: Person,
  history: any,
  previousRoute: any,
}

const closeModal = (previousRoute, history) => {
  if (previousRoute && previousRoute.pathname.search(/$\/people\/?/)) {
    history.goBack()
  }
  else {
    history.push('/people')
  }
}

export const PersonDetail = ({ person, history, previousRoute }: Props) => (
  <div className="PersonDetail">
    <ModalPage onClose={() => { closeModal(previousRoute, history) }}>
      <>
        <div className="PersonDetail__content">
          <header>
            <div className="person-image">
              {person.image ? (
                <Image src={person.image.images.thumb.url} image={person.image} sizes="220px" />
              ) : null}
            </div>
            <div className="person-name">{person.name}</div>
            <div className="person-title quiet">{person.title}</div>
          </header>

          <div className="PersonDetail__bio">
            <Markup>{person.bio}</Markup>
          </div>

          {/*
          <p>
            <Link to="/work/project/slug" className="button">Projects by {person.name}</Link>
          </p>
          */}

        </div>
      </>
    </ModalPage>

    {/*
    <Link
      to="/people"
      className="modal-back-link"
      onClick={(e: React.MouseEvent) => {
        e.stopPropagation()
        e.preventDefault()
        closeModal(previousRoute, history)
      }}>
      <Icon name="arrow-right" />
      <span>Back to the Team</span>
    </Link>
    */}
  </div>
)

export default connect(state => ({
  previousRoute: getPreviousRoute(state),
}))(PersonDetail)
