import { Location } from '~/types'

export const LOCATION_CHANGED = 'routes/LOCATION_CHANGED'


export type Action = {
  type: typeof LOCATION_CHANGED,
  payload: Location,
}

export const locationChanged = (location: Location): Action => ({
  type: LOCATION_CHANGED,
  payload: location,
})
