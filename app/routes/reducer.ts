import { LOCATION_CHANGED, Action } from '~/routes/actions'
import { Location } from '~/types'

/**
 * Keeps track of route history in a stack
 * Handles push/pop/replace state transitions.
 */
export const routeHistory = (state: Array<Location> = [], action: Action) => {
  switch (action.type) {
    case LOCATION_CHANGED: {
      const location = { ...action.payload }
      const historyAction = state.length < 1 ? 'PUSH' : location.action

      switch (historyAction) {
        case 'PUSH': {
          return [
            location,
            ...state,
          ]
        }
        case 'POP': {
          return state.slice(1)
        }
        case 'REPLACE': {
          return [
            location,
            ...state.slice(1),
          ]
        }
        default:
          return state
      }
    }
    default:
      return state
  }
}

export default routeHistory
