import * as React from 'react'
import { Route, Switch } from 'react-router-dom'
import NotFound from '~/core/components/NotFound'

import Home from './components/Home'
import Login from './components/Login'
import Page from './components/Page'
import GeneralWork from './components/GeneralWork'
import WorkCategoryIndex from './components/WorkCategoryIndex'
import WorkProjectPage from './components/WorkProjectPage'
// import CategoryPage from './components/CategoryPage'
import MiniSite from './components/MiniSite'
import ProjectPage from './components/ProjectPage'
import ReelPage from './components/ReelPage'
import BlogIndex from './components/BlogIndex'
import UserProjectsPage from './components/UserProjectsPage'
import VimeoAlbumPage from './components/VimeoAlbumPage'
import PeoplePage from './components/PeoplePage'
import TypographyPage from './components/TypographyPage'


export default (
  <Switch>
    <Route path="/" exact component={Home} />
    <Route path="/media/:media_id" exact component={Home} />

    <Route path="/work/project/:slug" component={WorkProjectPage} />
    <Route path="/work/category/:slug" component={WorkCategoryIndex} />
    <Route path="/work/category" component={WorkCategoryIndex} />
    <Route path="/work" component={GeneralWork} />

    <Route path="/promotion/project/:slug" component={ProjectPage} />
    <Route path="/promotion/reel/:slug" component={ReelPage} />
    <Route path="/promotion/:slug/category/:category_slug" component={MiniSite} />
    <Route path="/promotion/:slug" component={MiniSite} />

    <Route path="/p/:slug" exact component={Page} />

    <Route path="/people" exact component={PeoplePage} />
    <Route path="/people/person/:slug" exact component={PeoplePage} />

    <Route path="/news/:year(\d{4})" component={BlogIndex} />
    <Route path="/news/:archive(archive)" component={BlogIndex} />
    <Route path="/news/c/:categorySlug" component={BlogIndex} />
    <Route path="/news" component={BlogIndex} />

    <Route path="/clients" exact component={UserProjectsPage} />
    <Route path="/clients/projectvideo" component={UserProjectsPage} />
    <Route path="/clients/:userId(\d+)" component={UserProjectsPage} />
    <Route path="/clients/:username" component={UserProjectsPage} />

    <Route path="/login" exact component={Login} />

    <Route path="/vimeo/album/:id" exact component={VimeoAlbumPage} />

    <Route path="/typography" exact component={TypographyPage} />

    <Route component={NotFound} />
  </Switch>
)
