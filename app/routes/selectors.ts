import { Location } from '~/types'

/**
 * Get route history from state
 */
export const getRouteHistory = state => (
  state.routeHistory as Location[]
)

/**
 * Get the previous route location
 */
export const getPreviousRoute = state => (
  getRouteHistory(state).slice(1)[0]
)
