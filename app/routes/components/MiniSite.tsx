import * as React from 'react'
import { join } from 'path'
import gql from 'graphql-tag'
import { useQuery } from '@apollo/react-hooks'
import { NetworkStatus } from 'apollo-client' // eslint-disable-line
import { Route, RouteComponentProps } from 'react-router-dom'
import NotFound from '~/core/components/NotFound'
import Spinner from '~/ui/components/Spinner'
import GQLError from '~/core/components/GQLError'
import { extractNodes } from '~/core/selectors'
import CategoryList from '~/projects/components/CategoryList'
import { Category } from '~/types'
import ProjectVideo from './ProjectVideo'
import { IMAGE_FRAGMENT } from '~/core/graphql/fragments'

const QUERY = gql`
  query MiniSiteCategoryList($minisite: String) {
    categories(minisite: $minisite) {
      id
      slug
      title
      introText
      introImage {
        ...Image
      }
      projects {
        edges {
          node {
            id
            slug
            title
            client
            image
            video {
              id
            }
          }
        }
      }
    }
  }
  ${IMAGE_FRAGMENT}
`

type QueryData = { categories: Category[] }

type Props = RouteComponentProps<{
  slug: string,
  category_slug: string,
}>

export const MiniSite = ({ match }: Props) => {
  const { loading, error, data } = useQuery<QueryData>(QUERY, {
    variables: { minisite: match.params.slug },
  })

  // Handle loading and error states
  if (loading) return <Spinner />
  if (error) return <GQLError error={error} />
  if (!data || !data.categories) return <NotFound />

  const categories = data.categories.map(cat => ({
    ...cat,
    projects: extractNodes(cat.projects),
  })).filter(cat => !!cat.projects.length)

  return (
    <>
      <CategoryList categories={categories} />

      <Route
        exact
        path={join(match.path, 'projectvideo/:project_slug')}
        component={ProjectVideo}
      />
    </>
  )
}

export default MiniSite
