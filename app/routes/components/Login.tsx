import * as React from 'react'
import LoginRequiredView from '~/core/components/LoginRequiredView'

export const Login = () => (
  <LoginRequiredView />
)

export default Login
