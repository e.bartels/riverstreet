import * as React from 'react'
import gql from 'graphql-tag'
import { useQuery } from '@apollo/react-hooks'
import { Helmet } from 'react-helmet-async'
import GQLError from '~/core/components/GQLError'
import NotFound from '~/core/components/NotFound'
import Spinner from '~/ui/components/Spinner'
import Page from '~/pages/components/Page'
import AboutPage from '~/pages/components/AboutPage'
import { Page as PageType } from '~/types'

import { IMAGE_FRAGMENT, CONTENT_ITEM_FRAGMENT } from '~/core/graphql/fragments'

const GET_PAGE = gql`
  query Page($slug: String!) {
    page(slug: $slug) {
    id
      slug
      title
      introText
      introImage {
        ...Image
      }
      content {
        ...ContentItem
      }
    }
  }
  ${IMAGE_FRAGMENT}
  ${CONTENT_ITEM_FRAGMENT}
`

type QueryVars = { slug: string }
type QueryData = { page: PageType }

type Props = {
  match: { params: QueryVars },
}

export const PageView = ({ match }: Props) => {
  const { loading, error, data } = useQuery<QueryData, QueryVars>(GET_PAGE, {
    variables: match.params,
  })

  if (loading) return <Spinner />
  if (error) return <GQLError error={error} />
  if (!data || !data.page) return <NotFound />

  return (
    <>
      <Helmet title={data.page.title} />
      {data.page.slug === 'about-us' ? (
        <AboutPage page={data.page} />
      ) : (
        <Page page={data.page} />
      )}
    </>
  )
}

export default PageView
