import * as React from 'react'
import gql from 'graphql-tag'
import { useQuery } from '@apollo/react-hooks'
import { NetworkStatus } from 'apollo-client' // eslint-disable-line
import { Helmet } from 'react-helmet-async'
import queryString from 'query-string'
import Spinner from '~/ui/components/Spinner'
import BodyClass from '~/ui/components/BodyClass'
import GQLError from '~/core/components/GQLError'
import { extractNodes } from '~/core/selectors'
import BlogLayout from '~/blog/components/BlogLayout'
import BlogIndex from '~/blog/components/BlogIndex'
import { CONTENT_ITEM_FRAGMENT } from '~/core/graphql/fragments'

const INDEX_QUERY = gql`
  query EntryIndex(
    $categorySlug: String,
    $year: Float,
    $archiveYear: Float,
    $perPage: Int = 3,
    $cursor: String,
  ) {
    category: blogCategory(slug: $categorySlug) {
      id
      slug
      name
    }

    entries: blogEntries(
      categories_Slug: $categorySlug,
      pubDate_Year: $year,
      pubDate_Year_Lt: $archiveYear,
      first: $perPage,
      after: $cursor,
    ) {
      edges {
        node {
          id
          slug
          title
          pubDate
          year
          excerpt
          content {
            ...ContentItem
          }
        }
      }
      pageInfo {
        hasNextPage
        endCursor
      }
    }
  }
  ${CONTENT_ITEM_FRAGMENT}
`

type Props = {
  match: {
    params: {
      categorySlug?: string,
      year?: string,
      archive?: 'archive',
    },
  },
  location: any,
}


export const BlogIndexPage = ({ match: { params }, location }: Props) => {
  const { loading, error, data, fetchMore, networkStatus } = useQuery(INDEX_QUERY, {
    variables: {
      categorySlug: params.categorySlug,
      year: params.year,
      archiveYear: params.archive ? 2012 : null,
      cursor: queryString.parse(location.search).page || null,
      perPage: 3,
    },
    notifyOnNetworkStatusChange: true,
  })

  // Handle loading and error states
  if (loading && networkStatus !== NetworkStatus.fetchMore) return <Spinner />
  if (error) return <GQLError error={error} />

  const { entries } = data
  const { category } = data
  const { pageInfo } = entries

  // Load more
  const loadMore = () => {
    fetchMore({
      variables: { cursor: pageInfo.endCursor, perPage: 5 },
      updateQuery: (previousResult, { fetchMoreResult }) => {
        const newPageInfo = fetchMoreResult.entries.pageInfo
        const newEdges = fetchMoreResult.entries.edges
        return newEdges.length
          ? {
            ...previousResult,
            entries: {
              ...previousResult.entries,
              edges: [...previousResult.entries.edges, ...newEdges],
              pageInfo: newPageInfo,
            },
          }
          : previousResult
      },
    })
  }

  const sectionTitle = category
    ? category.name
    : (params.archive ? 'Archive' : params.year || '')

  return (
    <BodyClass className="BlogIndex--active">
      <Helmet title={sectionTitle || 'News'} />
      <BlogLayout sectionTitle={sectionTitle}>
        <BlogIndex
          entries={extractNodes(entries)}
          location={location}
          onLoadMore={pageInfo.hasNextPage ? loadMore : undefined}
          cursor={pageInfo.hasNextPage ? pageInfo.endCursor : ''}
          isLoading={networkStatus === NetworkStatus.fetchMore}
        />
      </BlogLayout>
    </BodyClass>
  )
}

export default BlogIndexPage
