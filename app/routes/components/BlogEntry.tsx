import * as React from 'react'
import gql from 'graphql-tag'
import { useQuery } from '@apollo/react-hooks'
import { Helmet } from 'react-helmet-async'
import GQLError from '~/core/components/GQLError'
import NotFound from '~/core/components/NotFound'
import Spinner from '~/ui/components/Spinner'
import BlogEntry from '~/blog/components/BlogEntry'
import { BlogEntry as BlogEntryType } from '~/types'
import { CONTENT_ITEM_FRAGMENT } from '~/core/graphql/fragments'

const GET_ENTRY = gql`
  query BlogEntry($slug: String) {
    entry: blogEntry(slug: $slug) {
      id
      slug
      title
      pubDate
      year
      content {
        ...ContentItem
      }
    }
  }
  ${CONTENT_ITEM_FRAGMENT}
`

type QueryData = { entry: BlogEntryType }
type QueryVars = { slug: string }

type Props = {
  match: { params: QueryVars },
}

export const BlogEntryPage = ({ match }: Props) => {
  const { loading, error, data } = useQuery<QueryData, QueryVars>(GET_ENTRY, {
    variables: match.params,
  })

  if (loading) return <Spinner />
  if (error) return <GQLError error={error} />
  if (!data || !data.entry) return <NotFound />

  return (
    <>
      <Helmet title={`${data.entry.title}`} />
      <BlogEntry entry={data.entry} />
    </>
  )
}

export default BlogEntryPage
