import * as React from 'react'
import gql from 'graphql-tag'
import { useQuery } from '@apollo/react-hooks'
import { RouteComponentProps } from 'react-router'
import { Helmet } from 'react-helmet-async'
import GQLError from '~/core/components/GQLError'
import NotFound from '~/core/components/NotFound'
import Spinner from '~/ui/components/Spinner'
import ProjectDetail from '~/projects/components/ProjectDetail'
import { VIDEO_FRAGMENT, CONTENT_ITEM_FRAGMENT } from '~/core/graphql/fragments'
import { Reel } from '~/types'

export const GET_REEL = gql`
  query Reel($slug: String!) {
    reel(slug: $slug) {
      id
      slug
      title
      content {
        ...ContentItem
      }
    }
  }
  ${VIDEO_FRAGMENT}
  ${CONTENT_ITEM_FRAGMENT}
`

type QueryVars = { slug: string }
type QueryData = { reel: Reel }

type Props = {
  match: RouteComponentProps<QueryVars>['match'],
  query?: any, // graphql query override
}

export const ReelPage = ({ match, query }: Props) => {
  const { loading, error, data } = useQuery<QueryData, QueryVars>(query || GET_REEL, {
    variables: match.params,
  })

  if (loading) return <Spinner />
  if (error) return <GQLError error={error} />
  if (!data || !data.reel) return <NotFound />

  return (
    <>
      <Helmet title={data.reel.title} />
      <ProjectDetail project={data.reel} />
    </>
  )
}

export default ReelPage
