import * as React from 'react'
import { connect } from 'react-redux'
import { withRouter, RouteComponentProps } from 'react-router'
import { locationChanged } from '~/routes/actions'

/**
 * Keeps track of history changes in redux.
 */

type Props = RouteComponentProps & {
  locationChanged: typeof locationChanged,
}

export class RouteHistoryWatcher extends React.Component<Props> {
  componentDidMount() {
    this.updateHistory()
  }

  componentDidUpdate(prevProps: Props) {
    if (prevProps.location !== this.props.location) {
      this.updateHistory()
    }
  }

  updateHistory = () => {
    const { location, history } = this.props
    const payload = {
      ...location,
      action: history.action,
    }
    this.props.locationChanged(payload)
  }

  render() {
    return null
  }
}

const mapDispatchToProps = ({
  locationChanged,
})

export default withRouter(connect(null, mapDispatchToProps)(RouteHistoryWatcher))
