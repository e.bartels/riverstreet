import * as React from 'react'
import gql from 'graphql-tag'
import { useQuery } from '@apollo/react-hooks'
import GQLError from '~/core/components/GQLError'
import Home from '~/core/components/Home'
import BodyClass from '~/ui/components/BodyClass'
import { VIDEO_FRAGMENT } from '~/core/graphql/fragments'

const HOMEPAGE_QUERY = gql`
  query Homepage {
    homepage {
      id
      introText
      videoReel {
        ...Video
      }
    }
  }
  ${VIDEO_FRAGMENT}
`

export const HomeRoute = () => {
  const { error, data } = useQuery(HOMEPAGE_QUERY)
  const homepage = data && data.homepage ? data.homepage : {}

  return error
    ? <GQLError error={error} />
    : (
      <BodyClass className="Home--active">
        <Home
          introText={homepage.introText}
          videoReel={homepage.videoReel}
        />
      </BodyClass>
    )
}

export default HomeRoute
