import * as React from 'react'
import gql from 'graphql-tag'
import { useQuery } from '@apollo/react-hooks'
import { Helmet } from 'react-helmet-async'
import GQLError from '~/core/components/GQLError'
import NotFound from '~/core/components/NotFound'
import BodyClass from '~/ui/components/BodyClass'
import Spinner from '~/ui/components/Spinner'
import PeopleIndex from '~/people/components/PeopleIndex'
import { Person } from '~/types'
import { IMAGE_FRAGMENT } from '~/core/graphql/fragments'

const QUERY = gql`
  query People {
    people {
      id
      slug
      name
      title
      bio
      image {
        ...Image
      }
    }
  }
  ${IMAGE_FRAGMENT}
`

type QueryData = { people: Person[] }

export const PeoplePage = () => {
  const { loading, error, data } = useQuery<QueryData>(QUERY)

  if (loading) return <Spinner />
  if (error) return <GQLError error={error} />
  if (!data || !data.people) return <NotFound />

  return (
    <BodyClass className="PeopleIndex--active">
      <Helmet title="About our Team" />
      <PeopleIndex people={data.people} />
    </BodyClass>
  )
}

export default PeoplePage
