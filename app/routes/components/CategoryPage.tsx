import * as React from 'react'
import { join } from 'path'
import gql from 'graphql-tag'
import { useQuery } from '@apollo/react-hooks'
import { NetworkStatus } from 'apollo-client' // eslint-disable-line
import { Helmet } from 'react-helmet-async'
import { Route, RouteComponentProps } from 'react-router-dom'
import NotFound from '~/core/components/NotFound'
import Spinner from '~/ui/components/Spinner'
import GQLError from '~/core/components/GQLError'
import { extractNodes } from '~/core/selectors'
import CategoryView from '~/projects/components/CategoryView'
import { Category } from '~/types'
import ProjectVideo from './ProjectVideo'
import { IMAGE_FRAGMENT } from '~/core/graphql/fragments'

const GET_CATEGORY = gql`
  query Category($slug: String!) {
    category(slug: $slug) {
      id
      slug
      title
      introText
      introImage {
        ...Image
      }
      projects {
        edges {
          node {
            id
            slug
            title
            client
            image
            video {
              id
            }
          }
        }
      }
    }
  }
  ${IMAGE_FRAGMENT}
`

type QueryData = { category: Category }
type QueryVars = { slug: string }

type Props = RouteComponentProps<QueryVars>

export const CategoryPage = ({ match }: Props) => {
  const { loading, error, data } = useQuery<QueryData, QueryVars>(GET_CATEGORY, {
    variables: match.params,
  })

  // Handle loading and error states
  if (loading) return <Spinner />
  if (error) return <GQLError error={error} />
  if (!data || !data.category) return <NotFound />

  const category = {
    ...data.category,
    projects: extractNodes(data.category.projects),
  }

  return (
    <>
      <Helmet title={category.title} />
      <CategoryView category={category} />

      <Route
        exact
        path={join(match.path, 'projectvideo/:project_slug')}
        component={ProjectVideo}
      />
    </>
  )
}

export default CategoryPage
