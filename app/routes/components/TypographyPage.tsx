import * as React from 'react'
import { Link } from 'react-router-dom'


export const TypographyPage = () => (
  <div className="TypographyPage">
    <div className="container">
      <br />
      <br />

      <div
        className="TypographyPage__header"
        style={{
          textAlign: 'center',
          maxWidth: 620,
          margin: '0 auto',
        }}
      >
        <h1 className="heading-large standout">A Team like no other</h1>

        <p>
          With our unique staff and relationships, we are able to offer concept
          development, scripting, design, animation, live action production,
          editorial and completion services under one unified direction.
        </p>
      </div>

      <hr />

      <h1 className="standout">We&apos;re really great at a lot of things</h1>

      <p>
        In light of our experience in branding and image campaigns,
        commercials, interstitials, cross-promotions and network specials, we
        pride ourselves on providing a fresh voice and impeccable production
        values for every project, as well as on constantly pushing into the
        future with both creativity and technology.
      </p>

      <p>
        Ultimately, we strive to make the most of our time on this mortal coil,
        so work feels like play for both ourselves and our clients. If we&apos;re
        having fun, your viewers will too!
      </p>

      <p>
        <button className="button">Meet the Team</button>
      </p>

      <h2 className="standout">Come say hi!</h2>

      <p>
        Aliquam at sapien mollis, sagittis mi vitae, faucibus ante. Proin
        non dignissim velit, ac ultrices lectus. Integer sed erat eget
        ligula imperdiet suscipit, and <Link to="/">check out more here</Link>!
      </p>

      <header>
        <h2 className="standout">It&apos;s Earth Month! Let&apos;s go to the park!</h2>
        <small>4 April, 2015</small>
      </header>

      <p>
        On the east coast, the mountains of snow are finally melting away, and
        here in California the desert wildflowers are in full bloom. It must be
        Earth Month! While we often associate Earth Month with saving the
        world, ...
      </p>
    </div>
  </div>
)

export default TypographyPage
