import * as React from 'react'
import gql from 'graphql-tag'
import { useQuery } from '@apollo/react-hooks'
import { Helmet } from 'react-helmet-async'
import GQLError from '~/core/components/GQLError'
import NotFound from '~/core/components/NotFound'
import Spinner from '~/ui/components/Spinner'
import VimeoAlbum from '~/vimeo/components/VimeoAlbum'

const GET_ALBUM = gql`
  query VimeoAlbum($id: String!) {
    album(id: $id) {
      id
      name
      videos {
        id
        name
        link
        width
        height
        duration
        embed
      }
    }
  }
`

type QueryVars = { id: string }

type Props = {
  match: { params: QueryVars },
}

export const VideoAlbumQuery = ({ match }: Props) => {
  const { loading, error, data } = useQuery<any, QueryVars>(GET_ALBUM, {
    variables: match.params,
  })

  if (loading) return <Spinner />
  if (error) return <GQLError error={error} />
  if (!data || !data.album) return <NotFound />

  return (
    <>
      <Helmet title={data.album.name} />
      <VimeoAlbum album={data.album} />
    </>
  )
}

export default VideoAlbumQuery
