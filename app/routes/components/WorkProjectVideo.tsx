import * as React from 'react'
import { RouteComponentProps } from 'react-router'
import ProjectVideo from './ProjectVideo'
import { GET_PROJECT } from './WorkProjectPage'

export const WorkProjectVideo = (props: RouteComponentProps<any>) => (
  <ProjectVideo {...props} query={GET_PROJECT} />
)

export default WorkProjectVideo
