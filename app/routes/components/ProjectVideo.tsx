/* eslint-disable camelcase */
import * as React from 'react'
import { useQuery } from '@apollo/react-hooks'
import { Link, RouteComponentProps } from 'react-router-dom'
import { connect } from 'react-redux'
import VideoModal from '~/ui/components/VideoModal'
import { getPreviousRoute } from '~/routes/selectors'
import { Project } from '~/types'
import { GET_PROJECT } from './ProjectPage'

type QueryData = { project: Project }
type Params = { project_slug: string }

type Props = {
  previousRoute: RouteComponentProps['location'],
  query?: any,
} & RouteComponentProps<Params>

export const ProjectVideo = ({ match, location, history, previousRoute, query }: Props) => {
  const { error, data } = useQuery<QueryData>(query || GET_PROJECT, {
    variables: {
      slug: match.params.project_slug,
    },
  })

  if (error) return null


  let video
  let caption
  let projectUrl
  if (data && data.project) {
    const project = data.project
    video = project.video
    caption = project.videoIntroCaption
    projectUrl = `${location.pathname.split('/').slice(0, 2).join('/')}/project/${project.slug}`
  }

  return (
    <VideoModal
      video={video}
      caption={caption}
      moreLink={projectUrl ? (
        <Link to={projectUrl} replace>View more</Link>
      ) : null}
      onClose={() => {
        if (previousRoute) {
          history.goBack()
        }
        else {
          const path = location.pathname.split('/projectvideo')[0]
          history.push(path)
        }
      }}
    />
  )
}

export default connect(state => ({
  previousRoute: getPreviousRoute(state),
}))(ProjectVideo)
