import * as React from 'react'
import gql from 'graphql-tag'
import { RouteComponentProps } from 'react-router'
import ProjectPage from './ProjectPage'
import { VIDEO_FRAGMENT, CONTENT_ITEM_FRAGMENT } from '~/core/graphql/fragments'

export const GET_PROJECT = gql`
  query WorkProject($slug: String!) {
    project: workProject(slug: $slug) {
      id
      slug
      title
      client
      video {
        ...Video
      }
      videoIntroCaption
      showProjectDetails
      content {
        ...ContentItem
      }
    }
  }
  ${VIDEO_FRAGMENT}
  ${CONTENT_ITEM_FRAGMENT}
`

export const WorkProjectPage = (props: RouteComponentProps<any>) => (
  <ProjectPage {...props} query={GET_PROJECT} />
)

export default WorkProjectPage
