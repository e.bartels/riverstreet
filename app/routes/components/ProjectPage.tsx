import * as React from 'react'
import gql from 'graphql-tag'
import { useQuery } from '@apollo/react-hooks'
import { RouteComponentProps } from 'react-router'
import { Helmet } from 'react-helmet-async'
import GQLError from '~/core/components/GQLError'
import NotFound from '~/core/components/NotFound'
import Spinner from '~/ui/components/Spinner'
import ProjectDetail from '~/projects/components/ProjectDetail'
import { VIDEO_FRAGMENT, CONTENT_ITEM_FRAGMENT } from '~/core/graphql/fragments'
import { Project } from '~/types'

export const GET_PROJECT = gql`
  query Project($slug: String!) {
    project(slug: $slug) {
      id
      slug
      title
      client
      video {
        ...Video
      }
      videoIntroCaption
      showProjectDetails
      content {
        ...ContentItem
      }
    }
  }
  ${VIDEO_FRAGMENT}
  ${CONTENT_ITEM_FRAGMENT}
`

type QueryVars = { slug: string }
type QueryData = { project: Project }

type Props = {
  match: RouteComponentProps<QueryVars>['match'],
  query?: any, // graphql query override
}

export const ProjectPage = ({ match, query }: Props) => {
  const { loading, error, data } = useQuery<QueryData, QueryVars>(query || GET_PROJECT, {
    variables: match.params,
  })

  if (loading) return <Spinner />
  if (error) return <GQLError error={error} />
  if (!data || !data.project) return <NotFound />

  return (
    <>
      <Helmet title={data.project.title} />
      <ProjectDetail project={data.project} />
    </>
  )
}

export default ProjectPage
