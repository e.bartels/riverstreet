import * as React from 'react'
import { join } from 'path'
import gql from 'graphql-tag'
import { useQuery } from '@apollo/react-hooks'
import { NetworkStatus } from 'apollo-client' // eslint-disable-line
import { Helmet } from 'react-helmet-async'
import { Route, RouteComponentProps } from 'react-router-dom'
import NotFound from '~/core/components/NotFound'
import Spinner from '~/ui/components/Spinner'
import GQLError from '~/core/components/GQLError'
import { extractNodes } from '~/core/selectors'
import GeneralWorkCategory from '~/work/components/GeneralWorkCategory'
import { Category } from '~/types'
import WorkProjectVideo from './WorkProjectVideo'
import { IMAGE_FRAGMENT } from '~/core/graphql/fragments'

const GET_CATEGORY = gql`
  query WorkCategory($slug: String!) {
    category: workCategory(slug: $slug) {
      id
      slug
      title
      introText
      introImage {
        ...Image
      }
      projects {
        edges {
          node {
            id
            slug
            title
            client
            image
            video {
              id
            }
          }
        }
      }
    }
  }
  ${IMAGE_FRAGMENT}
`

type QueryData = { category: Category }

type Props = RouteComponentProps

export const GeneralWork = ({ match }: Props) => {
  const { loading, error, data } = useQuery<QueryData>(GET_CATEGORY, {
    variables: { slug: 'general-work' },
  })

  // Handle loading and error states
  if (loading) return <Spinner />
  if (error) return <GQLError error={error} />
  if (!data || !data.category) return <NotFound />

  const category = {
    ...data.category,
    projects: extractNodes(data.category.projects),
  }

  return (
    <>
      <Helmet title="Our Work" />

      <GeneralWorkCategory category={category} />

      <Route
        exact
        path={join(match.path, 'projectvideo/:project_slug')}
        component={WorkProjectVideo}
      />
    </>
  )
}

export default GeneralWork
