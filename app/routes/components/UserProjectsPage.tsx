import * as React from 'react'
import { join } from 'path'
import gql from 'graphql-tag'
import { useQuery } from '@apollo/react-hooks'
import { NetworkStatus } from 'apollo-client' // eslint-disable-line
import { Route, RouteComponentProps } from 'react-router-dom'
import { Helmet } from 'react-helmet-async'
import queryString from 'query-string'
import Spinner from '~/ui/components/Spinner'
import GQLError from '~/core/components/GQLError'
import LoginRequiredView from '~/core/components/LoginRequiredView'
import UserProjectsList from '~/projects/components/UserProjectsList'
import ProjectVideo from './ProjectVideo'
import { extractNodes } from '~/core/selectors'

const GET_USER_PROJECTS = gql`
  query UserProjects($userId: Int, $username: String, $cursor: String, $perPage: Int = 12) {
    projects: userProjects(userId: $userId, username: $username, first: $perPage, after: $cursor) {
      edges {
        node {
          id
          slug
          title
          client
          image
          video {
            id
          }
        }
      }
      pageInfo {
        hasNextPage
        endCursor
      }
    }
  }
`

type Params = {
  userId?: string,
  username?: string,
}

type Props = RouteComponentProps<Params>

export const UserProjectsPage = ({ location, match }: Props) => {
  const {
    loading,
    error,
    data,
    fetchMore,
    networkStatus,
  } = useQuery(GET_USER_PROJECTS, {
    variables: {
      userId: match.params.userId,
      username: match.params.username,
      cursor: queryString.parse(location.search).page || null,
    },
    notifyOnNetworkStatusChange: true,
  })

  // Handle loading and error states
  if (loading && networkStatus !== NetworkStatus.fetchMore) return <Spinner />
  if (error) return <GQLError error={error} />
  if (!data || !data.projects) return <LoginRequiredView forPath={location.pathname} />

  const { projects } = data
  const { pageInfo } = projects

  const loadMore = () => {
    fetchMore({
      variables: { cursor: pageInfo.endCursor },
      updateQuery: (previousResult, { fetchMoreResult }) => {
        const newEdges = fetchMoreResult.projects.edges
        const newPageInfo = fetchMoreResult.projects.pageInfo
        return newEdges.length
          ? {
            ...previousResult,
            projects: {
              ...previousResult.projects,
              edges: [...previousResult.projects.edges, ...newEdges],
              pageInfo: newPageInfo,
            },
          }
          : previousResult
      },
    })
  }

  return (
    <>
      <Helmet title="Projects" />
      <UserProjectsList
        projects={extractNodes(projects)}
        onLoadMore={pageInfo.hasNextPage ? loadMore : undefined}
        cursor={pageInfo.hasNextPage ? pageInfo.endCursor : ''}
        isLoading={networkStatus === NetworkStatus.fetchMore}
        location={location}
      />

      <Route
        exact
        path={join(match.path.replace('/projectvideo', ''), 'projectvideo/:project_slug')}
        component={ProjectVideo}
      />
    </>
  )
}

export default UserProjectsPage
