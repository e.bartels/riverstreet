import * as React from 'react'
import BlogNav from '~/blog/components/BlogNav'
import { BlogCategory } from '~/types'

import './FixedBlogNav.scss'

type Props = {
  categories: BlogCategory[],
  years: number[],
}

export const FixedBlogNav = ({ categories, years }: Props) => (
  <div className="FixedBlogNav">
    <div className="FixedBlogNav__container">
      <BlogNav categories={categories} years={years} />
    </div>
  </div>
)

export default FixedBlogNav
