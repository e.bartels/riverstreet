import * as React from 'react'
import { NavLink } from 'react-router-dom'
import { BlogCategory } from '~/types'
import SocialIcons from '~/core/components/SocialIcons'

import './BlogNav.scss'

type Props = {
  categories: BlogCategory[],
  years: number[],
}

export const BlogNav = ({ categories, years }: Props) => (
  <div className="BlogNav">
    <nav className="BlogNav__nav">
      {categories.map(category => (
        <NavLink to={`/news/c/${category.slug}`} key={category.slug}>{category.name}</NavLink>
      ))}
    </nav>

    {/*
    <nav className="BlogNav__nav">
      {years.map(year => (
        <NavLink key={`year-${year}`} to={`/news/${year}/`}>{year}</NavLink>
      ))}
      <NavLink to="/news/archive">Older</NavLink>
    </nav>
    */}

    <nav className="BlogNav__nav">
      <a href="/news/feeds" target="_blank" rel="noopener noreferrer">RSS</a>
      <a href="https://www.artdesignoffice.com/">Art Design Office</a>
    </nav>

    <nav className="BlogNav__nav BlogNav__social-icons">
      <SocialIcons />
    </nav>
  </div>
)

export default BlogNav
