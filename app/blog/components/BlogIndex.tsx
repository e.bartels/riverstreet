/* eslint-disable jsx-a11y/anchor-is-valid */
import * as React from 'react'
import { join } from 'path'
import { Route, Switch } from 'react-router-dom'
import ModalLink from '~/ui/components/ModalLink'
import ColorTheme from '~/ui/components/ColorTheme'
import ShareLinks from '~/ui/components/ShareLinks'
import LoadMoreButton from '~/ui/components/LoadMoreButton'
import InfiniteScroll from '~/ui/components/InfiniteScroll'
import Markup from '~/ui/components/Markup'
import ContentItem from '~/core/components/ContentItem'
import BlogEntry from '~/routes/components/BlogEntry'
import { dt } from '~/utils/datetime'

import { BlogEntry as BlogEntryType } from '~/types'

import './BlogIndex.scss'

type Props = {
  entries: BlogEntryType[],
  location: any,
  onLoadMore?: () => any,
  cursor?: string,
  isLoading?: boolean,
}

const relLink = (entry, location) => (
  /^\/news\/?$/.test(location.pathname)
    ? `/news/${entry.year}/${entry.slug}`
    : join(location.pathname, entry.slug)
)

const permaLink = (entry) => (
  `/news/${entry.year}/${entry.slug}`
)

export const BlogIndex = ({ entries, location, ...props }: Props) => (
  <>
    <ColorTheme theme="black-blue">
      <div className="BlogIndex">
        <section className="BlogIndex__posts">
          <InfiniteScroll
            loadMore={props.onLoadMore}
            hasMore={!!props.cursor}
            cursor={props.cursor}
            offset={350}>
            {entries.map(entry => (
              <article className="BlogIndex__post" key={entry.slug}>
                <header className="BlogIndex__post-header">
                  <time className="BlogIndex__post-date" dateTime={entry.pubDate}>{dt(entry.pubDate)}</time>
                  <ModalLink to={permaLink(entry)} className="read-more">Permalink</ModalLink>
                </header>

                <section className="BlogIndex__post-body">
                  <h2 className="BlogIndex__post-title">
                    <ModalLink to={relLink(entry, location)}>{entry.title}</ModalLink>
                  </h2>

                  {entry.excerpt ? (
                    <div className="BlogIndex__post-body__excerpt">
                      <Markup>{entry.excerpt}</Markup>
                    </div>
                  ) : (
                    entry.content.map(item => (
                      <div className="BlogIndex__content-item" key={`${entry.slug}-${item.id}`}>
                        <ContentItem
                          item={item}
                          sizes={[
                            '(min-width: 740px) 670px',
                            'calc(100vw - 60px)',
                          ].join(', ')}
                        />
                      </div>
                    ))
                  )}

                  <footer className="BlogIndex__post-footer">
                    <ShareLinks url={permaLink(entry)} title={entry.title} />
                  </footer>
                </section>
              </article>
            ))}

            {props.onLoadMore && props.cursor ? (
              <p className="BlogIndex__load-more">
                <span className="BlogIndex__load-more__spacer" />
                <span className="BlogIndex__load-more__button">
                  <LoadMoreButton
                    to={location.pathname}
                    cursor={props.cursor}
                    onLoadMore={props.onLoadMore}
                  >
                    {props.isLoading ? 'Loading...' : 'Load More'}
                  </LoadMoreButton>
                </span>
              </p>
            ) : null}
          </InfiniteScroll>
        </section>
      </div>
    </ColorTheme>

    <Switch>
      <Route path="/news/:year(\d{4})/:slug" exact component={BlogEntry} />
      <Route path="/news/:archive(archive)/:slug" component={BlogEntry} />
      <Route path="/news/c/:categorySlug/:slug" component={BlogEntry} />
      <Route path="/news/:year(\d{4})/:month/:day/:slug" exact component={BlogEntry} />
    </Switch>
  </>
)

export default BlogIndex
