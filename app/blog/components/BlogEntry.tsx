/* eslint-disable jsx-a11y/anchor-is-valid */
import * as React from 'react'
import { connect } from 'react-redux'
import { withRouter, RouteComponentProps } from 'react-router'
import { Link } from 'react-router-dom'
import ModalPage from '~/ui/components/ModalPage'
import ShareLinks from '~/ui/components/ShareLinks'
import Icon from '~/ui/components/Icon'
import ContentItem from '~/core/components/ContentItem'
import { dt } from '~/utils/datetime'
import { getPreviousRoute } from '~/routes/selectors'

import { BlogEntry as BlogEntryType } from '~/types'
import { Location } from '~/types'

import './BlogEntry.scss'

type Props = RouteComponentProps & {
  entry: BlogEntryType,
  previousRoute: Location,
}

const closeModal = (previousRoute, history) => {
  if (previousRoute && previousRoute.pathname.search(/$\/news\/?/)) {
    history.goBack()
  }
  else {
    history.push('/news')
  }
}

const permaLink = (entry) => (
  `/news/${entry.year}/${entry.slug}`
)

export const BlogEntry = ({ entry, history, previousRoute }: Props) => (
  <div className="BlogEntry">
    <ModalPage onClose={() => { closeModal(previousRoute, history) }}>
      <div className="BlogEntry__content">
        <header className="BlogEntry__post-header">
          <h2 className="BlogEntry__post-title standout">{entry.title}</h2>
          <time className="BlogEntry__post-date" dateTime={entry.pubDate}>{dt(entry.pubDate)}</time>
        </header>

        <div className="BlogEntry__post-body">
          {entry.content.map(item => (
            <div className="BlogEntry__content-item" key={item.id}>
              <ContentItem
                item={item}
                key={item.id}
                sizes={[
                  '(min-width: 740px) 670px',
                  'calc(100vw - 60px)',
                ].join(', ')}
              />
            </div>
          ))}
        </div>

        <footer className="BlogEntry__post-footer">
          <ShareLinks url={permaLink(entry)} title={entry.title} />
        </footer>
      </div>
    </ModalPage>

    <Link
      to="/news/"
      className="modal-back-link"
      onClick={(e: React.MouseEvent) => {
        e.stopPropagation()
        e.preventDefault()
        closeModal(previousRoute, history)
      }}>
      <Icon name="arrow-right" />
      <span>Back to the Blog</span>
    </Link>
  </div>
)

export default withRouter(connect(state => ({
  previousRoute: getPreviousRoute(state),
}))(BlogEntry))
