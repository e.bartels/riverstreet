import * as React from 'react'
import gql from 'graphql-tag'
import { useQuery } from '@apollo/react-hooks'
import FixedBlogNav from '~/blog/components/FixedBlogNav'
import OverlayBlogNav from '~/blog/components/OverlayBlogNav'

import './BlogLayout.scss'

const BLOG_NAV = gql`
  query BlogNav {
    categories: blogCategories {
      id
      slug
      name
    }

    years: blogYears
  }
`

type Props = {
  children: React.ReactNode,
  sectionTitle?: string,
}

export const BlogLayout = ({ children, sectionTitle }: Props) => {
  const { data } = useQuery(BLOG_NAV)
  const categories = data && data.categories ? data.categories : []
  const years = (data && data.years ? data.years : []).slice(0, 6)

  return (
    <div className="BlogLayout">
      {children}
      <FixedBlogNav
        categories={categories}
        years={years}
      />
      <OverlayBlogNav
        categories={categories}
        years={years}
        sectionTitle={sectionTitle}
      />
    </div>
  )
}

export default BlogLayout
