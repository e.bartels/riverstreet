import * as React from 'react'
import { useState } from 'react'
import MDIcon from '~/ui/components/MDIcon'
import OverlayNav from '~/ui/components/OverlayNav'
import BlogNav from '~/blog/components/BlogNav'
import { BlogCategory } from '~/types'

import './OverlayBlogNav.scss'

/**
 * View component
 */
type Props = {
  categories: BlogCategory[],
  years: number[],
  sectionTitle?: string,
}

const useNavState = (): [boolean, () => void, () => void] => {
  const [isOpen, setNavOpen] = useState(false)
  const openNav = () => setNavOpen(true)
  const closeNav = () => setNavOpen(false)
  return [isOpen, openNav, closeNav]
}

export const OverlayBlogNav = ({ categories, years, ...props }: Props) => {
  const [isOpen, openNav, closeNav] = useNavState()

  return (
    <div className="OverlayBlogNav">
      <button className="OverlayBlogNav__button button" onClick={isOpen ? closeNav : openNav}>
        {props.sectionTitle || 'Filter' }
        <MDIcon name={isOpen ? 'chevron-up' : 'chevron-down'} />
      </button>

      <OverlayNav isOpen={isOpen} onClose={closeNav}>
        <BlogNav categories={categories} years={years} />
      </OverlayNav>
    </div>
  )
}


export default OverlayBlogNav
