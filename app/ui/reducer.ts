import { combineReducers } from 'redux'
import { MOBILE_MAX_WIDTH, TABLET_MAX_WIDTH } from '~/core/constants'
import { ColorThemeType } from '~/types'
import { Action, types as t, MainNavViewType } from './actions'


/**
 * Stores whether a touch device is detected
 */
export const isTouch = (state = false, action: Action) => {
  switch (action.type) {
    case t.TOUCH_DETECTED:
      return action.payload
    default:
      return state
  }
}

// Initial viewport
const w = window as any
const wW = w.innerWidth
const wH = w.visualViewport ? w.visualViewport.height : window.innerHeight
const initialViewport = {
  width: wW,
  height: wH,
  scrollY: window.scrollY,
  scrollX: window.scrollX,
  isMobile: wW <= MOBILE_MAX_WIDTH,
  isTablet: wW <= TABLET_MAX_WIDTH && wW > MOBILE_MAX_WIDTH,
  isDesktop: wW > TABLET_MAX_WIDTH,
}

export type Viewport = typeof initialViewport

/**
 * Stores the window/viewport dimensions
 */
export const viewport = (state = initialViewport, action: Action) => {
  switch (action.type) {
    case t.VIEWPORT_RESIZE: {
      const { width } = action.payload
      const isMobile = width <= MOBILE_MAX_WIDTH
      const isTablet = !isMobile && width <= TABLET_MAX_WIDTH
      const isDesktop = !isMobile && !isTablet
      return {
        ...state,
        ...action.payload,
        isMobile,
        isTablet,
        isDesktop,
      }
    }
    case t.VIEWPORT_SCROLL: {
      return {
        ...state,
        scrollY: action.payload.y,
        scrollX: action.payload.x,
      }
    }
    default:
      return state
  }
}

/**
 * Stores color theme data.
 */
export const colorTheme = (state: ColorThemeType | null = null, action: Action) => {
  switch (action.type) {
    case t.COLOR_THEME_REQUEST: {
      return action.payload
    }
    default:
      return state
  }
}

/**
 * Stores main nav menu state
 */
type MainNavState = {
  isOpen: boolean,
  view: MainNavViewType,
}

const defaultNavState: MainNavState = {
  isOpen: false,
  view: 'default',
}

export const mainNav = (state: MainNavState = defaultNavState, action: Action): MainNavState => {
  switch (action.type) {
    case t.MAIN_NAV_OPEN:
      return { ...state, isOpen: true }
    case t.MAIN_NAV_CLOSE:
      return { ...state, isOpen: false, view: 'default' }
    case t.MAIN_NAV_SHOW_VIEW:
      return { ...state, view: action.payload }
    default:
      return state
  }
}

/**
 * Stores url path that login has been requested for
 */
export const loginPath = (state: string | null = null, action: Action) => {
  switch (action.type) {
    case t.LOGIN_PATH_REQUESTED:
      return action.payload || null
    default:
      return state
  }
}


/**
 * Stores whether or not modal views are active.
 */
export const activeModals = (state: Array<string> = [], action: Action) => {
  switch (action.type) {
    case t.MODAL_OPEN: {
      return [
        ...state,
        'modal',
      ]
    }
    case t.MODAL_CLOSE: {
      return state.slice(1)
    }
    default:
      return state
  }
}

export default combineReducers({
  isTouch,
  viewport,
  colorTheme,
  mainNav,
  activeModals,
  loginPath,
})
