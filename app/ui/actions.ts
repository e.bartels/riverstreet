/* eslint-disable no-use-before-define */
import { ColorThemeType } from '~/types'

export enum t {
  TOUCH_DETECTED = 'ui/TOUCH_DETECTED',
  VIEWPORT_RESIZE = 'ui/VIEWPORT_RESIZE',
  VIEWPORT_SCROLL = 'ui/VIEWPORT_SCROLL',
  COLOR_THEME_REQUEST = 'ui/COLOR_THEME_REQUEST',
  MAIN_NAV_OPEN = 'ui/MAIN_NAV_OPEN',
  MAIN_NAV_CLOSE = 'ui/MAIN_NAV_CLOSE',
  MAIN_NAV_SHOW_VIEW = 'ui/MAIN_NAV_SHOW_VIEW',
  MODAL_OPEN = 'ui/MODAL_OPEN',
  MODAL_CLOSE = 'ui/MODAL_CLOSE',
  LOGIN_PATH_REQUESTED = 'ui/LOGIN_PATH_REQUESTED',
}

export const types = t

export type Action =
  | TouchDetected
  | ViewportResize
  | ViewporScroll
  | ColorThemeRequest
  | MainNavOpen
  | MainNavClose
  | MainNavShowView
  | RequestLoginPath
  | ModalOpen
  | ModalClose


/**
 * Store in ui.viewport whehter we have a touchscreen.
 */
export type TouchDetected = {
  type: t.TOUCH_DETECTED,
  payload: true,
}

export const touchDetected = (): TouchDetected => ({
  type: t.TOUCH_DETECTED,
  payload: true,
})

/**
 * Set ui.viewport width & height
 */
export type Dimensions = { width: number, height: number }

export type ViewportResize = {
  type: t.VIEWPORT_RESIZE,
  payload: Dimensions,
}

export const viewportResize = (d: Dimensions): ViewportResize => ({
  type: t.VIEWPORT_RESIZE,
  payload: d,
})


/**
 * Set in ui.viewport the x & y scroll amounts
 */
export type Scroll = { y: number, x: number }

export type ViewporScroll = {
  type: t.VIEWPORT_SCROLL,
  payload: Scroll,
}
export const viewportScroll = (s: Scroll): ViewporScroll => ({
  type: t.VIEWPORT_SCROLL,
  payload: s,
})

/**
 * Set the color theme
 */
export type ColorThemeRequest = {
  type: t.COLOR_THEME_REQUEST,
  payload: ColorThemeType | null,
}

export const colorThemeRequest = (theme: ColorThemeType): ColorThemeRequest => ({
  type: t.COLOR_THEME_REQUEST,
  payload: theme,
})


/**
 * Main nav menu open/close actions
 */
export type MainNavOpen = {
  type: t.MAIN_NAV_OPEN,
}

export const mainNavOpen = (): MainNavOpen => ({
  type: t.MAIN_NAV_OPEN,
})

export type MainNavClose = {
  type: t.MAIN_NAV_CLOSE,
}
export const mainNavClose = (): MainNavClose => ({
  type: t.MAIN_NAV_CLOSE,
})

/**
 * Showing main nav views
 */
export type MainNavViewType = 'default' | 'login'

export type MainNavShowView = {
  type: t.MAIN_NAV_SHOW_VIEW,
  payload: MainNavViewType,
}

export const mainNavShowView = (view: MainNavViewType): MainNavShowView => ({
  type: t.MAIN_NAV_SHOW_VIEW,
  payload: view,
})


/**
 * Modal open/close actions
 */
export type ModalOpen = {
  type: t.MODAL_OPEN,
  payload: 'modal',
}

export const modalOpen = (): ModalOpen => ({
  type: t.MODAL_OPEN,
  payload: 'modal',
})

export type ModalClose = {
  type: t.MODAL_CLOSE,
  payload: 'modal',
}
export const modalClose = (): ModalClose => ({
  type: t.MODAL_CLOSE,
  payload: 'modal',
})


/**
 * Request login for url path
 */
export type RequestLoginPath = {
  type: t.LOGIN_PATH_REQUESTED,
  payload: string | null,
}
export const requestLoginPath = (path: string | null): RequestLoginPath => ({
  type: t.LOGIN_PATH_REQUESTED,
  payload: path,
})


/**
 * Opens the main nav menu & shows login view
 */
export const showLogin = (path: string | null) => (dispatch: Function) => {
  dispatch(requestLoginPath(path))
  dispatch(mainNavShowView('login'))
  dispatch(mainNavOpen())
}

export const hideLogin = () => (dispatch: Function) => {
  dispatch(requestLoginPath(null))
  dispatch(mainNavShowView('default'))
  dispatch(mainNavClose())
}
