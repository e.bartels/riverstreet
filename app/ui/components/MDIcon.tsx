import * as React from 'react'
import Icon from '~/ui/components/Icon'

type Props = {
  name: string,
}

export const MDIcon = ({ name, ...props }: Props) => (
  <Icon name={name} className="zmdi" prefix="zmdi" />
)

export default MDIcon
