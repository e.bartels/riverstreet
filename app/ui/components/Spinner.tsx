import * as React from 'react'

import './Spinner.scss'

type Props = {
  delay?: number,
  className?: string,
}

type State = {
  show: boolean,
}

export class Spinner extends React.PureComponent<Props, State> {
  timeout: number | null = null

  static defaultProps = {
    delay: 350,
  }

  state = {
    show: false,
  }

  componentDidMount() {
    this.show()
  }

  componentWillUnmount() {
    if (this.timeout) {
      clearTimeout(this.timeout)
    }
  }

  show = () => {
    this.timeout = window.setTimeout(() => {
      this.timeout = null
      this.setState({
        show: true,
      })
    }, this.props.delay || 0)
  }

  render() {
    if (!this.state.show) {
      return null
    }
    const { className = '' } = this.props
    return (
      <div className={`Spinner ${className}`}><div className="Spinner__spin" /></div>
    )
  }
}

export default Spinner
