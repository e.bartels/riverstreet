import * as React from 'react'
import { useEffect, useCallback } from 'react'
import { default as cx } from 'classnames'
import { connect } from 'react-redux'
import { useEventListener } from '~/ui/hooks/events'
import CloseButton from '~/ui/components/CloseButton'
import CircleLogo from '~/ui/components/CircleLogo'
import BodyClass from '~/ui/components/BodyClass'
import { modalOpen, modalClose } from '~/ui/actions'
import { getActiveModals } from '~/ui/selectors'

import './Modal.scss'

type Props = {
  onClose: () => void,
  modalOpen: () => any,
  modalClose: () => any,
  activeModals: any[],
  children?: React.ReactNode,
  className?: string,
}

export const Modal = (props: Props) => {
  // Handle modalOpen & modalClose callbacks
  useEffect(() => {
    props.modalOpen()
    return () => { props.modalClose() }
  }, [])

  // Esc key closes
  useEventListener('keydown', useCallback((e: KeyboardEvent) => {
    if (e.keyCode === 27) props.onClose()
  }, []))

  return (
    <>
      {props.activeModals.length ? <BodyClass className="Modal--isOpen" /> : null}
      <div className={cx('Modal', props.className)}>
        <CircleLogo className="Modal__logo" />
        <CloseButton className="Modal__close" onClick={props.onClose} />
        <div className="Modal__content">
          {props.children || null}
        </div>
      </div>
    </>
  )
}


const mapStateToProps = state => ({
  activeModals: getActiveModals(state),
})

const mapDispatchToProps = ({
  modalOpen,
  modalClose,
})

export default connect(mapStateToProps, mapDispatchToProps)(Modal)
