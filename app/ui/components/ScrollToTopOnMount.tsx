import * as React from 'react'

type Props = {
  children?: React.ReactNode,
}

export class ScrollToTopOnMount extends React.PureComponent<Props> {
  componentDidMount() {
    window.scrollTo(0, 0)
  }

  render() {
    return this.props.children || null
  }
}

export default ScrollToTopOnMount
