import * as React from 'react'
import { connect } from 'react-redux'
import { colorThemeRequest } from '~/ui/actions'
import { getViewport, getColorTheme } from '~/ui/selectors'
import { ColorThemeType } from '~/types'

export interface Props {
  theme: ColorThemeType,
  children: React.ReactElement<HTMLDivElement>,
}

interface EnhancedProps {
  viewport: any,
  currentColorTheme: ColorThemeType,
  colorThemeRequest: (theme: ColorThemeType) => any,
}

export type AllProps = Props & EnhancedProps

type State = {
  savedTheme: ColorThemeType,
}

export class ColorTheme extends React.PureComponent<AllProps, State> {
  wrapper = React.createRef<HTMLSpanElement>()

  componentDidMount() {
    this.setState({ savedTheme: this.props.currentColorTheme })
    setTimeout(this.checkTheme)
    setTimeout(this.checkTheme, 500)
  }

  componentDidUpdate(prevProps: AllProps) {
    if (prevProps.viewport !== this.props.viewport) {
      this.checkTheme()
    }
  }

  componentWillUnmount() {
    this.props.colorThemeRequest(this.state.savedTheme)
  }

  checkTheme = () => {
    if (this.props.theme !== this.props.currentColorTheme) {
      const wrapper = this.wrapper.current
      if (!wrapper) return
      const bbox = wrapper.getBoundingClientRect()
      if (bbox.top <= 20 && bbox.bottom > 50) {
        // console.log('Use theme', this.props.theme)
        this.props.colorThemeRequest(this.props.theme)
      }
    }
  }

  render() {
    return <span className="ColorTheme" ref={this.wrapper}>{this.props.children}</span>
  }
}

const mapStateToProps = (state, props: Props) => ({
  viewport: getViewport(state),
  currentColorTheme: getColorTheme(state) as ColorThemeType,
})

const mapDispatchToProps = {
  colorThemeRequest,
}

const enhanced = connect(mapStateToProps, mapDispatchToProps)

export default enhanced(ColorTheme)
