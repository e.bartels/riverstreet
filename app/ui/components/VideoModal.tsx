import * as React from 'react'
import Modal from '~/ui/components/Modal'
import Markup from '~/ui/components/Markup'
import Video from '~/ui/components/Video'
import CloseButton from '~/ui/components/CloseButton'
import Spinner from '~/ui/components/Spinner'
import ColorTheme from '~/ui/components/ColorTheme'
import { Video as VideoType } from '~/types'
import { DEBUG } from '~/core/constants'

import './VideoModal.scss'

type Props = {
  video?: VideoType,
  caption?: string,
  moreLink?: React.ReactNode,
  onClose: () => void,
}

export const VideoModal = ({ video, caption, moreLink = null, onClose }: Props) => {
  const onBgClick = (e) => {
    if (!e.target.closest('.VideoModal__content')) {
      onClose()
    }
  }

  return (
    <div className="VideoModal" onClick={onBgClick}>
      <Modal onClose={onClose} className="VideoModal__Modal">
        <ColorTheme theme="white-white">
          <div className="VideoModal__wrap">
            <div className="VideoModal__content">
              {video ? (
                <CloseButton className="VideoModal__close" onClick={onClose} />
              ) : (
                <Spinner />
              )}
              {video ? (
                <Video
                  className="VideoModal__Video"
                  data={video}
                  autoplay={!DEBUG}
                />
              ) : null}
              {caption || moreLink ? (
                <div className="VideoModal__caption">
                  {caption && <Markup>{caption}</Markup>}
                  {moreLink && <div className="VideoModal__link">{moreLink}</div>}
                </div>
              ) : null}
            </div>
          </div>
        </ColorTheme>
      </Modal>
    </div>
  )
}

export default VideoModal
