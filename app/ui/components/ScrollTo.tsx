/* eslint-disable jsx-a11y/anchor-is-valid */
import * as React from 'react'
import scrollTo from 'scroll-to-element'


type Props = {
  to: string,
  duration?: number,
  offset?: number,
  ease?: string,
  className?: string,
  children?: React.ReactNode,
}

export class ScrollTo extends React.PureComponent<Props> {
  static defaultProps = {
    duration: 550,
    offset: 0,
    ease: 'in-out-quad',
  }

  handleClick = (e: React.MouseEvent) => {
    e.preventDefault()
    scrollTo(`#${this.props.to}`, {
      duration: this.props.duration,
      offset: this.props.offset,
      ease: this.props.ease,
    })
  }

  render() {
    return (
      <a
        className={this.props.className || ''}
        onClick={this.handleClick}
      >
        {this.props.children || null}
      </a>
    )
  }
}

export default ScrollTo
