import * as React from 'react'
import Icon from '~/ui/components/Icon'

import './CloseButton.scss'

type Props = {
  className?: string,
} & React.ButtonHTMLAttributes<HTMLButtonElement>

export const CloseButton = ({ className, ...props }: Props) => (
  <button
    className={`CloseButton ${className || ''} `}
    {...props}
  >
    <Icon name="close" />
  </button>
)

export default CloseButton
