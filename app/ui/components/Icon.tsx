import * as React from 'react'

// import './Icon.scss'

type Props = {
  name: string,
  className?: string,
  prefix?: string,
}

export const Icon = ({ name, className = 'riverstreet-icon', prefix = 'icon' }: Props) => (
  <i className={`Icon ${className} ${prefix}-${name}`} />
)

export default Icon
