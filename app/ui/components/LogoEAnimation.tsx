import * as React from 'react'
import anime from 'animejs'
import EventListener from '~/ui/components/EventListener'

type Props = {
  enabled?: boolean,
  children: React.ReactNode,
}

export class LogoEAnimation extends React.Component<Props> {
  e: HTMLElement | undefined
  timeout: number | undefined

  wrapper = React.createRef<HTMLSpanElement>()

  static defaultProps = {
    enabled: true,
  }

  componentDidMount() {
    const wrapper = this.wrapper.current as HTMLElement
    if (!wrapper) return
    this.e = wrapper.querySelector('svg .st0') as HTMLElement
  }

  rotate = (deg: number) => {
    if (!this.e) return
    this.e.style.transform = `rotate(${deg}deg)`
  }

  resetAnimation = () => {
    if (!this.e) return
    anime({
      targets: this.e,
      rotate: 0,
      duration: 3000,
      elasticity: 600,
    })
  }

  handleMouseMove = (e: MouseEvent) => {
    if (!this.e || !this.props.enabled) return
    const x = e.clientX
    const y = e.clientY
    const bbox = this.e.getBoundingClientRect()
    const radians = Math.atan2(bbox.top - y, bbox.left - x)
    const d = (radians / Math.PI) * 180

    // stop existing animation
    anime.remove(this.e)

    // rotate
    const deg = d > 100
      ? d - 180
      : d + 180
    this.rotate(deg)

    // timeout for reset animation
    if (this.timeout) {
      clearTimeout(this.timeout)
    }
    this.timeout = window.setTimeout(this.resetAnimation, 1500)
  }

  render() {
    return (
      <span ref={this.wrapper}>
        <EventListener target={document} event="mousemove" handler={this.handleMouseMove} />
        {this.props.children}
      </span>
    )
  }
}

export default LogoEAnimation
