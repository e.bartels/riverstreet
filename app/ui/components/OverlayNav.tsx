import * as React from 'react'
import { useEffect, useCallback } from 'react'
import { withRouter, RouteComponentProps } from 'react-router'
import { default as cx } from 'classnames'
import { useEventListener } from '~/ui/hooks/events'
import BodyClass from '~/ui/components/BodyClass'
import CircleLogo from '~/ui/components/CircleLogo'

import './OverlayNav.scss'

type Props = {
  isOpen: boolean,
  onClose: () => void,
  children: React.ReactNode,
} & RouteComponentProps

export const OverlayNav = ({ isOpen, onClose, children, location }: Props) => {
  // call onClose when location changes
  useEffect(() => {
    onClose()
  }, [location])

  // Handle escape key
  useEventListener('keydown', useCallback((e: KeyboardEvent) => {
    if (e.keyCode === 27) onClose()
  }, []))

  return (
    <div className={cx('OverlayNav', { 'OverlayNav--isOpen': isOpen })}>
      {isOpen ? <BodyClass className="OverlayNav--isOpen" /> : null }
      <div className="OverlayNav__background" onClick={onClose} />
      <div className="OverlayNav__container">
        <CircleLogo className="OverlayNav__logo" />
        {children}
      </div>
    </div>
  )
}

export default withRouter(OverlayNav)
