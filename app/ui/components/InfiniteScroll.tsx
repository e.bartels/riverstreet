import * as React from 'react'
import ScrollMonitor from '~/ui/components/ScrollMonitor'

/**
 * Handles innfinite scrolling pagination
 *
 * This uses cursor based pagingation
 * see: https://dev-blog.apollodata.com/understanding-pagination-rest-graphql-and-relay-b10f835549e7
 */

type Props = {
  loadMore?: () => any, // call to load more results
  hasMore: boolean, // are there more results?
  cursor?: string, // cursor for the current set of items
  offset: number, // offset from bottom of page to start next load
  children: React.ReactNode,
}

type State = {
  loadingCursor: string | null, // cursor that is currently loading
}

export class InfiniteScroll extends React.Component<Props, State> {
  static defaultProps = {
    offset: 200,
    hasMore: true,
    loadMore: () => undefined,
  }

  state = {
    loadingCursor: null,
  }

  handleScroll = (bbox: DOMRect) => {
    const { hasMore, loadMore, offset, cursor } = this.props
    const { loadingCursor } = this.state

    // If there are more elements to load
    if (hasMore && loadMore && loadingCursor !== cursor) {
      const height = window.innerHeight
      const trigger = bbox.bottom - height - offset
      if (Math.floor(trigger) <= 0) {
        this.setState({ loadingCursor: cursor || null })
        loadMore()
      }
    }
    else if (!hasMore) {
      this.setState({ loadingCursor: null })
    }
  }

  render() {
    return (
      <ScrollMonitor onScroll={this.handleScroll}>
        {this.props.children}
      </ScrollMonitor>
    )
  }
}

export default InfiniteScroll
