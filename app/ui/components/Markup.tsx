/* eslint react/no-danger: 0 */
import * as React from 'react'
import { withRouter, RouteComponentProps } from 'react-router'

type Props = RouteComponentProps & {
  children: string,
}

export class Markup extends React.PureComponent<Props> {
  ref = React.createRef<HTMLSpanElement>()

  static defaultProps = {
    children: '',
  }

  componentDidMount() {
    this.postRender()
  }

  componentDidUpdate(prevProps: Props) {
    if (prevProps.children !== this.props.children) {
      this.postRender()
    }
  }

  postRender = () => {
    this.attachLinkListeners()
  }

  attachLinkListeners = () => {
    const el = this.ref.current as HTMLElement
    if (el) {
      const anchors = el.querySelectorAll('a')
      const { location } = window
      Array.from(anchors)
        .filter(a => (
          a.pathname && a.host === location.host
        ))
        .forEach(a => {
          a.removeEventListener('click', this.handleAnchorClick)
          a.addEventListener('click', this.handleAnchorClick)
        })
    }
  }

  // Anchor click handler
  handleAnchorClick = (e: MouseEvent) => {
    const { history } = this.props
    e.preventDefault()
    const anchor = e.currentTarget as HTMLAnchorElement
    history.push(anchor.pathname)
  }

  renderContent() {
    const html = this.props.children
    const container = document.createElement('div')
    container.innerHTML = html

    // wrap iframes
    Array.from(container.querySelectorAll('iframe'))
      .filter(iframe => (
        /(youtube|vimeo)/.test(iframe.src)
      ))
      .forEach(iframe => {
        const wrapper = document.createElement('div')
        wrapper.classList.add('video-embed')
        const parent = iframe.parentNode
        if (!parent) return
        parent.insertBefore(wrapper, iframe)
        wrapper.appendChild(iframe)
      })
    return container.innerHTML
  }

  render() {
    return (
      <span
        className="Markup"
        ref={this.ref}
        dangerouslySetInnerHTML={{ __html: this.renderContent() }}
      />
    )
  }
}

export default withRouter(Markup)
