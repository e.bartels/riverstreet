import * as React from 'react'
import { connect } from 'react-redux'
import EventListener from '~/ui/components/EventListener'
import { viewportResize, viewportScroll } from '~/ui/actions'
import { Viewport } from '~/ui/reducer'
import { getViewport } from '~/ui/selectors'

type Props = {
  children?: React.ReactNode,
  viewport: Viewport,
  viewportResize: (args: { width: number, height: number }) => any,
  viewportScroll: (args: { y: number, x: number }) => any,
}

const win = window

// return window/viewport dimensions
const getViewportDimensions = () => ({
  width: win.innerWidth,
  height: (win.visualViewport ? win.visualViewport.height : win.innerHeight),
})

// return scroll position
const getScrollPosition = () => ({
  y: win.scrollY || win.pageYOffset,
  x: win.scrollX || win.pageXOffset,
})

export class ViewportListener extends React.PureComponent<Props> {
  componentDidMount() {
    this.handleResize()
    this.handleScroll()
  }

  handleResize = () => {
    this.props.viewportResize(getViewportDimensions())
  }

  handleScroll = () => {
    this.props.viewportScroll({
      ...getScrollPosition(),
    })

    const { viewport } = this.props
    const dimensions = getViewportDimensions()
    if (viewport.height !== dimensions.height || viewport.width !== dimensions.width) {
      this.props.viewportResize(dimensions)
    }
  }

  render() {
    return (
      <>
        <EventListener event="resize" handler={this.handleResize} />
        <EventListener event="scroll" handler={this.handleScroll} />
        {this.props.children || null}
      </>
    )
  }
}

export const connected = connect(
  state => ({ viewport: getViewport(state) }),
  { viewportResize, viewportScroll },
)

export default connected(ViewportListener)
