import * as React from 'react'
import detectPassive from 'detect-passive-events'
import { connect } from 'react-redux'
import { touchDetected } from '~/ui/actions'

type Props = {
  children?: React.ReactNode,
  touchDetected: () => any,
}

export class DetectTouch extends React.Component<Props> {
  touchDetected = () => {
    this.props.touchDetected()
    if (document.body) {
      document.body.classList.add('is-touch')
    }
  }

  componentDidMount() {
    window.addEventListener(
      'touchstart',
      this.touchDetected,
      detectPassive.hasSupport ? { passive: true } : undefined,
    )
  }

  componentWillUnmount() {
    window.removeEventListener('touchstart', this.touchDetected)
  }

  render() {
    return this.props.children || null
  }
}

export default connect(null, {
  touchDetected,
})(DetectTouch)
