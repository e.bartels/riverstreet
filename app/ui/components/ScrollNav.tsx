import * as React from 'react'

import './ScrollNav.scss'

export type Props = {
  className?: string,
  children: React.ReactNode,
}

export const ScrollNav = ({ children, className }: Props) => (
  <nav className={`ScrollNav ${className}`}>
    {children}
  </nav>
)

export default ScrollNav
