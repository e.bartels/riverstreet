import * as React from 'react'
import MDIcon from '~/ui/components/MDIcon'

// import './ShareLinks.scss'


type Props = {
  url: string,
  title: string,
}


const shareLink = (url) => (
  `${window.location.origin}${url}`
)

export const ShareLinks = ({ url, title }: Props) => (
  <span className="ShareLinks">
    <a
      href={`https://facebook.com/sharer/sharer.php?u=${shareLink(url)}`}
      target="_blank"
      rel="noopener noreferrer">
      share <MDIcon name="facebook" />
    </a>
    <a
      href={`https://twitter.com/intent/tweet/?text=${title}&amp;${shareLink(url)}`}
      target="_blank"
      rel="noopener noreferrer">
      tweet <MDIcon name="twitter" />
    </a>
  </span>
)

export default ShareLinks
