import * as React from 'react'
import Headroom from 'react-headroom'
import CircleLogo from '~/ui/components/CircleLogo'
import LogoAnimation from '~/ui/components/LogoAnimation'

import './MainLogo.scss'

type Props = {
}

type State = {
  animationComplete: boolean,
}

class MainLogo extends React.Component<Props, State> {
  state = {
    animationComplete: false,
  }

  handleAnimationComplete = () => {
    this.setState({ animationComplete: true })
  }

  render() {
    return (
      <LogoAnimation onAnimationComplete={this.handleAnimationComplete}>
        <Headroom className="MainLogo">
          <CircleLogo animate={this.state.animationComplete} />
        </Headroom>
      </LogoAnimation>
    )
  }
}

export default MainLogo
