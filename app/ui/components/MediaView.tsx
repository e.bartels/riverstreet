import * as React from 'react'
import { connect } from 'react-redux'
import MediaItem from '~/ui/components/MediaItem'
import SlideView from '~/ui/components/SlideView'
import BodyClass from '~/ui/components/BodyClass'
import ColorTheme from '~/ui/components/ColorTheme'
import Icon from '~/ui/components/Icon'
import { getViewport } from '~/ui/selectors'
import { Image, Video } from '~/types'

import './MediaView.scss'

type ImageOrVideo = Image | Video

type Props = {
  items: ImageOrVideo[],
  height: number,
  transition: number,
  onShowItem: (i: string) => void,
  onClose: () => void,
  caption?: string,
  activeId?: string | null,
}

export const MediaView = ({ items, height, transition, onShowItem, onClose, caption = '', ...props }: Props) => {
  const activeId = props.activeId || items[0].id
  const activeIndex = items.findIndex(i => i.id === activeId)

  const handleShowItem = (index: number) => {
    const { id } = items[index]
    if (items.length > 1 && id !== activeId) {
      onShowItem(id)
    }
  }

  const onBgClick = (e) => {
    if (!e.target.closest('.MediaItem__content')) {
      onClose()
    }
  }

  return (
    <BodyClass className="MediaView--active">
      <div className="MediaView" style={{ height }} onClick={onBgClick}>
        <SlideView
          activeIndex={activeIndex}
          onShowItem={handleShowItem}
          orientation="horizontal"
          transition={transition}
          nextButton={<Icon name="angle-right" />}
          prevButton={<Icon name="angle-left" />}
        >
          {items.map((item, i) => (
            <ColorTheme theme="white-white" key={item.id}>
              <MediaItem
                item={item}
                isActive={i === activeIndex}
                caption={caption}
                onClose={onClose}
              />
            </ColorTheme>
          ))}
        </SlideView>
      </div>
    </BodyClass>
  )
}

export default connect(state => {
  // Use different transition based on viewport size
  const { height, isMobile, isTablet } = getViewport(state)
  const transition = isMobile ? 200 : (isTablet ? 280 : 350)
  return {
    transition,
    height,
  }
})(MediaView)
