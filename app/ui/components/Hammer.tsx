import * as React from 'react'
import Hammer from 'hammerjs'

export { Hammer as hammer }

type Handler = (e: Event) => void

type Props = {
  children: React.ReactNode,
  className?: string,
  options?: any,

  onSwipe?: Handler,
  swipeOpts?: any,

  onPan?: Handler,
  panOpts?: any,

  onTap?: Handler,
  tapOpts?: any,

  onDoubletap?: Handler,
  doubletapOpts?: any,

  onPress?: Handler,
  pressOpts?: any,

  onPinch?: Handler,
  pinchOpts?: any,

  onRotate?: Handler,
  rotateOpts?: any,
}

const HAMMER_ACTIONS = [
  'Swipe',
  'Pan',
  'Tap',
  'Doubletap',
  'Press',
  'Pinch',
  'Rotate',
]

export class HammerComponent extends React.PureComponent<Props> {
  hammer: any

  wrapper = React.createRef<HTMLDivElement>()

  componentDidMount() {
    this.setup()
  }

  componentWillUnmount() {
    this.destroy()
  }

  setup = () => {
    if (this.hammer) {
      this.destroy()
    }

    const wrapper = this.wrapper.current
    if (!wrapper) return
    this.hammer = new Hammer(wrapper, this.props.options)

    // Set up handlers
    HAMMER_ACTIONS.forEach(actionName => {
      const action = actionName.toLowerCase() // action name (lowercase)
      const handlerProp = `on${actionName}` // handler prop
      const recognizer = this.hammer.get(action) // get hammer recognizer

      // disabled handlers
      if (!this.props[handlerProp]) {
        recognizer.set({ enable: false })
      }
      // enabled handlers
      else {
        const optsProp = `${action}Opts`
        const opts = this.props[optsProp]
        const handler = this.props[handlerProp]
        recognizer.set({ enable: true, ...opts })

        // Set up handlers
        ;['', 'start', 'end', 'cancel'].forEach(event => {
          this.hammer.on(`${action}${event}`, handler)
        })
      }
    })
  }

  destroy = () => {
    if (this.hammer) {
      this.hammer.destroy()
    }
  }

  render() {
    return (
      <div
        className={this.props.className || 'Hammer'}
        ref={this.wrapper}>
        {this.props.children}
      </div>
    )
  }
}

export default HammerComponent
