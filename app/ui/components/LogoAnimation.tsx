import * as React from 'react'
import { connect } from 'react-redux'
import { withRouter, RouteComponentProps } from 'react-router'
import anime from 'animejs'
import BodyClass from '~/ui/components/BodyClass'
import EventListener from '~/ui/components/EventListener'
import { DEBUG } from '~/core/constants'
import { getViewport } from '~/ui/selectors'

type Props = {
  shouldAnimate?: boolean,
  viewportWidth: number,
  viewportHeight: number,
  onAnimationComplete: () => void,
  children: React.ReactNode,
}

type EnhancedProps = {
  shouldAnimate?: boolean,
  onAnimationComplete: () => void,
  children: React.ReactNode,
}

type State = {
  animating: boolean,
  animationComplete: boolean,
}


export class LogoAnimation extends React.PureComponent<Props, State> {
  wrapper = React.createRef<HTMLSpanElement>()

  static defaultProps = {
    onAnimationComplete: () => undefined,
  }

  state = {
    animating: false,
    animationComplete: false,
  }

  componentDidMount() {
    if (this.props.shouldAnimate) {
      this.startAnimation()
    }
    else {
      this.handleAnimationComplete()
    }
  }

  startAnimation = () => {
    if (this.state.animating) {
      return
    }

    const wrapper = this.wrapper.current
    if (!wrapper) return

    const { viewportWidth, viewportHeight } = this.props

    const fadeDuration = 800
    const spinDuration = 1100
    const moveDuration = 1400

    const size = Math.min(viewportWidth * 1.7, 1220)

    // Finish any ongoing animations
    if (!wrapper) {
      return
    }

    const logo = wrapper.querySelector('.CircleLogo')
    if (!logo) return

    const e = logo.querySelector('svg .st0')
    if (!e) return

    this.setState({ animating: true })

    // get ending animation props based on css values
    const logoStyle = window.getComputedStyle(logo)
    const initialProps = {
      width: logoStyle.getPropertyValue('width'),
      height: logoStyle.getPropertyValue('height'),
      borderWidth: logoStyle.getPropertyValue('border-top-width'),
    }

    const finalTranslate = viewportWidth >= 1370 ? -48 : -32

    anime({
      targets: e,
      rotateZ: [
        { value: 225, duration: 0 },
        {
          value: -360 * 3,
          duration: moveDuration + spinDuration,
          easing: 'easeOutQuad',
        },
      ],
      complete: () => {
        e.removeAttribute('style')
      },
    })

    anime({
      targets: logo,
      easing: 'easeInOutQuad',

      // Size
      width: [
        { value: size, duration: 0 },
        { value: initialProps.width, duration: moveDuration, delay: spinDuration },
      ],
      height: [
        { value: size, duration: 0 },
        { value: initialProps.height, duration: moveDuration, delay: spinDuration },
      ],
      borderWidth: [
        { value: size * 0.16, duration: 0 },
        { value: initialProps.borderWidth, duration: moveDuration, delay: spinDuration },
      ],

      // Fade
      opacity: [
        { value: 0, duration: 0 },
        { value: 1, duration: fadeDuration, delay: 200, easing: 'easeInCubic' },
      ],

      // Position
      translateX: [
        { value: (viewportWidth - size) / 2, duration: 0 },
        { value: finalTranslate, duration: moveDuration, delay: spinDuration },
      ],
      translateY: [
        { value: (viewportHeight - size) / 2, duration: 0 },
        { value: finalTranslate, duration: moveDuration, delay: spinDuration },
      ],

      // Spin
      rotateZ: [
        { value: 0, duration: 0 },
        { value: -45 + (360 * 3), duration: moveDuration + spinDuration, easing: 'easeOutQuad' },
      ],

      complete: () => {
        logo.removeAttribute('style')
      },
    })

    setTimeout(() => {
      this.handleAnimationComplete()
    }, spinDuration + (moveDuration * 0.70))
  }

  handleAnimationComplete = () => {
    this.setState({ animating: false, animationComplete: true })
    this.props.onAnimationComplete()
  }

  handleKeydown = (event: KeyboardEvent) => {
    // trigger animation to run (useful for testing)
    if (event.keyCode === 32 && event.ctrlKey) { // ctrl-space
      event.preventDefault()
      this.startAnimation()
    }
  }

  render() {
    const { animating, animationComplete } = this.state
    return (
      <span ref={this.wrapper}>
        <EventListener event="keydown" handler={this.handleKeydown} />
        {!animating && animationComplete ? <BodyClass className="LogoAnimation--animation-complete" /> : null}
        {this.props.children}
      </span>
    )
  }
}

type WithRouteProps = EnhancedProps & RouteComponentProps

const mapStateToProps = (state, props: WithRouteProps) => {
  const viewport = getViewport(state)
  const shouldAnimate = (
    (props.shouldAnimate !== undefined ? props.shouldAnimate : !DEBUG) &&
    props.location.pathname === '/'
  )
  return {
    viewportWidth: viewport.width,
    viewportHeight: viewport.height,
    shouldAnimate,
  }
}

export default withRouter(connect(mapStateToProps)(LogoAnimation))
