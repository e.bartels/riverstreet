import * as React from 'react'
import EventListener from '~/ui/components/EventListener'

type Props = {
  children: React.ReactNode,
  onScroll: (arg: DOMRect) => void,
}

export class ScrollMonitor extends React.PureComponent<Props> {
  ref = React.createRef<HTMLDivElement>()

  handleScroll = () => {
    const el = this.ref.current
    if (el) {
      const bbox = el.getBoundingClientRect() as DOMRect
      this.props.onScroll(bbox)
    }
  }

  render() {
    return (
      <div ref={this.ref}>
        <EventListener event="scroll" handler={this.handleScroll} />
        {this.props.children}
      </div>
    )
  }
}

export default ScrollMonitor
