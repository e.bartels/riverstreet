import * as React from 'react'
import { useEventListener } from '~/ui/hooks/events'


/**
 * EventListener component for global (e.g. window) events
 *
 * Usage:
 * <EventListener event="keydown" handler={myHandler}> ...  </EventListener>
 */
type Props = {
  event: string,
  handler: (e?: any) => void,
  target?: EventTarget,
  opts?: boolean | AddEventListenerOptions,
  children?: React.ReactNode,
}

export const EventListener = ({ event, handler, target, opts, children }: Props) => {
  useEventListener(event, handler, target, opts)
  return <>{children}</>
}

export default EventListener
