import * as React from 'react'
import { Link } from 'react-router-dom'
import LogoSingleLine from '~/styles/icons/logo-single-line.svg'

import './TextLogo.scss'

type Props = {
  className?: string,
} & React.HTMLAttributes<HTMLElement>

export const TextLogo = ({ className = '', ...props }: Props) => (
  <h1 className={`TextLogo ${className}`} {...props}>
    <Link to="/"><LogoSingleLine width="160" /></Link>
  </h1>
)

export default TextLogo
