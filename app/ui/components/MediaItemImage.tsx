import * as React from 'react'
import Image from '~/ui/components/Image'
import { Image as ImageType } from '~/types'

type Props = {
  item: ImageType,
}

export const MediaItemImage = ({ item }: Props) => (
  <Image
    image={item}
    sizes={[
      '(max-width: 739px) calc(100vw - 40px)',
      '(max-width: 899px) calc(100vw - 320px)',
      'calc(100vw - 380px)',
      '100vw',
    ].join(', ')}
    style={{
      maxWidth: item.images.large.width,
      maxHeight: item.images.large.height,
    }}
  />
)

export default MediaItemImage
