import * as React from 'react'
import WheelIndicator from 'wheel-indicator'

type Props = {
  children: React.ReactNode,
  onWheel: (e: any) => void,
  preventMouse?: boolean,
  className?: string,
}

export class WheelHandler extends React.PureComponent<Props> {
  wheelIndicator: any

  wrapper = React.createRef<HTMLSpanElement>()

  static defaultProps = {
    preventMouse: true,
  }

  componentDidMount() {
    const wrapper = this.wrapper.current as HTMLElement
    this.wheelIndicator = new WheelIndicator({
      elem: wrapper,
      callback: this.handleMouseWheel,
      preventMouse: this.props.preventMouse,
    })
  }

  componentWillUnmount() {
    if (this.wheelIndicator) {
      this.wheelIndicator.destroy()
    }
  }

  handleMouseWheel = (e: Event) => {
    this.props.onWheel(e)
  }

  render() {
    return (
      <span
        className={this.props.className || 'WheelHandler'}
        ref={this.wrapper}
      >
        { this.props.children || null }
      </span>
    )
  }
}

export default WheelHandler
