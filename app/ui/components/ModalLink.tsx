import * as React from 'react'
import { Link, LinkProps } from 'react-router-dom'

type Props = {
  to: string | any,
  onClick?: (e: React.MouseEvent) => void,
} & LinkProps

export const ModalLink = ({ to, ...props }: Props) => (
  <Link
    {...props}
    to={typeof to === 'string' ? ({
      pathname: to,
      state: { modal: true },
    }) : ({
      ...to,
      state: { modal: true, ...(to.state || {}) },
    })}
  />
)

export default ModalLink
