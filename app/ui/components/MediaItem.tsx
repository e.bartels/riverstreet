/* eslint react/no-did-mount-set-state: off */
import * as React from 'react'
import { default as cx } from 'classnames'
import { withRouter, RouteComponentProps } from 'react-router'
import queryString from 'query-string'
import CloseButton from '~/ui/components/CloseButton'
import MediaItemImage from '~/ui/components/MediaItemImage'
import MediaItemVideo from '~/ui/components/MediaItemVideo'
import Markup from '~/ui/components/Markup'
import { Image, Video } from '~/types'

import './MediaItem.scss'

type Props = RouteComponentProps & {
  item: Image | Video,
  isActive: boolean,
  caption?: string,
  onClose: () => void,
}

type State = {
  videoPlaying: boolean,
}

export class MediaItem extends React.PureComponent<Props, State> {
  state: State = {
    videoPlaying: false,
  }

  componentDidMount() {
    const { location, isActive } = this.props
    const query = queryString.parse(location.search)
    this.setState({ videoPlaying: isActive && query.play !== undefined })
  }

  static getDerivedStateFromProps(props: Props, state: State) {
    if (!props.isActive) {
      return { videoPlaying: false }
    }
    return null
  }

  handlePlayClick = (e: React.MouseEvent) => {
    e.preventDefault()
    e.stopPropagation()
    this.setState({ videoPlaying: true })
  }

  render() {
    const { item, onClose } = this.props
    const caption = this.props.caption || item.caption
    return (
      <div className={cx('MediaItem', `type-${item.type}`)}>
        <div className="MediaItem__wrap">
          <div className="MediaItem__content">
            <CloseButton className="MediaItem__close" onClick={onClose} />

            {item.type === 'video' ? (
              <MediaItemVideo
                item={item}
                onPlayClick={this.handlePlayClick}
                isPlaying={this.state.videoPlaying}
              />
            ) : null}

            {item.type === 'image' ? (
              <MediaItemImage item={item} />
            ) : null}

            {caption ? (
              <div className="MediaItem__caption">
                <Markup>{caption}</Markup>
              </div>
            ) : null}
          </div>
        </div>
      </div>
    )
  }
}

export default withRouter(MediaItem)
