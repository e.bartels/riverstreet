import * as React from 'react'
import Icon from '~/ui/components/Icon'
import Video from '~/ui/components/Video'
import Image from '~/ui/components/Image'
import { Video as VideoType } from '~/types'
import { DEBUG } from '~/core/constants'

type Props = {
  item: VideoType,
  onPlayClick?: (e: React.MouseEvent) => void,
  isPlaying?: boolean,
}

export const MediaItemVideo = ({ item, ...props }: Props) => (
  props.isPlaying
    ? (
      <Video
        data={item}
        maxWidth={1100}
        autoplay={!DEBUG}
      />
    ) : (
      <div className="video-image">
        <Image
          image={item}
          sizes={[
            '(max-width: 767px) calc(100vw - 40px)',
            '(max-width: 899px) calc(100vw - 320px)',
            'calc(100vw - 380px)',
            '100vw',
          ].join(', ')}
        />
        <button
          className="play-button"
          onClick={props.onPlayClick}>
          <Icon name="play" />
        </button>
      </div>
    )
)

export default MediaItemVideo
