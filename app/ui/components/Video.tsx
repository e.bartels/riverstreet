/* eslint react/no-danger: 0 */
import * as React from 'react'
import queryString from 'query-string'
import EventListener from '~/ui/components/EventListener'

type Props = {
  data: {
    provider: string,
    width: number,
    height: number,
    embed: string,
  },
  maxWidth?: number,
  autoplay?: boolean,
  className?: string,
}

type State = {
  maxWidth: number,
  maxHeight: number,
}

export class Video extends React.PureComponent<Props, State> {
  ref = React.createRef<HTMLDivElement>()

  state = {
    maxWidth: 0,
    maxHeight: 0,
  }

  static videoOptions = {
    youtube: {
      autoplay: 0,
      modestbranding: 1,
      showinfo: 0,
    },
    vimeo: {
      autoplay: 0,
      badge: 0,
      byline: 0,
      portrait: 0,
      title: 0,
    },
  }

  componentDidMount() {
    this.setVideoSrc()
    setTimeout(this.handleResize, 100)
  }

  setVideoSrc = () => {
    const el = this.ref.current as HTMLElement
    if (el) {
      // get video iframe and alter src and set options
      const iframe = el.querySelector('iframe')
      if (iframe) {
        const iframeSrc = iframe.getAttribute('src') as string
        const [url, querystring] = iframeSrc.split('?')
        const query = queryString.parse(querystring)
        const provider = this.props.data.provider.toLowerCase()
        const options = Object.assign(
          Video.videoOptions[provider],
          { autoplay: this.props.autoplay ? 1 : 0 },
        )
        if (options) {
          Object.assign(query, options)
        }
        iframe.setAttribute('src', `${url}?${queryString.stringify(query)}`)
      }
    }
  }

  handleResize = () => {
    const el = this.ref.current
    if (el) {
      const parent = el.parentElement
      if (parent) {
        const maxWidth = Math.min(parent.offsetWidth, this.props.maxWidth || 1920)
        const maxHeight = parent.scrollHeight
        this.setState({ maxWidth, maxHeight })
      }
    }
  }

  get style() {
    const { maxWidth, maxHeight } = this.state
    const ratio = this.props.data.width / this.props.data.height
    let width = maxWidth
    let height = width / ratio

    if (width > maxWidth) {
      width = maxWidth
      height = width / ratio
    }
    if (height > maxHeight) {
      height = maxHeight
      width = height * ratio
    }

    return { width, height }
  }

  render() {
    return (
      <>
        <EventListener event="resize" handler={this.handleResize} />
        <div
          ref={this.ref}
          className={`Video video-embed ${this.props.className}`}
          style={this.style}
          dangerouslySetInnerHTML={{ __html: this.props.data.embed }}
        />
      </>
    )
  }
}

export default Video
