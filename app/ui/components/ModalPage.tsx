import * as React from 'react'
import Modal from '~/ui/components/Modal'
import BodyClass from '~/ui/components/BodyClass'

import './ModalPage.scss'

type Props = {
  children: React.ReactNode,
  onClose: () => void,
}

export const ModalPage = ({ children, onClose }: Props) => (
  <div className="ModalPage">
    <BodyClass className="ModalPage--isOpen" />
    <div className="ModalPage__wrap container">
      <Modal onClose={onClose}>
        <div className="ModalPage__content">
          {children}
        </div>
      </Modal>
    </div>
  </div>
)

export default ModalPage
