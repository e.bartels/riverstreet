import * as React from 'react'
import { Link } from 'react-router-dom'
import Logo from '~/styles/icons/logo.svg'
import LogoEAnimation from '~/ui/components/LogoEAnimation'

import './CircleLogo.scss'

type Props = {
  className?: string,
  animate?: boolean,
} & React.HTMLAttributes<HTMLElement>

export const CircleLogo = ({ className = '', animate = true, ...props }: Props) => (
  <LogoEAnimation enabled={animate}>
    <h1 className={`CircleLogo ${className}`} {...props}>
      <Link to="/"><Logo width="150" /></Link>
    </h1>
  </LogoEAnimation>
)

export default CircleLogo
