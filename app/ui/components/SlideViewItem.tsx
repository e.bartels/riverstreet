import * as React from 'react'

type Props = {
  children: React.ReactNode,
  width: number,
  height: number,
  index: number,
  placement: number,
  orientation: 'vertical' | 'horizontal',
}

export class SlideViewItem extends React.PureComponent<Props> {
  render() {
    const { children, width, height, index, placement } = this.props

    const transform = this.props.orientation === 'vertical'
      ? `translate3d(0, ${index * height}px, 0)`
      : `translate3d(${index * width}px, 0, 0)`

    return (
      <div
        className="SlideView__item"
        style={{
          width: width || '',
          height: height || '',
          zIndex: placement === 0 ? 2 : 1,
          transform,
        }}>
        {Math.abs(placement) <= 1 ? children : null}
      </div>
    )
  }
}

export default SlideViewItem
