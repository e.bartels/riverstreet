import * as React from 'react'
import { default as cx } from 'classnames'
import memoize from 'lodash/memoize'
import throttle from 'lodash/throttle'
import Hammer, { hammer } from '~/ui/components/Hammer'
import EventListener from '~/ui/components/EventListener'
import WheelHandler from '~/ui/components/WheelHandler'
import SlideViewItem from './SlideViewItem'

import './SlideView.scss'


type Props = {
  children: React.ReactNode,
  activeIndex: number,
  onShowItem: (i: number) => void,
  orientation: 'vertical' | 'horizontal',
  transition: number,
  disableNavigation?: boolean,
  nextButton?: React.ReactNode,
  prevButton?: React.ReactNode,
}

type State = {
  prevIndex: number,
  offset: number,
  animating: { forceComplete: Function } | false,
  width: number,
  height: number,
}

export class SlideView extends React.PureComponent<Props, State> {
  static defaultProps = {
    activeIndex: 0,
    orientation: 'vertical',
    transition: 300,
  }

  state: State = {
    prevIndex: this.props.activeIndex,  // eslint-disable-line
    offset: 0,
    animating: false,
    width: 0,
    height: 0,
  }

  containerRef = React.createRef<HTMLDivElement>()

  // lifecycle
  componentDidMount() {
    setTimeout(this.setDimensions)
  }

  static getDerivedStateFromProps(props: Props, state: State) {
    // Set offset to 0 when activeIndex is updated
    if (props.activeIndex !== state.prevIndex) {
      return {
        prevIndex: props.activeIndex,
        offset: 0,
      }
    }
    return null
  }

  setDimensions = () => {
    const container = this.containerRef.current
    if (container instanceof HTMLElement) {
      const bbox = container.getBoundingClientRect()
      this.setState({
        width: bbox.width,
        height: bbox.height,
      })
    }
  }

  // Actions
  goTo = (i: number) => {
    this.props.onShowItem(i)
  }

  goFirst = () => {
    this.goTo(0)
  }

  goLast = () => {
    this.goTo(this.count - 1)
  }

  slideNext = () => {
    this.slideTo(1)
  }

  slidePrev = () => {
    this.slideTo(-1)
  }

  slideTo = (direction: number) => {
    // bail if disabled
    if (this.props.disableNavigation) {
      return
    }

    // if still animating, then end the animation
    if (this.state.animating) {
      this.state.animating.forceComplete()
    }

    const goingForwards = direction > 0
    const goingBackwards = direction < 0
    const atStart = this.activeIndex === 0
    const atEnd = this.activeIndex === this.count - 1

    // handle last/first items
    if ((goingForwards && atEnd) || (goingBackwards && atStart)) {
      this.slideTo(0)
      return
    }

    // Calculate new item's offset (will be animated to using css transition)
    const size = this.isVertical
      ? this.state.height
      : this.state.width
    const offset = size * (direction * -1)

    // On completing slide animation
    const onComplete = () => {
      this.setState({ animating: false })
      if (goingForwards) this.goTo(this.nextIndex)
      else if (goingBackwards) this.goTo(this.prevIndex)
    }

    // Promise which can be run early
    const timer = setTimeout(onComplete, this.props.transition + 1)

    this.setState({
      offset,
      animating: {
        forceComplete: () => { clearTimeout(timer); onComplete() },
      },
    })
  }

  // Getters
  getIndex(i: number) {
    return Math.min(this.count - 1, Math.max(0, i))
  }

  getItems = memoize(children => React.Children.toArray(children))

  get items(): Array<React.ReactNode> {
    return this.getItems(this.props.children)
  }

  get count(): number {
    return this.items.length
  }

  get activeIndex(): number {
    return this.props.activeIndex
  }

  get nextIndex(): number {
    return this.getIndex(this.props.activeIndex + 1)
  }

  get prevIndex(): number {
    return this.getIndex(this.props.activeIndex - 1)
  }

  get isVertical() {
    return this.props.orientation === 'vertical'
  }

  getPlacement = (i: number): number => {
    if (this.activeIndex === i) {
      return 0
    }
    let placement = i - this.props.activeIndex
    if (placement >= this.count - 1) {
      placement -= this.count
    }
    else if (placement <= (-1 * this.count) + 1) {
      placement += this.count
    }

    return placement
  }

  // Event handlers
  handleResize = () => {
    this.setDimensions()
  }

  handleKeyDown = throttle((e: KeyboardEvent) => {
    switch (e.keyCode) {
      case 40: // down | pgdown | right
      case 34:
      case 39:
      case 74:
        this.slideNext()
        break
      case 38: // up | pgup | left
      case 33:
      case 37:
      case 75:
        this.slidePrev()
        break
      case 36: // home
        this.goFirst()
        break
      case 35: // end
        this.goLast()
        break
      default:
        break
    }
  }, 100)

  handlePrevClick = (e: React.MouseEvent) => {
    e.stopPropagation()
    this.slidePrev()
  }

  handleNextClick = (e: React.MouseEvent) => {
    e.stopPropagation()
    this.slideNext()
  }

  handleMouseWheel = (e: any) => {
    if (e.direction === 'down') {
      this.slideNext()
    }
    else {
      this.slidePrev()
    }
  }

  handlePan = (e: any) => {
    if (this.count < 2) {
      return
    }

    if (this.props.disableNavigation) {
      return
    }

    // ignore mouse
    if (e.pointerType === 'mouse') {
      return
    }

    const dragStartThresh = 20 // minimum drag amount to start
    const size = this.isVertical ? this.state.height : this.state.width
    const navThresh = size * 0.30 // minimum drag amount to trigger nav
    const navVelcotyThresh = 0.30 // minimum velocity to trigger nav

    const dragDelta = this.isVertical ? e.deltaY : e.deltaX
    const direction = dragDelta < 0
      ? (this.isVertical ? 'up' : 'left')
      : (this.isVertical ? 'down' : 'right')

    // dragging
    if (e.type === 'pan') {
      const dragActiviated = Math.abs(dragDelta) > dragStartThresh
      if (dragActiviated) {
        // Calculate amount to move the dragged item by
        // Movement is less for first/last elements
        const i = this.activeIndex
        const n = this.count
        const firstOrLast = (
          (i === n - 1 && (direction === 'up' || direction === 'left')) ||
          (i === 0 && (direction === 'down' || direction === 'right'))
        )
        const offset = firstOrLast
          ? dragDelta * 0.08
          : dragDelta * 0.6
        this.setState({ offset })
      }
    }
    // drag ended
    else if (e.type === 'panend') {
      // Calculate if drag amount or velocity is enough to trigger navigating
      const velocity = this.isVertical ? e.overallVelocityY : e.overallVelocityX
      const navActivated = (
        Math.abs(dragDelta) > navThresh ||
        Math.abs(velocity) > navVelcotyThresh
      )
      // If navigation should be done
      if (navActivated) {
        if (direction === 'up' || direction === 'left') {
          this.slideNext()
        }
        else {
          this.slidePrev()
        }
      }
      // otherwise, return item back into position
      else if (this.state.offset !== 0) {
        this.slideTo(0)
      }
    }
  }

  render() {
    const items = this.items
      .map((item, i) => ({
        item,
        index: i,
        placement: this.getPlacement(i),
      }))
      .filter(item => Math.abs(item.placement) <= 1)

    const size = this.isVertical ? this.state.height : this.state.width
    const offset = (size * this.activeIndex * -1) + this.state.offset
    const transition = this.state.animating ? this.props.transition : 0
    const transform = this.isVertical
      ? `translate3d(0, ${offset}px, 0)`
      : `translate3d(${offset}px, 0, 0)`

    return (
      <Hammer
        className="SlideView__hammer"
        onPan={this.handlePan}
        panOpts={{
          direction: hammer.DIRECTION_ALL,
        }}>
        <EventListener event="keydown" handler={this.handleKeyDown} />
        <EventListener event="resize" handler={this.handleResize} />
        <WheelHandler onWheel={this.handleMouseWheel}>
          <div
            ref={this.containerRef}
            className={cx('SlideView', {
              'SlideView--nav-disabled': this.props.disableNavigation,
              'SlideView--single-item': this.count < 2,
              'SlideView--at-start': this.activeIndex === 0,
              'SlideView--at-end': this.activeIndex === this.count - 1,
            })}
          >
            <div
              className="SlideView__slider"
              style={{
                height: this.state.height || '',
                width: this.state.width || '',
                transform,
                transition: transition ? `transform ${transition}ms` : 'none',
              }}>
              {items.map(({ item, placement, index }) => (
                <SlideViewItem
                  key={index}
                  width={this.state.width}
                  height={this.state.height}
                  index={index}
                  placement={placement}
                  orientation={this.props.orientation}
                >
                  {item}
                </SlideViewItem>
              ))}
            </div>

            <button className="SlideView__control SlideView__control--prev" onClick={this.handlePrevClick}>
              <span>{this.props.prevButton || 'Previous'}</span>
            </button>

            <button className="SlideView__control SlideView__control--next" onClick={this.handleNextClick}>
              <span>{this.props.nextButton || 'Next'}</span>
            </button>
          </div>
        </WheelHandler>
      </Hammer>
    )
  }
}

export default SlideView
