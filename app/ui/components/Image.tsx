import * as React from 'react'
import { useState } from 'react'
import { default as cx } from 'classnames'
import { SizedImageSpec } from '~/types'

export type Props = {
  image: {
    images: SizedImageSpec,
    altText?: string,
  },
  src?: string,
  sizes?: string,
  className?: string,
} & React.ImgHTMLAttributes<HTMLImageElement>

// Handle image load state
const useLoading = () => {
  const [isLoading, setIsLoading] = useState(true)
  const finishedLoading = () => setIsLoading(false)
  return [isLoading, finishedLoading] as [boolean, () => void]
}

export const Image = ({ image, src, sizes, className, ...props }: Props) => {
  const [isLoading, finishedLoading] = useLoading()

  return (
    <img
      src={src || image.images.large.url}
      srcSet={Object.values(image.images).map(img => (
        `${img.url} ${img.width}w`
      )).join(', ')}
      sizes={sizes || '100vw'}
      alt={image.altText}
      className={cx('Image', className, { isLoading })}
      onLoad={finishedLoading}
      {...props}
    />
  )
}

export default React.memo<Props>(Image)
