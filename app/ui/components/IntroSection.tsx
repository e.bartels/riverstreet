import * as React from 'react'
import { useRef, useCallback } from 'react'
import { connect } from 'react-redux'
import scrollTo from 'scroll-to-element'
import WheelHandler from '~/ui/components/WheelHandler'
import { useEventListener } from '~/ui/hooks/events'
import ColorTheme from '~/ui/components/ColorTheme'
import Icon from '~/ui/components/Icon'
import HideOnScroll from '~/ui/components/HideOnScroll'
import { getViewport } from '~/ui/selectors'

import './IntroSection.scss'

type Props = {
  children: React.ReactNode,
  height: number,
  scrollY: number,
  className?: string,
  showDownArrow?: boolean,
  useMouseWheel?: boolean,
}

export const IntroSection = ({ children, height, scrollY, className = '', showDownArrow = true, useMouseWheel = false }: Props) => {
  const containerRef = useRef<HTMLDivElement>(null)

  // Scroll to content after this component
  const scrollToNext = () => {
    const container = containerRef.current
    if (!container) {
      return
    }
    if (scrollY > 50) {
      return
    }

    scrollTo(container, {
      duration: 550,
      ease: 'in-out-quad',
      offset: container.scrollHeight,
    })
  }

  // Mouse wheel handler
  const handleWheel = (e: any) => {
    if (!useMouseWheel) return
    if (e.direction === 'down') {
      scrollToNext()
    }
  }

  // Keyboard handler
  useEventListener('keydown', useCallback((e: KeyboardEvent) => {
    if (e.keyCode === 40 || e.keyCode === 34) { // down, pgdown
      scrollToNext()
    }
  }, []))

  return (
    <WheelHandler onWheel={handleWheel} preventMouse={false}>
      <ColorTheme theme="white-white">
        <div className={`IntroSection ${className}`} ref={containerRef} style={{ height }}>
          <div className="IntroSection__content">
            {children}
          </div>

          {showDownArrow ? (
            <HideOnScroll>
              <button className="scroll-down-link" onClick={scrollToNext}>
                <Icon name="arrow-down" />
              </button>
            </HideOnScroll>
          ) : null}
        </div>
      </ColorTheme>
    </WheelHandler>
  )
}

export default connect(state => {
  const { height, scrollY } = getViewport(state)
  return {
    height,
    scrollY,
  }
})(IntroSection)
