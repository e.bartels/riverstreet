import * as React from 'react'

type Props = {
  to: string,
  onLoadMore: () => void,
  cursor?: string,
  children: React.ReactNode,
}

export const LoadMoreButton = ({ to, onLoadMore, children, cursor = '' }: Props) => (
  <span className="LoadMoreButton">
    <a
      href={`${to}?page=${cursor}`}
      className="button"
      onClick={(e: React.MouseEvent) => {
        e.preventDefault()
        onLoadMore()
      }}
    >
      {children}
    </a>
  </span>
)

export default LoadMoreButton
