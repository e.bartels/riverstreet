import * as React from 'react'
import { ScrollContext } from 'react-router-scroll-4'

type Props = {
  children: React.ReactNode,
}

function shouldUpdateScroll(prevRouterProps, { location }) {
  const prevLocation = prevRouterProps ? prevRouterProps.location : ''
  const samePath = prevLocation && prevLocation.pathname === location.pathname
  const state = location && location.state ? location.state : {}
  const isModal = !!state.modal
  const noAutoScroll = !!state.noAutoScroll

  return !samePath && !noAutoScroll && !isModal
}

// Use scroll behavior unless location.modal is true
export const ScrollBehavior = ({ children }: Props) => (
  <ScrollContext shouldUpdateScroll={shouldUpdateScroll}>
    <>{children}</>
  </ScrollContext>
)

export default ScrollBehavior
