import * as React from 'react'
import { connect } from 'react-redux'
import { default as cx } from 'classnames'
import { getViewport } from '~/ui/selectors'

type Props = {
  children: React.ReactNode,
  scrollY: number,
  offset?: number,
}

export const HideOnScroll = ({ children, scrollY, offset = 100 }: Props) => (
  <span className={cx({ hide: scrollY > offset })}>{children}</span>
)

export default connect(state => ({
  scrollY: getViewport(state).scrollY,
}))(HideOnScroll)
