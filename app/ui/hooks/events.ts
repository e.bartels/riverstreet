import { useEffect } from 'react'
import detectPassive from 'detect-passive-events'

/**
 * This hook registers a global event listener.
 * The default target is the window object.
 *
 * Usage:
 *
 * useEventListener('keydown', myCallback)
 *
 * This will add the event listener for keydown to the window object
 *
 */

export const useEventListener = (
  event: string,
  handler: (e?: any) => void,
  target: EventTarget = window,
  opts?: boolean | AddEventListenerOptions,
) => {
  useEffect(() => {
    // add event listener
    let optsOrCapture = opts
    if (opts && !detectPassive.hasSupport && typeof opts !== 'boolean') {
      optsOrCapture = opts.capture
    }
    target.addEventListener(event, handler, optsOrCapture)

    // remove event listener
    return () => {
      target.removeEventListener(event, handler)
    }
  }, [event, handler, target])
}
