import { createSelector } from 'reselect'

/**
 * Get ui stae atom from redux store.
 */
export const getUIStateAtom = state => state.ui

/**
 * Get ui.isTouch
 */
export const getIsTouch = createSelector(
  [getUIStateAtom],
  (state) => state.isTouch,
)

/**
 * Get ui.viewport
 */
export const getViewport = createSelector(
  [getUIStateAtom],
  (state) => state.viewport,
)

/**
 * Get ui.colorTheme
 */
export const getColorTheme = createSelector(
  [getUIStateAtom],
  (state) => state.colorTheme,
)

/**
 * Get ui.mainNav
 */
export const getMainNav = createSelector(
  [getUIStateAtom],
  (state) => state.mainNav,
)

/**
 * Get mainNav.isOpen
 */
export const getMainNavOpen = createSelector(
  [getMainNav],
  (state) => state.isOpen,
)

/**
 * Get mainNav.view
 */
export const getMainNavView = createSelector(
  [getMainNav, getViewport],
  (nav, viewport) => (
    nav.view === 'default' && !viewport.isMobile
      ? 'work'
      : nav.view
  ),
)

/**
 * Active modals
 */
export const getActiveModals = createSelector(
  [getUIStateAtom],
  (state) => state.activeModals,
)

export const isModalActive = createSelector(
  [getActiveModals],
  (state) => !!state.length,
)

/**
 * Login url path
 */
export const getLoginPath = createSelector(
  [getUIStateAtom],
  (state) => state.loginPath,
)
