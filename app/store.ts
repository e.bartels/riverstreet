/* eslint global-require: 0, no-underscore-dangle: 0 */
import { applyMiddleware, createStore, compose } from 'redux'
import type { Middleware, StoreEnhancer } from 'redux'
import thunkMiddleware from 'redux-thunk'
import rootReducer from '~/reducer'
import { DEBUG } from '~/core/constants'

const devToolsExtension = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__

function configureStore(initialState = undefined) {
  // middleware
  const middlewares = [
    thunkMiddleware,
  ] as Middleware[]

  // enhancers
  const enhancers = [] as StoreEnhancer[]
  if (typeof devToolsExtension === 'function') {
    enhancers.push(devToolsExtension())
  }

  // Debug only
  if (DEBUG) {
    // redux-freeze
    const freeze = require('redux-freeze')
    middlewares.push(freeze)
  }

  // create store
  const store = createStore(
    rootReducer,
    initialState,
    compose(
      applyMiddleware(...middlewares),
      ...enhancers,
    ),
  )

  // Hot module replacement
  if (module.hot) {
    module.hot.accept('./reducer', () => {
      const nextRootReducer = require('./reducer').default
      store.replaceReducer(nextRootReducer)
    })
  }

  return store
}

export default configureStore
