import { ContentItem, Image, Video, SizedImageSpec } from '~/types'

export interface Project {
  id: string,
  slug: string,
  title: string,
  client: string,
  content: ContentItem[],
  video: Video,
  videoIntroCaption: string,
  showProjectDetails: boolean,
}

export interface ListProject {
  id: string,
  slug: string,
  title: string,
  client: string,
  image: SizedImageSpec,
  video: { id: number },
}

export interface Category {
  id: string,
  slug: string,
  title: string,
  introText: string,
  introImage: Image | null,
  projects: ListProject[],
}

export interface Reel {
  id: string,
  slug: string,
  title: string,
  content: ContentItem[],
}
