import { Image, Video } from './media'

// ContentItem
export interface BaseContentItem {
  id: string,
}

export interface TextContentItem extends BaseContentItem {
  type: 'text',
  text: string,
}

export interface ImageContentItem extends BaseContentItem {
  type: 'image',
  image: Image,
}

export interface VideoContentItem extends BaseContentItem {
  type: 'video',
  video: Video,
}

export interface MediaGroupItem {
  id: string,
  type: 'mediagroup',
  items: Array<ImageContentItem | VideoContentItem>,
}

export type ContentItem = TextContentItem | ImageContentItem | VideoContentItem

export type GroupedContentItem = ContentItem | MediaGroupItem
