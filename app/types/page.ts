import { ContentItem, Image } from '~/types'

export interface Page {
  id: string,
  slug: string,
  title: string,
  introText: string,
  introImage: Image | null,
  content: Array<ContentItem>,
}
