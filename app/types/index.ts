import { RouteComponentProps } from 'react-router'

export * from './media'
export * from './contentitems'
export * from './auth'
export * from './page'
export * from './projects'
export * from './blog'
export * from './people'

// Theme options
export type ColorThemeType = 'white-white' | 'black-blue' | 'white-blue'


export type Location = RouteComponentProps['location'] & {
  action: string,
}
