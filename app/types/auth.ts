export interface AuthenticatedUser {
  id: number,
  username: string,
  anonymous: false,
  authenticated: true,
}

export interface AnonymousUser {
  id: null,
  anonymous: true,
  authenticated: false,
}

export type User = AuthenticatedUser | AnonymousUser
