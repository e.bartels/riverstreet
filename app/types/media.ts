
// Media Types
export type MediaType = 'image' | 'video' | 'file'

export interface GenericMediaItem {
  type: MediaType,
  id: string,
  uid: string,
  title: string,
  caption: string,
}

export interface SizedImage {
  url: string,
  width: number,
  height: number,
}

export type SizedImageSpec = {
  [key: string]: SizedImage,
}

export interface Image extends GenericMediaItem {
  type: 'image',
  url: string,
  width: number,
  height: number,
  images: SizedImageSpec,
  altText: string,
}

export interface Video extends GenericMediaItem {
  type: 'video',
  url: string,
  provider: string,
  embed: string,
  width: number,
  height: number,
  images: SizedImageSpec,
  altText: string,
}

export interface File extends GenericMediaItem {
  type: 'file',
  filetype: string,
  filesize: number,
}

export type VisualMedia = Image | Video

export type MediaItem = Image | Video | File
