import { ContentItem } from '~/types'

export interface BlogEntry {
  id: string,
  slug: string,
  title: string,
  pubDate: string,
  year: number,
  content: ContentItem[],
  excerpt?: string | null,
}

export interface BlogCategory {
  id: string,
  slug: string,
  name: string,
}
