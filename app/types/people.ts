import { Image } from '~/types'

export interface Person {
  id: string,
  slug: string,
  name: string,
  title: string,
  bio: string,
  image: Image,
}
