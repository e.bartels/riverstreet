This directory contains all client-side application files. The app is entirely written in Typescript, React, and Sass. 


Directories & Files
===================

[index.ts](index.ts) - The entrypoint for the React app - renders the react component tree  
[App.ts](App.ts) - The top-level App component

[`analytics/`](analytics/)  Google analytics code using the `react-ga` project  
[`core/`](core/)  Shared application components and logic  
[`routes/`](routes/)  Client-side routes using [react-router][react-router]  
[`styles/`](styles/)  Shared Sass stylesheets  
[`types/`](types/)  Shared typescript types  
[`ui/`](ui/)  Shared UI components  
[`utils/`](utils/)  Utility code  

The other folders are domain specific and largely mirror the names of corresponding Django apps.

[`blog/`](blog/)  
[`pages/`](pages/)  
[`people/`](people/)  
[`projects/`](projects/)  
[`vimeo/`](vimeo/)  
[`work/`](work/)  


Components
===========

Components are organized into their respective apps. Most of the above folders contain a `components` folder. These contain the React component code. Most components also have a corresponding Sass stylesheet file with the same basename. These get imported by the component .ts files so that the webpack build will pick them up automatically and convert the Sass into plain CSS.

---

**react-router**  
Client-side routes are handled by [react-router][react-router]. View [`routes/index.tsx`](routes/index.tsx). These routes form the entry point for rendering the component tree, depending on which URL you are visiting on the website.

See: [routes/index.tsx](routes/index.tsx) for application routes

**Apollo**  
[Apollo][apollo] is used to query the GraphQL API from the server and maintain a normalized cache of data. Most top-level route-based components use Apollo hooks to query the server for data before rendering child component tree.

See: [routes/components/Page.tsx](routes/components/Page.tsx) as an example

The [Apollo Developer Tools](https://chrome.google.com/webstore/detail/apollo-client-developer-t/jdkknkkbebbapilgoeccciglkfbmbnfm?hl=en-US) could be useful

**react-redux**  
For state management, [react-redux][react-redux] is used. Redux is exclusively for shared UI state (Apollo is used for connecting to the GraphQL API). You can view files in the `ui/` folder:

[`store.ts`](store.ts) - configure redux store  
[`reducer.ts`](store.ts) - top-level redux reducer  
[`ui/reducer.ts`](ui/reducer.ts) -  files for redux logic.  
[`ui/actions.ts`](ui/actions.ts) - redux reducers  
[`ui/selectors.ts`](ui/selectors.ts) - redux selectors, using [reselect][reselet]

[Redux Developer Tools](https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd?hl=en) alongside  [React Developer Tools](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi?hl=en) could be useful.

[react]: https://facebook.github.io/react/
[apollo]: https://www.apollographql.com/docs/react/
[react-redux]: https://react-redux.js.org/
[react-router]: https://reacttraining.com/react-router/web/guides/quick-start
[reselet]: https://github.com/reduxjs/reselect
