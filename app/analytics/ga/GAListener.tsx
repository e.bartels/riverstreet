// NOTE: this works with react-router v4
/**
 *  Usage:
 *    <Router>
 *      <GAListener>
 *        <MainLayout />
 *      </GAListener>
 *    </Router>
*/
import * as React from 'react'
import { withRouter, RouteComponentProps } from 'react-router'
import ReactGA from 'react-ga'
import djangoConfig from '~/utils/django'


type Props = {
  children?: React.ReactNode,
  gaId?: string,
  debug?: boolean,
} & RouteComponentProps

class GAListener extends React.Component<Props> {
  static defaultProps = {
    gaId: djangoConfig.google_analytics_id || '',
  }

  componentDidMount() {
    const { gaId } = this.props
    if (gaId) {
      ReactGA.initialize(gaId, {
        debug: this.props.debug || false,
      })
      this.sendPageView(this.props.location)
    }
  }

  componentDidUpdate(prevProps: Props) {
    const { location, gaId } = this.props
    if (gaId && location.pathname !== prevProps.location.pathname) {
      this.sendPageView(location)
    }
  }

  sendPageView = (location: any) => {
    const page = `${location.pathname}${location.search}`
    ReactGA.pageview(page)
  }

  render() {
    return <>{ this.props.children || null }</>
  }
}

export default withRouter(GAListener)
