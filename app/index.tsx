import * as React from 'react'
import ReactDOM from 'react-dom'
import configureStore from './store'
import configureApolloClient from './apolloClient'
import App from './App'

// Attach react app to this element
const rootElement = document.getElementById('root')

// redux store
const store = configureStore()

// apollo client
const apolloClient = configureApolloClient()

ReactDOM.render(
  <App store={store} apolloClient={apolloClient} />,
  rootElement,
)
