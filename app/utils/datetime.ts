import parseISO from 'date-fns/parseISO'
import format from 'date-fns/format'


/**
 * Parses and formats a datetime string
 */
export const dt = (dtstring: string, fstring: string = 'd MMMM yyyy') => (
  format(parseISO(dtstring), fstring)
)
