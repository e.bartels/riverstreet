import cookie from 'cookie'

export const djangoConfig = window.django_config || {}
export default djangoConfig


export function getDjangoCSRFToken(cookieSrc = document.cookie) {
  const cookies = cookie.parse(cookieSrc)
  return cookies.csrftoken
}
