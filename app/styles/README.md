Sass Styles
===========

This directory contains shared Sass styles used throughout the site.


[`index.scss`](index.scss) Entrypoint that imports other .scss files  

[`_settings.scss`](_settings.scss) Contains Sass variables  
[`_mixins.scss`](_mixins.scss) Mixins used by other .scss files  
[`_typography.scss`](_typography.scss)  Mixins for each typographic style  

[`default-type.scss`](default-type.scss) Default typographic styles  
[`base.scss`](base.scss)  Base styles for the website  
[`icons.scss`](icons.scss)  Custom icon font  
[`scrollbar.scss`](scrollbar.scss)  Custom scrollbar style  
[`editor_content.scss`](editor_content.scss)  Styles for CKeditor in the admin  

[`fonts/`](fonts/)  Custom fonts folder  
[`icons/`](icons/)  SVG icons  

NOTE: This folder only contains the base styles. In addition React components have their own `.scss` style files that live alongside the corresponding `.ts` component files.

For example:  
[../pages/components/AboutPage.tsx](../pages/components/AboutPage.tsx) and  
[../pages/components/AboutPage.scss](../pages/components/AboutPage.scss)  
