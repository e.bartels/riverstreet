export const GRAPHQL_ENDPOINT = '/graphql'
export const API_ROOT = '/api/'
export const DOCUMENT_TITLE = ((document && document.title) || '')
  .split('|')
  .slice(-1)[0]
export const MOBILE_MAX_WIDTH = 740
export const TABLET_MAX_WIDTH = 899
export const PRODUCTION = process.env.NODE_ENV === 'production'
export const DEBUG = process.env.NODE_ENV !== 'production'
