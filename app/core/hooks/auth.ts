import gql from 'graphql-tag'
import { useMutation, useApolloClient } from '@apollo/react-hooks'

/**
 * A React hook to provide a component logout functionality.
 * It returns 'handleLogout' function, which can be called to send the logout
 * mutation using apollo/graphql.
 */
const LOGOUT = gql`
  mutation LogoutMutation {
    logout { ok }
  }
`

export const useLogout = (onCompleted?: Function) => {
  const client = useApolloClient()
  const [handleLogout] = useMutation(LOGOUT, {
    onCompleted: async (data) => {
      if (data && data.logout && data.logout.ok) {
        await client.resetStore().catch(e => { /* pass */ })
      }
      if (onCompleted) onCompleted(data)
    },
  })
  return handleLogout
}

/**
 * A React hook to provide a component login functionality.
 * It returns 'handleLogin' prop, which can be called to send the login
 * mutation using apollo/graphql.
 */
const LOGIN = gql`
  mutation LoginMutation($username: String!, $password: String!) {
    login(username: $username, password: $password) {
      ok
    }
  }
`

export const useLogin = (onCompleted?: Function) => {
  const client = useApolloClient()
  const [login] = useMutation(LOGIN, {
    onCompleted: async (data) => {
      if (data && data.login && data.login.ok) {
        await client.resetStore().catch(e => { /* pass */ })
      }
      if (onCompleted) onCompleted(data)
    },
  })

  const handleLogin = (username: string, password: string) => {
    login({ variables: { username, password } })
  }

  return handleLogin
}
