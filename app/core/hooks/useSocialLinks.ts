import gql from 'graphql-tag'
import { useQuery } from '@apollo/react-hooks'

const SOCIALMEDIA_LINKS = gql`
  query SocialMediaLinks {
    configsettings {
      socialmedia {
        facebookLink
        twitterLink
        instagramLink
        youtubeLink
      }
    }
  }
`

type QueryData = {
  configsettings: { socialmedia: { [key: string]: string } },
}

/**
 * A hook to get socialLinks from graphql query
 */

type SocialLinks = {
  [key: string]: string,
}

export const useSocialLinks = () => {
  const { loading, error, data } = useQuery<QueryData>(SOCIALMEDIA_LINKS)
  if (loading || error) return null
  if (!data || !data.configsettings) return null

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { __typename, ...socialLinks } = data.configsettings.socialmedia
  return socialLinks as SocialLinks
}

export default useSocialLinks
