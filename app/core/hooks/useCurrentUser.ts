import gql from 'graphql-tag'
import { useQuery } from '@apollo/react-hooks'
import { User } from '~/types'

const CURRENT_USER = gql`
  query CurrentUser {
    currentUser {
      id
      username
      anonymous
      authenticated
    }
  }
`

type QueryData = { currentUser: User }

export const useCurrentUser = () => {
  const { loading, error, data } = useQuery<QueryData>(CURRENT_USER)
  if (loading || error) return null
  if (!data || !data.currentUser) return null
  return data.currentUser
}

export default useCurrentUser
