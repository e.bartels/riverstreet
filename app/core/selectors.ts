import { defaultMemoize as memoize } from 'reselect'
import { ContentItem, MediaGroupItem } from '~/types'

/**
 * For relay data, this will extract nodes from edges to give just a list of
 * nodes without the extra noise.
 */
export const extractNodes = memoize((items) => (
  items.edges.map(edge => edge.node)
))

/**
 * A generator that yields successive items or groups of items.
 * This is meant to be used with the ContentItem type returned from graphql,
 * which can have a type of 'text', 'image', or 'video.' It will group image &
 * videos types together.
 */
function* contentGrouper(items: ContentItem[]) {
  const makeGroup = (groupItems): MediaGroupItem => ({
    id: `media-${groupItems[0].id}`,
    type: 'mediagroup',
    items: groupItems,
  })

  const remainingItems = [...items]
  let currentGroup = [] as Array<ContentItem>

  while (remainingItems.length) {
    const nextItem = remainingItems.shift() as ContentItem
    const nextType = nextItem.type.replace(/(image|video)/, 'media')
    const groupType = currentGroup.length
      ? currentGroup[0].type.replace(/(image|video)/, 'media')
      : ''

    // This item's type does not match current group's,
    // so we yield and reset.
    if (groupType && nextType !== groupType) {
      remainingItems.unshift(nextItem)
      yield makeGroup(currentGroup)
      currentGroup = []
    }
    // Only media get grouped (i.e. not text)
    else if (nextType !== 'media') {
      yield nextItem
    }
    // Add the next item to the group
    else {
      currentGroup.push(nextItem)
      if (!remainingItems.length) {
        yield makeGroup(currentGroup)
      }
    }
  }
}

/**
 * Takes flat list of project.content items and groups images and
 * videos together.
 */
export const getGroupedContent = memoize((items: ContentItem[]) => (
  [...contentGrouper(items)]
))
