import * as React from 'react'
import { withRouter, RouteComponentProps } from 'react-router'
import ErrorView from '~/core/components/ErrorView'
import NotFound from '~/core/components/NotFound'
import LoginRequiredView from '~/core/components/LoginRequiredView'
import { DEBUG } from '~/core/constants'

type Props = {
  error: GQLErrorType,
} & RouteComponentProps

interface GQLErrorType extends Error {
  [key: string]: any,
}

export const GQLError = ({ error, location }: Props) => {
  const statusCode = error.statusCode || (error.networkError ? error.networkError.statusCode : null)
  if (statusCode === 404) {
    return <NotFound />
  }

  const [reason, description] = error.message.split(':').map(i => i.trim())
  if (description === 'not found') {
    return <NotFound />
  }
  if (description === 'login required') {
    return <LoginRequiredView forPath={location.pathname} />
  }

  const title = `${statusCode || ''} ${reason || 'Unknown Error'}`
  const message = description || ''
  const debugMessage = DEBUG ? JSON.stringify(error, null, 2) : ''
  return (
    <ErrorView
      title={title}
      message={message}
      debugMessage={debugMessage}
    />
  )
}

export default withRouter(GQLError)
