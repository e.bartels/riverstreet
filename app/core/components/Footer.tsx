import * as React from 'react'
import { useCallback } from 'react'
import scrollTo from 'scroll-to-element'
import { connect } from 'react-redux'
import Hammer, { hammer } from '~/ui/components/Hammer'
import ColorTheme from '~/ui/components/ColorTheme'
import MDIcon from '~/ui/components/MDIcon'
import Logo from '~/styles/icons/logo.svg'
import SocialIcons from '~/core/components/SocialIcons'
import { getViewport } from '~/ui/selectors'

import './Footer.scss'

type Props = {
  height: number,
}

export const Footer = ({ height }: Props) => {
  // Scroll footer on swipe
  const onSwipe = useCallback((e: any) => {
    const footerEl = document.getElementById('footer') as HTMLDivElement
    const offset = e.direction === hammer.DIRECTION_UP
      ? 0
      : footerEl.offsetHeight * -1

    scrollTo('footer', {
      offset,
      duration: 1000,
      ease: 'out-quart',
    })
  }, [])

  return (
    <ColorTheme theme="white-white">
      <Hammer
        onSwipe={onSwipe}
        swipeOpts={{
          threshold: 5,
          velocity: 0.05,
          direction: hammer.DIRECTION_VERTICAL,
        }}
      >
        <footer className="Footer" id="footer" style={{ height }}>
          <div className="container">
            <div className="Footer__logo">
              <div className="logo-wrap"><Logo width="180" /></div>
            </div>

            <div className="Footer__contact">
              Riverstreet Productions, Inc.<br />
              <a href="https://goo.gl/maps/RZHTLjYxRzF2" target="_blank" rel="noopener noreferrer">
                8619 Washington Blvd.<br />
                Culver City, CA 90232 <MDIcon name="pin" /><br />
              </a>
              <a href="tel:13102892890">310-289-2890</a><br />
              <a href="fax:13102892897">310-289-2897 (fax)</a><br />
            </div>

            <div className="Footer__social">
              <div className="Footer__social__icons">
                <h4>Find us on social media</h4>
                <SocialIcons />
              </div>

              <div className="email-form">
                <form>
                  <input type="text" placeholder="Join our mailing list" />
                  <button className="go-button" aria-label="Submit" />
                </form>
              </div>
            </div>
          </div>
        </footer>
      </Hammer>
    </ColorTheme>
  )
}

/**
 * Get height from redux state
 */
export const withHeight = connect(state => ({
  height: getViewport(state).height,
}))


export default withHeight(Footer)
