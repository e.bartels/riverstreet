import * as React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { default as cx } from 'classnames'
import BodyClass from '~/ui/components/BodyClass'
import MainLogo from '~/ui/components/MainLogo'
import MainNav from '~/core/components/MainNav'
import Footer from '~/core/components/Footer'
import { getColorTheme, getMainNavOpen } from '~/ui/selectors'

import './MainLayout.scss'

type Props = {
  children: React.ReactNode,
  colorTheme: string,
  mainNavOpen: boolean,
}

export const MainLayout = (props: Props) => (
  <>
    <div className={cx('MainLayout', { 'MainLayout--navOpen': props.mainNavOpen })}>
      <BodyClass className={props.colorTheme ? `theme-${props.colorTheme}` : ''} />

      <MainLogo />

      <div className="MainLayout__content">
        {props.children}
      </div>

      <Footer />
    </div>

    <MainNav />
  </>
)

const mapStateToProps = (state, props) => ({
  colorTheme: getColorTheme(state),
  mainNavOpen: getMainNavOpen(state),
})

export const ConnectedMainLayout = connect(mapStateToProps)(MainLayout)

export default withRouter(ConnectedMainLayout)
