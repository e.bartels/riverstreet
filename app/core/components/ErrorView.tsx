import * as React from 'react'
import Markup from '~/ui/components/Markup'
import ColorTheme from '~/ui/components/ColorTheme'

import './ErrorView.scss'

type Props = {
  title: string,
  message?: string,
  debugMessage?: string,
  children?: React.ReactNode,
}

export const ErrorView = ({ title, message = 'Sorry, something went wrong', ...props }: Props) => (
  <ColorTheme theme="black-blue">
    <div className="ErrorView">
      <h1 className="standout">{title}</h1>

      <p>
        <Markup>{message || ''}</Markup>
      </p>

      {props.children || null}

      <p>
        <a href="/">Go to the Home Page</a>
      </p>

      {props.debugMessage ? (
        <code className="ErrorView__debug" style={{ whiteSpace: 'pre-wrap' }}>{props.debugMessage}</code>
      ) : null}
    </div>
  </ColorTheme>
)

export default ErrorView
