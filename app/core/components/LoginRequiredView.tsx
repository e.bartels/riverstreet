import * as React from 'react'
import { useEffect } from 'react'
import { connect } from 'react-redux'
import ErrorView from '~/core/components/ErrorView'
import { showLogin, hideLogin } from '~/ui/actions'

type Props = {
  forPath?: string,
  showLogin: (path: string | null) => void,
  hideLogin: () => void,
}

export const LoginRequiredView = ({ ...props }: Props) => {
  useEffect(() => {
    props.showLogin(props.forPath || null)
    return () => props.hideLogin()
  }, [])

  return (
    <div className="LoginRequiredView">
      <ErrorView
        title="Login Required"
        message="You don't have permission to view this page.">
        <p>
          <button
            className="button"
            onClick={() => {
              props.showLogin(props.forPath || null)
            }}>
            Click here to login.
          </button>
        </p>
      </ErrorView>
    </div>
  )
}

export default connect(null, { showLogin, hideLogin })(LoginRequiredView)
