import * as React from 'react'
import { useState, useEffect } from 'react'
import { withRouter, RouteComponentProps } from 'react-router'
import Icon from '~/ui/components/Icon'
import Video from '~/ui/components/Video'
import Image from '~/ui/components/Image'
import Markup from '~/ui/components/Markup'
import { VideoContentItem as VideoContentItemType } from '~/types'

import './VideoContentItem.scss'

type Props = {
  item: VideoContentItemType,
  showCaption?: boolean,
} & RouteComponentProps

export const VideoContentItem = ({ item, location, showCaption = false }: Props) => {
  const [isPlaying, setIsPlaying] = useState(false)

  useEffect(() => {
    setIsPlaying(false)
  }, [location.pathname])

  return (
    <div className="VideoContentItem">
      {!isPlaying ? (
        <div className="VideoContentItem__image">
          <Image image={item.video} className="VideoContentItem__image__img" />
          <span className="VideoContentItem__button" onClick={() => { setIsPlaying(true) }}>
            <Icon name="play" />
          </span>
        </div>
      ) : (
        <Video data={item.video} autoplay />
      )}

      {showCaption && item.video.caption ? (
        <Markup>{item.video.caption}</Markup>
      ) : null}
    </div>
  )
}

export default withRouter(VideoContentItem)
