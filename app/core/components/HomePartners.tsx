import * as React from 'react'
import { default as cx } from 'classnames'
import { Link } from 'react-router-dom'
import ColorTheme from '~/ui/components/ColorTheme'

import './HomePartners.scss'

type Props = {
  height: number,
  isActive: boolean,
}

export const HomePartners = ({ height, isActive }: Props) => (
  <ColorTheme theme="white-white">
    <section
      className={cx('HomePartners', { isActive })}
      id="home-partners"
      style={{ height }}
    >

      <div className="HomePartners__background" />

      <div className="HomePartners__container">
        <div className="HomePartners__text">
          <div className="HomePartners__text__content">
            <h2><em>Who</em><br /> we work<br /> with</h2>
            <Link to="/p/about-us" className="button">About Us</Link>
          </div>

          <div className="HomePartners__circle" />
        </div>
      </div>
    </section>
  </ColorTheme>
)

export default HomePartners
