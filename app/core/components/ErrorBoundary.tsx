import * as React from 'react'
import ErrorView from '~/core/components/ErrorView'

const DEBUG = process.env.NODE_ENV !== 'production'

type Props = {
  children: React.ReactNode,
}

type State = {
  error: Error | null,
  info: any | null,
}

export class ErrorBoundary extends React.Component<Props, State> {
  state: State = {
    error: null,
    info: null,
  }

  componentDidCatch(error: Error | null, info: any) {
    this.setState({
      error: error || new Error('missing error'),
      info,
    })
  }

  render() {
    const { error } = this.state
    if (!error) {
      return this.props.children
    }

    const { info } = this.state
    const componentStack = (info && info.componentStack) || ''
    const msg = `${error.name}: ${error.message}\n${componentStack}`
    return (
      <ErrorView
        title="Unknown Error"
        debugMessage={DEBUG ? msg : ''}
      />
    )
  }
}

export default ErrorBoundary
