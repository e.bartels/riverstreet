import * as React from 'react'
import { useEffect, useState, useCallback, useRef } from 'react'
import { default as cx } from 'classnames'
import { connect } from 'react-redux'
import { withRouter, RouteComponentProps } from 'react-router'
import { useLogin } from '~/core/hooks/auth'
import { getLoginPath } from '~/ui/selectors'

import './LoginForm.scss'


export type Props = {
  redirectTo: string,
  className?: string,
} & RouteComponentProps


export const LoginForm = ({ className, redirectTo, location, history }: Props) => {
  // Focus form input
  const formRef = useRef<HTMLFormElement>(null)
  useEffect(() => {
    const form = formRef ? formRef.current : null
    if (form) {
      const input = form.querySelector('input')
      if (input) input.focus()
    }
  }, [])

  // Handle error state
  const [errorMessage, setError] = useState<string | null>(null)

  // Login handler (graphql)
  const handleLogin = useLogin((data) => {
    if (data && data.login && data.login.ok) {
      const redirectPath = redirectTo || '/clients'
      if (redirectPath !== location.pathname) {
        history.push(redirectPath)
      }
    }
    else {
      setError('Please enter a correct username and password')
    }
  })

  // Form submit
  const onSubmit = useCallback((e: any) => {
    // get form data
    e.preventDefault()
    const form = e.target as HTMLFormElement
    const username = form.username.value
    const password = form.password.value

    // check for errors
    if (!username) {
      setError('Please enter a username or email')
    }
    else if (!password) {
      setError('Please enter a password')
    }
    // handle login
    else {
      setError(null)
      handleLogin(username, password)
    }
  }, [])

  return (
    <div
      className={cx('LoginForm', className, {
        'LoginForm--error': !!errorMessage,
      })}
    >
      {errorMessage && (
        <p className="LoginForm__error-message">{errorMessage}</p>
      )}

      <form onSubmit={onSubmit} ref={formRef}>
        <label htmlFor="id_username">Username</label>
        <input type="text" name="username" id="id_username" />

        <label htmlFor="id_password">Password</label>
        <input type="password" name="password" id="id_password" />

        <button aria-label="submit" className="go-button" />
      </form>
    </div>
  )
}


export default withRouter(connect(state => ({
  redirectTo: getLoginPath(state),
}))(LoginForm))
