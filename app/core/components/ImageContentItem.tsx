import * as React from 'react'
import Image from '~/ui/components/Image'
import Markup from '~/ui/components/Markup'
import { ImageContentItem as ImageContentItemType } from '~/types'

import './ImageContentItem.scss'

type Props = {
  item: ImageContentItemType,
  sizes?: string, // sizes property for images
  showCaption?: boolean,
}

export const ImageContentItem = ({ item, sizes, showCaption = false }: Props) => (
  <div className="ImageContentItem">
    <Image image={item.image} sizes={sizes} />
    {showCaption && item.image.caption ? (
      <Markup>{item.image.caption}</Markup>
    ) : null}
  </div>
)

export default ImageContentItem
