import * as React from 'react'
import { useLogout } from '~/core/hooks/auth'

type Props = {
  children?: React.ReactNode,
} & React.ButtonHTMLAttributes<HTMLButtonElement>

export const LogoutButton = ({ children, ...props }: Props) => {
  const handleLogout = useLogout()
  return (
    <button
      {...props}
      onClick={() => { handleLogout() }}>
      {children || 'Logout'}
    </button>
  )
}

export default LogoutButton
