import * as React from 'react'
import { connect } from 'react-redux'
import { default as cx } from 'classnames'
import throttle from 'lodash/throttle'
import scrollTo from 'scroll-to-element'
import EventListener from '~/ui/components/EventListener'
import WheelHandler from '~/ui/components/WheelHandler'
import Hammer, { hammer } from '~/ui/components/Hammer'
import BodyClass from '~/ui/components/BodyClass'
import ScrollNav from '~/ui/components/ScrollNav'
import { Video } from '~/types'
import { getViewport } from '~/ui/selectors'

import HomeSplash from './HomeSplash'
import HomeWhatWeDo from './HomeWhatWeDo'
import HomePartners from './HomePartners'
import HomeReel from './HomeReel'

import './Home.scss'

type ViewProps = {
  items: string[],
  activeIndex: number,
  onWheel: (e: any) => void,
  onSwipe: (e: any) => void,
  onButtonClick: (id: string) => void,
  introText?: string,
  videoReel?: Video,
  height: number,
}

const ITEMS = [
  'home-splashpage',
  'home-what-we-do',
  'home-partners',
  'home-reel',
  'footer',
]

export const Home = ({ ...props }: ViewProps) => (
  <div className="Home" id="homepage">
    <BodyClass className="Home--homepage" />

    <ScrollNav className="Home__scroll-nav">
      {props.items.map((id, i) => (
        <button
          key={id}
          aria-label={id}
          className={cx('Home__scroll-nav__button', { active: props.activeIndex === i })}
          onClick={() => { props.onButtonClick(id) }}
        />
      ))}
    </ScrollNav>

    <WheelHandler onWheel={props.onWheel}>
      <Hammer
        onSwipe={props.onSwipe}
        swipeOpts={{
          threshold: 5,
          velocity: 0.05,
          direction: hammer.DIRECTION_VERTICAL,
        }}
      >
        <div className="Home__sections">
          <HomeSplash
            text={props.introText}
            height={props.height}
            isActive={props.activeIndex === 0}
          />
          <HomeWhatWeDo
            height={props.height}
            isActive={props.activeIndex === 1}
          />
          <HomePartners
            height={props.height}
            isActive={props.activeIndex === 2}
          />
          <HomeReel
            videoReel={props.videoReel}
            isActive={props.activeIndex >= 3}
            height={props.height}
          />
        </div>
      </Hammer>
    </WheelHandler>
  </div>
)


type Props = {
  introText?: string,
  videoReel?: Video,
  height: number,
}

type State = {
  activeIndex: number,
}

export class HomeContainer extends React.PureComponent<Props, State> {
  observer?: IntersectionObserver

  state = {
    activeIndex: 0,
  }

  componentDidMount() {
    this.observeScroll()
  }

  componentDidUpdate(prevProps, prevState) {
    const scrollIt = throttle((id) => {
      this.scrollToItem(id, 1)
    }, 50)

    if (prevProps.height !== this.props.height) {
      scrollIt(ITEMS[this.state.activeIndex])
    }
  }

  componentWillUnmount() {
    if (this.observer) this.observer.disconnect()
  }

  observeScroll = () => {
    // This requires IntersectionObserver api
    if (!IntersectionObserver) return

    // Use IntersectionObserver api to find active section based on
    // the amount the page is scrolled.
    this.observer = new IntersectionObserver((entries) => {
      const activeEntry = entries
        .filter(e => e.intersectionRatio > 0.6)
        .sort((a, b) => (
          a.intersectionRatio < b.intersectionRatio ? 1 : -1
        ))[0]

      if (activeEntry) {
        const index = ITEMS.indexOf(activeEntry.target.id)
        this.setState({ activeIndex: index })
      }
    }, {
      threshold: 0.6,
    })

    // Set up observers for each section
    ITEMS.forEach(id => {
      const el = document.getElementById(id)
      if (el && this.observer) this.observer.observe(el)
    })
  }

  handleWheel = (e: any) => {
    const { activeIndex } = this.state
    if (e.direction === 'down') {
      this.goToItem(activeIndex + 1)
    }
    else {
      this.goToItem(activeIndex - 1)
    }
  }

  handleSwipe = (e: any) => {
    const { activeIndex } = this.state
    if (e.direction === hammer.DIRECTION_UP) {
      this.goToItem(activeIndex + 1)
    }
    else if (e.direction === hammer.DIRECTION_DOWN) {
      this.goToItem(activeIndex - 1)
    }
  }

  scrollToItem = (id: string, duration = 1000) => {
    scrollTo(`#${id}`, {
      duration,
      ease: 'out-quart',
    })
  }

  goToItem = (n: number) => {
    const index = Math.min(ITEMS.length, Math.max(n, 0))
    const id = ITEMS[index]
    this.scrollToItem(id)
    this.setState({ activeIndex: index })
  }

  handleButtonClick = (id: string) => {
    const index = ITEMS.indexOf(id)
    this.goToItem(index)
  }

  handleKeyDown = (e: KeyboardEvent) => {
    switch (e.keyCode) {
      case 40: // down
      case 34: // pgdown
        e.preventDefault()
        this.goToItem(this.state.activeIndex + 1)
        break
      case 38: // up
      case 33: // pgup
        e.preventDefault()
        this.goToItem(this.state.activeIndex - 1)
        break
      case 36: // home
        e.preventDefault()
        this.goToItem(0)
        break
      case 35: // end
        e.preventDefault()
        this.goToItem(ITEMS.length - 1)
        break
      default:
        break
    }
  }

  render() {
    return (
      <>
        <EventListener event="keydown" handler={this.handleKeyDown} />
        <Home
          items={ITEMS.slice(0, -1)}
          activeIndex={this.state.activeIndex}
          onWheel={this.handleWheel}
          onSwipe={this.handleSwipe}
          onButtonClick={this.handleButtonClick}
          introText={this.props.introText}
          videoReel={this.props.videoReel}
          height={this.props.height}
        />
      </>
    )
  }
}

export default connect(state => ({
  height: getViewport(state).height,
}))(HomeContainer)
