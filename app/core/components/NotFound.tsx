import * as React from 'react'
import ErrorView from '~/core/components/ErrorView'

export const NotFound = () => (
  <ErrorView
    title="404 Not Found"
    message="Sorry, we couldn't find this page."
  />
)

export default NotFound
