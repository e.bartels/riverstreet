import * as React from 'react'
import { default as cx } from 'classnames'
import { Link } from 'react-router-dom'
import ColorTheme from '~/ui/components/ColorTheme'

import './HomeWhatWeDo.scss'

export type Props = {
  height: number,
  isActive: boolean,
}

export const HomeWhatWeDo = ({ height, isActive }: Props) => (
  <ColorTheme theme="white-white">
    <section
      className={cx('HomeWhatWeDo', { isActive })}
      id="home-what-we-do"
      style={{ height }}
    >
      <div className="HomeWhatWeDo__background" />

      <div className="HomeWhatWeDo__content">
        <Link to="/work">
          <div>
            <small>*placeholder*</small>
            <br />
            <br />
            <div>Production</div>
            <div>Concepting</div>
            <div>Scripting</div>
            <div>Branding</div>
            <div>Design</div>
            <div>Editorial</div>
            <div>Finishing</div>
            <div>Animation &amp; VFX</div>
            <div>International production</div>
          </div>

          <h2>What <br /> we do</h2>
        </Link>
      </div>
    </section>
  </ColorTheme>
)

export default HomeWhatWeDo
