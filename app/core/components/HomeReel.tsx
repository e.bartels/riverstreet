import * as React from 'react'
import { default as cx } from 'classnames'
import { connect } from 'react-redux'
import { withRouter, RouteComponentProps } from 'react-router'
import { Route } from 'react-router-dom'
import CountUp from 'react-countup'
import ModalLink from '~/ui/components/ModalLink'
import Icon from '~/ui/components/Icon'
import VideoModal from '~/ui/components/VideoModal'
import ColorTheme from '~/ui/components/ColorTheme'
import { getPreviousRoute } from '~/routes/selectors'
import { Video } from '~/types'

import './HomeReel.scss'


const Counter = ({ val }: { val: number }) => (
  <CountUp
    start={0}
    end={val}
    separator=","
  />
)

const onClose = (lastLocation, history) => {
  if (lastLocation && lastLocation.pathname === '/') {
    history.goBack()
  }
  else {
    history.push('/')
  }
}

type Props = RouteComponentProps & {
  isActive: boolean,
  lastLocation: any,
  videoReel?: Video,
  height: number,
}

export const HomeReel = ({ isActive, videoReel, height, ...props }: Props) => (
  <ColorTheme theme="white-white">
    <section
      className={cx('HomeReel', { isActive })}
      id="home-reel"
      style={{ height }}
    >

      <div className="HomeReel__background" />

      <div className="HomeReel__content">
        <dl className="HomeReel__stats fancy-dl">
          <dt>{ isActive ? <Counter val={20} /> : 20 }</dt>
          <dd>Years branding experience</dd>
          <dt>{ isActive ? <Counter val={3000} /> : '3,000' }+</dt>
          <dd>Original spots created</dd>
          <dt>{ isActive ? <Counter val={40} /> : 40 }</dt>
          <dd>Promax Gold Awards</dd>
          <dt>{ isActive ? <Counter val={4} /> : 4 }</dt>
          <dd>Emmy nominations</dd>
          <dt>{ isActive ? <Counter val={2} /> : 2 }</dt>
          <dd>Emmy awards</dd>
        </dl>

        {videoReel && (
          <ModalLink
            to={{
              pathname: `/media/${videoReel.id}`,
              search: 'play',
            }}
            className="play-button">
            <Icon name="angle-right" /> Play Reel
          </ModalLink>
        )}
      </div>

      {videoReel ? (
        <Route
          path="/media/:media_id"
          exact
          render={(routeProps) => (
            <VideoModal
              video={videoReel}
              caption={videoReel.caption}
              onClose={() => { onClose(props.lastLocation, props.history) }}
            />
          )}
        />
      ) : null}
    </section>
  </ColorTheme>
)

export default withRouter(connect(state => ({
  lastLocation: getPreviousRoute(state),
}))(HomeReel))
