import * as React from 'react'
import MDIcon from '~/ui/components/MDIcon'
import useSocialLinks from '~/core/hooks/useSocialLinks'

import './SocialIcons.scss'

type Props = {
  socialLinks?: { [key: string]: string },
}

const ICONS = {
  facebookLink: 'facebook',
  twitterLink: 'twitter',
  instagramLink: 'instagram',
  youtubeLink: 'youtube-play',
}

export const SocialIcons = () => {
  const socialLinks = useSocialLinks()

  return (
    socialLinks ? (
      <span className="SocialIcons">
        {Object.keys(ICONS).map(prop => (
          socialLinks[prop] ? (
            <a href={socialLinks[prop]} key={prop} target="_blank" rel="noopener noreferrer">
              <MDIcon name={ICONS[prop]} />
            </a>
          ) : null
        ))}
      </span>
    ) : null
  )
}

export default SocialIcons
