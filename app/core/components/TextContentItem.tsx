import * as React from 'react'
import Markup from '~/ui/components/Markup'
import { TextContentItem as TextContentItemType } from '~/types'

import './TextContentItem.scss'

type Props = {
  item: TextContentItemType,
}

export const TextContentItem = ({ item }: Props) => (
  <div className="TextContentItem">
    <Markup>{item.text}</Markup>
  </div>
)

export default TextContentItem
