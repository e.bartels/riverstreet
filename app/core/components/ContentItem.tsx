import * as React from 'react'
import TextContentItem from '~/core/components/TextContentItem'
import ImageContentItem from '~/core/components/ImageContentItem'
import VideoContentItem from '~/core/components/VideoContentItem'

import {
  ContentItem as ContentItemType,
  MediaGroupItem,
} from '~/types'

type Props = {
  item: ContentItemType | MediaGroupItem,
  sizes?: string, // sizes property for images
  height?: number,
  showCaption?: boolean,
}

export const ContentItem = ({ item, ...props }: Props) => {
  switch (item.type) {
    case 'text':
      return <TextContentItem item={item} />
    case 'image':
      return <ImageContentItem item={item} sizes={props.sizes} showCaption={props.showCaption} />
    case 'video':
      return <VideoContentItem item={item} showCaption={props.showCaption} />
    default:
      return null
  }
}

export default ContentItem
