import * as React from 'react'
import { useEffect, useCallback } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { useHistory, useLocation } from 'react-router'
import usePrevious from 'use-previous'
import { default as cx } from 'classnames'
import { Link, NavLink } from 'react-router-dom'
import { useEventListener } from '~/ui/hooks/events'
import Icon from '~/ui/components/Icon'
import BodyClass from '~/ui/components/BodyClass'
import CircleLogo from '~/ui/components/CircleLogo'
import LoginForm from '~/core/components/LoginForm'
// import LogoutButton from '~/core/components/LogoutButton'
// import useCurrentUser from '~/core/hooks/useCurrentUser'
import { getViewport, getMainNavOpen, getMainNavView } from '~/ui/selectors'
import * as uiActions from '~/ui/actions'

import './MainNav.scss'

type Props = {}

export const MainNav = () => {
  // redux state
  const isOpen = useSelector(getMainNavOpen)
  const activeView = useSelector(getMainNavView)
  const isMobile = useSelector(getViewport).isMobile

  // redux dispatch
  const dispatch = useDispatch()

  const openMenu = () => dispatch(uiActions.mainNavOpen())
  const closeMenu = () => dispatch(uiActions.mainNavClose())
  const showDefaultView = () => dispatch(uiActions.mainNavShowView('default'))
  const toggleMenu = () => {
    if (isOpen) closeMenu()
    else openMenu()
  }

  // Esc key closes menu
  useEventListener('keydown', useCallback((e: KeyboardEvent) => {
    if (e.keyCode === 27) closeMenu()
  }, []))

  // Menu button toggle
  const onButtonClick = (e: React.MouseEvent) => {
    if (isMobile && isOpen) {
      if (activeView !== 'default') showDefaultView()
      else toggleMenu()
    }
    else {
      toggleMenu()
    }
  }

  // Handle history state so back button closes menu (nice to have for mobile devices)
  const history = useHistory<any>()
  const location = useLocation<any>()
  const prevLocation = usePrevious(location)
  const prevIsOpen = usePrevious(isOpen)
  useEffect(() => {
    const currentState = location.state ?? {}
    const prevState = prevLocation?.state ?? {}

    // Close when location path changes
    if (isOpen && location.pathname !== prevLocation?.pathname) {
      closeMenu()
    }

    // close menu with history.goBack
    // 1) Opened - push history state to allow history back
    if (isOpen && !prevIsOpen) {
      history.push(location.pathname, {
        menuOpen: true,
        modal: true,
      })
    }

    // 2) Closed, but history state has not been updated
    else if (!isOpen && currentState.menuOpen) {
      // Menu has just closed, so go back in history to pop state
      if (prevIsOpen) {
        history.goBack()
      }
      // ...otherwise, clean up history state
      else {
        history.replace(location.pathname, {})
      }
    }

    // 3) Opened, but no history state - that history.goBack was called,
    // so we close the menu in resposne.
    else if (isOpen && !currentState.menuOpen && prevState.menuOpen) {
      closeMenu()
    }
  })

  // TODO: remove this (and commented-out jsx below) if not using logins anymore
  // Get currently logged in user (graphql)
  // const currentUser = useCurrentUser()
  // const showLogout = /^\/clients/.test(props.location.pathname)

  // const showLoginView = () => {
  //   dispatch(uiActions.mainNavShowView('login'))
  //   dispatch(uiActions.mainNavOpen())
  // }

  return (
    <>
      {!isOpen && (activeView === 'default' || !isMobile) ? (
        <nav className={cx('MainNav__top-nav', { 'MainNav__top-nav--isOpen': isOpen })}>
          <NavLink to="/work">Work</NavLink>
          <NavLink to="/p/about-us">About</NavLink>
          <NavLink to="/news">News</NavLink>
          {/*
          {currentUser && currentUser.authenticated ? (
            showLogout
              ? <LogoutButton>Logout</LogoutButton>
              : <Link to="/clients">Login</Link>
          ) : (
            <button
              className={cx({ active: activeView === 'login' })}
              onClick={showLoginView}>
              Login
            </button>
          )}
          */}
        </nav>
      ) : null}

      { isOpen && <BodyClass className="MainNav--isOpen" /> }
      <nav id="main-nav" className={cx('MainNav', { 'menu-open': isOpen })}>
        <CircleLogo />

        <div className="MainNav__content">
          {activeView === 'default' || !isMobile ? (
            <nav className="MainNav__nav-items">
              <Link to="/work">Work</Link>
              <Link to="/p/about-us">About</Link>
              <Link to="/news">News</Link>
              {/* currentUser && currentUser.authenticated ? (
                showLogout
                  ? <LogoutButton>Logout</LogoutButton>
                  : <Link to="/clients">Login</Link>
              ) : (
                <button
                  className={cx({ active: activeView === 'login' })}
                  onClick={showLoginView}>
                  Login
                </button>
              ) */}
            </nav>
          ) : null}

          {activeView === 'login' ? (
            <section className="MainNav__nav-section MainNav__login">
              <LoginForm className="MainNav__login__form" />
            </section>
          ) : null}
        </div>
      </nav>

      <button className="MainNav__button" onClick={onButtonClick}>
        {isOpen ? (
          <Icon name="close" />
        ) : (
          <Icon name="menu" />
        )}
      </button>
    </>
  )
}

export default MainNav
