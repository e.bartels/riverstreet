import * as React from 'react'
import { useEffect, useState, useRef } from 'react'
import { default as cx } from 'classnames'
import Icon from '~/ui/components/Icon'
import ScrollTo from '~/ui/components/ScrollTo'
import HideOnScroll from '~/ui/components/HideOnScroll'
import ColorTheme from '~/ui/components/ColorTheme'
import Markup from '~/ui/components/Markup'

import './HomeSplash.scss'

type Props = {
  text?: string,
  height: number,
  isActive: boolean,
}

export const HomeSplash = ({ text, height, isActive }: Props) => {
  const contentRef = useRef<HTMLDivElement>(null)
  const [showContent, setShowContent] = useState(false)

  useEffect(() => {
    setTimeout(() => {
      setShowContent(true)
    }, 100)
  }, [])

  return (
    <ColorTheme theme="white-white">
      <section
        className={cx('HomeSplash', { isActive })}
        id="home-splashpage"
        style={{ height }}
      >
        <div className="HomeSplash__background" />
        <div className="HomeSplash__content" ref={contentRef} style={{ opacity: showContent ? '' : 0 }}>
          <section id="who-we-are">
            <Markup>{text || ''}</Markup>
          </section>

          <HideOnScroll>
            <ScrollTo to="partners-section" className="scroll-down-link">
              <Icon name="arrow-down" />
            </ScrollTo>
          </HideOnScroll>
        </div>
      </section>
    </ColorTheme>
  )
}

export default HomeSplash
