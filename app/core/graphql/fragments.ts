import gql from 'graphql-tag'

export const IMAGE_FRAGMENT = gql`
  fragment Image on Image {
    id
    uid
    type
    title
    url
    width
    height
    images
    altText
    caption
  }
`

export const VIDEO_FRAGMENT = gql`
  fragment Video on Video {
    id
    uid
    type
    title
    width
    height
    images
    caption
    provider
    url
    embed
    altText
  }
`

export const CONTENT_ITEM_FRAGMENT = gql`
  fragment ContentItem on ContentItem {
    id
    type
    ...on TextContentItem {
      text
    }
    ...on ImageContentItem {
      image {
        ...Image
      }
    }
    ...on VideoContentItem {
      video {
        ...Video
      }
    }
  }
  ${IMAGE_FRAGMENT}
  ${VIDEO_FRAGMENT}
`
