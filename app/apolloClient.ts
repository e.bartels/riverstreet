import ApolloClient from 'apollo-boost'
import { InMemoryCache, IntrospectionFragmentMatcher } from 'apollo-cache-inmemory'
import { GRAPHQL_ENDPOINT } from '~/core/constants'
import { getDjangoCSRFToken } from '~/utils/django'
import schema from '~/types/schemaFragments.json'


/**
 * Create ApolloClient instance
 */
function configureApolloClient() {
  const client = new ApolloClient({
    uri: GRAPHQL_ENDPOINT,

    request: async (operation) => {
      operation.setContext({
        headers: {
          'X-CSRFToken': getDjangoCSRFToken(),
        },
      })
    },

    cache: new InMemoryCache({
      // required for union/interface fragments
      // see: https://www.apollographql.com/docs/react/advanced/fragments.html#fragment-matcher
      fragmentMatcher: new IntrospectionFragmentMatcher({
        introspectionQueryResultData: schema.data,
      }),
    }),
  })
  return client
}

export default configureApolloClient
