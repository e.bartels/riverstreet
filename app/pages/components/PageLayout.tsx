import * as React from 'react'
import { default as cx } from 'classnames'
import IntroSection from '~/ui/components/IntroSection'
import ColorTheme from '~/ui/components/ColorTheme'
import Markup from '~/ui/components/Markup'
import { SizedImageSpec } from '~/types/media'

import './PageLayout.scss'

type Props = {
  introImage?: {
    images: SizedImageSpec,
  } | null,
  introText?: string,
  children: React.ReactNode,
}

export const PageLayout = ({ introImage, introText, children }: Props) => (
  <div className={cx('PageLayout', { 'Page--has-intro': introImage && introText })}>
    {!!introImage && !!introText && (
      <IntroSection useMouseWheel>
        <div
          className="intro-image"
          style={{ backgroundImage: `url(${introImage.images.large.url})` }}
        >
          <div className="PageLayout__intro-caption sidebar">
            <Markup>{introText}</Markup>
          </div>
        </div>
      </IntroSection>
    )}

    <ColorTheme theme="black-blue">
      <section className="PageLayout__content">
        {children}
      </section>
    </ColorTheme>
  </div>
)

export default PageLayout
