/* eslint-disable jsx-a11y/anchor-is-valid */
import * as React from 'react'
import MDIcon from '~/ui/components/MDIcon'
import Markup from '~/ui/components/Markup'
import ContentItem from '~/core/components/ContentItem'
import PageLayout from '~/pages/components/PageLayout'
import SocialIcons from '~/core/components/SocialIcons'
import { Page as PageType } from '~/types'

import './AboutPage.scss'

type Props = {
  page: PageType,
}

export const AboutPage = ({ page }: Props) => (
  <div className="AboutPage">
    <PageLayout introImage={page.introImage} introText={page.introText}>
      <div className="Page__content">
        <div className="AboutPage__row">
          {/* first section */}
          {page.content.slice(0, 1).map(item => (
            (item.type === 'text' && item.text ? (
              <div className="AboutPage__content-left" key="first-content-item">
                <Markup>{item.text}</Markup>
                <hr className="show-for-mobile" />
              </div>
            ) : (
              <div className="Page__content-item" key={item.id}>
                <ContentItem item={item} />
              </div>
            ))
          ))}

          {/* sidebar info */}
          <div className="AboutPage__content-right">
            <h3 className="standout">Come say hi!</h3>
            <p>
              Riverstreet Productions, Inc.<br />
              <a href="https://goo.gl/maps/RZHTLjYxRzF2" target="_blank" rel="noopener noreferrer">
                8619 Washington Blvd.<br />
                Culver City, CA 90232
                &nbsp;<MDIcon name="pin" />
              </a>
            </p>

            <h3 className="standout">Contact Us</h3>
            <p>
              <a href="tel:13102892890">310-289-2890</a><br />
              <a href="fax:13102892897">310-289-2897 (fax)</a><br />
              <a href="mailto:info@riverstreet.net">info@riverstreet.net</a><br />

              <span className="AboutPage__social-icons">
                <SocialIcons />
              </span>
            </p>
          </div>
        </div>

        {/* remaining content sections */}
        {page.content.slice(1).map(item => (
          <div className="Page__content-item" key={item.id}>
            <ContentItem item={item} />
          </div>
        ))}
      </div>
    </PageLayout>
  </div>
)

export default AboutPage
