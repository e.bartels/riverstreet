import * as React from 'react'
import ContentItem from '~/core/components/ContentItem'
import PageLayout from '~/pages/components/PageLayout'
import { Page as PageType } from '~/types'

import './Page.scss'

type Props = {
  page: PageType,
}

export const Page = ({ page }: Props) => (
  <div className="Page">
    <PageLayout introImage={page.introImage} introText={page.introText}>
      <div className="Page__content">
        {page.content.map(item => (
          <div className="Page__content-item" key={item.id}>
            <ContentItem item={item} />
          </div>
        ))}
      </div>
    </PageLayout>
  </div>
)

export default Page
