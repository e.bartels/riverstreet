from django.db import models

from positions import PositionField
from autoslug import AutoSlugField


class PersonQuserySet(models.QuerySet):

    def published(self):
        return self.filter(published=True)


class Person(models.Model):
    name = models.CharField(max_length=255)
    slug = AutoSlugField(
        populate_from='name',
        max_length=512,
        unique=True,
        editable=True,
        help_text='A URL-friendly version of the title used in URLs',
    )
    title = models.CharField(max_length=255)
    published = models.BooleanField(
        default=True,
        help_text='Whether to publish on the site.')
    position = PositionField(help_text="Order of appearance")
    image = models.ForeignKey(
        'media.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
    )
    bio = models.TextField(blank=True)

    objects = PersonQuserySet.as_manager()

    class Meta:
        ordering = ('position', 'id')

    def __str__(self):
        return self.name

    @models.permalink
    def get_absolute_url(self):
        return 'people:detail', [self.slug]
