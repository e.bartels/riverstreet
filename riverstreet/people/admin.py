from django.contrib import admin


from ado.customadmin.admin import BaseModelAdminMixin
from .models import Person


@admin.register(Person)
class PersonAdmin(BaseModelAdminMixin, admin.ModelAdmin):
    icon = '<i class="icon material-icons">person_pin</i>'
    list_filter = ('published',)
    list_display = ('name', 'title', 'position', 'published',)
    prepopulated_fields = {'slug': ('name',)}
    html_editor_fields = ('bio',)
    verbose_image_fk_fields = ['image']
