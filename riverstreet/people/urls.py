from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^person/(?P<slug>[^/]+)/?$', views.person_detail, name='detail'),
]
