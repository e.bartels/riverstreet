from django.apps import AppConfig


class PeopleConfig(AppConfig):
    name = 'riverstreet.people'
    icon = '<i class="icon material-icons">person_pin</i>'
