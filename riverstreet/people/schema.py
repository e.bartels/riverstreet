import graphene
from graphene_django import DjangoObjectType
from django.core.exceptions import ObjectDoesNotExist

from riverstreet.people import models


class PersonType(DjangoObjectType):
    class Meta:
        name = 'Person'
        model = models.Person
        only_fields = [
            'id',
            'slug',
            'name',
            'title',
            'image',
            'bio',
        ]


class Query(object):
    people = graphene.List(PersonType)
    person = graphene.Field(
        PersonType,
        id=graphene.Int(),
        slug=graphene.String())

    def resolve_people(self, info, **kwargs):
        return models.Person.objects.published()

    def resolve_person(self, info, **kwargs):
        id = kwargs.get('id')
        slug = kwargs.get('slug')

        qs = models.Person.objects.all()
        if info.context.user.is_anonymous():
            qs = qs.published()

        try:
            if id is not None:
                return qs.get(pk=id)
            if slug is not None:
                return qs.get(slug=slug)
        except ObjectDoesNotExist:
            return None

        return None
