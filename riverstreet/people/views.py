from django.views.generic import DetailView

from riverstreet.core.views import IndexView
from .models import Person


index = IndexView.as_view(page_title='About our Team')


class PersonDetailView(DetailView):
    template_name = 'index.html'

    def get_queryset(self):
        user = self.request.user
        if user.is_authenticated() and user.is_staff:
            qs = Person.objects.all()
        else:
            qs = Person.objects.published()
        return qs


person_detail = PersonDetailView.as_view()
