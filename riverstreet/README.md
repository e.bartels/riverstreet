This directory contains the Django apps and configuration for the riverstreet.net project.

Django settings
===============

[`settings.py`](settings.py) The entrypoint for Django settings  
[`config/defaults.py`](config/defaults.py) contains shared settings for all environments  
[`config/dev.py`](config/dev.py) contains development-only settings  
[`config/production.py`](config/production.py) contains production-only settings  
[`config/tests.py`](config/tests.py) contains settings for running tests  
[`config/local.py`](config/local.py) an optional file for any local settings overrides

Using dev vs production settings can be toggled using an environment variable, `DJANGO_ENV`. The default is to use the dev settings if it is not set. To turn on production settings, set:

    DJANGO_ENV=production

The `Dockerfile` in the top-level of this repository does this when building the docker image.

The settings in `settings.py` can also  be set via environment variables using [django-environ][django-environ]. See the top-level project README file for more explanation.


GraphQL schemas
===============

Aside from the normal Django files, each app has a `schema.py` file. These are the [GraphQL][graphql] type definitions and loaders that provide the GraphQL API.  See [django-graphene][graphene] project for more information on how schemas work.

You will also find some `loaders.py` files. These are used by the `schema.py` files to load data from Django models. See [the dataloader documentation][dataloader] for more information on usage.


Django templates
================

The Django template files can be found in folder [`templates/`](templates/)

Because this website is largely built in React, and connected to the server via GraphQL, it does not use Django templates to a large extent. There is a [templates/index.html](templates/index.html) template, which is used to bootstrap the site's javascript and styles, as well as setting of meta tags.


Django apps
===========

[`blog/`](blog/)  
The blog app, located at `/news` on the site.

[`configuration/`](configuration/)  
Configuration settings that are stored in the database. It includes site title, meta tags, google analytics code, and social media urls

[`core/`](core/)  
A Django app for code that is shared between other apps.

It includes global signals listeners in [core/listeners.py](core/listeners.py).

GraphQL utility code is in [core/gql/](core/gql/), and GraphQL schemas are defined in [core/schema/](core/schema/). In particular, there are schemas for `Image` and `Video` models.

[`pages/`](pages/)  
An app for site pages (currently this is just the About page located at `/about`)

[`people/`](people/)  
The people app is currently not visible in the menu, but can be viewed at `/people`

[`projects/`](projects/)  
The projects app contains projects and categories. Projects consist of videos and images and text descriptions.

[`work/`](work/)  
`Project` and `Category` models. Projects consist of videos and images and text content. Each Project can be put into one or more Categories.

This app provides the core functionality for website visitors to view video portfolios. You can view the work section at URL path `/work` and `/work/category/SOME_CATEGORY_SLUG`

NOTE: Most Django models in the admin have a `View on site` link at the top/right, which you can use to view the front-end website for that model instance.

[`promotion/`](promotion/)  
This app is similar to the `work` app, but for promotional content. It allows creating `MiniSite` models, each of which itself contains `Project` and `Category` models.

Example URL path `/promotion/MINISITE_SLUG/category/CATEGORY_SLUG`

[`vimeovideos/`](vimeovideos/)  
This app provides a wrapper/mirror into Riverstreet's Vimeo albums.

For example, an album with id `5658009` would be viewed at URL path `/vimeo/album/5658009`

See [vimeovideos/schema.py](vimeovideos/schema.py), which is where access sto the api is defined. You will need to provide settings to access vimeo videos as Django settings:

For access to Vimeo albums via the Vimeo API, these Django settings are required to be set:  
`VIMEO_ACCESS_TOKEN`  
`VIMEO_CLIENT_ID`  
`VIMEO_CLIENT_SECRET`  

These settings can be set via environment variable.


[django-environ]: https://github.com/joke2k/django-environ
[graphql]: https://graphql.org/
[graphene]: https://docs.graphene-python.org/projects/django/en/latest/
[dataloader]: https://docs.graphene-python.org/en/latest/execution/dataloader/

