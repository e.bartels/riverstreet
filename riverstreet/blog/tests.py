import factory
import factory.django
# from datetime import datetime

# from django_webtest import WebTest
from django.test import TestCase
# from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
# from django.utils import timezone

from .models import Entry


class UserFactory(factory.Factory):
    username = factory.Sequence(lambda n: 'test{0}'.format(n))

    class Meta:
        model = User

    @factory.post_generation
    def password(self, create, extracted, **kwargs):
        if create:
            self.set_password(extracted or 'test')
            self.save()


class CategoryFactory(factory.DjangoModelFactory):
    name = factory.Sequence(lambda n: u'Test Category {0}'.format(n))

    class Meta:
        model = 'blog.Category'


class EntryContentFactory(factory.DjangoModelFactory):
    text = '<p>Test blog post contentp>'

    class Meta:
        model = 'blog.EntryContent'


class EntryFactory(factory.DjangoModelFactory):
    title = factory.Sequence(lambda n: u'Test Entry {0}'.format(n))
    published = True
    author = factory.SubFactory(UserFactory)

    content = factory.RelatedFactory(
        EntryContentFactory,
        'entry',
    )

    class Meta:
        model = 'blog.Entry'


class LinkFactory(factory.DjangoModelFactory):
    title = factory.Sequence(lambda n: u'Test Entry {0}'.format(n))
    url = factory.Sequence(lambda n: u'http://test-{0}'.format(n))

    class Meta:
        model = 'blog.Link'


class BlogEntryModelTest(TestCase):

    def test_create(self):
        entry = EntryFactory()
        self.assertTrue(entry.title)
        self.assertTrue(entry.slug)
        self.assertTrue(entry.author)
        self.assertTrue(entry.published)
        self.assertTrue(entry.pub_date)

    def test_excerpt(self):
        entry = EntryFactory()
        entry.content.all().delete()
        EntryContentFactory(entry=entry, text='<p>This is a test entry</p>')
        EntryContentFactory(entry=entry, text='<p>with more text here</p>')
        excerpt = entry.get_excerpt(4)
        self.assertEqual(excerpt, '<p>This is a test ...</p>')

    def test_published_queryset(self):
        entry1 = EntryFactory()
        entry2 = EntryFactory(published=False)

        self.assertEqual(Entry.objects.all().count(), 2)
        published = Entry.objects.published()
        self.assertEqual(published.count(), 1)

        self.assertTrue(entry1 in published)
        self.assertTrue(entry2 not in published)

    def test_entry_categories(self):
        cat1 = CategoryFactory(name='cat1')
        cat2 = CategoryFactory(name='cat2')

        entry1 = EntryFactory()
        entry1.categories.add(cat1)
        entry1.categories.add(cat2)

        entry2 = EntryFactory(published=False)
        entry2.categories.add(cat1)

        self.assertTrue(cat1 in entry1.categories.all())
        self.assertTrue(cat2 in entry1.categories.all())
        self.assertTrue(cat1 in entry2.categories.all())
        self.assertFalse(cat2 in entry2.categories.all())

        self.assertTrue(entry1 in cat1.entries.all())
        self.assertTrue(entry2 in cat1.entries.all())
        self.assertTrue(entry1 in cat2.entries.all())
        self.assertFalse(entry2 in cat2.entries.all())


# class EntryListViewTest(WebTest):
#
#     @classmethod
#     def setUpTestData(cls):
#         cls.url = reverse('api:blog:entry-list')
#
#         cat1 = CategoryFactory(name='cat1')
#         cat2 = CategoryFactory(name='cat2')
#
#         entry1 = EntryFactory()
#         entry1.categories.add(cat1)
#         entry1.categories.add(cat2)
#
#         entry2 = EntryFactory()
#         entry2.categories.add(cat1)
#
#     def test_get(self):
#         resp = self.app.get(self.url, status=200)
#         self.assertTrue(resp.json['results'])
#         self.assertEqual(len(resp.json['results']), 2)
#
#         resp_keys = set(resp.json['results'][0].keys())
#         self.assertSetEqual(
#             resp_keys,
#             set(['title', 'slug', 'published', 'pub_date',
#                  'year', 'month', 'day',
#                  'image', 'categories', 'excerpt']))
#
#     def test_published_not_returned(self):
#         EntryFactory(published=False)
#
#         resp = self.app.get(self.url, status=200)
#         self.assertEqual(len(resp.json['results']), 2)
#
#     def test_filter_by_category(self):
#         url = self.url + '?category=cat1'
#         resp = self.app.get(url, status=200)
#         self.assertEqual(len(resp.json['results']), 2)
#
#         url = self.url + '?category=cat2'
#         resp = self.app.get(url, status=200)
#         self.assertEqual(len(resp.json['results']), 1)
#
#         empty_cat = CategoryFactory()
#         url = self.url + '?category={0}'.format(empty_cat.slug)
#         resp = self.app.get(url, status=200)
#         self.assertEqual(len(resp.json['results']), 0)
#
#     def test_filter_by_featured(self):
#         entry = EntryFactory(featured=True)
#         url = self.url + '?featured=True'
#         resp = self.app.get(url, status=200)
#         self.assertEqual(len(resp.json['results']), 1)
#         self.assertTrue(resp.json['results'][0]['slug'], entry.slug)
#
#     def test_paginated_entries(self):
#         for i in xrange(0, 20):
#             EntryFactory()
#
#         resp = self.app.get(self.url, status=200)
#         n_per_page = 7
#         self.assertEqual(len(resp.json['results']), n_per_page)
#
#         resp2 = self.app.get(self.url + '?page=2', status=200)
#         self.assertEqual(len(resp.json['results']), n_per_page)
#         self.assertNotEqual(resp2.json['results'][0], resp.json['results'][0])
#
#
# class EntryDetailViewTest(WebTest):
#
#     @classmethod
#     def setUpTestData(cls):
#
#         cat1 = CategoryFactory(name='cat1')
#         cat2 = CategoryFactory(name='cat2')
#
#         entry = EntryFactory()
#         entry.categories.add(cat1)
#         entry.categories.add(cat2)
#         cls.entry = entry
#
#         cls.url = reverse('api:blog:entry-detail',
#                           args=[entry.slug])
#
#     def test_get(self):
#         resp = self.app.get(self.url, status=200)
#         self.assertEqual(self.entry.slug, resp.json['slug'])
#
#         resp_keys = set(resp.json.keys())
#         self.assertSetEqual(
#             resp_keys,
#             set(['title', 'slug', 'published', 'pub_date',
#                  'year', 'month', 'day', 'image',
#                  'categories', 'content', 'excerpt']))
#
#     def test_unpublished_returns_404(self):
#         entry = EntryFactory(published=False)
#         url = reverse('api:blog:entry-detail',
#                           args=[entry.slug])
#         self.app.get(url, status=404)
#
#
# class EntryDatesViewTest(WebTest):
#
#     @classmethod
#     def setUpTestData(cls):
#         EntryFactory(
#             pub_date=timezone.make_aware(datetime(2015, 3, 1))
#         )
#         EntryFactory(
#             pub_date=timezone.make_aware(datetime(2014, 11, 1))
#         )
#         EntryFactory(
#             pub_date=timezone.make_aware(datetime(2013, 5, 1))
#         )
#         EntryFactory(
#             pub_date=timezone.make_aware(datetime(2015, 3, 10))
#         )
#         cls.url = reverse('api:blog:entry-dates')
#
#     def test_get(self):
#         resp = self.app.get(self.url, status=200)
#         self.assertEqual(len(resp.json), 3)
#         self.assertListEqual(
#                 [(2015, 3), (2014, 11), (2013, 5)],
#                 [(d['year'], d['month']) for d in resp.json])
#
#
# class CategoryListView(WebTest):
#
#     @classmethod
#     def setUpTestData(cls):
#         CategoryFactory(name='cat1')
#         CategoryFactory(name='cat2')
#
#         cls.url = reverse('api:blog:category-list')
#
#     def test_get(self):
#         resp = self.app.get(self.url, status=200)
#         self.assertEqual(len(resp.json), 2)
#
#         resp_keys = set(resp.json[0].keys())
#         self.assertSetEqual(
#             resp_keys,
#             set(['name', 'slug', 'position']))
#
#
# class LinkListView(WebTest):
#
#     @classmethod
#     def setUpTestData(cls):
#         LinkFactory()
#         LinkFactory()
#         LinkFactory()
#         cls.url = reverse('api:blog:link-list')
#
#     def test_get(self):
#         resp = self.app.get(self.url, status=200)
#         self.assertEqual(len(resp.json), 3)
#
#         resp_keys = set(resp.json[0].keys())
#         self.assertSetEqual(
#             resp_keys,
#             set(['title', 'url', 'position']))
