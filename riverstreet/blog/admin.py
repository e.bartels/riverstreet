from django.contrib import admin
from django import forms
from django.contrib.auth.models import User

from ado.customadmin.admin import BaseModelAdminMixin, SortableAdminListMixin
from ado.customadmin.forms import SmartMultiSelect

from ado.content.admin import ContentItemsInline
from .models import Entry, EntryContent, Category, Link


class EntryContentInline(ContentItemsInline):
    """Inline for blog entry contents (images/video/text)"""
    model = EntryContent
    verbose_name_plural = 'Entry Content'


class EntryAdminForm(forms.ModelForm):
    """Base Entry admin form"""
    class Meta:
        widgets = {
            'categories': SmartMultiSelect,
        }


@admin.register(Entry)
class EntryAdmin(BaseModelAdminMixin, admin.ModelAdmin):
    icon = '<i class="icon material-icons">format_align_left</i>'
    list_display = [
        'title',
        'pub_date',
        'author',
        'created',
        'modified',
        'published'
    ]
    list_filter = [
        'categories',
        'published',
        'author',
        'pub_date',
    ]
    list_select_related = True
    search_fields = ['title', 'slug', 'categories__name', 'content__text']
    date_hierarchy = 'pub_date'
    ordering = ('-pub_date',)

    form = EntryAdminForm
    prepopulated_fields = {
        'slug': ('title',)
    }
    fieldsets = (
        (None, {
            'fields': ('title', 'slug', 'author', ('pub_date', 'published'), 'excerpt')
        }),
        (None, {
            'fields': ('categories',)
        }),
    )
    inlines = [EntryContentInline]
    html_editor_fields = ['excerpt']

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        # Override author field. If not a superuser, then only allow that user
        # to select themselves as author.
        if db_field.name == 'author':
            if request:
                if not request.user.is_superuser:
                    kwargs['queryset'] = User.objects.filter(
                                                        pk=request.user.pk)
                kwargs['empty_label'] = None
                kwargs['initial'] = request.user.pk
        return super(EntryAdmin, self).formfield_for_foreignkey(
                                        db_field, request, **kwargs)


@admin.register(Category)
class CategoryAdmin(BaseModelAdminMixin, SortableAdminListMixin, admin.ModelAdmin):
    icon = '<i class="icon material-icons">collections_bookmark</i>'
    prepopulated_fields = {
        'slug': ('name',)
    }


@admin.register(Link)
class LinkAdmin(BaseModelAdminMixin, SortableAdminListMixin, admin.ModelAdmin):
    icon = '<i class="icon material-icons">link</i>'
    list_display = ('title', 'url', 'position',)
