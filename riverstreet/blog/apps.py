from django.apps import AppConfig


class BlogConfig(AppConfig):
    name = 'riverstreet.blog'
    icon = '<i class="icon material-icons">format_align_left</i>'
