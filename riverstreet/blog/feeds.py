from django.contrib.syndication.views import Feed
from django.utils.feedgenerator import Atom1Feed
from django.core.urlresolvers import reverse

from .models import Entry


class LatestEntriesFeed(Feed):
    link = "/news/feed/"
    description_template = "blog_feed_description.html"

    def description(self):
        return 'Latest blog entries.'

    def link(self):
        return reverse('blog:feed')

    def items(self):
        return Entry.objects.recent()

    def item_pubdate(self, item):
        return item.pub_date

    def item_description(self, item):
        return item.get_excerpt()

    def title(self):
        return 'Latest blog entries'

    def item_author_name(self, item):
        return item.author.get_full_name() or item.author.username


class AtomLatestEntriesFeed(LatestEntriesFeed):
    feed_type = Atom1Feed
    subtitle = LatestEntriesFeed.description
