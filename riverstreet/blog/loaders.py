from promise import Promise
from promise.dataloader import DataLoader

from riverstreet.blog import models


class EntryContentLoader(DataLoader):
    def batch_load_fn(self, keys):
        qs = (models.EntryContent.objects
              .filter(entry_id__in=keys)
              .select_related('image', 'video')
              .order_by('entry_id', 'position', 'id'))
        return Promise.resolve([
            [item for item in qs if item.entry_id == id]
            for id in keys
        ])


class EntryCategoryLoader(DataLoader):
    def batch_load_fn(self, keys):
        qs = (models.Entry.categories.through.objects
              .filter(entry_id__in=keys)
              .exclude(entry__published=False)
              .select_related('category')
              )
        return Promise.resolve([
            [item.category for item in qs if item.entry_id == id]
            for id in keys
        ])


Loaders = dict(
    entry_content=EntryContentLoader,
    entry_category=EntryCategoryLoader,
)
