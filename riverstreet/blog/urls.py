from django.conf.urls import url

from . import feeds
from . import views


urlpatterns = [
    # index
    url(r'^$', views.index, name='index'),

    # category index
    url(r'^c/(?P<category_slug>[\w-]+)/?$',
        views.index,
        name='category-index'),

    # year index
    url(r'^(?P<year>\d{4})/?$',
        views.index,
        name='year-index'),
    url(r'^(?P<year>\d{4})/(?P<month>\w{3})/?$',
        views.index),

    # archive
    url(r'^archive/?$',
        views.index,
        name='archive'),

    # entry detail
    url(r'^(?P<year>\d{4})/(?P<slug>[\w-]+)/?$',
        views.entry_detail,
        name='entry-detail'),
    url(r'^(?P<year>\d{4})/(?P<month>\w{3})/(?P<day>\d{1,2})/(?P<slug>[\w-]+)/?$',
        views.entry_detail,
        name='entry-detail'),

    # entry detail by category
    url(r'^c/(?P<category_slug>[\w-]+)/(?P<slug>[\w-]+)/?$',
        views.entry_detail,
        name='entry-detail-category'),

    # entry detail for archive
    url(r'^archive/(?P<slug>[\w-]+)/?$',
        views.entry_detail,
        name='entry-detail-archive'),

    # Feeds
    url(r'^feeds/$', feeds.LatestEntriesFeed(), name='feed'),
    url(r'^feeds/latest/?$', feeds.LatestEntriesFeed(), name='feed'),
    url(r'^feeds/rss/$', feeds.LatestEntriesFeed(), name='feed-rss'),
    url(r'^feeds/atom/$', feeds.AtomLatestEntriesFeed(), name='feed-atom'),
]
