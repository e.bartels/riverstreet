import graphene
from graphene import relay
from graphene_django import DjangoObjectType
from graphene_django.filter import DjangoFilterConnectionField
from django.core.exceptions import ObjectDoesNotExist

from riverstreet.core.schema.content import ContentItem
from riverstreet.blog import models


class CategoryType(DjangoObjectType):

    class Meta:
        name = 'BlogCategory'
        model = models.Category
        only_fields = [
            'id',
            'slug',
            'name',
            'position',
            'entries',
        ]

    def resolve_entries(self, info, **kwargs):
        return self.entries.published()


class AssocEntryCategory(DjangoObjectType):
    class Meta:
        model = models.Category
        only_fields = [
            'id',
            'slug',
            'name',
            'position',
        ]


class EntryType(DjangoObjectType):
    year = graphene.Int(required=True)
    categories = graphene.List(AssocEntryCategory, required=True)
    content = graphene.List(graphene.NonNull(ContentItem), required=True)

    class Meta:
        name = 'BlogEntry'
        model = models.Entry
        interfaces = [relay.Node]
        only_fields = [
            'id',
            'slug',
            'title',
            'pub_date',
            'year',
            'categories',
            'excerpt',
            'content',
        ]
        filter_fields = {
            'categories__slug': ['exact'],
            'pub_date': ['year', 'year__gt', 'year__lt'],
        }

    def resolve_year(self, info, **kwargs):
        return self.pub_date.year

    def resolve_categories(self, info, **kwargs):
        return info.context.loaders.blog.entry_category.load(self.id)

    def resolve_content(self, info, **kwargs):
        return info.context.loaders.blog.entry_content.load(self.id)


class LinkType(DjangoObjectType):

    class Meta:
        name = 'BlogLink'
        model = models.Link
        only_fields = [
            'id',
            'url',
            'title',
            'position',
        ]


class Query(object):
    blog_entries = DjangoFilterConnectionField(EntryType)
    blog_categories = graphene.List(CategoryType)
    blog_links = graphene.List(LinkType)
    blog_years = graphene.List(graphene.Int)

    blog_entry = graphene.Field(
        EntryType,
        id=graphene.Int(),
        slug=graphene.String())

    blog_category = graphene.Field(
        CategoryType,
        id=graphene.Int(),
        slug=graphene.String())

    def resolve_blog_entries(self, info, **kwargs):
        return models.Entry.objects.published()

    def resolve_blog_categories(self, info, **kwargs):
        return models.Category.objects.in_use()

    def resolve_blog_links(self, info, **kwargs):
        return models.Link.objects.all()

    def resolve_blog_years(self, info, **kwargs):
        qs = (models.Entry.objects
              .published()
              .dates('pub_date', 'year', order='DESC'))
        return [y.year for y in qs]

    def resolve_blog_entry(self, info, **kwargs):
        id = kwargs.get('id')
        slug = kwargs.get('slug')

        qs = models.Entry.objects.all()
        user = info.context.user
        if user.is_anonymous() or not user.is_staff:
            qs = qs.published()

        try:
            if id is not None:
                return qs.get(pk=id)
            if slug is not None:
                return qs.get(slug=slug)
        except ObjectDoesNotExist:
            return None

        return None

    def resolve_blog_category(self, info, **kwargs):
        id = kwargs.get('id')
        slug = kwargs.get('slug')

        qs = models.Category.objects.in_use()

        try:
            if id is not None:
                return qs.get(pk=id)
            if slug is not None:
                return qs.get(slug=slug)
        except ObjectDoesNotExist:
            return None

        return None
