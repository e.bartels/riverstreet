import re

from django import http
from django.core.urlresolvers import reverse
from django.views.generic import DetailView
from django.views.generic import RedirectView
from django.conf import settings

from sorl import thumbnail
from riverstreet.core.views import IndexView
from .models import Entry


class BlogIndex(IndexView):
    page_title = 'News'

    def get(self, request, *args, **kwargs):
        cat = self.request.GET.get('cat')
        if cat:
            url = reverse('blog:category-index', args=[cat])
            return http.HttpResponseRedirect(url)
        return super(BlogIndex, self).get(request, *args, **kwargs)


index = BlogIndex.as_view()


class EntryDetailView(DetailView):
    template_name = 'index.html'

    def get_queryset(self):
        user = self.request.user
        if user.is_authenticated() and user.is_staff:
            qs = Entry.objects.all()
        else:
            qs = Entry.objects.published()

        return qs.prefetch_related('content')

    def get_image(self, entry):
        content_items = entry.content.all()
        try:
            item = [c for c in content_items if c.type in ('image', 'video')][0]
            image = getattr(item, item.type).get_image()
            sizes = dict(settings.SCALED_IMAGE_SIZES)
            thumb = thumbnail.get_thumbnail(image, sizes['large'])
            return [
                self.request.build_absolute_uri(thumb.url),
                thumb.width,
                thumb.height,
            ]
        except IndexError:
            pass

        return [None, None, None]

    def get_context_data(self, **kwargs):
        context = super(EntryDetailView, self).get_context_data(**kwargs)

        # meta tags
        entry = context['entry']
        [og_image, og_image_width, og_image_height] = self.get_image(entry)
        context.update({
            'page_title': entry.title,
            'og_type': 'article',
            'og_url': entry.get_absolute_url(),
            'og_image': og_image,
            'og_image_width': og_image_width,
            'og_image_height': og_image_height,
        })

        return context


entry_detail = EntryDetailView.as_view()


class BlogRedirectView(RedirectView):
    permanent = True

    def get_redirect_url(self, *args, **kwargs):
        url = self.request.get_full_path()
        return re.sub(r'^/blog', '/news', url)
