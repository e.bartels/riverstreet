from django.db import models
from django.utils.text import Truncator
from django.utils import timezone
from django.contrib.auth.models import User
from django.core.cache import cache
from django.core.urlresolvers import reverse
from django.conf import settings

from positions.fields import PositionField
from autoslug import AutoSlugField
from ado.content.models import ContentItem


class CategoryQuerySet(models.QuerySet):
    def in_use(self):
        return self.filter(entries__isnull=False).distinct()


class Category(models.Model):
    name = models.CharField(max_length=200)
    slug = AutoSlugField(
        populate_from='name',
        max_length=200,
        unique=True,
        editable=True,
        help_text='A URL-friendly version of the title used in URLs',
    )
    position = PositionField()

    objects = CategoryQuerySet.as_manager()

    class Meta:
        ordering = ('position', 'id')
        verbose_name_plural = 'Categories'

    def __str__(self):
        return self.name

    @models.permalink
    def get_absolute_url(self):
        return 'blog:category-index', [self.slug]


class EntryQuerySet(models.QuerySet):
    NOW_CACHE_KEY = 'EntryQuerySet.now'

    def published(self):
        now = cache.get(self.NOW_CACHE_KEY)
        if not now:
            now = timezone.now()
            cache.set(self.NOW_CACHE_KEY, now)
        return self.filter(published=True, pub_date__lte=now)

    def recent(self, count=10):
        return self.published()[:count]


class Entry(models.Model):
    title = models.CharField(max_length=200)
    slug = AutoSlugField(
            populate_from='title',
            max_length=200,
            unique=True,
            editable=True,
            help_text='A URL-friendly version of the title used in URLs')
    author = models.ForeignKey(User)
    pub_date = models.DateTimeField(
            'Date published',
            default=timezone.now,
            db_index=True,
            help_text='Date you would like this entry to go live on the site.')
    published = models.BooleanField(
            default=True,
            help_text='Select to publish on the site.')
    excerpt = models.TextField(blank=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    categories = models.ManyToManyField(
            Category,
            related_name='entries',
            blank=True)

    objects = EntryQuerySet.as_manager()

    class Meta:
        verbose_name = 'Blog Entry'
        verbose_name_plural = 'Blog Entries'
        ordering = ('-pub_date', 'id')
        get_latest_by = 'pub_date'

    def __str__(self):
        return u'%s' % self.title

    @models.permalink
    def get_absolute_url(self):
        if settings.USE_TZ:
            tz = timezone.get_default_timezone()
            pub_date = timezone.localtime(self.pub_date, tz)
        else:
            pub_date = self.pub_date
        args = [pub_date.strftime("%Y"), self.slug]
        return ('blog:entry-detail', args)

    def get_excerpt(self, length=100):
        if self.excerpt:
            return self.excerpt
        texts = (self.content
                 .exclude(text='')
                 .values_list('text', flat=True)
                 .order_by('position'))
        body = '\n'.join(texts)
        return Truncator(body).words(length, html=True, truncate=' ...')


class EntryContent(ContentItem):
    related_model = Entry

    def parent_admin_url(self):
        return reverse('admin:blog_entry_change', args=[self.entry.pk])


class Link(models.Model):
    url = models.URLField(max_length=255)
    title = models.CharField(max_length=255)
    position = PositionField()

    class Meta:
        ordering = ('position', 'id',)

    def __str__(self):
        return self.url
