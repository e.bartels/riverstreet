import graphene

# from graphene_django.debug import DjangoDebug

from riverstreet.core.schema import schema as core_schema
from riverstreet.work import schema as work_schema
from riverstreet.promotion import schema as promotion_schema
from riverstreet.pages import schema as pages_schema
from riverstreet.blog import schema as blog_schema
from riverstreet.people import schema as people_schema
from riverstreet.configuration import schema as configuration_schema
from riverstreet.core.schema.content import TextContentItem, ImageContentItem, VideoContentItem
from riverstreet.vimeovideos import schema as vimeo_schema


class Query(
        core_schema.Query,
        pages_schema.Query,
        work_schema.Query,
        promotion_schema.Query,
        blog_schema.Query,
        people_schema.Query,
        configuration_schema.Query,
        vimeo_schema.Query,
        graphene.ObjectType):
    """
    The root graphql query
    It inherits from any other Query objects
    """
    # Get debug info - used in conjunction with DjangoDebugMiddleware.
    # debug = graphene.Field(DjangoDebug, name='_debug')


class Mutation(
        core_schema.Mutation,
        graphene.ObjectType):
    """
    The root graphql mutations
    """


schema = graphene.Schema(
    query=Query,
    mutation=Mutation,
    types=[
        TextContentItem,
        ImageContentItem,
        VideoContentItem,
    ]
)
