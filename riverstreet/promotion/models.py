from django.db import models
from django.core.urlresolvers import reverse
from autoslug import AutoSlugField
from positions import PositionField
from ado.content.models import ContentItem
from riverstreet.core.models import BaseProject


class MiniSiteQuerySet(models.QuerySet):
    def published(self):
        return self.filter(published=True)


class MiniSite(models.Model):
    name = models.CharField(max_length=512)
    slug = AutoSlugField(
        populate_from='name',
        max_length=512,
        unique=True,
        editable=True,
        help_text='A URL-friendly version of the name used in URLs',
    )
    published = models.BooleanField(
        default=True,
        db_index=True,
        help_text='Whether to publish on the site.')
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    objects = MiniSiteQuerySet.as_manager()

    class Meta:
        verbose_name = 'Mini Site'
        verbose_name_plural = 'Mini Sites'
        ordering = ('-created', 'id')

    def __str__(self):
        return self.name

    @models.permalink
    def get_absolute_url(self):
        return 'promotion:minisite', [self.slug]


class CategoryQuerySet(models.QuerySet):
    def published(self):
        return self.filter(published=True)


class Category(models.Model):
    """
    Project Category
    """
    minisite = models.ForeignKey(MiniSite, related_name='categories')
    title = models.CharField(max_length=512)
    slug = AutoSlugField(
        populate_from='title',
        max_length=512,
        unique=True,
        editable=True,
        help_text='A URL-friendly version of the title used in URLs',
    )
    published = models.BooleanField(
        default=True,
        db_index=True,
        help_text='Whether to publish on the site.')

    position = PositionField(collection='minisite')

    intro_image = models.ForeignKey(
        'media.Image',
        null=True,
        blank=True,
        help_text="A full-bleed image shown at top of category pages",
        related_name='+',
    )
    intro_text = models.TextField(
        blank=True,
        help_text="Text to show in intro section of category page",
    )

    notes = models.TextField(blank=True, help_text="For internal use")

    objects = CategoryQuerySet.as_manager()

    class Meta:
        ordering = ('minisite', 'position', 'id',)
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'

    def __str__(self):
        return '{}: {}'.format(self.minisite, self.title)

    @models.permalink
    def get_absolute_url(self):
        return 'promotion:category', [self.minisite.slug, self.slug]


class Project(BaseProject):
    """
    Project model
    """
    categories = models.ManyToManyField(
        Category,
        through='ProjectCategoryMembership',
        related_name='projects'
    )

    @models.permalink
    def get_absolute_url(self):
        return 'promotion:project-detail', [self.slug]


class ProjectContent(ContentItem):
    related_model = Project

    def parent_admin_url(self):
        return reverse('admin:promotion_project_change', args=[self.project.pk])


class ProjectCategoryMembership(models.Model):
    """
    Join model for project categories
    """
    project = models.ForeignKey(
        Project,
        related_name='category_memberships',
        on_delete=models.CASCADE,
    )
    category = models.ForeignKey(
        Category,
        related_name='project_memberships',
        on_delete=models.CASCADE,
    )
    position = PositionField(collection='category')

    class Meta:
        ordering = ('category', 'position', 'id')
        unique_together = ('project', 'category')

    def __str__(self):
        return self.project.title

    def get_absolute_url(self):
        return self.project.get_absolute_url()


class ReelQuerySet(models.QuerySet):
    def published(self):
        return self.filter(published=True)


class Reel(models.Model):
    """
    Reel model (client pages)
    """
    title = models.CharField(max_length=512)
    slug = AutoSlugField(
        populate_from='title',
        max_length=512,
        unique=True,
        editable=True,
        help_text='A URL-friendly version of the title used in URLs',
    )
    published = models.BooleanField(
        default=True,
        db_index=True,
        help_text='Whether to publish on the site.')
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    notes = models.TextField(blank=True, help_text="For internal use")

    objects = ReelQuerySet.as_manager()

    class Meta:
        ordering = ('-id',)
        verbose_name = 'Reel'
        verbose_name_plural = 'Reels'

    def __str__(self):
        return self.title

    @models.permalink
    def get_absolute_url(self):
        return 'promotion:reel-detail', [self.slug]

    def get_image(self):
        return self.get_image_from_content()

    def get_image_from_content(self):
        content = self.content.filter(text='').select_related('image', 'video')
        videos = [i for i in content if i.video]
        if videos:
            return videos[0].video.video_image

        images = [i for i in content if i.image]
        if images:
            return images[0].image.filename


class ReelContent(ContentItem):
    related_model = Reel

    def parent_admin_url(self):
        return reverse('admin:promotion_reel_change', args=[self.reel.pk])
