import graphene
from graphene import relay
from graphene_django import DjangoObjectType
from graphene_django.filter import DjangoFilterConnectionField

from django.core.exceptions import ObjectDoesNotExist

from riverstreet.core.schema.content import ContentItem
from riverstreet.core.schema.media import SizedImages, Video
from riverstreet.promotion import models


class CategoryType(DjangoObjectType):
    class Meta:
        name = 'ProjectCategory'
        model = models.Category
        only_fields = [
            'id',
            'slug',
            'title',
            'position',
            'intro_image',
            'intro_text',
            'projects',
        ]

    def resolve_projects(self, info, **kwargs):
        return info.context.loaders.promotion.category_projects.load(self.id)

    def resolve_intro_image(self, info, **kwargs):
        if self.intro_image_id:
            return info.context.loaders.core.image.load(self.intro_image_id)
        return None

    def resolve_intro_text(self, info, **kwargs):
        if not self.intro_image_id and not self.intro_text:
            return self.title
        return self.intro_text


class AssocProjectCategory(DjangoObjectType):
    class Meta:
        model = models.Category
        only_fields = [
            'id',
            'slug',
            'title',
        ]


class ProjectType(DjangoObjectType):
    image = SizedImages()
    video = graphene.Field(Video)
    categories = graphene.List(graphene.NonNull(AssocProjectCategory), required=True)
    content = graphene.List(graphene.NonNull(ContentItem), required=True)

    class Meta:
        name = 'Project'
        model = models.Project
        interfaces = [relay.Node]
        only_fields = [
            'id',
            'slug',
            'title',
            'client',
            'date',
            'image',
            'video',
            'video_intro_caption',
            'show_project_details',
            'categories',
            'content',
        ]
        filter_fields = {
            'categories__slug': ['exact'],
        }

    def resolve_video(self, info, **kwargs):
        return self.video

    def resolve_image(self, info, **kwargs):
        loaders = info.context.loaders

        if self.image_id:
            return (loaders.core
                    .image.load(self.image_id)
                    .then(lambda image: image.filename))
        elif self.video_id:
            return (loaders.core
                    .video.load(self.video_id)
                    .then(lambda video: video.video_image))

        def extract_content_image(items):
            media_items = [item for item in items if item.video or item.image]
            try:
                item = media_items[0]
                if item.video:
                    return item.video.video_image
                else:
                    return item.image.filename
            except IndexError:
                return None

        return (loaders.promotion
                .project_content.load(self.id)
                .then(extract_content_image))

    def resolve_categories(self, info, **kwargs):
        return info.context.loaders.promotion.project_categories.load(self.id)

    def resolve_content(self, info, **kwargs):
        return info.context.loaders.promotion.project_content.load(self.id)


class ReelType(DjangoObjectType):
    content = graphene.List(graphene.NonNull(ContentItem), required=True)

    class Meta:
        name = 'Reel'
        model = models.Reel
        only_fields = [
            'id',
            'slug',
            'title',
            'content',
        ]

    def resolve_content(self, info, **kwargs):
        return info.context.loaders.promotion.reel_content.load(self.id)


class Query(object):
    categories = graphene.List(CategoryType, minisite=graphene.String())
    projects = DjangoFilterConnectionField(ProjectType)

    category = graphene.Field(
        CategoryType,
        id=graphene.Int(),
        slug=graphene.String(),
    )

    project = graphene.Field(
        ProjectType,
        id=graphene.Int(),
        slug=graphene.String(),
    )

    reel = graphene.Field(
        ReelType,
        id=graphene.Int(),
        slug=graphene.String()
    )

    def resolve_categories(self, info, **kwargs):
        minisite = kwargs.get('minisite')
        qs = models.Category.objects.published()
        if minisite:
            qs = qs.filter(minisite__slug=minisite)
            return qs or None
        return qs

    def resolve_projects(self, info, **kwargs):
        return models.Project.objects.published()

    def resolve_category(self, info, **kwargs):
        id = kwargs.get('id')
        slug = kwargs.get('slug')

        qs = models.Category.objects.all()
        if info.context.user.is_anonymous():
            qs = qs.published()

        try:
            if id is not None:
                return qs.get(pk=id)
            if slug is not None:
                return qs.get(slug=slug)
        except ObjectDoesNotExist:
            return None

        return None

    def resolve_project(self, info, **kwargs):
        id = kwargs.get('id')
        slug = kwargs.get('slug')

        qs = models.Project.objects.all()

        user = info.context.user
        if user.is_anonymous() or not user.is_staff:
            qs = qs.published()

        try:
            # Find project by id or slug
            if id is not None:
                project = qs.get(pk=id)
            elif slug is not None:
                project = qs.get(slug=slug)
            else:
                project = None

            return project
        except ObjectDoesNotExist:
            return None

        return None

    def resolve_reel(self, info, **kwargs):
        id = kwargs.get('id')
        slug = kwargs.get('slug')

        qs = models.Reel.objects.all()

        user = info.context.user
        if user.is_anonymous() or not user.is_staff:
            qs = qs.published()

        try:
            # Find reel by id or slug
            if id is not None:
                reel = qs.get(pk=id)
            elif slug is not None:
                reel = qs.get(slug=slug)
            else:
                reel = None

            return reel
        except ObjectDoesNotExist:
            return None

        return None
