import factory
# from django.test import TestCase
from snapshottest.django import TestCase
from graphene.test import Client
from django.contrib.auth.models import AnonymousUser
from django.utils.text import slugify
from django.core.exceptions import ValidationError

from ado.media.tests.tests import ImageFactory
from riverstreet.schema import schema
from riverstreet.core.gql.middleware import LoaderMiddleware


class ProjectCategoryMembershipFactory(factory.DjangoModelFactory):
    class Meta:
        model = 'promotion.ProjectCategoryMembership'


class MiniSiteFactory(factory.DjangoModelFactory):
    class Meta:
        model = 'promotion.MiniSite'

    name = 'MiniSite 1'

    @factory.lazy_attribute
    def slug(self):
        return slugify(self.name)


class CategoryFactory(factory.DjangoModelFactory):
    class Meta:
        model = 'promotion.Category'

    title = factory.Sequence(lambda n: u'Category {0}'.format(n + 1))

    @factory.lazy_attribute
    def slug(self):
        return slugify(self.title)


class ProjectContentFactory(factory.DjangoModelFactory):
    class Meta:
        model = 'promotion.ProjectContent'

    text = factory.Faker('text')


class ProjectFactory(factory.DjangoModelFactory):
    class Meta:
        model = 'promotion.Project'

    title = factory.Sequence(lambda n: u'Project {0}'.format(n + 1))
    content = factory.RelatedFactory(
        ProjectContentFactory,
        'project',
    )

    @factory.lazy_attribute
    def slug(self):
        return slugify(self.title)


class ProjectModelTest(TestCase):

    def test_create(self):
        project = ProjectFactory.build()
        project.full_clean()
        project.save()

        self.assertTrue(project.id)
        self.assertTrue(project.title)
        self.assertTrue(project.slug)
        self.assertTrue(project.published)
        self.assertTrue(project.created)
        self.assertTrue(project.modified)

        project = ProjectFactory()
        self.assertEqual(1, project.content.count())


class ProjectContentTest(TestCase):

    def test_content_model_attributes(self):
        project = ProjectFactory()
        item = project.content.all()[0]
        self.assertEqual('project', item.related_model._meta.model_name)
        self.assertEqual('project', item.related_property)

    def test_add_content(self):
        project = ProjectFactory()
        project.content.create(text='Test content')
        project.content.create(image=ImageFactory())
        project.save()
        items = project.content.all()
        self.assertEqual(3, items.count())
        self.assertListEqual([0, 1, 2], [c.position for c in items.all()])
        self.assertListEqual(
            ['text', 'text', 'image'],
            [c.type for c in items],
        )

    def test_content_requires_image_or_text(self):
        content = ProjectContentFactory.build(
            image=None,
            video=None,
            text='',
        )
        with self.assertRaises(ValidationError) as cm:
            content.full_clean()

        self.assertTrue('text' in cm.exception.error_dict)
        self.assertTrue('image' in cm.exception.error_dict)
        self.assertTrue('video' in cm.exception.error_dict)

    def test_positions(self):
        project = ProjectFactory()

        # make sure positions assigned in sequence
        item1 = project.content.all()[0]
        self.assertEqual(0, item1.position)

        item2 = project.content.create(text='Test content')
        self.assertEqual(1, item2.position)

        item3 = project.content.create(text='Test content')
        self.assertEqual(2, item3.position)

        # change position
        item3.position = 0
        item3.save()

        # make sure positions still in sequence
        items = list(project.content.all())
        self.assertListEqual(
            [3, 1, 2],
            [item.pk for item in items]
        )


class SchemaTest(TestCase):
    def setUp(self):
        minisite = MiniSiteFactory()
        c1 = CategoryFactory(minisite=minisite)
        c2 = CategoryFactory(minisite=minisite)
        self.category = c1

        p1 = ProjectFactory(content=None)
        p2 = ProjectFactory(content=None)
        p3 = ProjectFactory(content=None)
        self.project = p3

        ProjectCategoryMembershipFactory(category=c1, project=p1)
        ProjectCategoryMembershipFactory(category=c2, project=p2)
        ProjectCategoryMembershipFactory(category=c1, project=p3)
        ProjectCategoryMembershipFactory(category=c2, project=p3)

        ProjectContentFactory(project=p1, text="test content")
        ProjectContentFactory(project=p2, text="test content")
        ProjectContentFactory(project=p3, text="test content")

        class Context(object):
            user = AnonymousUser()

        self.client = Client(schema, context=Context(), middleware=[
            LoaderMiddleware(),
        ])

    def test_categories_query(self):
        result = self.client.execute('''
            query CategoryList {
              categories {
                id
                slug
                title
                introText
                introImage {
                  id
                }
                projects {
                  edges {
                    node {
                      id
                      slug
                      title
                      client
                      image
                    }
                  }
                }
              }
            }
        ''')
        self.assertFalse('errors' in result, result.get('errors'))
        self.assertMatchSnapshot(result)

    def test_category_query(self):
        result = self.client.execute('''
            query Category {
              category(slug: "%(slug)s") {
                id
                slug
                title
                introText
                introImage {
                  id
                }
              }

              projects(categories_Slug: "%(slug)s") {
                edges {
                  node {
                    id
                    slug
                    title
                    client
                    image
                  }
                }
                pageInfo {
                  hasNextPage
                  endCursor
                }
              }
            }
        ''' % {'slug': self.category.slug})
        self.assertFalse('errors' in result, result.get('errors'))
        self.assertMatchSnapshot(result)

    def test_project_query(self):
        result = self.client.execute('''
            query Project {
              project(slug: "%(slug)s") {
                id
                slug
                title
                content {
                  id
                  type
                  ...on TextContentItem {
                    text
                  }
                  ...on ImageContentItem {
                      image {
                        id
                      }
                  }
                  ...on VideoContentItem {
                      video {
                        id
                      }
                  }
                }
              }
            }
        ''' % {'slug': self.project.slug})
        self.assertFalse('errors' in result, result.get('errors'))
        self.assertMatchSnapshot(result)
