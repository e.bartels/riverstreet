from django.views.generic import DetailView
from django.conf import settings
from sorl import thumbnail

from riverstreet.core.views import index # noqa
from .models import MiniSite, Project, Reel


class MiniSiteView(DetailView):
    template_name = 'index.html'

    def get_queryset(self):
        user = self.request.user
        if user.is_authenticated() and user.is_staff:
            qs = MiniSite.objects.all()
        else:
            qs = MiniSite.objects.published()
        return qs


minisite = MiniSiteView.as_view()


class ProjectDetailView(DetailView):
    template_name = 'index.html'

    def get_queryset(self):
        user = self.request.user
        if user.is_authenticated() and user.is_staff:
            qs = Project.objects.all()
        else:
            qs = Project.objects.published()
        return qs.prefetch_related('content')

    def get_image(self, project):
        content_items = project.content.all()
        try:
            item = [c for c in content_items if c.type in ('image', 'video')][0]
            image = getattr(item, item.type).get_image()
            sizes = dict(settings.SCALED_IMAGE_SIZES)
            thumb = thumbnail.get_thumbnail(image, sizes['large'])
            return [
                self.request.build_absolute_uri(thumb.url),
                thumb.width,
                thumb.height,
            ]
        except IndexError:
            pass

        return [None, None, None]

    def get_context_data(self, **kwargs):
        context = super(ProjectDetailView, self).get_context_data(**kwargs)
        project = context['project']
        [og_image, og_image_width, og_image_height] = self.get_image(project)
        context.update({
            'page_title': project.title,
            'og_image': og_image,
            'og_image_width': og_image_width,
            'og_image_height': og_image_height,
        })
        return context


project_detail = ProjectDetailView.as_view()


class ReelDetailView(DetailView):
    template_name = 'index.html'

    def get_queryset(self):
        user = self.request.user
        if user.is_authenticated() and user.is_staff:
            qs = Reel.objects.all()
        else:
            qs = Reel.objects.published()
        return qs.prefetch_related('content')

    def get_image(self, reel):
        content_items = reel.content.all()
        try:
            item = [c for c in content_items if c.type in ('image', 'video')][0]
            image = getattr(item, item.type).get_image()
            sizes = dict(settings.SCALED_IMAGE_SIZES)
            thumb = thumbnail.get_thumbnail(image, sizes['large'])
            return [
                self.request.build_absolute_uri(thumb.url),
                thumb.width,
                thumb.height,
            ]
        except IndexError:
            pass

        return [None, None, None]

    def get_context_data(self, **kwargs):
        context = super(ReelDetailView, self).get_context_data(**kwargs)
        reel = context['reel']
        [og_image, og_image_width, og_image_height] = self.get_image(reel)
        context.update({
            'page_title': reel.title,
            'og_image': og_image,
            'og_image_width': og_image_width,
            'og_image_height': og_image_height,
        })
        return context


reel_detail = ReelDetailView.as_view()
