from itertools import chain
from django.contrib import admin
from django import forms
from django.db import models
from django.core.urlresolvers import reverse

from ado.customadmin.admin import BaseModelAdminMixin
from ado.customadmin.forms import SmartMultiSelect
from ado.content.admin import ContentItemsInline
from .models import (
    Project,
    Category,
    MiniSite,
    ProjectContent,
    ProjectCategoryMembership,
    Reel,
    ReelContent,
)


class MiniSiteCategoryInline(admin.StackedInline):
    model = Category
    extra = 0
    classes = ['sortable', 'sortable-field__position']
    fields = ['id', 'position', 'admin_url']
    readonly_fields = ['admin_url']

    def has_delete_permission(self, request, obj=None):
        return False

    def admin_url(self, obj=None):
        return reverse('admin:promotion_category_change', args=[obj.id])


@admin.register(MiniSite)
class MiniSiteAdmin(admin.ModelAdmin):
    icon = '<i class="icon material-icons">layers</i>'
    dashboard_order = 0
    list_display = ['name', 'published']
    prepopulated_fields = {
        'slug': ('name',)
    }
    inlines = [MiniSiteCategoryInline]


class CategoryAdminForm(forms.ModelForm):
    notes = forms.CharField(
        required=False,
        help_text='For internal use',
        widget=forms.Textarea(attrs={'rows': 3, 'class': 'vLargeTextField'}),
    )


class CategoryProjectInline(admin.StackedInline):
    model = ProjectCategoryMembership
    verbose_name = 'Project'
    verbose_name_plural = 'Projects'
    extra = 0
    classes = ['sortable', 'sortable-field__position']
    raw_id_fields = ['project']


@admin.register(Category)
class CategoryAdmin(BaseModelAdminMixin, admin.ModelAdmin):
    icon = '<i class="icon material-icons">collections_bookmark</i>'
    dashboard_order = 1
    list_display = ['title', 'minisite', 'published', 'notes']
    list_filter = ['published', 'minisite']
    form = CategoryAdminForm
    prepopulated_fields = {
        'slug': ('title',)
    }
    verbose_image_fk_fields = ['intro_image']
    html_editor_fields = ['intro_text']
    fieldsets = (
        (None, {
            'fields': [
                'minisite',
                'title',
                'slug',
                'published',
            ],
        }),
        ('Intro Section', {
            'fields': [
                'intro_image',
                'intro_text',
            ]
        }),
        (None, {
            'fields': ['notes'],
        }),
    )
    inlines = [CategoryProjectInline]


class ProjectAdminForm(forms.ModelForm):
    project_categories = forms.ModelMultipleChoiceField(
        queryset=Category.objects.all(),
        required=False,
        widget=SmartMultiSelect,
    )
    notes = forms.CharField(
        required=False,
        help_text='For internal use',
        widget=forms.Textarea(attrs={'rows': 3, 'class': 'vLargeTextField'}),
    )

    def __init__(self, *args, instance=None, initial=None, **kwargs):
        if instance:
            if not initial:
                initial = {}
            initial.update({
                'project_categories': instance.categories.all(),
            })
        super(ProjectAdminForm, self).__init__(*args, instance=instance, initial=initial, **kwargs)

    def _save_m2m(self):
        project = self.instance

        # Save project categories
        # This needs to be done manually due to our use of a 'through' model

        # Get selected & existing categories or the project
        selected_categories = set(self.cleaned_data.get('project_categories'))
        existing_categories = set(project.categories.all())

        # Calculate which categories are new and which have been removed
        new_categories = selected_categories.difference(existing_categories)
        removed_categories = existing_categories.difference(selected_categories)

        # Save newly selected categories
        for category in new_categories:
            ProjectCategoryMembership.objects.create(
                project=project,
                category=category
            )

        # Remove existing categories not in the form data
        project.category_memberships.filter(category__in=removed_categories).delete()

        # Save other m2m fields
        cleaned_data = self.cleaned_data
        exclude = self._meta.exclude
        fields = self._meta.fields
        opts = self.instance._meta
        for f in chain(opts.many_to_many, opts.private_fields):
            # Skip categories (handled above)
            if f.name == 'categories':
                continue
            if not hasattr(f, 'save_form_data'):
                continue
            if fields and f.name not in fields:
                continue
            if exclude and f.name in exclude:
                continue
            if f.name in cleaned_data:
                f.save_form_data(self.instance, cleaned_data[f.name])


class ProjectContentInline(ContentItemsInline):
    """Inline for Project contents (images/video/text)"""
    model = ProjectContent
    verbose_name_plural = ''


@admin.register(Project)
class ProjectAdmin(BaseModelAdminMixin, admin.ModelAdmin):
    icon = '<i class="icon material-icons">collections</i>'
    dashboard_order = 2
    list_display = (
        'image_column',
        'title',
        'date',
        'published',
        'categories_col',
        'notes',
    )
    list_display_links = ('image_column', 'title',)
    list_filter = ('published', 'categories__minisite', 'categories')
    list_select_related = True
    list_per_page = 50
    search_fields = ['title', 'id', 'slug']
    ordering = ('-date', 'id')
    date_hierarchy = 'date'

    fieldsets = (
        (None, {
            'fields': [
                'title',
                'slug',
                'client',
                'date',
                'published',
                'project_categories',
                'notes',
            ],
        }),
        ('Intro Media', {
            'fields': [
                'image',
                'video',
                'video_intro_caption',
            ]
        }),
        ('Project Details', {
            'fields': [
                'show_project_details',
            ],
        })
    )
    prepopulated_fields = {
        'slug': ('title',)
    }
    html_editor_fields = [('video_intro_caption', 'simple')]
    verbose_image_fk_fields = ['image']
    verbose_video_fk_fields = ['video']
    form = ProjectAdminForm
    inlines = [ProjectContentInline]

    def get_queryset(self, request):
        return (super(ProjectAdmin, self).get_queryset(request)
                .prefetch_related('categories'))

    def categories_col(self, obj):
        return ", ".join(c.title for c in obj.categories.all())
    categories_col.short_description = 'Categories'

    def get_image(self, obj):
        return obj.get_image()


class ReelContentInline(ContentItemsInline):
    """Inline for Reel content (images/video/text)"""
    model = ReelContent
    verbose_name_plural = ''


@admin.register(Reel)
class ReelAdmin(BaseModelAdminMixin, admin.ModelAdmin):
    icon = '<i class="icon material-icons">collections</i>'
    dashboard_order = 3
    list_display = (
        'image_column',
        'title',
        'published',
        'notes',
    )
    list_display_links = ('image_column', 'title',)
    list_filter = ('published',)
    list_select_related = True
    list_per_page = 50
    search_fields = ['title', 'id', 'slug']

    fieldsets = (
        (None, {
            'fields': [
                'title',
                'slug',
                'published',
                'notes',
            ],
        }),
    )
    prepopulated_fields = {
        'slug': ('title',)
    }
    formfield_overrides = {
        models.TextField: {'widget': forms.Textarea(attrs={'rows': 3, 'class': 'vLargeTextField'})},
    }
    inlines = [ReelContentInline]

    def get_image(self, obj):
        return obj.get_image()
