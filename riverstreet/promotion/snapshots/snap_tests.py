# -*- coding: utf-8 -*-
# snapshottest: v1 - https://goo.gl/zC4yUc
from __future__ import unicode_literals

from snapshottest import Snapshot


snapshots = Snapshot()

snapshots['SchemaTest::test_categories_query 1'] = {
    'data': {
        'categories': [
            {
                'id': '1',
                'introImage': None,
                'introText': 'Category 1',
                'projects': {
                    'edges': [
                        {
                            'node': {
                                'client': '',
                                'id': 'UHJvamVjdDox',
                                'image': None,
                                'slug': 'project-6',
                                'title': 'Project 6'
                            }
                        },
                        {
                            'node': {
                                'client': '',
                                'id': 'UHJvamVjdDoz',
                                'image': None,
                                'slug': 'project-8',
                                'title': 'Project 8'
                            }
                        }
                    ]
                },
                'slug': 'category-1',
                'title': 'Category 1'
            },
            {
                'id': '2',
                'introImage': None,
                'introText': 'Category 2',
                'projects': {
                    'edges': [
                        {
                            'node': {
                                'client': '',
                                'id': 'UHJvamVjdDoy',
                                'image': None,
                                'slug': 'project-7',
                                'title': 'Project 7'
                            }
                        },
                        {
                            'node': {
                                'client': '',
                                'id': 'UHJvamVjdDoz',
                                'image': None,
                                'slug': 'project-8',
                                'title': 'Project 8'
                            }
                        }
                    ]
                },
                'slug': 'category-2',
                'title': 'Category 2'
            }
        ]
    }
}

snapshots['SchemaTest::test_category_query 1'] = {
    'data': {
        'category': {
            'id': '1',
            'introImage': None,
            'introText': 'Category 3',
            'slug': 'category-3',
            'title': 'Category 3'
        },
        'projects': {
            'edges': [
                {
                    'node': {
                        'client': '',
                        'id': 'UHJvamVjdDox',
                        'image': None,
                        'slug': 'project-9',
                        'title': 'Project 9'
                    }
                },
                {
                    'node': {
                        'client': '',
                        'id': 'UHJvamVjdDoz',
                        'image': None,
                        'slug': 'project-11',
                        'title': 'Project 11'
                    }
                }
            ],
            'pageInfo': {
                'endCursor': 'YXJyYXljb25uZWN0aW9uOjE=',
                'hasNextPage': False
            }
        }
    }
}

snapshots['SchemaTest::test_project_query 1'] = {
    'data': {
        'project': {
            'content': [
                {
                    'id': '3',
                    'text': 'test content',
                    'type': 'text'
                }
            ],
            'id': 'UHJvamVjdDoz',
            'slug': 'project-14',
            'title': 'Project 14'
        }
    }
}
