from django.apps import AppConfig


class PromotionConfig(AppConfig):
    name = 'riverstreet.promotion'
    name = 'riverstreet.promotion'
    verbose_name = 'Promotion'
    icon = '<i class="icon material-icons">collections</i>'
