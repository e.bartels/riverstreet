from promise import Promise
from promise.dataloader import DataLoader
from riverstreet.promotion import models


class ProjectContentLoader(DataLoader):
    def batch_load_fn(self, keys):
        qs = (models.ProjectContent.objects
              .filter(project_id__in=keys)
              .select_related('image', 'video')
              .order_by('project_id', 'position', 'id'))
        return Promise.resolve([
            [item for item in qs if item.project_id == id]
            for id in keys
        ])


class ReelContentLoader(DataLoader):
    def batch_load_fn(self, keys):
        qs = (models.ReelContent.objects
              .filter(reel_id__in=keys)
              .select_related('image', 'video')
              .order_by('reel_id', 'position', 'id'))
        return Promise.resolve([
            [item for item in qs if item.reel_id == id]
            for id in keys
        ])


class CategoryProjectsLoader(DataLoader):
    def batch_load_fn(self, keys):
        qs = (models.ProjectCategoryMembership.objects
              .filter(category_id__in=keys)
              .filter(project__published=True)
              .select_related('project')
              .order_by('position', 'id'))
        return Promise.resolve([
            [m.project for m in qs if m.category_id == id]
            for id in keys
        ])


class ProjectCategoriesLoader(DataLoader):
    def batch_load_fn(self, keys):
        qs = (models.Project.categories.through.objects
              .filter(project_id__in=keys)
              .select_related('category'))
        return Promise.resolve([
            [item.category for item in qs if item.project_id == id]
            for id in keys
        ])


Loaders = dict(
    project_content=ProjectContentLoader,
    reel_content=ReelContentLoader,
    category_projects=CategoryProjectsLoader,
    project_categories=ProjectCategoriesLoader,
)
