from django.conf.urls import url

from riverstreet.promotion import views

urlpatterns = [
    # projects
    url(r'^project/(?P<slug>[^/]+)/?$', views.project_detail, name='project-detail'),
    url(r'^project/(?P<slug>[^/]+)/media/(?P<id>\d+)?$',
        views.project_detail,
        name='project-media'),

    # reels
    url(r'^reel/(?P<slug>[^/]+)/?$', views.reel_detail, name='reel-detail'),
    url(r'^reel/(?P<slug>[^/]+)/media/(?P<id>\d+)?$',
        views.reel_detail,
        name='reel-media'),

    # minisite
    url(r'^(?P<slug>[^/]+)/?$', views.minisite, name='minisite'),
    url(r'^(?P<slug>[^/]+)/category/(?P<category_slug>[^/]+)/?$', views.minisite, name='category'),
    url(r'^(?P<slug>[^/]+)/projectvideo/(?P<project_slug>[^/]+)?$', views.minisite),
]
