"""
Settings for use in development environment.
"""
import environ
from .defaults import *

DEBUG = True

# Database Settings
# You should set this up in a ".env" file in top-level of this project
# See django-environ docs
# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.sqlite3',
#         'NAME': root('db.sqlite'),
#         'USER': '',                      # Not used with sqlite3.
#         'PASSWORD': '',                  # Not used with sqlite3.
#         'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
#         'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
#     },
# }

INTERNAL_IPS = ('127.0.0.1',)
ALLOWED_HOSTS = (
    '.localhost',
)

# Temp folders
TEMP_DIR = root('../tmp')
FILE_UPLOAD_TEMP_DIR = TEMP_DIR

# Cache Setup
CACHE_TIMEOUT = 60 * 10
CACHE_PREFIX = 'RIVERSTREET'
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': 'riverstreet-cache',
        'KEY_PREFIX': CACHE_PREFIX,
        'TIMEOUT': CACHE_TIMEOUT,
        'OPTIONS': {
            'MAX_ENTRIES': 10000,
        },
    }
}
SESSION_ENGINE = "django.contrib.sessions.backends.cached_db"
CACHE_MIDDLEWARE_SECONDS = CACHE_TIMEOUT
CACHE_MIDDLEWARE_KEY_PREFIX = CACHE_PREFIX

# sorl-thumbnail settings
THUMBNAIL_DEBUG = DEBUG
THUMBNAIL_DUMMY = True
THUMBNAIL_DUMMY_SOURCE = 'https://placeholder.pics/svg/%(width)sx%(height)s/FFF/AAA'

# Email settings
EMAIL_BACKEND = 'django.core.mail.backends.filebased.EmailBackend'
EMAIL_FILE_PATH = environ.Path(TEMP_DIR)('emails')

# logging
LOGGING['loggers'].update({
    'riverstreet': {
        'handlers': ['console'],
        'level': 'INFO',
        'propagate': True,
    },
})

# Settings for django-webpack-loader
import ado
ado_root = environ.Path(ado.__file__) - 1
WEBPACK_LOADER = {
    'DEFAULT': {
        'BUNDLE_DIR_NAME': 'bundle/',
        'STATS_FILE': root('../static/bundle/webpack-stats.json'),
    },
    'CUSTOMADMIN': {
        'BUNDLE_DIR_NAME': 'customadmin/bundle/',
        'STATS_FILE': ado_root('customadmin/static/customadmin/bundle/webpack-stats.json'),
    },
}
