"""
Settings for use in production.
"""
import os
from .defaults import *

# Debug is off by default in production
DEBUG = False
THUMBNAIL_DEBUG = DEBUG

# Admins and managers get error notifications by email
ADMINS = list(ADMINS) + [
    # Add any extra email addresses
]
MANAGERS = ADMINS

# White-list hosts that the Django server should respond to
ALLOWED_HOSTS = (
    '*',
    # 'riverstreet.net',
    # '*.riverstreet.net',
)

# Database default settings
# These can be overrided/extended using django-environ
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': '_name_',
    },
}

# Cache Setup
CACHE_TIMEOUT = 60 * 10
CACHE_PREFIX = 'RIVERSTREET'
CACHES = {
    'default': {
        'BACKEND': "django_redis.cache.RedisCache",
        'LOCATION': 'redis://127.0.0.1:6379',
        'KEY_PREFIX': CACHE_PREFIX,
        'TIMEOUT': CACHE_TIMEOUT,
        'OPTIONS': {
            'PARSER_CLASS': 'redis.connection.HiredisParser',
            'CONNECTION_POOL_CLASS': 'redis.BlockingConnectionPool',
            'CONNECTION_POOL_KWARGS': {
                'max_connections': 50,
                'timeout': 10,
                'retry_on_timeout': True,
            },
        },
    }
}
DJANGO_REDIS_IGNORE_EXCEPTIONS = True
DJANGO_REDIS_LOG_IGNORED_EXCEPTIONS = True
CACHE_MIDDLEWARE_SECONDS = CACHE_TIMEOUT
CACHE_MIDDLEWARE_KEY_PREFIX = CACHE_PREFIX
SESSION_ENGINE = "django.contrib.sessions.backends.cached_db"

# Temp folders
TEMP_DIR = '/tmp/'
FILE_UPLOAD_TEMP_DIR = TEMP_DIR

# AWS Storage Settings
AWS_STORAGE_BUCKET_NAME = 'riverstreet-media-w2'
AWS_S3_REGION_NAME = 'us-west-2'
AWS_S3_HOST = 's3-us-west-2.amazonaws.com'
AWS_QUERYSTRING_AUTH = False
AWS_PRELOAD_METADATA = False
AWS_HEADERS = {
    'Cache-Control': 'max-age=864000',
}

# File Storage
DEFAULT_FILE_STORAGE = 'storages.backends.s3boto.S3BotoStorage'
MEDIA_ROOT = os.path.join(TEMP_DIR, 'media')
MEDIA_URL = 'https://riverstreet-media-w2.s3-us-west-2.amazonaws.com/'

STATIC_ROOT = '/static'
STATIC_URL = '/s/'

# This fixes performance problems with sorl-thumbnail when used with s3
THUMBNAIL_FORCE_OVERWRITE = True

# Settings for django-webpack-loader
WEBPACK_LOADER = {
    'DEFAULT': {
        'BUNDLE_DIR_NAME': 'bundle/',
        'STATS_FILE': os.path.join(STATIC_ROOT, 'bundle/webpack-stats.json'),
    },
    'CUSTOMADMIN': {
        'BUNDLE_DIR_NAME': 'customadmin/bundle/',
        'STATS_FILE': os.path.join(STATIC_ROOT, 'customadmin/bundle/webpack-stats.json'),
    },
}
