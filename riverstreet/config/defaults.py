"""
Default settings for riverstreet
"""
import environ

DEBUG = False

# Project paths
root = environ.Path(__file__) - 2
BASE_DIR = BASE_PATH = root()

# Admins and managers get error notifications by email
ADMINS = []
MANAGERS = ADMINS

# Default email settings
DEFAULT_FROM_EMAIL = 'riverstreet.net <no-reply@riverstreet.net>'
SERVER_EMAIL = DEFAULT_FROM_EMAIL
EMAIL_SUBJECT_PREFIX = '[riverstreet.net]'

# Django apps
INSTALLED_APPS = [
    'ado.customadmin',
    'django.contrib.admin',
    'ado.customadmin.apps.AuthConfig',  # 'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'sorl.thumbnail',
    'polymorphic',
    'taggit',
    'ckeditor',
    'webpack_loader',
    'cachalot',
    'graphene_django',

    'ado.utils',
    'ado.media',
    'ado.content',

    'riverstreet.core',
    'riverstreet.configuration',
    'riverstreet.work',
    'riverstreet.promotion',
    'riverstreet.pages',
    'riverstreet.blog',
    'riverstreet.people',
]

# Django middleware
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    # 'riverstreet.core.middleware.slowdown_middleware',
]

ROOT_URLCONF = 'riverstreet.urls'

WSGI_APPLICATION = 'riverstreet.wsgi.application'

# Internationalization
# https://docs.djangoproject.com/en/1.10/topics/i18n/
TIME_ZONE = 'America/Los_Angeles'
LANGUAGE_CODE = 'en-us'
USE_I18N = False
USE_L10N = True
USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.10/howto/static-files/
STATIC_URL = '/s/'
STATIC_ROOT = root('../../htdocs/static')
STATICFILES_DIRS = (
    root('../static'),
)

# Uploaded media & files
MEDIA_URL = '/m/'
MEDIA_ROOT = root('../../htdocs/media')
FILE_UPLOAD_PERMISSIONS = 0o664
DEFAULT_FILE_STORAGE = 'ado.utils.storage.overwrite.OverwriteFileSystemStorage'

# Django templates
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            root('templates'),
            # cms_root('templates'),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'riverstreet.configuration.context_processors.config_settings',
            ],
        },
    },
]

# Used by Django to determine if connection is secure
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTOCOL', 'https')

# Logging
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        },
    },
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'regular': {
            'format': '%(levelname)s %(asctime)s %(module)s %(message)s',
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'stream': 'ext://sys.stdout',
        },
        'stderr': {
            'level': 'ERROR',
            'class': 'logging.StreamHandler',
            'stream': 'ext://sys.stderr',
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['stderr', 'mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
        'py.warnings': {
            'handlers': ['console'],
        },
    }
}

# Auth & Profiles
AUTHENTICATION_BACKENDS = (
    'ado.utils.auth.backends.EmailAuthBackend',
    'django.contrib.auth.backends.ModelBackend',
)

# Adding SHA1PasswordHasher for backwards compat from legacy database import
PASSWORD_HASHERS = [
    'django.contrib.auth.hashers.PBKDF2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher',
    'django.contrib.auth.hashers.Argon2PasswordHasher',
    'django.contrib.auth.hashers.BCryptSHA256PasswordHasher',
    'django.contrib.auth.hashers.BCryptPasswordHasher',
    'django.contrib.auth.hashers.SHA1PasswordHasher',
]

# LOGIN_URL = '/account/login/'
# LOGIN_REDIRECT_URL = '/'

# sorl-thumbnail settings
THUMBNAIL_DEBUG = DEBUG
THUMBNAIL_BACKEND = 'ado.media.thumbnail.ThumbnailBackend'
THUMBNAIL_ENGINE = 'ado.media.thumbnail.PILEngine'
THUMBNAIL_UPSCALE = False
THUMBNAIL_PRESERVE_FORMAT = True

# These image sizes are pre-generated for items in the media library
SCALED_IMAGE_SIZES = (
    ('thumb', '200x200'),
    ('largethumb', '360x300'),
    ('small', '420x420'),
    ('medium', '880x880'),
    ('large', '1900x1200'),
)

# graphene-django settings
GRAPHENE = {
    'SCHEMA': 'riverstreet.schema.schema',  # root graphql schema
    'MIDDLEWARE': [
        'riverstreet.core.gql.middleware.CachedResponseMiddleware',
        'riverstreet.core.gql.middleware.LoaderMiddleware',
    ],
    "SCHEMA_OUTPUT": "app/types/schema.json",
    "SCHEMA_TYPES_OUTPUT": "app/types/schemaFragments.json",
}

# django-taggit settings
TAGGIT_TAGS_FROM_STRING = 'ado.utils.tags.comma_splitter'
TAGGIT_STRING_FROM_TAGS = 'ado.utils.tags.comma_joiner'

# Specify the order Django apps should appear in the admin dashboard.
# If this is not set, or set to None, then all apps will be shown in
# alphabetical order (Django's default)
CUSTOMADMIN_DASHBOARD_APPS = (
    'media',
    'work',
    'promotion',
    'blog',
    'pages',
    'people',
    'auth',
    'configuration',
)

# Specify apps that should not be shown in the django dashboard
CUSTOMADMIN_DASHBOARD_HIDDEN_APPS = (
    'taggit',
)

# django-ckeditor
CKEDITOR_COMMON_CONFIG = {
    'width': '100%',
    'allowedContent': True,
    # 'allowedContent': 'i b em strong br p h1 h2 h3 h4 h5 h6 blockquote ul ol table pre [*](*); img[!src,*]; a[!href,*]; iframe[*]{*}(*)',
    # 'extraAllowedContent': 'iframe[*]{*}(*)',
    'removePlugins': 'stylesheetparser',
    'extraPlugins': ','.join([
        'autogrow',
        'horizontalrule',
    ]),
    'autoGrow_minHeight': '120',
    'autoGrow_onStartup': True,
    'contentsCss': STATIC_URL + 'bundle/editor_content.css',
    'stylesCombo_stylesSet': 'admin_styles:' + STATIC_URL + 'assets/editor/editor_styles.js',
    # 'customConfig': STATIC_URL + "assets/editor/editor_extra_config.js"
}


def make_ckeditor_config(config):
    default = CKEDITOR_COMMON_CONFIG.copy()
    default.update(config)
    return default


CKEDITOR_CONFIGS = {
    'default': make_ckeditor_config({
        'height': '300',
        'toolbar': 'Custom',
        'toolbar_Custom': [
            ['Styles', '-', 'Italic', 'Bold', 'Blockquote'],
            ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
            ['NumberedList', 'BulletedList', 'HorizontalRule', 'Table'],
            ['Link', 'Unlink'],
            ['RemoveFormat', '-', 'Source'],
            # ['Maximize'],
        ],
        'autoGrow_maxHeight': '600',
    }),
    'simple': make_ckeditor_config({
        'height': 120,
        'toolbar': 'Simple',
        'toolbar_Simple': [
            ['Italic', 'Bold'],
            ['Link', 'Unlink'],
            ['RemoveFormat', '-', 'Source']
            # ['Maximize'],
        ],
        'autoGrow_minHeight': '80',
        'autoGrow_maxHeight': '300',
    }),
}
