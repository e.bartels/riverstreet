# This file contains a script to migrate database contents from Riverstreet's
# old website database (mysql) to the new  database (postgresql)
# TODO: it is safe to delete this file once migraiton is done.
from os.path import basename
import requests
import requests.utils
import json
import re
import logging
import pytz
from PIL import Image as PILImage
from bs4 import BeautifulSoup
import markdown

import django
django.setup()

from django.db import connections, transaction
from django.core.files.base import ContentFile, File as FileProxy
from django.contrib.auth.models import User, Group

from ado.media.models import MediaItem, Image, Video
# from ado.media.tests.tests import make_image

from riverstreet.promotion.models import (
    Reel,
    MiniSite,
    Project as PromoProject,
    Category as PromoCategory,
    ProjectCategoryMembership as PromoMembership,
)
from riverstreet.work.models import (
    Project as WorkProject,
    Category as WorkCategory,
    ProjectCategoryMembership as WorkMembership)
from riverstreet.blog.models import Entry, Link, Category as BlogCategory


log = logging.getLogger(__name__)


# The django label for the legacy database connection
LEGACY_DB = 'legacy'

# The path to images
LOCAL_IMAGES_PATH = '/www/ado/riverstreet/media/'
LOCAL_FILES_PATH = '/www/ado/riverstreet/media/'


RE_EMPTY_TAG = re.compile(r'<\w+>\s*&nbsp;\s*</\w+>')


def _u(bad_str, charset='utf8'):
    """Make sure we're dealing with unicode string"""
    return bad_str.encode(charset).decode('utf8')


def dt(dtime):
    """Convert dt to utc tz"""
    return pytz.utc.localize(dtime)


def fix_img_tags(html):
    soup = BeautifulSoup(html, 'html.parser')

    img_tags = soup.findAll('img')
    for img in img_tags:
        src = img.get('src', '')
        if src.startswith('/static'):
            original_filename = basename(src)
            image = Image.objects.get(original_filename=original_filename)
            img['src'] = image.filename.url

    return str(soup)


def clean_markup(html):
    # sanitize with whitelist
    sanitized = html.strip()

    # remove single br
    if sanitized == '<br>':
        sanitized = ''

    # remove empty tags
    sanitized = RE_EMPTY_TAG.sub('', sanitized)

    # Fix image src
    sanitized = fix_img_tags(sanitized)

    return sanitized


def convert_markdown(md):
    return clean_markup(markdown.markdown(md))


class LegacyImporter(object):
    """
    Handles importing data from legacy database.
    """

    @transaction.atomic
    def run_import(self):
        """
        Run the import.
        """
        # self.import_images()
        # self.import_videos()
        # self.import_users()
        # self.import_projects()
        # self.import_categories()
        self.import_clientsections()
        self.import_clientpages()
        # self.import_blog()

    # Helper methods
    def _query(self, sql, args=None, database=LEGACY_DB):
        connection = connections[database]
        with connection.cursor() as cursor:
            cursor.execute(sql, args)
            return cursor.fetchall()

    def _get_image_file(self, filename, path=LOCAL_IMAGES_PATH):
        """Return file object for an image file on disk"""
        # return FileProxy(make_image())
        filepath = '{0}{1}'.format(path, filename)

        # Verify image file first and return empty image if it is invalid.
        # At least one file was an invalid image
        im = PILImage.open(filepath)
        im.verify()

        file_ = open(filepath, 'rb')
        return FileProxy(file_)

    def _get_file_file(self, filename, path=LOCAL_FILES_PATH):
        """Return file object for a file on disk"""
        filepath = '{0}{1}'.format(path, filename)
        file_ = open(filepath, 'rb')
        return FileProxy(file_)

    def _download_image(self, filename):
        """Download image and return File object """
        url = 'http://riverstreet.net/static/images/{0}'.format(
                requests.utils.quote(filename))
        resp = requests.get(url)
        if resp.status_code == 200:
            file_ = ContentFile(resp.content)
            return file_
        else:
            raise Exception('could not download file: {0}'.format(filename))
        return None

    # Data import methods
    def import_users(self):
        log.info('Importing users...')

        legacy_users = User.objects.using(LEGACY_DB).all()
        existing_usernames = set([u.username for u in User.objects.all()])

        for user in legacy_users:
            if user.username not in existing_usernames:
                user.save(using='default')

        legacy_groups = Group.objects.using(LEGACY_DB).all()
        existing_groups = set([g.name for g in Group.objects.all()])

        for group in legacy_groups:
            if group.name not in existing_groups:
                group.save(using='default')
                for user in group.user_set.using(LEGACY_DB).all():
                    group.user_set.add(User.objects.get(pk=user.pk))

    def import_images(self):
        log.info('Importing images...')

        # Get existing image ids
        existing_images = set(MediaItem.objects.values_list('pk', flat=True))

        # Query legacy images
        sql = """
        SELECT
            media_mediaitem.id,
            media_mediaitem.title,
            media_mediaitem.created,
            media_mediaitem.modified,
            media_mediaitem.caption,
            media_image.filename,
            media_image.width,
            media_image.height
        FROM media_image
            INNER JOIN media_mediaitem
            ON (media_image.mediaitem_ptr_id = media_mediaitem.id)
        """
        results = self._query(sql)
        n = len(results)
        for i, (id,
                title,
                created,
                modified,
                caption,
                filename,
                width,
                height) in enumerate(results):

            # Skip already existing image
            if id in existing_images:
                continue

            log.info('Importing image {0} of {1}: {2} (id: {3})'.format(
                                        i + 1, n, filename, id))

            # Sanitize strings
            filename = _u(filename).strip()
            title = _u(title).strip()
            caption = _u(caption or '').strip()

            # Create new Image instance
            image = Image(
                pk=id,
                width=width,
                height=height,
                title=title,
                caption=caption,
                original_filename=basename(filename),
                created=dt(created),
                modified=dt(modified),
            )
            file_ = self._get_image_file(filename)
            image.filename.save(filename, file_, save=True)

            Image.objects.filter(pk=image.pk).update(
                created=dt(created),
                modified=dt(modified),
            )

    def import_videos(self):
        log.info('Importing videos...')

        # Get existing video ids
        existing_videos = set(MediaItem.objects.values_list('pk', flat=True))

        # Query legacy images
        sql = """
        SELECT
            media_mediaitem.id,
            media_mediaitem.title,
            media_mediaitem.created,
            media_mediaitem.modified,
            media_mediaitem.caption,
            media_video.url,
            media_video.thumbnail_url,
            media_video.image,
            media_video.data
        FROM media_video
            INNER JOIN media_mediaitem
            ON (media_video.mediaitem_ptr_id = media_mediaitem.id)
        """
        results = self._query(sql)
        n = len(results)
        for i, (id,
                title,
                created,
                modified,
                caption,
                url,
                thumbnail_url,
                image,
                data) in enumerate(results):

            # Skip already existing image
            if id in existing_videos:
                continue

            log.info('Importing video {0} of {1}: {2} (id: {3})'.format(
                                        i + 1, n, url, id))

            # Sanitize strings
            title = _u(title).strip()
            caption = _u(caption or '').strip()

            # Get object from json
            try:
                data = json.loads(data)
            except ValueError:
                continue

            # Create new Video instance
            video = Video(
                pk=id,
                title=title,
                caption=caption,
                url=url,
                thumbnail_url=thumbnail_url,
                video_image=image,
                data=data,
                created=dt(created),
                modified=dt(modified),
            )
            video.save()

            Video.objects.filter(pk=video.pk).update(
                created=dt(created),
                modified=dt(modified),
            )

    def import_projects(self):
        log.info('Importing projects...')

        sql = """
        SELECT
            id,
            title,
            slug,
            date,
            published,
            client,
            text,
            created,
            modified
        FROM content_collection ORDER by sort, id;
        """
        results = self._query(sql)
        for sort, (collection_id,
                   title,
                   slug,
                   date,
                   published,
                   client,
                   text,
                   created,
                   modified
                   ) in enumerate(results):

            # sanitize charset
            title = _u(title).strip()
            client = _u(client).strip()
            text = convert_markdown(_u(text))
            published = bool(published)
            modified = dt(modified)
            created = dt(created)

            # create project
            try:
                project = WorkProject.objects.get(pk=collection_id)
                was_created = False
                continue  # skip the rest
            except WorkProject.DoesNotExist:
                project = WorkProject()
                project.pk = collection_id
                was_created = True

            project.title = title
            project.slug = slug
            project.date = date
            project.published = published
            project.client = client
            project.show_project_details = True
            project.save(force_insert=was_created)

            # Set created/modified fields
            WorkProject.objects.filter(pk=project.pk).update(
                created=created,
                modified=modified,
            )

            # Get ctype for collection
            ctype_id_sql = """
                SELECT id from django_content_type
                    WHERE app_label='content' and model='collection';
            """
            ctype_id = self._query(ctype_id_sql)[0][0]

            # import text content
            if text:
                project.content.create(text=text)

            # import videos
            videos_sql = """
            SELECT item_id
            FROM media_videorelation
                WHERE content_type_id=%s and object_id=%s
            ORDER BY sort, id;
            """
            video_results = self._query(videos_sql, [ctype_id, collection_id])
            video_ids = [r[0] for r in video_results]
            for video_id in video_ids:
                video = Video.objects.get(pk=video_id)
                project.content.create(video=video)

            # import images
            images_sql = """
            SELECT item_id
            FROM media_imagerelation
                WHERE content_type_id=%s and object_id=%s
            ORDER BY sort, id;
            """
            image_results = self._query(images_sql, [ctype_id, collection_id])
            image_ids = [r[0] for r in image_results]
            for image_id in image_ids:
                image = Image.objects.get(pk=image_id)
                project.content.create(image=image)

            # Set main video/image fields
            if not project.video and video_ids:
                project.video_id = video_ids[0]
            if not project.image and image_ids:
                project.image_id = image_ids[0]

            project.save()

    def import_categories(self):
        log.info('Importing categories...')

        sql = """
        SELECT id, name, slug, published
            FROM content_category ORDER by sort, id;
        """
        results = self._query(sql)
        for sort, (category_id,
                   name,
                   slug,
                   published,
                   ) in enumerate(results):

            # sanitize charset
            title = _u(name).strip()
            published = bool(published)

            # create category
            try:
                category = WorkCategory.objects.get(pk=category_id)
                was_created = False
                continue  # skip the rest
            except WorkCategory.DoesNotExist:
                category = WorkCategory()
                category.pk = category_id
                was_created = True

            category.title = title
            category.slug = slug
            category.published = published
            category.save(force_insert=was_created)

            # update position
            WorkCategory.objects.filter(pk=category_id).update(position=sort)

            # Add projects for category
            projects_sql = """
            SELECT
                membership.collection_id
            FROM content_collection as collection
                INNER JOIN content_collection_categories as membership
                ON (collection.id = membership.collection_id)
            WHERE membership.collectioncategory_id = %s
            ORDER BY collection.date DESC
            """
            projects_results = self._query(projects_sql, [category.id])
            project_ids = [p[0] for p in projects_results]
            for project_id in project_ids:
                WorkMembership.objects.create(
                    category=category,
                    project_id=project_id,
                )

        # create 'General Work' category
        general_work, _ = WorkCategory.objects.get_or_create(
            title='general work',
            slug='general-work',
        )

        general_work_projects = (WorkProject.objects
                                 .exclude(categories__isnull=True)
                                 .exclude(categories=general_work))

        for project in general_work_projects:
            WorkMembership.objects.create(
                category=general_work,
                project=project,
            )

    def import_clientsections(self):
        log.info('Importing clientsections...')

        minisite_sql = """
        SELECT id, name, slug, published from clientsections_clientsection;
        """
        section_results = self._query(minisite_sql)
        for (section_id, section_name, section_slug, section_published) in section_results:
            if MiniSite.objects.filter(pk=section_id).exists():
                continue  # skip

            # Create minisite from section
            minisite = MiniSite()
            minisite.id = section_id
            minisite.slug = section_slug
            minisite.name = section_name
            minisite.published = section_published
            minisite.save(force_insert=True)

            # Import projects
            projects_sql = """
            SELECT
                id,
                section_id,
                title,
                slug,
                date,
                published,
                text,
                created,
                modified
            FROM clientsections_collection
            WHERE section_id = %s
            ORDER by sort, id;
            """
            project_results = self._query(projects_sql, [section_id])
            for sort, (
                collection_id,
                section_id,
                title,
                slug,
                date,
                published,
                text,
                created,
                modified,
            ) in enumerate(project_results):

                # sanitize charset
                title = _u(title).strip()
                text = convert_markdown(_u(text))
                published = bool(published)
                modified = dt(modified)
                created = dt(created)

                if PromoProject.objects.filter(id=collection_id).exists():
                    continue  # skip

                project = PromoProject()
                project.id = collection_id
                project.title = title
                project.slug = slug
                project.date = date
                project.published = published
                project.save(force_insert=True)

                # Set created/modified fields
                PromoProject.objects.filter(pk=project.pk).update(
                    created=created,
                    modified=modified,
                )

                # Get ctype for collection
                ctype_id_sql = """
                    SELECT id from django_content_type
                        WHERE app_label='clientsections' and model='collection';
                """
                ctype_id = self._query(ctype_id_sql)[0][0]

                # import text content
                if text:
                    project.content.create(text=text)

                # import videos
                videos_sql = """
                SELECT item_id
                FROM media_videorelation
                    WHERE content_type_id=%s and object_id=%s
                ORDER BY sort, id;
                """
                video_results = self._query(videos_sql, [ctype_id, collection_id])
                video_ids = [r[0] for r in video_results]
                for video_id in video_ids:
                    video = Video.objects.get(pk=video_id)
                    project.content.create(video=video)

                # import images
                images_sql = """
                SELECT item_id
                FROM media_imagerelation
                    WHERE content_type_id=%s and object_id=%s
                ORDER BY sort, id;
                """
                image_results = self._query(images_sql, [ctype_id, collection_id])
                image_ids = [r[0] for r in image_results]
                for image_id in image_ids:
                    image = Image.objects.get(pk=image_id)
                    project.content.create(image=image)

                # Set main video/image fields
                if not project.video and video_ids:
                    project.video_id = video_ids[0]
                if not project.image and image_ids:
                    project.image_id = image_ids[0]

                project.save()

            # import categories
            categories_sql = """
            SELECT id, name, slug, published
                FROM clientsections_category
            WHERE section_id=%s
            ORDER BY sort, id;
            """
            category_results = self._query(categories_sql, [section_id])
            for i, (
                category_id,
                category_name,
                category_slug,
                category_published,
            ) in enumerate(category_results):
                category_name = _u(category_name).strip()
                category_published = bool(category_published)

                category = PromoCategory()
                category.id = category_id
                category.minisite_id = section_id
                category.title = category_name
                category.slug = category_slug
                category.published = category_published
                category.save(force_insert=True)

                category_projects_sql = """
                SELECT
                    membership.collection_id
                FROM clientsections_collection as collection
                    INNER JOIN clientsections_collection_categories as membership
                    ON (collection.id = membership.collection_id)
                WHERE membership.category_id =  %s
                ORDER BY collection.date DESC
                """
                category_projects_results = self._query(category_projects_sql, [category.id])
                project_ids = [p[0] for p in category_projects_results]
                for project_id in project_ids:
                    PromoMembership.objects.create(
                        category=category,
                        project_id=project_id,
                    )

                # Handle case where no projects, only category videos
                if not project_ids:
                    # Get ctype for category
                    ctype_id_sql = """
                        SELECT id from django_content_type
                            WHERE app_label='clientsections' and model='category';
                    """
                    ctype_id = self._query(ctype_id_sql)[0][0]

                    # Get videos
                    videos_sql = """
                    SELECT item_id
                    FROM media_videorelation
                        WHERE content_type_id=%s and object_id=%s
                    ORDER BY sort, id;
                    """
                    video_results = self._query(videos_sql, [ctype_id, category_id])
                    video_ids = [r[0] for r in video_results]
                    for video_id in video_ids:
                        try:
                            video = Video.objects.get(pk=video_id)
                            project = PromoProject.objects.create(
                                title=video.title,
                                video=video,
                            )
                            project.content.create(video=video)
                            PromoMembership.objects.create(category=category, project=project)
                        except Video.DoesNotExist:
                            pass

            # If no categories, import splash page
            if not category_results:
                self._query("""
                 SELECT
                    setval(pg_get_serial_sequence('"promotion_project"','id'),
                            coalesce(max("id"), 1), max("id") IS NOT null)
                 FROM "promotion_project";

                 SELECT
                    setval(pg_get_serial_sequence('"promotion_category"','id'),
                            coalesce(max("id"), 1), max("id") IS NOT null)
                 FROM "promotion_category";
                """, database='default')

                splash_sql = """
                SELECT id from clientsections_clientsplash
                WHERE section_id=%s
                """
                splash_results = self._query(splash_sql, [section_id])
                if splash_results:
                    [splash_id] = splash_results[0]
                    # Get ctype for splash
                    ctype_id_sql = """
                        SELECT id from django_content_type
                            WHERE app_label='clientsections' and model='clientsplash';
                    """
                    ctype_id = self._query(ctype_id_sql)[0][0]

                    videos_sql = """
                    SELECT item_id
                    FROM media_videorelation
                        WHERE content_type_id=%s and object_id=%s
                    ORDER BY sort, id;
                    """
                    video_results = self._query(videos_sql, [ctype_id, splash_id])
                    video_ids = [r[0] for r in video_results]
                    if video_ids:
                        category = PromoCategory.objects.create(
                            minisite=minisite,
                            title='Splash',
                        )
                        for video_id in video_ids:
                            try:
                                video = Video.objects.get(pk=video_id)
                                project = PromoProject.objects.create(
                                    title=video.title,
                                    video=video,
                                )
                                project.content.create(video=video)
                                PromoMembership.objects.create(category=category, project=project)
                            except Video.DoesNotExist:
                                pass

    def import_clientpages(self):
        log.info('Importing reels (client projects)...')

        # clientpages
        sql = """
        SELECT
            id,
            title,
            slug,
            published
        FROM clientpages_clientpage ORDER by id DESC;
        """
        results = self._query(sql)
        for sort, (clientpage_id,
                   title,
                   slug,
                   published,
                   ) in enumerate(results):

            # sanitize charset
            title = _u(title).strip()
            published = bool(published)

            # create reel
            if Reel.objects.filter(pk=clientpage_id).exists():
                continue  # skip

            reel = Reel(
                id=clientpage_id,
                title=title,
                slug=slug,
                published=published,
            )
            reel.save(force_insert=True)

            # Get ctype for collection
            ctype_id_sql = """
                SELECT id from django_content_type
                    WHERE app_label='clientpages' and model='clientpage';
            """
            ctype_id = self._query(ctype_id_sql)[0][0]

            # import videos
            videos_sql = """
            SELECT item_id
            FROM media_videorelation
                WHERE content_type_id=%s and object_id=%s
            ORDER BY sort, id;
            """
            video_results = self._query(videos_sql, [ctype_id, clientpage_id])
            video_ids = [r[0] for r in video_results]
            for video_id in video_ids:
                video = Video.objects.get(pk=video_id)
                reel.content.create(video=video)

            # import images
            images_sql = """
            SELECT item_id
            FROM media_imagerelation
                WHERE content_type_id=%s and object_id=%s
            ORDER BY sort, id;
            """
            image_results = self._query(images_sql, [ctype_id, clientpage_id])
            image_ids = [r[0] for r in image_results]
            for image_id in image_ids:
                image = Image.objects.get(pk=image_id)
                reel.content.create(image=image)

    def import_blog(self):
        log.info('Importing blog...')

        # Import categories
        category_sql = """
        SELECT id, name, slug FROM blog_category order by id
        """
        results = self._query(category_sql)
        for sort, (category_id,
                   name,
                   slug,
                   ) in enumerate(results):

            name = _u(name).strip()
            slug = _u(slug).strip()

            # create category
            try:
                category = BlogCategory.objects.get(pk=category_id)
                was_created = False
                continue  # skip the rest
            except BlogCategory.DoesNotExist:
                category = BlogCategory()
                category.pk = category_id
                was_created = True

            category.name = name
            category.slug = slug
            category.save(force_insert=was_created)

        # Import Links
        link_sql = """
        SELECT id, url, title FROM blog_link order by id
        """
        results = self._query(link_sql)
        for sort, (link_id,
                   url,
                   title,
                   ) in enumerate(results):

            title = _u(title).strip()
            url = _u(url).strip()

            # create link
            try:
                link = Link.objects.get(pk=link_id)
                was_created = False
                continue  # skip the rest
            except Link.DoesNotExist:
                link = Link()
                link.pk = link_id
                was_created = True

            link.url = url
            link.title = title
            link.save(force_insert=was_created)

        # Import Entries
        sql = """
        SELECT
            id,
            title,
            slug,
            author_id,
            pub_date,
            published,
            body,
            created,
            modified
        FROM blog_entry
        """
        results = self._query(sql)
        for sort, (entry_id,
                   title,
                   slug,
                   author_id,
                   pub_date,
                   published,
                   body,
                   created,
                   modified,
                   ) in enumerate(results):

            # Sanitize strings
            title = _u(title).strip()
            slug = _u(slug).strip()
            body = convert_markdown(_u(body))
            pub_date = dt(pub_date)
            created = dt(created)
            modified = dt(modified)

            try:
                entry = Entry.objects.get(pk=entry_id)
                was_created = False
                continue  # skip the rest
            except Entry.DoesNotExist:
                entry = Entry()
                entry.pk = entry_id
                was_created = True

            entry.title = title
            entry.slug = slug
            entry.pub_date = pub_date
            entry.published = published
            entry.author_id = author_id
            entry.save(force_insert=was_created)

            Entry.objects.filter(pk=entry.pk).update(
                created=created,
                modified=modified,
            )

            # Add text for the entry
            entry.content.create(
                entry=entry,
                text=body,
            )

            # Add categories for entry
            categories_sql = """
            SELECT
                blog_category.id
            FROM blog_category
                INNER JOIN blog_entry_categories
                    ON (blog_category.id = blog_entry_categories.category_id)
            WHERE blog_entry_categories.entry_id = %s
            """
            categories_results = self._query(categories_sql, [entry.id])
            category_ids = [p[0] for p in categories_results]
            entry.categories = category_ids


def run_legacy_import(database=LEGACY_DB):
    """
    Handles importing data from legacy database.
    """
    importer = LegacyImporter()
    importer.run_import()


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    run_legacy_import()
