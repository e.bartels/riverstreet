import graphene

from ado.media.models import MediaItem, Image, Video, File
from riverstreet.core.gql.caching import skip_cache
from riverstreet.core.schema.media import (
    MediaItem as MediaItemType,
    Image as ImageType,
    Video as VideoType,
    File as FileType,
)
from riverstreet.core.schema.auth import CurrentUserType, LoginMutation, LogoutMutation


class Query(object):
    # media = graphene.List(MediaItemType)
    # images = graphene.List(ImageType)
    # videos = graphene.List(VideoType)
    # files = graphene.List(FileType)

    mediaitem = graphene.Field(
        MediaItemType,
        id=graphene.Int(),
        uid=graphene.String(),
    )

    image = graphene.Field(
        ImageType,
        id=graphene.Int(),
        uid=graphene.String(),
    )
    video = graphene.Field(
        VideoType,
        id=graphene.Int(),
        uid=graphene.String(),
    )

    file = graphene.Field(
        FileType,
        id=graphene.Int(),
        uid=graphene.String(),
    )

    current_user = graphene.Field(
        CurrentUserType,
    )

    @staticmethod
    def get_item(model, info, **kwargs):
        id = kwargs.get('id')
        uid = kwargs.get('uid')

        qs = model.objects.all()

        if id is not None:
            return qs.get(pk=id)

        if uid is not None:
            return qs.get(uid=uid)

        return None

    def resolve_mediaitem(self, info, **kwargs):
        return Query.get_item(MediaItem, info, **kwargs)

    def resolve_image(self, info, **kwargs):
        return Query.get_item(Image, info, **kwargs)

    def resolve_video(self, info, **kwargs):
        return Query.get_item(Video, info, **kwargs)

    def resolve_file(self, info, **kwargs):
        return Query.get_item(File, info, **kwargs)

    def resolve_media(self, info, **kwargs):
        return MediaItem.objects.all()

    def resolve_images(self, info, **kwargs):
        return Image.objects.all()

    def resolve_videos(self, info, **kwargs):
        return Video.objects.all()

    def resolve_files(self, info, **kwargs):
        return File.objects.all()

    @skip_cache
    def resolve_current_user(self, info, **kwargs):
        return info.context.user


class Mutation(graphene.ObjectType):
    login = LoginMutation.Field()
    logout = LogoutMutation.Field()
