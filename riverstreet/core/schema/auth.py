import graphene
from django.contrib.auth import authenticate, login, logout


class CurrentUserType(graphene.ObjectType):
    id = graphene.Int()
    username = graphene.String()
    anonymous = graphene.Boolean(required=True)
    authenticated = graphene.Boolean(required=True)

    class Meta:
        name = 'CurrentUser'

    def resolve_anonymous(self, info, **kwargs):
        return self.is_anonymous()

    def resolve_authenticated(self, info, **kwargs):
        return self.is_authenticated()


class LoginMutation(graphene.Mutation):
    class Arguments:
        username = graphene.String(required=True)
        password = graphene.String(required=True)

    ok = graphene.Boolean(required=True)
    user = graphene.Field(lambda: CurrentUserType, required=True)

    def mutate(self, info, username, password):
        request = info.context
        user = authenticate(request, username=username, password=password)
        if user:
            login(request, user)
            return LoginMutation(ok=True, user=user)
        return LoginMutation(ok=False)


class LogoutMutation(graphene.Mutation):
    ok = graphene.Boolean(required=True)
    user = graphene.Field(lambda: CurrentUserType, required=True)

    def mutate(self, info):
        request = info.context
        logout(request)
        return LogoutMutation(ok=True, user=request.user)
