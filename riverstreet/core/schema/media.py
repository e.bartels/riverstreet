from django.conf import settings

import graphene
from graphene_django.types import DjangoObjectType
from sorl import thumbnail

from ado.media import models as media_models

# Resized image settings
IMAGE_SIZES = settings.SCALED_IMAGE_SIZES


# Media types
class ImageType(graphene.Enum):
    image = 0


class VideoType(graphene.Enum):
    video = 1


class FileType(graphene.Enum):
    file = 2


class SizedImages(graphene.Scalar):
    @staticmethod
    def serialize(imgfile):
        images = {}
        # for key, size in (('medium', '420x9000'),):
        for key, size in IMAGE_SIZES:
            thumb = thumbnail.get_thumbnail(imgfile, size)
            images[key] = {
                'url': thumb.url,
                'width': thumb.width,
                'height': thumb.height,
            }
        return images


class Image(DjangoObjectType):
    type = graphene.NonNull(ImageType)
    url = graphene.String(required=True)
    images = SizedImages(required=True)

    class Meta:
        name = 'Image'
        model = media_models.Image
        only_fields = [
            'type',
            'id',
            'uid',
            'title',
            'caption',
            'url',
            'width',
            'height',
            'alt_text',
            'images',
        ]

    def resolve_type(self, info, **kwargs):
        return ImageType.image

    def resolve_url(self, info, **kwargs):
        return self.filename.url

    def resolve_images(self, info, **kwargs):
        return self.filename


class Video(DjangoObjectType):
    type = graphene.NonNull(VideoType)
    provider = graphene.String(required=True)
    embed = graphene.String(required=True)
    width = graphene.Int(required=True)
    height = graphene.Int(required=True)
    images = SizedImages(required=True)
    alt_text = graphene.String(required=True)

    class Meta:
        name = 'Video'
        model = media_models.Video
        only_fields = [
            'type',
            'id',
            'uid',
            'title',
            'caption',
            'provider',
            'url',
            'embed',
            'width',
            'height',
            'images',
            'alt_text',
        ]

    def resolve_type(self, info, **kwargs):
        return VideoType.video

    def resolve_provider(self, info, **kwargs):
        return self.provider

    def resolve_embed(self, info, **kwargs):
        return self.embed

    def resolve_width(self, info, **kwargs):
        return self.data['width']

    def resolve_height(self, info, **kwargs):
        return self.data['height']

    def resolve_images(self, info, **kwargs):
        return self.video_image

    def resolve_alt_text(self, info, **kwargs):
        # Empty altText, mostly to make consistent with Image type
        return ''


class File(DjangoObjectType):
    type = graphene.NonNull(FileType)
    url = graphene.String(required=True)
    filetype = graphene.String()

    class Meta:
        name = 'File'
        model = media_models.File
        only_fields = [
            'type',
            'id',
            'uid',
            'title',
            'caption',
            'url',
            'filetype',
            'filesize',
        ]

    def resolve_type(self, info, **kwargs):
        return FileType.file

    def resolve_url(self, info, **kwargs):
        return self.filename.url

    def resolve_filetype(self, info, **kwargs):
        return self.mimetype.split('/')[-1].upper()


class MediaItem(graphene.Union):
    class Meta:
        name = 'MediaItem'
        types = [Image, Video, File]
