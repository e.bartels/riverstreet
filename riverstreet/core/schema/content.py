import graphene

from riverstreet.core.schema.media import Image, Video


# 'type' field enums
class ContentItemType(graphene.Enum):
    text = 0
    image = 1
    video = 2


class TextItemType(graphene.Enum):
    text = 0


class ImageItemType(graphene.Enum):
    image = 1


class VideoItemType(graphene.Enum):
    video = 2


# Object types
class AbstractContentItem(graphene.AbstractType):
    type = graphene.NonNull(ContentItemType)
    id = graphene.ID(required=True)


class ContentItem(graphene.Interface):
    type = graphene.NonNull(ContentItemType)
    id = graphene.ID(required=True)

    @classmethod
    def resolve_type(cls, instance, info, **kwargs):
        """
        This resolve the specific type to use for each instance, not the "type"
        field, which are resolved in the instance types below.
        """
        if instance.type == 'image':
            return ImageContentItem
        elif instance.type == 'video':
            return VideoContentItem
        return TextContentItem


class TextContentItem(graphene.ObjectType):
    text = graphene.String(required=True)

    class Meta:
        interfaces = [ContentItem]

    def resolve_type(self, info, **kwargs):
        return ContentItemType.text


class ImageContentItem(graphene.ObjectType):
    image = graphene.Field(Image, required=True)

    class Meta:
        interfaces = [ContentItem]

    def resolve_type(self, info, **kwargs):
        return ContentItemType.image


class VideoContentItem(graphene.ObjectType):
    video = graphene.Field(Video, required=True)

    class Meta:
        interfaces = [ContentItem]

    def resolve_type(self, info, **kwargs):
        return ContentItemType.video
