from promise import Promise
from promise.dataloader import DataLoader
from ado.media.models import (MediaItem, Image, Video, File)


class MediaLoader(DataLoader):
    def batch_load_fn(self, keys):
        items = MediaItem.objects.filter(id__in=keys)
        return Promise.resolve(
            sorted(items, key=lambda i: keys.index(i.id))
        )


class ImageLoader(DataLoader):
    def batch_load_fn(self, keys):
        items = Image.objects.filter(id__in=keys)
        return Promise.resolve(
            sorted(items, key=lambda i: keys.index(i.id))
        )


class VideoLoader(DataLoader):
    def batch_load_fn(self, keys):
        items = Video.objects.filter(id__in=keys)
        return Promise.resolve(
            sorted(items, key=lambda i: keys.index(i.id))
        )


class FileLoader(DataLoader):
    def batch_load_fn(self, keys):
        items = File.objects.filter(id__in=keys)
        return Promise.resolve(
            sorted(items, key=lambda i: keys.index(i.id))
        )


Loaders = dict(
    media=MediaLoader,
    image=ImageLoader,
    video=VideoLoader,
    file=FileLoader,
)
