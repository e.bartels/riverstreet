from django.views.generic import TemplateView
from django.views.decorators.csrf import ensure_csrf_cookie
from django.utils.decorators import method_decorator
from django.views.generic import RedirectView
# from django.views.decorators.csrf import csrf_exempt
from django.conf import settings

from graphene_django.views import GraphQLView as BaseGraphQLView
from riverstreet.core.gql.caching import CachedResponseGraphQLViewMixin


# Main index view used for homepage
@method_decorator(ensure_csrf_cookie, name='dispatch')
class IndexView(TemplateView):
    template_name = 'index.html'
    page_title = None

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context.update({
            'page_title': self.page_title,
        })
        return context


index = IndexView.as_view()


class Redirect(RedirectView):
    permanent = True


# Main graphql view
class GraphQLView(CachedResponseGraphQLViewMixin, BaseGraphQLView):
    cache_enabled = True  # not settings.DEBUG


graphql_view = GraphQLView.as_view(
    graphiql=settings.DEBUG,  # enable graphiql for debugging
)

# enable for debugging with requests outside of webpage, i.e. with curl
# graphql_view = csrf_exempt(graphql_view)
