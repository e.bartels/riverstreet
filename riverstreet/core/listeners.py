from django.apps import apps
from django.db.models import signals
from django.dispatch import receiver
from django.conf import settings

from sorl import thumbnail

from ado.media.models import Image
from riverstreet.core.gql.caching import change_graphql_updated_at
from riverstreet.core import signals as core_signals


IMAGE_SIZES = settings.SCALED_IMAGE_SIZES


@receiver(signals.post_save, sender=Image)
def pre_render_image_thumbs(instance, **kwargs):
    """
    Pre-generate thumbnail in various sizes when an image is saved
    """
    if not kwargs.get('raw') and not getattr(settings, 'TESTING', False):
        for key, size in IMAGE_SIZES:
            thumbnail.get_thumbnail(instance.filename, size)


app_configs = [c for c in apps.get_app_configs()
               if 'riverstreet' in c.name or 'ado' in c.name]

cached_models = set()
for app_config in app_configs:
    for model in app_config.get_models():
        cached_models.add(model)


@receiver(signals.post_save)
@receiver(signals.post_delete)
@receiver(core_signals.post_bulk_update)
@receiver(core_signals.post_bulk_delete)
def invalidate_graphql_view_cache(sender, *args, **kwargs):
    """
    Invalidate graphql view caches on model save/delete
    """
    if sender in cached_models:
        change_graphql_updated_at()
