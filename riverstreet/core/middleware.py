import re
import time


def slowdown_middleware(get_response):

    def middleware(request):
        regex = re.compile(r'^/(graphql/)')
        if regex.match(request.path):
            time.sleep(1.3)

        return get_response(request)

    return middleware
