import time
import json
from hashlib import sha1

from django.core.cache import cache


class CachedResponseGraphQLViewMixin(object):
    """
    View mixin that caches graphene_django responses based on request.

    Respones are cached forever. Database writes will invalidate the entire cache.

    Note: The invalidation scheme is not fine-grained, which would require
    intimate knowledge of which responses depend on which data.  This method
    will suffice for read-heavy applications.

    Usage:

    class CachedGraphQLView(CachedResponseGraphQLViewMixin, GraphQLView):
        pass

    """
    cache_enabled = True
    UPDATED_AT_CACHE_KEY = 'api_updated_at_timestamp'

    def get_response(self, request, data, show_graphiql=False):
        """Get the graphql data response, with caching"""

        if not self.cache_enabled:
            return super(CachedResponseGraphQLViewMixin,
                         self).get_response(request, data, show_graphiql)

        # look for a cached response
        cache_key = self.get_cache_key(request, data, show_graphiql)
        response = cache.get(cache_key, None)

        # or generate a new respones on miss
        if response is None:
            response = super(CachedResponseGraphQLViewMixin,
                             self).get_response(request, data, show_graphiql)

            # See if middleware has told us to skip cache?
            should_cache = cache_key and not getattr(request, 'skip_cache', False)

            # Cache response on success
            result, status_code = response
            if should_cache and status_code >= 200 and status_code < 300:
                cache_timeout = getattr(request, 'cache_timeout', None)
                cache.set(cache_key, response, timeout=cache_timeout)

        return response

    def get_cache_key(self, request, data, show_graphiql):
        key_bits = self.get_cache_key_bits(request, data, show_graphiql)
        key_str = json.dumps(key_bits, sort_keys=True).encode('utf-8')
        return sha1(key_str).hexdigest()

    def get_cache_key_bits(self, request, data, show_graphiql):
        """Get cache key bits"""
        query, variables, operation_name, id = self.get_graphql_params(request, data)

        # The cache can be invalidated with the updated_at cache key, so we add
        # that to the cache bits here.
        updated_at = cache.get(self.UPDATED_AT_CACHE_KEY, None)
        if not updated_at:
            updated_at = change_graphql_updated_at()

        return (query, variables, operation_name, id, show_graphiql, updated_at)


def change_graphql_updated_at():
    """
    Changes the updated_at key bit.
    Call this function to invalidate cached graphql responses.
    """
    cache_key = CachedResponseGraphQLViewMixin.UPDATED_AT_CACHE_KEY
    timestamp = int(time.time())
    cache.set(cache_key,
              timestamp,
              timeout=None)
    return timestamp


def skip_cache(resolve_fn):
    """
    A decorator for graphql resolvers to set 'skip_cache' property on
    info.context. The 'skip_cache' property tells the
    CachedResponseGraphQLViewMixin to not cache the graphql query results.
    """
    def _resolve_fn(self, info, **kwargs):
        context = getattr(info, 'context', None)
        if context:
            context.skip_cache = True
        return resolve_fn(self, info, **kwargs)
    return _resolve_fn


def cache_timeout(timeout=None):
    """
    A decorator to allow graphql resolvers to set cache timeout property on
    info.context. The default timeout is to cache forever
    """
    def wrap(resolve_fn):
        def _resolve_fn(self, info, **kwargs):
            context = getattr(info, 'context', None)
            if context:
                context.cache_timeout = timeout
            return resolve_fn(self, info, **kwargs)
        return _resolve_fn

    return wrap
