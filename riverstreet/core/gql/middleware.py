from riverstreet.loaders import Loaders


class CachedResponseMiddleware(object):
    """
    Adds flag to context to skip response caching in case of non-query
    operations (i.e. mutations) and when an error occurs.
    Use in conjunction with CachedResponseGraphQLViewMixin
    """
    def resolve(self, next, root, info, **args):
        # skip response cache if operation is not 'query'
        if info.operation.operation != 'query':
            info.context.skip_cache = True

        try:
            return next(root, info, **args)
        except Exception as e:
            info.context.skip_cache = True
            return e


class LoaderMiddleware(object):
    """
    Adds DataLoader instances to context.loaders to make them available
    to resolvers.
    """
    def resolve(self, next, root, info, **args):
        if not hasattr(info.context, 'loaders'):
            info.context.loaders = Loaders()
        return next(root, info, **args)


class TestMiddleware(object):
    def resolve(self, next, root, info, **args):
        print('root:', root, 'field_name:', info.field_name, 'args:', args)
        # how to skip caching
        return next(root, info, **args)
