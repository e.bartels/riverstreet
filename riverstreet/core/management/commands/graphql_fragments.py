"""
This command outputs json for graphql types for unions & interfaces.
This is needed by apollo-client so union/interface fragments can be used.

see: https://www.apollographql.com/docs/react/advanced/fragments.html#fragment-matcher
"""
import importlib
import json

from django.conf import settings
from django.core.management.base import BaseCommand, CommandError

from graphene_django.settings import graphene_settings


DEFAULT_OUTPUT = getattr(settings, 'GRAPHENE', {}).get(
    'SCHEMA_TYPES_OUTPUT',
    'schemaFragments.json'
)


class CommandArguments(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument(
            "--schema",
            type=str,
            dest="schema",
            default=graphene_settings.SCHEMA,
            help="Django app containing schema to dump, e.g. myproject.core.schema.schema",
        )

        parser.add_argument(
            "--out",
            type=str,
            dest="out",
            default=DEFAULT_OUTPUT,
            help="Output file (default: schemaFragments.json)",
        )

        parser.add_argument(
            "--indent",
            type=int,
            dest="indent",
            default=graphene_settings.SCHEMA_INDENT,
            help="Output file indent (default: None)",
        )


# graphql query for type information
fragment_types_query = """
query FragmentTypesQuery {
  __schema {
    types {
      kind
      name
      possibleTypes {
        name
      }
    }
  }
}
"""


class Command(CommandArguments):
    help = "Dump graphql schema fragment types JSON to file"
    can_import_settings = True

    def save_file(self, out, schema_dict, indent):
        with open(out, "w") as outfile:
            json.dump(schema_dict, outfile, indent=indent)

    def handle(self, *args, **options):
        options_schema = options.get("schema")

        if options_schema and type(options_schema) is str:
            module_str, schema_name = options_schema.rsplit(".", 1)
            mod = importlib.import_module(module_str)
            schema = getattr(mod, schema_name)

        elif options_schema:
            schema = options_schema

        else:
            schema = graphene_settings.SCHEMA

        out = options.get("out") or graphene_settings.SCHEMA_OUTPUT

        if not schema:
            raise CommandError(
                "Specify schema on GRAPHENE.SCHEMA setting or by using --schema"
            )

        indent = options.get("indent")
        schema_dict = self.query_schema(schema)
        self.save_file(out, schema_dict, indent)

        style = getattr(self, "style", None)
        success = getattr(style, "SUCCESS", lambda x: x)

        self.stdout.write(success("Successfully dumped GraphQL fragment types to %s" % out))

    def query_schema(self, schema):
        """Run graphql query"""
        introspection = schema.execute(fragment_types_query)
        if introspection.errors:
            raise introspection.errors[0]
        data = introspection.data

        # Filter types with a 'possibleTypes' key
        # These should include unions and interfaces that we want to output
        data['__schema']['types'] = [
            t for t in data['__schema']['types']
            if t['possibleTypes'] is not None
        ]

        return {"data": data}
