from django.db import models
from autoslug import AutoSlugField


class ProjectQuerySet(models.QuerySet):
    def published(self):
        return self.filter(published=True)


class BaseProject(models.Model):
    """
    Base Project model
    """
    title = models.CharField(max_length=512)
    slug = AutoSlugField(
        populate_from='title',
        max_length=512,
        unique=True,
        editable=True,
        help_text='A URL-friendly version of the title used in URLs',
    )
    client = models.CharField(max_length=255, blank=True)
    date = models.DateField(blank=True, null=True)
    published = models.BooleanField(
        default=True,
        db_index=True,
        help_text='Whether to publish on the site.')
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    image = models.ForeignKey(
        'media.Image',
        verbose_name='Main Image',
        null=True,
        blank=True,
        related_name='+',
        help_text='Representative image used in project lists.',
    )

    video = models.ForeignKey(
        'media.Video',
        verbose_name='Main Video',
        null=True,
        blank=True,
        related_name='+',
        help_text='Main video to play from project list pages.',
    )

    video_intro_caption = models.TextField(
        blank=True,
        help_text='Caption to be shown along with main video',
    )

    show_project_details = models.BooleanField(
        default=True,
        verbose_name='Show project details link',
        help_text='Show "view more" link to project details page from video popups.',
    )

    notes = models.TextField(blank=True, help_text="For internal use")

    objects = ProjectQuerySet.as_manager()

    class Meta:
        abstract = True
        ordering = ('-date', 'id')
        verbose_name = 'Project'
        verbose_name_plural = 'Projects'

    def __str__(self):
        return self.title

    @models.permalink
    def get_absolute_url(self):
        return 'work:project-detail', [self.slug]

    def get_image(self):
        if self.image:
            return self.image.filename
        elif self.video:
            return self.video.video_image
        return self.get_image_from_content()

    def get_image_from_content(self):
        content = self.content.filter(text='').select_related('image', 'video')
        videos = [i for i in content if i.video]
        if videos:
            return videos[0].video.video_image

        images = [i for i in content if i.image]
        if images:
            return images[0].image.filename

        return None
