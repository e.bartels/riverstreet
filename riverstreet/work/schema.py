import graphene
from graphene import relay
from graphene_django import DjangoObjectType
from graphene_django.filter import DjangoFilterConnectionField

from django.core.exceptions import ObjectDoesNotExist

from riverstreet.core.schema.content import ContentItem
from riverstreet.core.schema.media import SizedImages, Video
from riverstreet.work import models


class CategoryType(DjangoObjectType):
    class Meta:
        name = 'WorkCategory'
        model = models.Category
        only_fields = [
            'id',
            'slug',
            'title',
            'position',
            'intro_image',
            'intro_text',
            'projects',
        ]

    def resolve_projects(self, info, **kwargs):
        return info.context.loaders.work.category_projects.load(self.id)

    def resolve_intro_image(self, info, **kwargs):
        if self.intro_image_id:
            return info.context.loaders.core.image.load(self.intro_image_id)
        return None

    def resolve_intro_text(self, info, **kwargs):
        if not self.intro_image_id and not self.intro_text:
            return self.title
        return self.intro_text


class AssocProjectCategory(DjangoObjectType):
    class Meta:
        name = 'WorkAssocProjectCategory'
        model = models.Category
        only_fields = [
            'id',
            'slug',
            'title',
        ]


class WorkProjectType(DjangoObjectType):
    image = SizedImages()
    video = graphene.Field(Video)
    categories = graphene.List(graphene.NonNull(AssocProjectCategory), required=True)
    content = graphene.List(graphene.NonNull(ContentItem), required=True)

    class Meta:
        name = 'WorkProject'
        model = models.Project
        interfaces = [relay.Node]
        only_fields = [
            'id',
            'slug',
            'title',
            'client',
            'date',
            'image',
            'video',
            'video_intro_caption',
            'show_project_details',
            'categories',
            'content',
        ]
        filter_fields = {
            'categories__slug': ['exact'],
        }

    def resolve_video(self, info, **kwargs):
        return self.video

    def resolve_image(self, info, **kwargs):
        loaders = info.context.loaders

        if self.image_id:
            return (loaders.core
                    .image.load(self.image_id)
                    .then(lambda image: image.filename))
        elif self.video_id:
            return (loaders.core
                    .video.load(self.video_id)
                    .then(lambda video: video.video_image))

        def extract_content_image(items):
            media_items = [item for item in items if item.video or item.image]
            try:
                item = media_items[0]
                if item.video:
                    return item.video.video_image
                else:
                    return item.image.filename
            except IndexError:
                return None

        return (loaders.work
                .project_content.load(self.id)
                .then(extract_content_image))

    def resolve_categories(self, info, **kwargs):
        return info.context.loaders.work.project_categories.load(self.id)

    def resolve_content(self, info, **kwargs):
        return info.context.loaders.work.project_content.load(self.id)


class Query(object):
    work_categories = graphene.List(CategoryType)
    work_projects = DjangoFilterConnectionField(WorkProjectType)

    work_category = graphene.Field(
        CategoryType,
        id=graphene.Int(),
        slug=graphene.String())

    work_project = graphene.Field(
        WorkProjectType,
        id=graphene.Int(),
        slug=graphene.String())

    def resolve_work_categories(self, info, **kwargs):
        return models.Category.objects.published().exclude(slug='general-work')

    def resolve_work_projects(self, info, **kwargs):
        return models.Project.objects.published()

    def resolve_work_category(self, info, **kwargs):
        id = kwargs.get('id')
        slug = kwargs.get('slug')

        qs = models.Category.objects.all()
        if info.context.user.is_anonymous():
            qs = qs.published()

        try:
            if id is not None:
                return qs.get(pk=id)
            if slug is not None:
                return qs.get(slug=slug)
        except ObjectDoesNotExist:
            return None

        return None

    def resolve_work_project(self, info, **kwargs):
        id = kwargs.get('id')
        slug = kwargs.get('slug')

        qs = models.Project.objects.all()
        user = info.context.user
        if user.is_anonymous() or not user.is_staff:
            qs = qs.published()

        try:
            if id is not None:
                return qs.get(pk=id)
            if slug is not None:
                return qs.get(slug=slug)
        except ObjectDoesNotExist:
            return None

        return None
