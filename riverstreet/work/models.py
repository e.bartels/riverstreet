from django.db import models
from django.core.urlresolvers import reverse
from autoslug import AutoSlugField
from positions import PositionField
from ado.content.models import ContentItem
from riverstreet.core.models import BaseProject


class CategoryQuerySet(models.QuerySet):
    def published(self):
        return self.filter(published=True)


class Category(models.Model):
    """
    Project Category
    """
    title = models.CharField(max_length=512)
    slug = AutoSlugField(
        populate_from='title',
        max_length=512,
        unique=True,
        editable=True,
        help_text='A URL-friendly version of the title used in URLs',
    )
    published = models.BooleanField(
        default=True,
        db_index=True,
        help_text='Whether to publish on the site.')

    position = PositionField()

    intro_image = models.ForeignKey(
        'media.Image',
        null=True,
        blank=True,
        help_text="A full-bleed image shown at top of category pages",
        related_name='+',
    )
    intro_text = models.TextField(
        blank=True,
        help_text="Text to show in intro section of category page",
    )

    notes = models.TextField(blank=True, help_text="For internal use")

    objects = CategoryQuerySet.as_manager()

    class Meta:
        ordering = ('position', 'id',)
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'

    def __str__(self):
        return self.title

    @models.permalink
    def get_absolute_url(self):
        if self.slug == 'general-work':
            return 'work:index', []
        return 'work:category', [self.slug]


class Project(BaseProject):
    """
    Project model
    """
    categories = models.ManyToManyField(
        Category,
        through='ProjectCategoryMembership',
        related_name='projects'
    )

    @models.permalink
    def get_absolute_url(self):
        return 'work:project-detail', [self.slug]


class ProjectContent(ContentItem):
    related_model = Project

    def parent_admin_url(self):
        return reverse('admin:work_project_change', args=[self.project.pk])


class ProjectCategoryMembership(models.Model):
    """
    Join model for project categories
    """
    project = models.ForeignKey(
        Project,
        related_name='category_memberships',
        on_delete=models.CASCADE,
    )
    category = models.ForeignKey(
        Category,
        related_name='project_memberships',
        on_delete=models.CASCADE,
    )
    position = PositionField(collection='category')

    class Meta:
        ordering = ('position', 'id')
        unique_together = ('project', 'category')

    def __str__(self):
        return self.project.title

    def get_absolute_url(self):
        return self.project.get_absolute_url()
