from django.views.generic import DetailView
from django.conf import settings
from sorl import thumbnail

from riverstreet.core.views import index # noqa
from .models import Category, Project


class CategoryDetailView(DetailView):
    template_name = 'index.html'

    def get_queryset(self):
        user = self.request.user
        if user.is_authenticated() and user.is_staff:
            qs = Category.objects.all()
        else:
            qs = Category.objects.published()
        return qs

    def get_image(self, category):
        if category.intro_image:
            image = category.intro_image.get_image()
            sizes = dict(settings.SCALED_IMAGE_SIZES)
            thumb = thumbnail.get_thumbnail(image, sizes['large'])
            return [
                self.request.build_absolute_uri(thumb.url),
                thumb.width,
                thumb.height,
            ]

        return [None, None, None]

    def get_context_data(self, **kwargs):
        context = super(CategoryDetailView, self).get_context_data(**kwargs)
        category = context['category']
        [og_image, og_image_width, og_image_height] = self.get_image(category)
        context.update({
            'page_title': category.title,
            'og_image': og_image,
            'og_image_width': og_image_width,
            'og_image_height': og_image_height,
        })
        return context


category_detail = CategoryDetailView.as_view()


class ProjectDetailView(DetailView):
    template_name = 'index.html'

    def get_queryset(self):
        user = self.request.user
        if user.is_authenticated() and user.is_staff:
            qs = Project.objects.all()
        else:
            qs = Project.objects.published()
        return qs.prefetch_related('content')

    def get_image(self, project):
        content_items = project.content.all()
        try:
            item = [c for c in content_items if c.type in ('image', 'video')][0]
            image = getattr(item, item.type).get_image()
            sizes = dict(settings.SCALED_IMAGE_SIZES)
            thumb = thumbnail.get_thumbnail(image, sizes['large'])
            return [
                self.request.build_absolute_uri(thumb.url),
                thumb.width,
                thumb.height,
            ]
        except IndexError:
            pass

        return [None, None, None]

    def get_context_data(self, **kwargs):
        context = super(ProjectDetailView, self).get_context_data(**kwargs)
        project = context['project']
        [og_image, og_image_width, og_image_height] = self.get_image(project)
        context.update({
            'page_title': project.title,
            'og_image': og_image,
            'og_image_width': og_image_width,
            'og_image_height': og_image_height,
        })
        return context


project_detail = ProjectDetailView.as_view()
