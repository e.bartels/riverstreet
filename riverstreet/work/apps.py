from django.apps import AppConfig


class WorkConfig(AppConfig):
    name = 'riverstreet.work'
    icon = '<i class="icon material-icons">collections</i>'
