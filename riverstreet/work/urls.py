from django.conf.urls import url

from riverstreet.core.views import index as index_view
from riverstreet.work import views

urlpatterns = [
    url(r'^$', index_view, name='index'),
    url(r'^projectvideo/(?P<project_slug>[^/]+)$', index_view),

    url(r'^category/?$', index_view, name='category-index'),
    url(r'^category/(?P<slug>[^/]+)/?$', views.category_detail, name='category'),
    url(r'^category/(?P<slug>[^/]+)/projectvideo/(?P<project_slug>[^/]+)?$', views.category_detail),

    url(r'^project/(?P<slug>[^/]+)/?$', views.project_detail, name='project-detail'),
    url(r'^project/(?P<slug>[^/]+)/media/(?P<id>\d+)?$',
        views.project_detail,
        name='project-media'),
]
