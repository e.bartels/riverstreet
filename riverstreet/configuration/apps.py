from django.apps import AppConfig


class ConfigurationConfig(AppConfig):
    name = 'riverstreet.configuration'
    verbose_name = 'Settings'
    icon = '<i class="icon material-icons">settings</i>'
