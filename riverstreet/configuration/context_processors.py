from .models import (
    ConfigSettingGroup,
)


def config_settings(request):
    groups = ConfigSettingGroup.objects.all()
    config_settings = {}

    for group in groups:
        settings = {}
        ignore_fields = (
            'id',
            'configsettinggroup_ptr',
            'polymorphic_ctype',
        )
        fields = [f for f in group._meta.fields
                  if f.name not in ignore_fields]

        for field in fields:
            settings[field.name] = getattr(group, field.name)
        config_settings[group._meta.model_name] = settings

    return {
        'config_settings': config_settings,
    }
