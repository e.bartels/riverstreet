from django.db import models
from polymorphic.models import PolymorphicModel


class SettingField(object):
    defaults = {}

    def __init__(self, *args, **kwargs):
        mergedKwargs = {}
        mergedKwargs.update(self.defaults)
        mergedKwargs.update(kwargs)
        super(SettingField, self).__init__(*args, **mergedKwargs)


class CharSetting(SettingField, models.CharField):
    defaults = {
        'max_length': 512,
        'blank': True,
    }


class TextSetting(SettingField, models.TextField):
    defaults = {
        'blank': True,
    }


class ConfigSettingGroup(PolymorphicModel):
    pass


class MetaTags(ConfigSettingGroup):
    title = CharSetting()
    description = TextSetting(help_text="Comma-separated list of keywords")
    keywords = TextSetting(help_text=("A short description for your site "
                                      "(used in search engine results)"))

    class Meta:
        verbose_name = 'Meta tags'
        verbose_name_plural = 'Meta tags'


class Analytics(ConfigSettingGroup):
    google_analytics_id = CharSetting(
        help_text="Enter your google analytics tracking id")

    class Meta:
        verbose_name = 'Analytics'
        verbose_name_plural = 'Analytics'


class SocialMedia(ConfigSettingGroup):
    facebook_link = CharSetting()
    twitter_link = CharSetting()
    instagram_link = CharSetting()
    youtube_link = CharSetting()

    class Meta:
        verbose_name = 'Social Media'
        verbose_name_plural = 'Social Media'
