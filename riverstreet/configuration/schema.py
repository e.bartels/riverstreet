from collections import namedtuple
import graphene
from graphene_django.types import DjangoObjectType
from riverstreet.configuration import models


class MetaTags(DjangoObjectType):
    class Meta:
        model = models.MetaTags


class Analytics(DjangoObjectType):
    class Meta:
        model = models.Analytics


class SocialMedia(DjangoObjectType):
    class Meta:
        model = models.SocialMedia


class ConfigSettings(graphene.ObjectType):
    metatags = graphene.Field(MetaTags)
    analytics = graphene.Field(Analytics)
    socialmedia = graphene.Field(SocialMedia)


class Query(object):
    configsettings = graphene.Field(ConfigSettings)

    def resolve_configsettings(self, info, **kwargs):
        groups = list(models.ConfigSettingGroup.objects.all())
        group_names = [g._meta.model_name for g in groups]
        ConfigStruct = namedtuple('ConfigStruct',
                                  group_names)
        data = ConfigStruct(**dict(zip(group_names, groups)))
        return data
