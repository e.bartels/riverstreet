from django.contrib import admin
from django import http
from django.core.urlresolvers import reverse
from ado.customadmin.admin import BaseModelAdmin
from . import models


class SettingAdmin(BaseModelAdmin):
    icon = '<i class="icon material-icons">settings</i>'

    def changelist_view(self, request, extra_context=None):
        opts = self.model._meta

        (instance, created) = self.model.objects.get_or_create()

        url_name = 'admin:{app}_{model}_change'.format(**{
            'app': opts.app_label,
            'model': opts.model_name,
        })
        url = reverse(url_name, args=[instance.pk])
        return http.HttpResponseRedirect(url)

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(models.MetaTags)
class MetaTagsAdmin(SettingAdmin):
    dashboard_order = 0


@admin.register(models.Analytics)
class AnalyticsAdmin(SettingAdmin):
    dashboard_order = 1


@admin.register(models.SocialMedia)
class SocialMediaAdmin(SettingAdmin):
    dashboard_order = 2
