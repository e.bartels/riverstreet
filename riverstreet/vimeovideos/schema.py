import vimeo
import graphene
from graphql import GraphQLError
from graphene.types.resolver import dict_resolver
from django.conf import settings
from riverstreet.core.gql.caching import cache_timeout


class VimeoPicture(graphene.ObjectType):
    class Meta:
        default_resolver = dict_resolver

    width = graphene.Int(required=True)
    height = graphene.Int(required=True)
    link = graphene.String(required=True)
    link_with_play_button = graphene.String(required=True)


class VimeoVideo(graphene.ObjectType):
    class Meta:
        default_resolver = dict_resolver

    id = graphene.ID(required=True)
    name = graphene.String(required=True)
    link = graphene.String(required=True)
    width = graphene.Int(required=True)
    height = graphene.Int(required=True)
    duration = graphene.Int(required=True)
    description = graphene.String()
    embed = graphene.String(required=True)
    pictures = graphene.List(graphene.NonNull(VimeoPicture), required=True)

    def resolve_pictures(self, info, **kwargs):
        return self['pictures']['sizes']

    def resolve_embed(self, info, **kwargs):
        return self['embed']['html']


class VimeoAlbum(graphene.ObjectType):
    class Meta:
        default_resolver = dict_resolver

    id = graphene.ID(required=True)
    name = graphene.String(required=True)
    videos = graphene.List(graphene.NonNull(VimeoVideo), required=True)


class Query(object):
    album = graphene.Field(
        VimeoAlbum,
        id=graphene.String(),
    )

    @cache_timeout(60)
    def resolve_album(self, info, **kwargs):
        v = vimeo.VimeoClient(
            token=settings.VIMEO_ACCESS_TOKEN,
            key=settings.VIMEO_CLIENT_ID,
            secret=settings.VIMEO_CLIENT_SECRET
        )

        album_id = kwargs['id']
        album = v.get('/me/albums/{}'.format(album_id))

        if album.status_code == 404:
            raise GraphQLError('not found')
        elif album.status_code != 200:
            raise GraphQLError(album.json().get('error', 'an error occurred'))

        album_videos = v.get('/me/albums/{}/videos?per_page=100&sort=manual'.format(album_id))

        album_data = album.json()
        album_data.update({
            'id': album_id,
            'videos': [{**v, **{'id': v['uri'].split('/')[-1]}}
                       for v in album_videos.json()['data']],
        })
        return album_data
