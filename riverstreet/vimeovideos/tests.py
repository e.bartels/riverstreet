# import vimeo
# from django.test import TestCase
# from django.conf import settings
#
#
# class VimeoAPITest(TestCase):
#     def setUp(self):
#         self.vimeo = vimeo.VimeoClient(
#             token=settings.VIMEO_ACCESS_TOKEN,
#             key=settings.VIMEO_CLIENT_ID,
#             secret=settings.VIMEO_CLIENT_SECRET
#         )
#
#     def test_get_vimeo_album(self):
#         albums = self.vimeo.get('/me/albums')
#         self.assertEqual(200, albums.status_code)
#         self.assertTrue(albums.json())
#
#         album = albums.json()['data'][0]
#         album_videos_uri = '{0}?per_page=100'.format(
#             album['metadata']['connections']['videos']['uri'],
#         )
#         self.assertTrue(album_videos_uri)
#         album_videos = self.vimeo.get(album_videos_uri)
#         self.assertEqual(200, album_videos.status_code)
#         self.assertTrue(album_videos.json()['data'])
