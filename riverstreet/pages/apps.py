from django.apps import AppConfig


class PagesConfig(AppConfig):
    name = 'riverstreet.pages'
    icon = '<i class="icon material-icons">content_copy</i>'
