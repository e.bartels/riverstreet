import factory
from django.test import TestCase
from django.utils.text import slugify
from django.core.exceptions import ValidationError

from ado.media.tests.tests import ImageFactory


class PageContentFactory(factory.DjangoModelFactory):
    text = factory.Faker('text')

    class Meta:
        model = 'pages.PageContent'


class PageFactory(factory.DjangoModelFactory):
    title = factory.Sequence(lambda n: u'Page {0}'.format(n + 1))

    @factory.lazy_attribute
    def slug(self):
        return slugify(self.title)

    content = factory.RelatedFactory(
        PageContentFactory,
        'page',
    )

    class Meta:
        model = 'pages.Page'


class PageModelTest(TestCase):

    def test_create(self):
        page = PageFactory.build()
        page.full_clean()
        page.save()

        self.assertTrue(page.id)
        self.assertTrue(page.title)
        self.assertTrue(page.slug)
        self.assertTrue(page.published)

        page = PageFactory()
        self.assertEqual(1, page.content.count())


class PageContentTest(TestCase):

    def test_content_model_attributes(self):
        page = PageFactory()
        item = page.content.all()[0]
        self.assertEqual('page', item.related_model._meta.model_name)
        self.assertEqual('page', item.related_property)

    def test_add_content(self):
        page = PageFactory()
        page.content.create(text='Test content')
        page.content.create(image=ImageFactory())
        page.save()
        items = page.content.all()
        self.assertEqual(3, items.count())
        self.assertListEqual([0, 1, 2], [c.position for c in items.all()])
        self.assertListEqual(
            ['text', 'text', 'image'],
            [c.type for c in items],
        )

    def test_content_requires_image_or_text(self):
        content = PageContentFactory.build(
            image=None,
            video=None,
            text='',
        )
        with self.assertRaises(ValidationError) as cm:
            content.full_clean()

        self.assertTrue('text' in cm.exception.error_dict)
        self.assertTrue('image' in cm.exception.error_dict)
        self.assertTrue('video' in cm.exception.error_dict)

    def test_positions(self):
        page = PageFactory()

        # make sure positions assigned in sequence
        item1 = page.content.all()[0]
        self.assertEqual(0, item1.position)

        item2 = page.content.create(text='Test content')
        self.assertEqual(1, item2.position)

        item3 = page.content.create(text='Test content')
        self.assertEqual(2, item3.position)

        # change position
        item3.position = 0
        item3.save()

        # make sure positions still in sequence
        items = list(page.content.all())
        self.assertListEqual(
            [3, 1, 2],
            [item.pk for item in items]
        )
