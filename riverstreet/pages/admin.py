from django.contrib import admin

from ado.customadmin.admin import BaseModelAdminMixin
from ado.content.admin import ContentItemsInline
from .models import Page, PageContent, Homepage


class PageContentInline(ContentItemsInline):
    model = PageContent
    verbose_name_plural = 'Page Content'


@admin.register(Page)
class PageAdmin(BaseModelAdminMixin, admin.ModelAdmin):
    icon = '<i class="icon material-icons">content_copy</i>'
    dashboard_order = 0
    list_filter = ['published']
    list_display = ['title', 'published']
    prepopulated_fields = {'slug': ('title',)}
    html_editor_fields = ['content', 'intro_text']
    verbose_image_fk_fields = ['intro_image']
    inlines = [PageContentInline]
    fieldsets = (
        (None, {
            'fields': ['title', 'slug', 'published'],
        }),
        ('Intro Content', {
            'fields': ['intro_image', 'intro_text'],
        }),
    )


@admin.register(Homepage)
class HomepageAdmin(BaseModelAdminMixin, admin.ModelAdmin):
    icon = '<i class="icon material-icons">home</i>'
    dashboard_order = 1
    html_editor_fields = ['intro_text']
    verbose_video_fk_fields = ['video_reel']

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return False
