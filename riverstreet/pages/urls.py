from django.conf.urls import url

from riverstreet.pages import views

urlpatterns = [
    url(r'^(?P<slug>[^/]+)/?$', views.page_detail, name='detail'),
]
