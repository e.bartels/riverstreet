import graphene
from graphene_django.types import DjangoObjectType

from riverstreet.core.schema.content import ContentItem
from riverstreet.pages.models import Page, Homepage


class PageType(DjangoObjectType):
    content = graphene.List(graphene.NonNull(ContentItem), required=True)

    class Meta:
        name = 'Page'
        model = Page
        only_fields = [
            'id',
            'slug',
            'title',
            'intro_image',
            'intro_text',
            'content',
        ]

    def resolve_content(self, info, **kwargs):
        return self.content.all()


class HomepageType(DjangoObjectType):

    class Meta:
        name = 'Homepage'
        model = Homepage
        only_fields = [
            'id',
            'intro_text',
            'video_reel',
        ]


class Query(object):
    pages = graphene.List(PageType)
    page = graphene.Field(
        PageType,
        id=graphene.Int(),
        slug=graphene.String())

    homepage = graphene.Field(
        HomepageType,
    )

    def resolve_pages(self, info, **kwargs):
        return Page.objects.published()

    def resolve_page(self, info, **kwargs):
        id = kwargs.get('id')
        slug = kwargs.get('slug')

        qs = Page.objects.all()
        user = info.context.user
        if user.is_anonymous() or not user.is_staff:
            qs = qs.published()

        try:
            if id is not None:
                return qs.get(pk=id)
            if slug is not None:
                return qs.get(slug=slug)
        except Page.DoesNotExist:
            return None

        return None

    def resolve_homepage(self, info, **kwargs):
        try:
            return Homepage.objects.all()[:1][0]
        except IndexError:
            return None
