from django.db import models
from django.core.urlresolvers import reverse
from autoslug import AutoSlugField

from ado.content.models import ContentItem


class PageQuerySet(models.QuerySet):
    def published(self):
        return self.filter(published=True)


class Page(models.Model):
    title = models.CharField(max_length=512)
    slug = AutoSlugField(
        populate_from='title',
        max_length=512,
        unique=True,
        editable=True,
        help_text='A URL-friendly version of the title used in URLs',
    )
    published = models.BooleanField(default=True)
    intro_image = models.ForeignKey(
        'media.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
    )
    intro_text = models.TextField(blank=True)

    objects = PageQuerySet.as_manager()

    def __str__(self):
        return self.title

    @models.permalink
    def get_absolute_url(self):
        return 'pages:detail', [self.slug]


class PageContent(ContentItem):
    related_model = Page

    def parent_admin_url(self):
        return reverse('admin:pages_page_change', args=[self.page.pk])


class Homepage(models.Model):
    intro_text = models.TextField(
        help_text="Text that appears at the top of home page.")
    video_reel = models.ForeignKey(
        'media.Video',
        help_text="Select a video reel to link to from home page")

    class Meta:
        verbose_name_plural = 'Homepage'

    def __str__(self):
        return 'Homepage'
