from django.views.generic import DetailView
from django.conf import settings
from sorl import thumbnail
from .models import Page


class PageDetailView(DetailView):
    template_name = 'index.html'

    def get_queryset(self):
        user = self.request.user
        if user.is_authenticated() and user.is_staff:
            qs = Page.objects.all()
        else:
            qs = Page.objects.published()
        return qs.prefetch_related('content')

    def get_image(self, page):
        if page.intro_image:
            image = page.intro_image.get_image()
            sizes = dict(settings.SCALED_IMAGE_SIZES)
            thumb = thumbnail.get_thumbnail(image, sizes['large'])
            return [
                self.request.build_absolute_uri(thumb.url),
                thumb.width,
                thumb.height,
            ]

        content_items = page.content.all()
        try:
            item = [c for c in content_items if c.type in ('image', 'video')][0]
            image = getattr(item, item.type).get_image()
            sizes = dict(settings.SCALED_IMAGE_SIZES)
            thumb = thumbnail.get_thumbnail(image, sizes['large'])
            return [
                self.request.build_absolute_uri(thumb.url),
                thumb.width,
                thumb.height,
            ]
        except IndexError:
            pass

        return [None, None, None]

    def get_context_data(self, **kwargs):
        context = super(PageDetailView, self).get_context_data(**kwargs)
        page = context['page']
        [og_image, og_image_width, og_image_height] = self.get_image(page)
        context.update({
            'page_title': page.title,
            'og_image': og_image,
            'og_image_width': og_image_width,
            'og_image_height': og_image_height,
        })
        return context


page_detail = PageDetailView.as_view()
