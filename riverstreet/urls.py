from django.conf.urls import include, url
from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin
from riverstreet.core.views import Redirect
from riverstreet.blog.views import BlogRedirectView
from riverstreet.core import views


urlpatterns = [
    url(r'^$', views.index, name='home'),
    url(r'^login$', views.index, name='login'),
    url(r'^media', views.index),
    url(r'^promotion/', include('riverstreet.promotion.urls', namespace='promotion')),
    url(r'^work/', include('riverstreet.work.urls', namespace='work')),
    url(r'^p/', include('riverstreet.pages.urls', namespace='pages')),
    url(r'^people/', include('riverstreet.people.urls', namespace='people')),
    url(r'^news/', include('riverstreet.blog.urls', namespace='blog')),

    url(r'^clients/(?P<id>\d+)/?', views.index, name='clients-detail'),
    url(r'^clients/(?P<username>[^/]+)/?', views.index, name='clients-detail'),
    url(r'^clients/?', views.index, name='clients'),

    url(r'^vimeo/album/(?P<id>[^/]+)/?$', views.index, name='vimeo-album'),

    url(r'^typography/?$', views.index, name='typography'),

    # Admin:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^customadmin/', include('ado.customadmin.urls')),
    url(r'^admin/media/', include('ado.media.admin.urls')),
    url(r'^admin/', include(admin.site.urls)),
]

# Graphql urls
urlpatterns += [
    url(r'^graphql', views.graphql_view),
]

# MEDIA For Development Server
urlpatterns += static(settings.MEDIA_URL,
                      document_root=settings.MEDIA_ROOT,
                      show_indexes=True)

# Legay site redirects
urlpatterns += [
    # categories & projects
    url(r'^collections/cat/(?P<slug>[^/]+)/?',
        Redirect.as_view(pattern_name='work:category')),
    url(r'^collections/(?P<slug>[^/]+)/?',
        Redirect.as_view(pattern_name='work:project-detail')),
    url(r'^collections/?', Redirect.as_view(url='/work')),

    # pages
    url(r'^pages/contact/?',
        Redirect.as_view(url='/p/about-us')),
    url(r'^pages/(?P<slug>[^/]+)/?',
        Redirect.as_view(pattern_name='pages:detail')),

    # blog
    url(r'^blog', BlogRedirectView.as_view()),

    # clientpages
    url(r'^clientpages/(?P<slug>[^/]+)/?',
        Redirect.as_view(pattern_name='promotion:reel-detail')),

    # clientsections (must be last!)
    url(r'^(?P<slug>(tracypion_director|selected-work-alternative-content|generation-create|editorial-work|showreel-database|advocacy|tracypion))/.*', # noqa
        Redirect.as_view(pattern_name='promotion:minisite')),
]
