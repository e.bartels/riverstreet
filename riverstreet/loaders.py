from riverstreet.core.loaders import Loaders as CoreLoaders
from riverstreet.promotion.loaders import Loaders as PromotionLoaders
from riverstreet.work.loaders import Loaders as WorkLoaders
from riverstreet.blog.loaders import Loaders as BlogLoaders


class LoaderSet(object):
    def __init__(self, **kwargs):
        for app, loader_set in kwargs.items():
            app_loaders = type(str('{}Loaders').format(app), (object,), {})
            for name, loader in loader_set.items():
                setattr(app_loaders, name, loader())

            setattr(self, app, app_loaders)


def Loaders():
    return LoaderSet(
        core=CoreLoaders,
        promotion=PromotionLoaders,
        work=WorkLoaders,
        blog=BlogLoaders,
    )
