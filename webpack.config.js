const path = require('path')
const autoprefixer = require('autoprefixer')
const cssnano = require('cssnano')
const BundleTracker = require('webpack-bundle-tracker')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const WebpackBar = require('webpackbar')
// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin

// dev-server  port
const PORT = 3000

const paths = {
  root: path.resolve(__dirname),
  buildPath: path.resolve('./static/bundle'),
  appPath: path.resolve('./app'),
  appStyles: path.resolve('./app/styles'),
}

/**
 * app config
 */
const config = (env = {}, name = 'app') => ({
  name,
  mode: env.production ? 'production' : 'development',
  context: paths.root,

  entry: {
    main: [
      './app/polyfill',
      './app/index',
    ],
  },

  output: {
    path: paths.buildPath,
    filename: env.production ? '[name].[contenthash:8].js' : '[name].js',
    publicPath: env.devServer ? `http://localhost:${PORT}/bundles/` : '/s/bundle/',
    crossOriginLoading: env.devServer ? 'anonymous' : false,
  },

  resolve: {
    extensions: ['.wasm', '.mjs', '.js', '.jsx', '.json', '.ts', '.tsx'],

    // Allow app imports from '~/some/module'
    alias: {
      '~': paths.appPath,
      styles: paths.appStyles,
      ...(env.devServer ? { 'react-dom': '@hot-loader/react-dom' } : {}),
    },
  },

  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            cacheDirectory: true,
            envName: env.production ? 'production' : 'development',
          },
        },
      },
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            cacheDirectory: true,
            envName: env.production ? 'production' : 'development',
          },
        },
      },
      {
        test: /\.s?css$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              hmr: env.devServer,
            },
          },
          {
            loader: 'css-loader',
            options: {
              sourceMap: !env.production,
            },
          },
          {
            loader: 'postcss-loader',
            options: {
              sourceMap: !env.production,
              plugins: [
                autoprefixer({ remove: false }),
                ...(env.production ? [cssnano()] : []),
              ],
            },
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: !env.production,
              sassOptions: {
                includePaths: [paths.appStyles],
              },
            },
          },
        ],
      },
      {
        test: /\.svg$/,
        exclude: /node_modules/,
        issuer: /\.[tj]sx?$/,
        loader: 'svg-react-loader',
      },
      {
        test: /\.svg?$/,
        issuer: /\.s?css$/,
        use: 'url-loader',
      },
      {
        test: /\.(png|gif|jpg|jpeg)?$/,
        use: 'url-loader',
      },
      {
        test: /\.(ttf|eot|woff|woff2)(\?\S*)?$/,
        use: !env.production ? 'url-loader' : {
          loader: 'file-loader',
          options: {
            name: 'fonts/[hash].[ext]',
          },
        },
      },
    ],
  },

  stats: 'errors-warnings',

  plugins: [
    new WebpackBar({ name }),

    // Outputs stats on generated files (used by django-webpack-loader)
    new BundleTracker({ filename: './static/bundle/webpack-stats.json' }),

    new MiniCssExtractPlugin({
      filename: `[name]${env.production ? '.[contenthash:8]' : ''}.css`,
    }),

    // production only
    ...(env.production ? [
      // new BundleAnalyzerPlugin(),
    ] : []),

    // dev & production, but not when running devServer
    ...(!env.devServer ? [
      new CleanWebpackPlugin({
        cleanAfterEveryBuildPatterns: ['editor_content.js'],
        cleanOnceBeforeBuildPatterns: ['**/*', '!editor_content.css'],
      }),
    ] : []),
  ],

  optimization: {
    splitChunks: {
      cacheGroups: {
        // vendor.js - node_modules, excluding css modules
        commons: {
          name: 'vendor',
          chunks: 'initial',
          test: module => (
            module.context.indexOf('/node_modules/') !== -1 &&
            !/\.s?css$/.test(module.nameForCondition && module.nameForCondition())
          ),
        },
      },
    },
  },

  performance: {
    hints: false,
  },

  ...(!env.production ? {
    devtool: 'inline-cheap-module-source-map',
  } : {}),

  // devServer settings
  ...(env.devServer ? {
    devServer: {
      port: PORT,
      hot: true,
      headers: {
        'Access-Control-Allow-Origin': '*',
      },
      // static assets are served by django
      proxy: {
        '/s': 'http://localhost:8000',
      },
    },
  } : {}),
})

/**
 * ckeditor content css config
 */
const editorConfig = (env = {}, name = 'editor') => ({
  ...config(env, name),
  ...({
    name,
    entry: {
      editor_content: './app/styles/editor_content.scss',
    },
    output: {
      path: paths.buildPath,
      filename: '[name].js',
    },
    plugins: [
      new WebpackBar({ name, color: '#c065f4' }),
      new MiniCssExtractPlugin({
        filename: '[name].css',
      }),
    ],
  }),
})

module.exports = (env) => ([
  config(env),
  editorConfig(env),
])
