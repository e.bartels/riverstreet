### Webpack build
FROM node:12-alpine AS build-webpack

WORKDIR /app
COPY package*.json ./
RUN npm ci

COPY webpack.config.js .babelrc ./
COPY app/ ./app
RUN npm run build && rm -rf /app/node_modules/


### Python build
FROM python:3.6 AS build-python

WORKDIR /app
COPY requirements.txt ./
COPY config/requirements*.txt ./config/
COPY ado/ ado/
RUN pip install --no-cache-dir -r requirements.txt

### Python app
FROM python:3.6-slim

RUN apt-get -qy update && apt-get -qy install --no-install-recommends \
  procps \
  libxml2 \
  libssl-dev \
  libcurl4-openssl-dev \
  shared-mime-info \
  mime-support \
  && rm -rf /var/cache/apt/* /var/lib/apt/lists/*

WORKDIR /app
COPY --from=build-python /usr/local /usr/local
COPY . ./
COPY --from=build-webpack /app/static/bundle ./static/bundle

ENV PYTHONUNBUFFERED=1 \
    PYTHONFAULTHANDLER=1 \
    DJANGO_ENV=production

RUN python -m compileall . && \
    python manage.py collectstatic --noinput && \
    useradd -d /app -M -u 1000 web

EXPOSE 8000 8000
USER web

ENTRYPOINT ["uwsgi", "--die-on-term", "--ini", "/app/config/uwsgi.ini"]
