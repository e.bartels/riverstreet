Project Information
----------------------------

This README describes the source code for in-development project for [riverstreet.net](http://riverstreet.net)

**Server-side technologies**

The server-side component of this project is written in [Python][python] using the [Django web framework][django].  
[GraphQL][graphql] is used on both server and client to create an API to connect the two.


* [Python 3.6.x][python]
* [Django 1.11.x][django]
* [PostgreSQL 11][postgres]
* [GraphQL][graphql] 
* [Graphene][graphene]

See `requirements*.txt` files in [config/](config) folder for more python dependencies.

**Client-side technologies:**

The client-side app is written in [Typescript][typescript] with [React][react] for UI, and [SASS][sass] for stylesheets. [Webpack][webpack] is used as build/bundling tool.

* [Node.js 12.x][nodejs]
* [Typescript][typescript]
* [React][react]
* [Sass][sass]
* [Webpack][webpack]
* [Apollo][apollo]
* [react-redux][react-redux]
* [react-router][react-router]

See [`package.json`](package.json) for more front-end dependencies.


Repository structure
---------------------

Each top-level directory contains its own README file with further information.

**Folders:**

[`ado/`](ado/): Django customizations (media library, admin helpers, custom admin theme)  
[`app/`](app/): client-side application files (Typescript and Sass)  
[`config/`](config/): configuration files  
[`riverstreet/`](riverstreet/): Django application  
[`static/`](static/): static assets & files

**Files:**

[`Dockerfile`](Dockerfile): Docker build file for container deployment  
[`package.json`](package.json): npm dependencies and scripts  
[`tasks.py`](tasks.py): helper tasks using [invoke][pyinvoke]  
[`tsconfig.json`](tsconfig.json): Typescript configuration file  
[`webpack.config.js`](webpack.config.js): Webpack configuration file  

---

NOTE: The [`ado`](ado/) package contains custom enhancements for the Django admin, and has vendored into this project. Many of the project's Django models and admin code plug into functionality from the `ado` package. For example, it provides a media library for attaching images and videos to other models. You will see imports from `ado` throughout the Django code.  

See the README in the [`ado`](ado/) folder for more info.

Set up development environment
-----------------------------

**Set up project folders**

First we'll create a folder for the project and create a folder for storing uploaded media files.

    mkdir riverstreet
    mkdir -p riverstreet/htdocs/media
    cd riverstreet

**Create a virtualenv**

It is recommended to use [virtualenv][virtualenv] to create a separate space to install Python packages into.  
Example of creating a virtual environment:

    virtualenv -p python3 ./venv
    source ./venv/bin/activate


**Clone the git repo**

Make a project folder and clone the git repository

    git clone git@gitlab.com:e.bartels/riverstreet.git
    cd riverstreet

**Install python dependencies**

After activating the virtualenv, use [pip][pip] to install python dependencies into the virtualenv.

    pip install -r config/requirements.dev.txt

**Install PostgreSQL database**

This project uses [PostgreSQL][postgres] as a database. If you have have a `riverstreet.sql` file, it can be used to bootstrap the initial data.

To do that, first make sure you have [PostgreSQL][postgres] installed on your system. Make sure you can access it using the `psql` command.

To create the database:

    createdb riverstreet

To import the .sql file, run:

    psql -U [YOUR_USERNAME] -d riverstreet -f riverstreet.sql

You should now be able to access the database with `psql -c '\dt' riverstreet`  
It should output all the tables that were created.

**Django setup**

Create a `.env` file to store database settings in

    cp config/env.template .env

Edit the `.env` file and set the following values:

    DATABASE_URL=postgres://$(PG_USER):$(PG_PASS)@localhost/riverstreet

Replace the variables above with your own username/password. If postgres is set up to trust your user, you may be able to leave the username and password out, e.g.:

    DATABASE_URL=postgres://localhost/riverstreet

You should be able to run `./manage.py check` without any errors. And you should be able to access your database by running `./manage.py dbshell`. If those commands are successful, then your database has been properly configured.

To finish the Django setup, run:

    ./manage.py migrate
    ./manage.py createsuperuser

This will run any unapplied database migrations and create a superuser to access the Django admin with.

**npm install**

Finally, install client-side dependencies with by running

    npm install

To check that npm installed correctly, you should be able to run: `npm run lint`.


Running the project
-------------------

Start the Django dev server, run:

    ./manage.py runserver

Start the webpack dev server. In a separate terminal window, run:

    npm start

The webpack dev server has hot reload enabled, so you should be able to make change to client-side files and have the page update without needing to reload the page in the browser.

To view the site, visit [http://localhost:8000/](http://localhost:8000/)  

The admin is located at http://localhost:8000/admin/  
You can log in using the username and password you created above.


Managing dependencies
---------------------

**Python dependencies**  

[Pip][pip] is used to manage python dependencies. All dependencies are listed in the `requirements*.txt` files in the [`config/`](config/) directory.

To install a new dependency, run `pip install <PACKAGE_NAME>` and add it to the appropriate requirements.txt file.

**Client dependencies**

Use npm to add new front-end dependencies. For example, if you wanted to install lodash, then run  `npm install lodash`


Task Runner
-----------

The [invoke][pyinvoke]  python app can be used to run tasks in the `tasks.py` file. There are a few tasks in there for running tests (see below) and building docker images.

To see available tasks, run: `invoke -l`. To view help on a task add the `--help` flag, e.g. `invoke test --help`


Python tests
--------------------

Django tests are provided in the `**/*/tests.py` files. To run tests:

    invoke test

This will run the Django tests with the proper settings. Test have their own `riverstreet/config/settings.py` file.

You can also run tests in watch mode:

    invoke test -w

This will re-run tests when you change a file. Run `invoke test -h` to see other options


Linting
-------

This project has a [.flake8](.flake8) file. To lint python files with (flake8)(flake8), run

    `flake8 .`

Eslint is used to lint typescript files. See [.eslintrc](.eslintrc) file. To lint. There is an npm script, which will both run `estlint` and run `tsc` to type check the .ts files.  It will also run `stylelint` to check the Sass files for errors.

    npm run lint



Docker build
------------

Docker is one option for deploying this app to production.

A Dockerfile is included to make deployment easier. To build and tag a docker image, run:

    docker build -t [URL]:latest .

There is also a shortcut task in `tasks.py` to help with this. Run:

    invoke docker.build

NOTE: To use this, you'll need to edit the tasks.py and set the `DOCKER_REGISTRY` variable to the url
that you want to push docker images to.

This will build a production ready version of the website.  You'll need to make
sure to run it with environment variables specified settings, like the database location.


Webpack build
=============

[Webpack][webpack] is used to biuld all front-end assets. This includes typescript and Sass files, which are compiled to plain javascript and CSS. See [webpack.config.js](webpack.config.js) file for configuration. 

The build output will be put in [`static/bundle/`](static/bundle/) folder. Django is configured to look in that folder, using the [django-webpack][django-webpack] project to load the correct assets in the Django template.

In addition to the `npm start` command (above) to run the wepback dev server, you can also build the front-end assets for production using webpack. To do this, run:

    npm run build

Note: see [`package.json`](package.json) for other npm scripts you can run.


Production Settings
===================

Production Django settings can be found in [`riverstreet/config/production.py`](riverstreet/config/production.py).

Django settings can also be configured using environment variables (see [`riverstreet/settings.py`](riverstreet/settings.py). Each of the settings shown in that file can be set using an environment variable. This is useful during production to avoid hard-coding passwords and keys. See the [django-environ][django-environ] project for more details.

For production you must set the environment variable `DJANGO_ENV=production` to trigger the production settings to be used. This is done automatically in [Dockerfile](Dockerfile) when building a docker image.

--- 

Production media is deployed to an [AWS S3][s3] bucket.

[django-storages][django-storages] is used for uploading media. You can view the `AWS_*` settings in `production.py`. You will need to make sure the correct S3 bucket is setup. For more information, see [django-storages documentation](https://django-storages.readthedocs.io/en/latest/).

To handle media during development, you can either download the files from S3 to your local machine. Or, if files are missing, Django is configured to serve dummy images in place of the real images.

--- 

Settings that are required to be set for the site to function:  

`DJANGO_ENV=production` must be set.

For database:  
`DATABASE_URL`  

For cache:  
`CACHE_URL`  

For AWS S3 media:  
`AWS_ACCESS_KEY_ID`   
`AWS_SECRET_ACCESS_KEY`   

For email sending:  
`EMAIL_HOST`  
`EMAIL_HOST_USER`  
`EMAIL_HOST_PASSWORD`  
`EMAIL_PORT`  

For Vimdeo videos:  
`VIMEO_ACCESS_TOKEN`  
`VIMEO_CLIENT_ID`  
`VIMEO_CLIENT_SECRET`  

The above settings need to be set in production, either by running the Django server with those settings as OS environment variables, or by adding them to a `.env` file (see [django-environ][django-environ] docs for more info).

For example, to add database and cache, you'll need to run the server with environment variable:

    DATABASE_URL=postgres://$(PG_USER):$(PG_PASS)@$(PG_HOST)/$(DATABASE)
    CACHE_URL=redis://$(REDIS_HOST):$(REDIS_PORT)


[python]: https://www.python.org/
[django]: http://djangoproject.com/
[postgres]: https://www.postgresql.org/docs/11/index.html
[graphql]: https://graphql.org/
[graphene]: https://docs.graphene-python.org/projects/django/en/latest/
[nodejs]: https://nodejs.org/en/
[react]: https://facebook.github.io/react/
[apollo]: https://www.apollographql.com/docs/react/
[react-redux]: https://react-redux.js.org/
[react-router]: https://reacttraining.com/react-router/web/guides/quick-start
[webpack]: https://webpack.github.io/
[typescript]: https://www.typescriptlang.org/docs/home.html
[sass]: http://sass-lang.com/guide
[tmux]: https://tmux.github.io/
[pip]: http://pip.readthedocs.org/en/latest/installing.html
[virtualenv]: http://virtualenv.readthedocs.org/en/latest/
[pyinvoke]: http://www.pyinvoke.org/
[flake8]: https://flake8.pycqa.org/en/latest/
[django-environ]: https://github.com/joke2k/django-environ
[django-storages]: https://django-storages.readthedocs.io/en/latest/
[django-webpack]: https://github.com/owais/django-webpack-loader
[s3]: https://aws.amazon.com/s3/
