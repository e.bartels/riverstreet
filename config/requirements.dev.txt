-r requirements.base.txt
-r requirements.tests.txt

# install ado package as editable
-e ./ado

flake8-per-file-ignores
termcolor
invoke
