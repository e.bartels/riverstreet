Configuration files
===================

**pip requirements**

`requirements*.txt` files contain Python dependencies for their respective environments.

To install development packages

    pip install -r config/requirements.dev.txt

This will install extra packages for development and testing that are not required for production deployments.

For production install, you can use the top-level file:

    pip install -r requirements.txt


**uwsgi**

`uwsgi.ini` has configuration for the [uwsgi][uwsgi] server. The Dockerfile currently uses uwsgi to run the Django application, but any other Python wsgi server should work as well (e.g. `gunicorn`).


[uwsgi]: https://uwsgi-docs.readthedocs.io/en/latest/
